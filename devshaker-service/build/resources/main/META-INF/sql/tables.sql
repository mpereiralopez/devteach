create table devShaker_Badge (
	badgeId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userCreatorId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	badgeTitle TEXT null,
	badgeDescription TEXT null,
	badgeIcon LONG,
	objPK LONG
);

create table devShaker_BadgeRule (
	ruleId LONG not null,
	badgeId LONG not null,
	companyId LONG,
	primary key (ruleId, badgeId)
);

create table devShaker_Category (
	uuid_ VARCHAR(75) null,
	categoryId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name TEXT null,
	description TEXT null,
	icon LONG
);

create table devShaker_Level (
	levelId LONG not null primary key,
	categoryId LONG,
	subcategoryId LONG,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	levelName TEXT null,
	levelDescription TEXT null,
	levelIcon LONG
);

create table devShaker_ReportedQuestions (
	reportId LONG not null primary key,
	companyId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	questionId LONG,
	userId LONG,
	reportContent VARCHAR(75) null,
	reportStatus VARCHAR(75) null
);

create table devShaker_Rule (
	ruleId LONG not null primary key,
	ruleDesc VARCHAR(75) null,
	companyId LONG,
	userCreatorId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null
);

create table devShaker_Subcategory (
	uuid_ VARCHAR(75) null,
	categoryId LONG,
	subcategoryId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	subcategoryName TEXT null,
	subcategoryDescription TEXT null,
	subcategoryIcon LONG
);

create table devShaker_TestQuestion (
	questionId LONG not null primary key,
	categoryId LONG,
	subcategoryId LONG,
	levelId LONG,
	groupId LONG,
	companyId LONG,
	userIdCreator LONG,
	userNameCreator VARCHAR(75) null,
	userIdModified LONG,
	userNameModified VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	questionText TEXT null,
	status INTEGER,
	type_ INTEGER,
	answer0Text TEXT null,
	answer1Text TEXT null,
	answer2Text TEXT null,
	answer3Text TEXT null,
	correctAnswers VARCHAR(75) null,
	reportContent TEXT null,
	reporterId LONG
);

create table devShaker_UserBadge (
	userId LONG not null,
	badgeId LONG not null,
	companyId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	primary key (userId, badgeId)
);

create table devShaker_UserTestQuestionResult (
	userTestQuestionResultId LONG not null primary key,
	questionId LONG,
	userId LONG,
	answersByUser VARCHAR(75) null,
	createDate DATE null,
	isCorrect BOOLEAN,
	tokenData VARCHAR(75) null
);