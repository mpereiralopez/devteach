create index IX_10FAE45D on devShaker_Badge (companyId);
create index IX_E212AA17 on devShaker_Badge (objPK);

create index IX_FCDDC727 on devShaker_BadgeRule (badgeId);
create index IX_77857141 on devShaker_BadgeRule (companyId);

create index IX_E9802926 on devShaker_Category (companyId);
create index IX_FD748401 on devShaker_Category (name[$COLUMN_LENGTH:5000$]);
create index IX_F4DB83FC on devShaker_Category (userId);
create index IX_3D6E6336 on devShaker_Category (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_E823F38 on devShaker_Category (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_4C82AF79 on devShaker_Level (categoryId);
create index IX_56F07A1C on devShaker_Level (companyId);
create index IX_73B6A75D on devShaker_Level (subcategoryId);
create index IX_1E47C6C6 on devShaker_Level (userId);

create index IX_26112F17 on devShaker_ReportedQuestions (questionId);
create index IX_6259FF7C on devShaker_ReportedQuestions (reportStatus[$COLUMN_LENGTH:75$]);
create index IX_FB919E51 on devShaker_ReportedQuestions (userId, questionId);

create index IX_E5BEACE8 on devShaker_Rule (companyId);
create index IX_1E9AB14C on devShaker_Rule (userCreatorId);

create index IX_9A10C833 on devShaker_Subcategory (categoryId);
create index IX_933F6222 on devShaker_Subcategory (companyId);
create index IX_D4E34C80 on devShaker_Subcategory (userId);
create index IX_724856BA on devShaker_Subcategory (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_E18B43BC on devShaker_Subcategory (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_267D92C9 on devShaker_TestQuestion (categoryId);
create index IX_B63ECCC on devShaker_TestQuestion (companyId);
create index IX_6C05D0D3 on devShaker_TestQuestion (levelId);
create index IX_A7D9B8C2 on devShaker_TestQuestion (status);
create index IX_6D1420D on devShaker_TestQuestion (subcategoryId);
create index IX_9316F4DA on devShaker_TestQuestion (userIdCreator);

create index IX_A45A5C2E on devShaker_UserBadge (badgeId);
create index IX_43EAD7DA on devShaker_UserBadge (userId);

create index IX_3B250657 on devShaker_UserTestQuestionResult (questionId, isCorrect);
create index IX_F9879CD2 on devShaker_UserTestQuestionResult (userId, isCorrect);
create index IX_87D25975 on devShaker_UserTestQuestionResult (userId, tokenData[$COLUMN_LENGTH:75$]);