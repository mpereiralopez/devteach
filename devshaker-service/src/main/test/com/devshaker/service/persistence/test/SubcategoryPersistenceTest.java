/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence.test;

import com.devshaker.exception.NoSuchSubcategoryException;

import com.devshaker.model.Subcategory;

import com.devshaker.service.SubcategoryLocalServiceUtil;
import com.devshaker.service.persistence.SubcategoryPersistence;
import com.devshaker.service.persistence.SubcategoryUtil;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.ReflectionTestUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.rule.TransactionalTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class SubcategoryPersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED));

	@Before
	public void setUp() {
		_persistence = SubcategoryUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<Subcategory> iterator = _subcategories.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Subcategory subcategory = _persistence.create(pk);

		Assert.assertNotNull(subcategory);

		Assert.assertEquals(subcategory.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		Subcategory newSubcategory = addSubcategory();

		_persistence.remove(newSubcategory);

		Subcategory existingSubcategory = _persistence.fetchByPrimaryKey(newSubcategory.getPrimaryKey());

		Assert.assertNull(existingSubcategory);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addSubcategory();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Subcategory newSubcategory = _persistence.create(pk);

		newSubcategory.setUuid(RandomTestUtil.randomString());

		newSubcategory.setCategoryId(RandomTestUtil.nextLong());

		newSubcategory.setGroupId(RandomTestUtil.nextLong());

		newSubcategory.setCompanyId(RandomTestUtil.nextLong());

		newSubcategory.setUserId(RandomTestUtil.nextLong());

		newSubcategory.setUserName(RandomTestUtil.randomString());

		newSubcategory.setCreateDate(RandomTestUtil.nextDate());

		newSubcategory.setModifiedDate(RandomTestUtil.nextDate());

		newSubcategory.setSubcategoryName(RandomTestUtil.randomString());

		newSubcategory.setSubcategoryDescription(RandomTestUtil.randomString());

		newSubcategory.setSubcategoryIcon(RandomTestUtil.nextLong());

		_subcategories.add(_persistence.update(newSubcategory));

		Subcategory existingSubcategory = _persistence.findByPrimaryKey(newSubcategory.getPrimaryKey());

		Assert.assertEquals(existingSubcategory.getUuid(),
			newSubcategory.getUuid());
		Assert.assertEquals(existingSubcategory.getCategoryId(),
			newSubcategory.getCategoryId());
		Assert.assertEquals(existingSubcategory.getSubcategoryId(),
			newSubcategory.getSubcategoryId());
		Assert.assertEquals(existingSubcategory.getGroupId(),
			newSubcategory.getGroupId());
		Assert.assertEquals(existingSubcategory.getCompanyId(),
			newSubcategory.getCompanyId());
		Assert.assertEquals(existingSubcategory.getUserId(),
			newSubcategory.getUserId());
		Assert.assertEquals(existingSubcategory.getUserName(),
			newSubcategory.getUserName());
		Assert.assertEquals(Time.getShortTimestamp(
				existingSubcategory.getCreateDate()),
			Time.getShortTimestamp(newSubcategory.getCreateDate()));
		Assert.assertEquals(Time.getShortTimestamp(
				existingSubcategory.getModifiedDate()),
			Time.getShortTimestamp(newSubcategory.getModifiedDate()));
		Assert.assertEquals(existingSubcategory.getSubcategoryName(),
			newSubcategory.getSubcategoryName());
		Assert.assertEquals(existingSubcategory.getSubcategoryDescription(),
			newSubcategory.getSubcategoryDescription());
		Assert.assertEquals(existingSubcategory.getSubcategoryIcon(),
			newSubcategory.getSubcategoryIcon());
	}

	@Test
	public void testCountByUuid() throws Exception {
		_persistence.countByUuid(StringPool.BLANK);

		_persistence.countByUuid(StringPool.NULL);

		_persistence.countByUuid((String)null);
	}

	@Test
	public void testCountByUUID_G() throws Exception {
		_persistence.countByUUID_G(StringPool.BLANK, RandomTestUtil.nextLong());

		_persistence.countByUUID_G(StringPool.NULL, 0L);

		_persistence.countByUUID_G((String)null, 0L);
	}

	@Test
	public void testCountByUuid_C() throws Exception {
		_persistence.countByUuid_C(StringPool.BLANK, RandomTestUtil.nextLong());

		_persistence.countByUuid_C(StringPool.NULL, 0L);

		_persistence.countByUuid_C((String)null, 0L);
	}

	@Test
	public void testCountByCompanyId() throws Exception {
		_persistence.countByCompanyId(RandomTestUtil.nextLong());

		_persistence.countByCompanyId(0L);
	}

	@Test
	public void testCountByUserId() throws Exception {
		_persistence.countByUserId(RandomTestUtil.nextLong());

		_persistence.countByUserId(0L);
	}

	@Test
	public void testCountByCategoryId() throws Exception {
		_persistence.countByCategoryId(RandomTestUtil.nextLong());

		_persistence.countByCategoryId(0L);
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		Subcategory newSubcategory = addSubcategory();

		Subcategory existingSubcategory = _persistence.findByPrimaryKey(newSubcategory.getPrimaryKey());

		Assert.assertEquals(existingSubcategory, newSubcategory);
	}

	@Test(expected = NoSuchSubcategoryException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<Subcategory> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("devShaker_Subcategory",
			"uuid", true, "categoryId", true, "subcategoryId", true, "groupId",
			true, "companyId", true, "userId", true, "userName", true,
			"createDate", true, "modifiedDate", true, "subcategoryName", true,
			"subcategoryDescription", true, "subcategoryIcon", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		Subcategory newSubcategory = addSubcategory();

		Subcategory existingSubcategory = _persistence.fetchByPrimaryKey(newSubcategory.getPrimaryKey());

		Assert.assertEquals(existingSubcategory, newSubcategory);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Subcategory missingSubcategory = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingSubcategory);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		Subcategory newSubcategory1 = addSubcategory();
		Subcategory newSubcategory2 = addSubcategory();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newSubcategory1.getPrimaryKey());
		primaryKeys.add(newSubcategory2.getPrimaryKey());

		Map<Serializable, Subcategory> subcategories = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, subcategories.size());
		Assert.assertEquals(newSubcategory1,
			subcategories.get(newSubcategory1.getPrimaryKey()));
		Assert.assertEquals(newSubcategory2,
			subcategories.get(newSubcategory2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, Subcategory> subcategories = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(subcategories.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		Subcategory newSubcategory = addSubcategory();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newSubcategory.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, Subcategory> subcategories = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, subcategories.size());
		Assert.assertEquals(newSubcategory,
			subcategories.get(newSubcategory.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, Subcategory> subcategories = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(subcategories.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		Subcategory newSubcategory = addSubcategory();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newSubcategory.getPrimaryKey());

		Map<Serializable, Subcategory> subcategories = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, subcategories.size());
		Assert.assertEquals(newSubcategory,
			subcategories.get(newSubcategory.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = SubcategoryLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<Subcategory>() {
				@Override
				public void performAction(Subcategory subcategory) {
					Assert.assertNotNull(subcategory);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		Subcategory newSubcategory = addSubcategory();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Subcategory.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("subcategoryId",
				newSubcategory.getSubcategoryId()));

		List<Subcategory> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Subcategory existingSubcategory = result.get(0);

		Assert.assertEquals(existingSubcategory, newSubcategory);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Subcategory.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("subcategoryId",
				RandomTestUtil.nextLong()));

		List<Subcategory> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		Subcategory newSubcategory = addSubcategory();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Subcategory.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property(
				"subcategoryId"));

		Object newSubcategoryId = newSubcategory.getSubcategoryId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("subcategoryId",
				new Object[] { newSubcategoryId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingSubcategoryId = result.get(0);

		Assert.assertEquals(existingSubcategoryId, newSubcategoryId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Subcategory.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property(
				"subcategoryId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("subcategoryId",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testResetOriginalValues() throws Exception {
		Subcategory newSubcategory = addSubcategory();

		_persistence.clearCache();

		Subcategory existingSubcategory = _persistence.findByPrimaryKey(newSubcategory.getPrimaryKey());

		Assert.assertTrue(Objects.equals(existingSubcategory.getUuid(),
				ReflectionTestUtil.invoke(existingSubcategory,
					"getOriginalUuid", new Class<?>[0])));
		Assert.assertEquals(Long.valueOf(existingSubcategory.getGroupId()),
			ReflectionTestUtil.<Long>invoke(existingSubcategory,
				"getOriginalGroupId", new Class<?>[0]));
	}

	protected Subcategory addSubcategory() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Subcategory subcategory = _persistence.create(pk);

		subcategory.setUuid(RandomTestUtil.randomString());

		subcategory.setCategoryId(RandomTestUtil.nextLong());

		subcategory.setGroupId(RandomTestUtil.nextLong());

		subcategory.setCompanyId(RandomTestUtil.nextLong());

		subcategory.setUserId(RandomTestUtil.nextLong());

		subcategory.setUserName(RandomTestUtil.randomString());

		subcategory.setCreateDate(RandomTestUtil.nextDate());

		subcategory.setModifiedDate(RandomTestUtil.nextDate());

		subcategory.setSubcategoryName(RandomTestUtil.randomString());

		subcategory.setSubcategoryDescription(RandomTestUtil.randomString());

		subcategory.setSubcategoryIcon(RandomTestUtil.nextLong());

		_subcategories.add(_persistence.update(subcategory));

		return subcategory;
	}

	private List<Subcategory> _subcategories = new ArrayList<Subcategory>();
	private SubcategoryPersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}