/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence.test;

import com.devshaker.exception.NoSuchTestQuestionException;

import com.devshaker.model.TestQuestion;

import com.devshaker.service.TestQuestionLocalServiceUtil;
import com.devshaker.service.persistence.TestQuestionPersistence;
import com.devshaker.service.persistence.TestQuestionUtil;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.rule.TransactionalTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class TestQuestionPersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED));

	@Before
	public void setUp() {
		_persistence = TestQuestionUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<TestQuestion> iterator = _testQuestions.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		TestQuestion testQuestion = _persistence.create(pk);

		Assert.assertNotNull(testQuestion);

		Assert.assertEquals(testQuestion.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		TestQuestion newTestQuestion = addTestQuestion();

		_persistence.remove(newTestQuestion);

		TestQuestion existingTestQuestion = _persistence.fetchByPrimaryKey(newTestQuestion.getPrimaryKey());

		Assert.assertNull(existingTestQuestion);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addTestQuestion();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		TestQuestion newTestQuestion = _persistence.create(pk);

		newTestQuestion.setCategoryId(RandomTestUtil.nextLong());

		newTestQuestion.setSubcategoryId(RandomTestUtil.nextLong());

		newTestQuestion.setLevelId(RandomTestUtil.nextLong());

		newTestQuestion.setGroupId(RandomTestUtil.nextLong());

		newTestQuestion.setCompanyId(RandomTestUtil.nextLong());

		newTestQuestion.setUserIdCreator(RandomTestUtil.nextLong());

		newTestQuestion.setUserNameCreator(RandomTestUtil.randomString());

		newTestQuestion.setUserIdModified(RandomTestUtil.nextLong());

		newTestQuestion.setUserNameModified(RandomTestUtil.randomString());

		newTestQuestion.setCreateDate(RandomTestUtil.nextDate());

		newTestQuestion.setModifiedDate(RandomTestUtil.nextDate());

		newTestQuestion.setQuestionText(RandomTestUtil.randomString());

		newTestQuestion.setStatus(RandomTestUtil.nextInt());

		newTestQuestion.setType(RandomTestUtil.nextInt());

		newTestQuestion.setAnswer0Text(RandomTestUtil.randomString());

		newTestQuestion.setAnswer1Text(RandomTestUtil.randomString());

		newTestQuestion.setAnswer2Text(RandomTestUtil.randomString());

		newTestQuestion.setAnswer3Text(RandomTestUtil.randomString());

		newTestQuestion.setCorrectAnswers(RandomTestUtil.randomString());

		newTestQuestion.setReportContent(RandomTestUtil.randomString());

		newTestQuestion.setReporterId(RandomTestUtil.nextLong());

		_testQuestions.add(_persistence.update(newTestQuestion));

		TestQuestion existingTestQuestion = _persistence.findByPrimaryKey(newTestQuestion.getPrimaryKey());

		Assert.assertEquals(existingTestQuestion.getQuestionId(),
			newTestQuestion.getQuestionId());
		Assert.assertEquals(existingTestQuestion.getCategoryId(),
			newTestQuestion.getCategoryId());
		Assert.assertEquals(existingTestQuestion.getSubcategoryId(),
			newTestQuestion.getSubcategoryId());
		Assert.assertEquals(existingTestQuestion.getLevelId(),
			newTestQuestion.getLevelId());
		Assert.assertEquals(existingTestQuestion.getGroupId(),
			newTestQuestion.getGroupId());
		Assert.assertEquals(existingTestQuestion.getCompanyId(),
			newTestQuestion.getCompanyId());
		Assert.assertEquals(existingTestQuestion.getUserIdCreator(),
			newTestQuestion.getUserIdCreator());
		Assert.assertEquals(existingTestQuestion.getUserNameCreator(),
			newTestQuestion.getUserNameCreator());
		Assert.assertEquals(existingTestQuestion.getUserIdModified(),
			newTestQuestion.getUserIdModified());
		Assert.assertEquals(existingTestQuestion.getUserNameModified(),
			newTestQuestion.getUserNameModified());
		Assert.assertEquals(Time.getShortTimestamp(
				existingTestQuestion.getCreateDate()),
			Time.getShortTimestamp(newTestQuestion.getCreateDate()));
		Assert.assertEquals(Time.getShortTimestamp(
				existingTestQuestion.getModifiedDate()),
			Time.getShortTimestamp(newTestQuestion.getModifiedDate()));
		Assert.assertEquals(existingTestQuestion.getQuestionText(),
			newTestQuestion.getQuestionText());
		Assert.assertEquals(existingTestQuestion.getStatus(),
			newTestQuestion.getStatus());
		Assert.assertEquals(existingTestQuestion.getType(),
			newTestQuestion.getType());
		Assert.assertEquals(existingTestQuestion.getAnswer0Text(),
			newTestQuestion.getAnswer0Text());
		Assert.assertEquals(existingTestQuestion.getAnswer1Text(),
			newTestQuestion.getAnswer1Text());
		Assert.assertEquals(existingTestQuestion.getAnswer2Text(),
			newTestQuestion.getAnswer2Text());
		Assert.assertEquals(existingTestQuestion.getAnswer3Text(),
			newTestQuestion.getAnswer3Text());
		Assert.assertEquals(existingTestQuestion.getCorrectAnswers(),
			newTestQuestion.getCorrectAnswers());
		Assert.assertEquals(existingTestQuestion.getReportContent(),
			newTestQuestion.getReportContent());
		Assert.assertEquals(existingTestQuestion.getReporterId(),
			newTestQuestion.getReporterId());
	}

	@Test
	public void testCountByCompanyId() throws Exception {
		_persistence.countByCompanyId(RandomTestUtil.nextLong());

		_persistence.countByCompanyId(0L);
	}

	@Test
	public void testCountByUserIdCreator() throws Exception {
		_persistence.countByUserIdCreator(RandomTestUtil.nextLong());

		_persistence.countByUserIdCreator(0L);
	}

	@Test
	public void testCountByCategoryId() throws Exception {
		_persistence.countByCategoryId(RandomTestUtil.nextLong());

		_persistence.countByCategoryId(0L);
	}

	@Test
	public void testCountBySubcategoryId() throws Exception {
		_persistence.countBySubcategoryId(RandomTestUtil.nextLong());

		_persistence.countBySubcategoryId(0L);
	}

	@Test
	public void testCountBylevelId() throws Exception {
		_persistence.countBylevelId(RandomTestUtil.nextLong());

		_persistence.countBylevelId(0L);
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		TestQuestion newTestQuestion = addTestQuestion();

		TestQuestion existingTestQuestion = _persistence.findByPrimaryKey(newTestQuestion.getPrimaryKey());

		Assert.assertEquals(existingTestQuestion, newTestQuestion);
	}

	@Test(expected = NoSuchTestQuestionException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<TestQuestion> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("devShaker_TestQuestion",
			"questionId", true, "categoryId", true, "subcategoryId", true,
			"levelId", true, "groupId", true, "companyId", true,
			"userIdCreator", true, "userNameCreator", true, "userIdModified",
			true, "userNameModified", true, "createDate", true, "modifiedDate",
			true, "questionText", true, "status", true, "type", true,
			"answer0Text", true, "answer1Text", true, "answer2Text", true,
			"answer3Text", true, "correctAnswers", true, "reportContent", true,
			"reporterId", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		TestQuestion newTestQuestion = addTestQuestion();

		TestQuestion existingTestQuestion = _persistence.fetchByPrimaryKey(newTestQuestion.getPrimaryKey());

		Assert.assertEquals(existingTestQuestion, newTestQuestion);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		TestQuestion missingTestQuestion = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingTestQuestion);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		TestQuestion newTestQuestion1 = addTestQuestion();
		TestQuestion newTestQuestion2 = addTestQuestion();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newTestQuestion1.getPrimaryKey());
		primaryKeys.add(newTestQuestion2.getPrimaryKey());

		Map<Serializable, TestQuestion> testQuestions = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, testQuestions.size());
		Assert.assertEquals(newTestQuestion1,
			testQuestions.get(newTestQuestion1.getPrimaryKey()));
		Assert.assertEquals(newTestQuestion2,
			testQuestions.get(newTestQuestion2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, TestQuestion> testQuestions = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(testQuestions.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		TestQuestion newTestQuestion = addTestQuestion();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newTestQuestion.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, TestQuestion> testQuestions = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, testQuestions.size());
		Assert.assertEquals(newTestQuestion,
			testQuestions.get(newTestQuestion.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, TestQuestion> testQuestions = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(testQuestions.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		TestQuestion newTestQuestion = addTestQuestion();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newTestQuestion.getPrimaryKey());

		Map<Serializable, TestQuestion> testQuestions = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, testQuestions.size());
		Assert.assertEquals(newTestQuestion,
			testQuestions.get(newTestQuestion.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = TestQuestionLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<TestQuestion>() {
				@Override
				public void performAction(TestQuestion testQuestion) {
					Assert.assertNotNull(testQuestion);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		TestQuestion newTestQuestion = addTestQuestion();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(TestQuestion.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("questionId",
				newTestQuestion.getQuestionId()));

		List<TestQuestion> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		TestQuestion existingTestQuestion = result.get(0);

		Assert.assertEquals(existingTestQuestion, newTestQuestion);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(TestQuestion.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("questionId",
				RandomTestUtil.nextLong()));

		List<TestQuestion> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		TestQuestion newTestQuestion = addTestQuestion();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(TestQuestion.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("questionId"));

		Object newQuestionId = newTestQuestion.getQuestionId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("questionId",
				new Object[] { newQuestionId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingQuestionId = result.get(0);

		Assert.assertEquals(existingQuestionId, newQuestionId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(TestQuestion.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("questionId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("questionId",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	protected TestQuestion addTestQuestion() throws Exception {
		long pk = RandomTestUtil.nextLong();

		TestQuestion testQuestion = _persistence.create(pk);

		testQuestion.setCategoryId(RandomTestUtil.nextLong());

		testQuestion.setSubcategoryId(RandomTestUtil.nextLong());

		testQuestion.setLevelId(RandomTestUtil.nextLong());

		testQuestion.setGroupId(RandomTestUtil.nextLong());

		testQuestion.setCompanyId(RandomTestUtil.nextLong());

		testQuestion.setUserIdCreator(RandomTestUtil.nextLong());

		testQuestion.setUserNameCreator(RandomTestUtil.randomString());

		testQuestion.setUserIdModified(RandomTestUtil.nextLong());

		testQuestion.setUserNameModified(RandomTestUtil.randomString());

		testQuestion.setCreateDate(RandomTestUtil.nextDate());

		testQuestion.setModifiedDate(RandomTestUtil.nextDate());

		testQuestion.setQuestionText(RandomTestUtil.randomString());

		testQuestion.setStatus(RandomTestUtil.nextInt());

		testQuestion.setType(RandomTestUtil.nextInt());

		testQuestion.setAnswer0Text(RandomTestUtil.randomString());

		testQuestion.setAnswer1Text(RandomTestUtil.randomString());

		testQuestion.setAnswer2Text(RandomTestUtil.randomString());

		testQuestion.setAnswer3Text(RandomTestUtil.randomString());

		testQuestion.setCorrectAnswers(RandomTestUtil.randomString());

		testQuestion.setReportContent(RandomTestUtil.randomString());

		testQuestion.setReporterId(RandomTestUtil.nextLong());

		_testQuestions.add(_persistence.update(testQuestion));

		return testQuestion;
	}

	private List<TestQuestion> _testQuestions = new ArrayList<TestQuestion>();
	private TestQuestionPersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}