/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence.test;

import com.devshaker.exception.NoSuchBadgeException;

import com.devshaker.model.Badge;

import com.devshaker.service.BadgeLocalServiceUtil;
import com.devshaker.service.persistence.BadgePersistence;
import com.devshaker.service.persistence.BadgeUtil;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.rule.TransactionalTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class BadgePersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED));

	@Before
	public void setUp() {
		_persistence = BadgeUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<Badge> iterator = _badges.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Badge badge = _persistence.create(pk);

		Assert.assertNotNull(badge);

		Assert.assertEquals(badge.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		Badge newBadge = addBadge();

		_persistence.remove(newBadge);

		Badge existingBadge = _persistence.fetchByPrimaryKey(newBadge.getPrimaryKey());

		Assert.assertNull(existingBadge);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addBadge();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Badge newBadge = _persistence.create(pk);

		newBadge.setGroupId(RandomTestUtil.nextLong());

		newBadge.setCompanyId(RandomTestUtil.nextLong());

		newBadge.setUserCreatorId(RandomTestUtil.nextLong());

		newBadge.setUserName(RandomTestUtil.randomString());

		newBadge.setCreateDate(RandomTestUtil.nextDate());

		newBadge.setModifiedDate(RandomTestUtil.nextDate());

		newBadge.setBadgeTitle(RandomTestUtil.randomString());

		newBadge.setBadgeDescription(RandomTestUtil.randomString());

		newBadge.setBadgeIcon(RandomTestUtil.nextLong());

		_badges.add(_persistence.update(newBadge));

		Badge existingBadge = _persistence.findByPrimaryKey(newBadge.getPrimaryKey());

		Assert.assertEquals(existingBadge.getBadgeId(), newBadge.getBadgeId());
		Assert.assertEquals(existingBadge.getGroupId(), newBadge.getGroupId());
		Assert.assertEquals(existingBadge.getCompanyId(),
			newBadge.getCompanyId());
		Assert.assertEquals(existingBadge.getUserCreatorId(),
			newBadge.getUserCreatorId());
		Assert.assertEquals(existingBadge.getUserName(), newBadge.getUserName());
		Assert.assertEquals(Time.getShortTimestamp(
				existingBadge.getCreateDate()),
			Time.getShortTimestamp(newBadge.getCreateDate()));
		Assert.assertEquals(Time.getShortTimestamp(
				existingBadge.getModifiedDate()),
			Time.getShortTimestamp(newBadge.getModifiedDate()));
		Assert.assertEquals(existingBadge.getBadgeTitle(),
			newBadge.getBadgeTitle());
		Assert.assertEquals(existingBadge.getBadgeDescription(),
			newBadge.getBadgeDescription());
		Assert.assertEquals(existingBadge.getBadgeIcon(),
			newBadge.getBadgeIcon());
	}

	@Test
	public void testCountByCompanyId() throws Exception {
		_persistence.countByCompanyId(RandomTestUtil.nextLong());

		_persistence.countByCompanyId(0L);
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		Badge newBadge = addBadge();

		Badge existingBadge = _persistence.findByPrimaryKey(newBadge.getPrimaryKey());

		Assert.assertEquals(existingBadge, newBadge);
	}

	@Test(expected = NoSuchBadgeException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<Badge> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("devShaker_Badge",
			"badgeId", true, "groupId", true, "companyId", true,
			"userCreatorId", true, "userName", true, "createDate", true,
			"modifiedDate", true, "badgeTitle", true, "badgeDescription", true,
			"badgeIcon", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		Badge newBadge = addBadge();

		Badge existingBadge = _persistence.fetchByPrimaryKey(newBadge.getPrimaryKey());

		Assert.assertEquals(existingBadge, newBadge);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Badge missingBadge = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingBadge);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		Badge newBadge1 = addBadge();
		Badge newBadge2 = addBadge();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newBadge1.getPrimaryKey());
		primaryKeys.add(newBadge2.getPrimaryKey());

		Map<Serializable, Badge> badges = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, badges.size());
		Assert.assertEquals(newBadge1, badges.get(newBadge1.getPrimaryKey()));
		Assert.assertEquals(newBadge2, badges.get(newBadge2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, Badge> badges = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(badges.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		Badge newBadge = addBadge();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newBadge.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, Badge> badges = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, badges.size());
		Assert.assertEquals(newBadge, badges.get(newBadge.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, Badge> badges = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(badges.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		Badge newBadge = addBadge();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newBadge.getPrimaryKey());

		Map<Serializable, Badge> badges = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, badges.size());
		Assert.assertEquals(newBadge, badges.get(newBadge.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = BadgeLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<Badge>() {
				@Override
				public void performAction(Badge badge) {
					Assert.assertNotNull(badge);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		Badge newBadge = addBadge();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Badge.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("badgeId",
				newBadge.getBadgeId()));

		List<Badge> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Badge existingBadge = result.get(0);

		Assert.assertEquals(existingBadge, newBadge);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Badge.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("badgeId",
				RandomTestUtil.nextLong()));

		List<Badge> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		Badge newBadge = addBadge();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Badge.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("badgeId"));

		Object newBadgeId = newBadge.getBadgeId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("badgeId",
				new Object[] { newBadgeId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingBadgeId = result.get(0);

		Assert.assertEquals(existingBadgeId, newBadgeId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Badge.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("badgeId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("badgeId",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	protected Badge addBadge() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Badge badge = _persistence.create(pk);

		badge.setGroupId(RandomTestUtil.nextLong());

		badge.setCompanyId(RandomTestUtil.nextLong());

		badge.setUserCreatorId(RandomTestUtil.nextLong());

		badge.setUserName(RandomTestUtil.randomString());

		badge.setCreateDate(RandomTestUtil.nextDate());

		badge.setModifiedDate(RandomTestUtil.nextDate());

		badge.setBadgeTitle(RandomTestUtil.randomString());

		badge.setBadgeDescription(RandomTestUtil.randomString());

		badge.setBadgeIcon(RandomTestUtil.nextLong());

		_badges.add(_persistence.update(badge));

		return badge;
	}

	private List<Badge> _badges = new ArrayList<Badge>();
	private BadgePersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}