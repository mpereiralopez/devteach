/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence.test;

import com.devshaker.exception.NoSuchLevelException;

import com.devshaker.model.Level;

import com.devshaker.service.LevelLocalServiceUtil;
import com.devshaker.service.persistence.LevelPersistence;
import com.devshaker.service.persistence.LevelUtil;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.rule.TransactionalTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class LevelPersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED));

	@Before
	public void setUp() {
		_persistence = LevelUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<Level> iterator = _levels.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Level level = _persistence.create(pk);

		Assert.assertNotNull(level);

		Assert.assertEquals(level.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		Level newLevel = addLevel();

		_persistence.remove(newLevel);

		Level existingLevel = _persistence.fetchByPrimaryKey(newLevel.getPrimaryKey());

		Assert.assertNull(existingLevel);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addLevel();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Level newLevel = _persistence.create(pk);

		newLevel.setCategoryId(RandomTestUtil.nextLong());

		newLevel.setSubcategoryId(RandomTestUtil.nextLong());

		newLevel.setGroupId(RandomTestUtil.nextLong());

		newLevel.setCompanyId(RandomTestUtil.nextLong());

		newLevel.setUserId(RandomTestUtil.nextLong());

		newLevel.setUserName(RandomTestUtil.randomString());

		newLevel.setCreateDate(RandomTestUtil.nextDate());

		newLevel.setModifiedDate(RandomTestUtil.nextDate());

		newLevel.setLevelName(RandomTestUtil.randomString());

		newLevel.setLevelDescription(RandomTestUtil.randomString());

		newLevel.setLevelIcon(RandomTestUtil.nextLong());

		_levels.add(_persistence.update(newLevel));

		Level existingLevel = _persistence.findByPrimaryKey(newLevel.getPrimaryKey());

		Assert.assertEquals(existingLevel.getLevelId(), newLevel.getLevelId());
		Assert.assertEquals(existingLevel.getCategoryId(),
			newLevel.getCategoryId());
		Assert.assertEquals(existingLevel.getSubcategoryId(),
			newLevel.getSubcategoryId());
		Assert.assertEquals(existingLevel.getGroupId(), newLevel.getGroupId());
		Assert.assertEquals(existingLevel.getCompanyId(),
			newLevel.getCompanyId());
		Assert.assertEquals(existingLevel.getUserId(), newLevel.getUserId());
		Assert.assertEquals(existingLevel.getUserName(), newLevel.getUserName());
		Assert.assertEquals(Time.getShortTimestamp(
				existingLevel.getCreateDate()),
			Time.getShortTimestamp(newLevel.getCreateDate()));
		Assert.assertEquals(Time.getShortTimestamp(
				existingLevel.getModifiedDate()),
			Time.getShortTimestamp(newLevel.getModifiedDate()));
		Assert.assertEquals(existingLevel.getLevelName(),
			newLevel.getLevelName());
		Assert.assertEquals(existingLevel.getLevelDescription(),
			newLevel.getLevelDescription());
		Assert.assertEquals(existingLevel.getLevelIcon(),
			newLevel.getLevelIcon());
	}

	@Test
	public void testCountByCompanyId() throws Exception {
		_persistence.countByCompanyId(RandomTestUtil.nextLong());

		_persistence.countByCompanyId(0L);
	}

	@Test
	public void testCountByUserId() throws Exception {
		_persistence.countByUserId(RandomTestUtil.nextLong());

		_persistence.countByUserId(0L);
	}

	@Test
	public void testCountByCategoryId() throws Exception {
		_persistence.countByCategoryId(RandomTestUtil.nextLong());

		_persistence.countByCategoryId(0L);
	}

	@Test
	public void testCountBySubcategoryId() throws Exception {
		_persistence.countBySubcategoryId(RandomTestUtil.nextLong());

		_persistence.countBySubcategoryId(0L);
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		Level newLevel = addLevel();

		Level existingLevel = _persistence.findByPrimaryKey(newLevel.getPrimaryKey());

		Assert.assertEquals(existingLevel, newLevel);
	}

	@Test(expected = NoSuchLevelException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<Level> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("devShaker_Level",
			"levelId", true, "categoryId", true, "subcategoryId", true,
			"groupId", true, "companyId", true, "userId", true, "userName",
			true, "createDate", true, "modifiedDate", true, "levelName", true,
			"levelDescription", true, "levelIcon", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		Level newLevel = addLevel();

		Level existingLevel = _persistence.fetchByPrimaryKey(newLevel.getPrimaryKey());

		Assert.assertEquals(existingLevel, newLevel);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Level missingLevel = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingLevel);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		Level newLevel1 = addLevel();
		Level newLevel2 = addLevel();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newLevel1.getPrimaryKey());
		primaryKeys.add(newLevel2.getPrimaryKey());

		Map<Serializable, Level> levels = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, levels.size());
		Assert.assertEquals(newLevel1, levels.get(newLevel1.getPrimaryKey()));
		Assert.assertEquals(newLevel2, levels.get(newLevel2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, Level> levels = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(levels.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		Level newLevel = addLevel();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newLevel.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, Level> levels = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, levels.size());
		Assert.assertEquals(newLevel, levels.get(newLevel.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, Level> levels = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(levels.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		Level newLevel = addLevel();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newLevel.getPrimaryKey());

		Map<Serializable, Level> levels = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, levels.size());
		Assert.assertEquals(newLevel, levels.get(newLevel.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = LevelLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<Level>() {
				@Override
				public void performAction(Level level) {
					Assert.assertNotNull(level);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		Level newLevel = addLevel();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Level.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("levelId",
				newLevel.getLevelId()));

		List<Level> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Level existingLevel = result.get(0);

		Assert.assertEquals(existingLevel, newLevel);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Level.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("levelId",
				RandomTestUtil.nextLong()));

		List<Level> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		Level newLevel = addLevel();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Level.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("levelId"));

		Object newLevelId = newLevel.getLevelId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("levelId",
				new Object[] { newLevelId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingLevelId = result.get(0);

		Assert.assertEquals(existingLevelId, newLevelId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Level.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("levelId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("levelId",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	protected Level addLevel() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Level level = _persistence.create(pk);

		level.setCategoryId(RandomTestUtil.nextLong());

		level.setSubcategoryId(RandomTestUtil.nextLong());

		level.setGroupId(RandomTestUtil.nextLong());

		level.setCompanyId(RandomTestUtil.nextLong());

		level.setUserId(RandomTestUtil.nextLong());

		level.setUserName(RandomTestUtil.randomString());

		level.setCreateDate(RandomTestUtil.nextDate());

		level.setModifiedDate(RandomTestUtil.nextDate());

		level.setLevelName(RandomTestUtil.randomString());

		level.setLevelDescription(RandomTestUtil.randomString());

		level.setLevelIcon(RandomTestUtil.nextLong());

		_levels.add(_persistence.update(level));

		return level;
	}

	private List<Level> _levels = new ArrayList<Level>();
	private LevelPersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}