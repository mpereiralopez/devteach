/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence.test;

import com.devshaker.exception.NoSuchUserBadgeException;

import com.devshaker.model.UserBadge;

import com.devshaker.service.UserBadgeLocalServiceUtil;
import com.devshaker.service.persistence.UserBadgePK;
import com.devshaker.service.persistence.UserBadgePersistence;
import com.devshaker.service.persistence.UserBadgeUtil;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.rule.TransactionalTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class UserBadgePersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED));

	@Before
	public void setUp() {
		_persistence = UserBadgeUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<UserBadge> iterator = _userBadges.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		UserBadgePK pk = new UserBadgePK(RandomTestUtil.nextLong(),
				RandomTestUtil.nextLong());

		UserBadge userBadge = _persistence.create(pk);

		Assert.assertNotNull(userBadge);

		Assert.assertEquals(userBadge.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		UserBadge newUserBadge = addUserBadge();

		_persistence.remove(newUserBadge);

		UserBadge existingUserBadge = _persistence.fetchByPrimaryKey(newUserBadge.getPrimaryKey());

		Assert.assertNull(existingUserBadge);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addUserBadge();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		UserBadgePK pk = new UserBadgePK(RandomTestUtil.nextLong(),
				RandomTestUtil.nextLong());

		UserBadge newUserBadge = _persistence.create(pk);

		newUserBadge.setCompanyId(RandomTestUtil.nextLong());

		newUserBadge.setCreateDate(RandomTestUtil.nextDate());

		newUserBadge.setModifiedDate(RandomTestUtil.nextDate());

		_userBadges.add(_persistence.update(newUserBadge));

		UserBadge existingUserBadge = _persistence.findByPrimaryKey(newUserBadge.getPrimaryKey());

		Assert.assertEquals(existingUserBadge.getUserId(),
			newUserBadge.getUserId());
		Assert.assertEquals(existingUserBadge.getBadgeId(),
			newUserBadge.getBadgeId());
		Assert.assertEquals(existingUserBadge.getCompanyId(),
			newUserBadge.getCompanyId());
		Assert.assertEquals(Time.getShortTimestamp(
				existingUserBadge.getCreateDate()),
			Time.getShortTimestamp(newUserBadge.getCreateDate()));
		Assert.assertEquals(Time.getShortTimestamp(
				existingUserBadge.getModifiedDate()),
			Time.getShortTimestamp(newUserBadge.getModifiedDate()));
	}

	@Test
	public void testCountByUserId() throws Exception {
		_persistence.countByUserId(RandomTestUtil.nextLong());

		_persistence.countByUserId(0L);
	}

	@Test
	public void testCountByBadgeId() throws Exception {
		_persistence.countByBadgeId(RandomTestUtil.nextLong());

		_persistence.countByBadgeId(0L);
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		UserBadge newUserBadge = addUserBadge();

		UserBadge existingUserBadge = _persistence.findByPrimaryKey(newUserBadge.getPrimaryKey());

		Assert.assertEquals(existingUserBadge, newUserBadge);
	}

	@Test(expected = NoSuchUserBadgeException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		UserBadgePK pk = new UserBadgePK(RandomTestUtil.nextLong(),
				RandomTestUtil.nextLong());

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		UserBadge newUserBadge = addUserBadge();

		UserBadge existingUserBadge = _persistence.fetchByPrimaryKey(newUserBadge.getPrimaryKey());

		Assert.assertEquals(existingUserBadge, newUserBadge);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		UserBadgePK pk = new UserBadgePK(RandomTestUtil.nextLong(),
				RandomTestUtil.nextLong());

		UserBadge missingUserBadge = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingUserBadge);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		UserBadge newUserBadge1 = addUserBadge();
		UserBadge newUserBadge2 = addUserBadge();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newUserBadge1.getPrimaryKey());
		primaryKeys.add(newUserBadge2.getPrimaryKey());

		Map<Serializable, UserBadge> userBadges = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, userBadges.size());
		Assert.assertEquals(newUserBadge1,
			userBadges.get(newUserBadge1.getPrimaryKey()));
		Assert.assertEquals(newUserBadge2,
			userBadges.get(newUserBadge2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		UserBadgePK pk1 = new UserBadgePK(RandomTestUtil.nextLong(),
				RandomTestUtil.nextLong());

		UserBadgePK pk2 = new UserBadgePK(RandomTestUtil.nextLong(),
				RandomTestUtil.nextLong());

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, UserBadge> userBadges = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(userBadges.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		UserBadge newUserBadge = addUserBadge();

		UserBadgePK pk = new UserBadgePK(RandomTestUtil.nextLong(),
				RandomTestUtil.nextLong());

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newUserBadge.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, UserBadge> userBadges = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, userBadges.size());
		Assert.assertEquals(newUserBadge,
			userBadges.get(newUserBadge.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, UserBadge> userBadges = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(userBadges.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		UserBadge newUserBadge = addUserBadge();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newUserBadge.getPrimaryKey());

		Map<Serializable, UserBadge> userBadges = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, userBadges.size());
		Assert.assertEquals(newUserBadge,
			userBadges.get(newUserBadge.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = UserBadgeLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<UserBadge>() {
				@Override
				public void performAction(UserBadge userBadge) {
					Assert.assertNotNull(userBadge);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		UserBadge newUserBadge = addUserBadge();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(UserBadge.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("id.userId",
				newUserBadge.getUserId()));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("id.badgeId",
				newUserBadge.getBadgeId()));

		List<UserBadge> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		UserBadge existingUserBadge = result.get(0);

		Assert.assertEquals(existingUserBadge, newUserBadge);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(UserBadge.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("id.userId",
				RandomTestUtil.nextLong()));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("id.badgeId",
				RandomTestUtil.nextLong()));

		List<UserBadge> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		UserBadge newUserBadge = addUserBadge();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(UserBadge.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("id.userId"));

		Object newUserId = newUserBadge.getUserId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("id.userId",
				new Object[] { newUserId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingUserId = result.get(0);

		Assert.assertEquals(existingUserId, newUserId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(UserBadge.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("id.userId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("id.userId",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	protected UserBadge addUserBadge() throws Exception {
		UserBadgePK pk = new UserBadgePK(RandomTestUtil.nextLong(),
				RandomTestUtil.nextLong());

		UserBadge userBadge = _persistence.create(pk);

		userBadge.setCompanyId(RandomTestUtil.nextLong());

		userBadge.setCreateDate(RandomTestUtil.nextDate());

		userBadge.setModifiedDate(RandomTestUtil.nextDate());

		_userBadges.add(_persistence.update(userBadge));

		return userBadge;
	}

	private List<UserBadge> _userBadges = new ArrayList<UserBadge>();
	private UserBadgePersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}