/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence.test;

import com.devshaker.exception.NoSuchUserTestQuestionResultException;

import com.devshaker.model.UserTestQuestionResult;

import com.devshaker.service.UserTestQuestionResultLocalServiceUtil;
import com.devshaker.service.persistence.UserTestQuestionResultPersistence;
import com.devshaker.service.persistence.UserTestQuestionResultUtil;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.rule.TransactionalTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class UserTestQuestionResultPersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED));

	@Before
	public void setUp() {
		_persistence = UserTestQuestionResultUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<UserTestQuestionResult> iterator = _userTestQuestionResults.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		UserTestQuestionResult userTestQuestionResult = _persistence.create(pk);

		Assert.assertNotNull(userTestQuestionResult);

		Assert.assertEquals(userTestQuestionResult.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		UserTestQuestionResult newUserTestQuestionResult = addUserTestQuestionResult();

		_persistence.remove(newUserTestQuestionResult);

		UserTestQuestionResult existingUserTestQuestionResult = _persistence.fetchByPrimaryKey(newUserTestQuestionResult.getPrimaryKey());

		Assert.assertNull(existingUserTestQuestionResult);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addUserTestQuestionResult();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		UserTestQuestionResult newUserTestQuestionResult = _persistence.create(pk);

		newUserTestQuestionResult.setQuestionId(RandomTestUtil.nextLong());

		newUserTestQuestionResult.setUserId(RandomTestUtil.nextLong());

		newUserTestQuestionResult.setAnswersByUser(RandomTestUtil.randomString());

		newUserTestQuestionResult.setCreateDate(RandomTestUtil.nextDate());

		_userTestQuestionResults.add(_persistence.update(
				newUserTestQuestionResult));

		UserTestQuestionResult existingUserTestQuestionResult = _persistence.findByPrimaryKey(newUserTestQuestionResult.getPrimaryKey());

		Assert.assertEquals(existingUserTestQuestionResult.getUserTestQuestionResultId(),
			newUserTestQuestionResult.getUserTestQuestionResultId());
		Assert.assertEquals(existingUserTestQuestionResult.getQuestionId(),
			newUserTestQuestionResult.getQuestionId());
		Assert.assertEquals(existingUserTestQuestionResult.getUserId(),
			newUserTestQuestionResult.getUserId());
		Assert.assertEquals(existingUserTestQuestionResult.getAnswersByUser(),
			newUserTestQuestionResult.getAnswersByUser());
		Assert.assertEquals(Time.getShortTimestamp(
				existingUserTestQuestionResult.getCreateDate()),
			Time.getShortTimestamp(newUserTestQuestionResult.getCreateDate()));
	}

	@Test
	public void testCountByUserId() throws Exception {
		_persistence.countByUserId(RandomTestUtil.nextLong());

		_persistence.countByUserId(0L);
	}

	@Test
	public void testCountByQuestionId() throws Exception {
		_persistence.countByQuestionId(RandomTestUtil.nextLong());

		_persistence.countByQuestionId(0L);
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		UserTestQuestionResult newUserTestQuestionResult = addUserTestQuestionResult();

		UserTestQuestionResult existingUserTestQuestionResult = _persistence.findByPrimaryKey(newUserTestQuestionResult.getPrimaryKey());

		Assert.assertEquals(existingUserTestQuestionResult,
			newUserTestQuestionResult);
	}

	@Test(expected = NoSuchUserTestQuestionResultException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<UserTestQuestionResult> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("devShaker_UserTestQuestionResult",
			"userTestQuestionResultId", true, "questionId", true, "userId",
			true, "answersByUser", true, "createDate", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		UserTestQuestionResult newUserTestQuestionResult = addUserTestQuestionResult();

		UserTestQuestionResult existingUserTestQuestionResult = _persistence.fetchByPrimaryKey(newUserTestQuestionResult.getPrimaryKey());

		Assert.assertEquals(existingUserTestQuestionResult,
			newUserTestQuestionResult);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		UserTestQuestionResult missingUserTestQuestionResult = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingUserTestQuestionResult);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		UserTestQuestionResult newUserTestQuestionResult1 = addUserTestQuestionResult();
		UserTestQuestionResult newUserTestQuestionResult2 = addUserTestQuestionResult();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newUserTestQuestionResult1.getPrimaryKey());
		primaryKeys.add(newUserTestQuestionResult2.getPrimaryKey());

		Map<Serializable, UserTestQuestionResult> userTestQuestionResults = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, userTestQuestionResults.size());
		Assert.assertEquals(newUserTestQuestionResult1,
			userTestQuestionResults.get(
				newUserTestQuestionResult1.getPrimaryKey()));
		Assert.assertEquals(newUserTestQuestionResult2,
			userTestQuestionResults.get(
				newUserTestQuestionResult2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, UserTestQuestionResult> userTestQuestionResults = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(userTestQuestionResults.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		UserTestQuestionResult newUserTestQuestionResult = addUserTestQuestionResult();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newUserTestQuestionResult.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, UserTestQuestionResult> userTestQuestionResults = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, userTestQuestionResults.size());
		Assert.assertEquals(newUserTestQuestionResult,
			userTestQuestionResults.get(
				newUserTestQuestionResult.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, UserTestQuestionResult> userTestQuestionResults = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(userTestQuestionResults.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		UserTestQuestionResult newUserTestQuestionResult = addUserTestQuestionResult();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newUserTestQuestionResult.getPrimaryKey());

		Map<Serializable, UserTestQuestionResult> userTestQuestionResults = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, userTestQuestionResults.size());
		Assert.assertEquals(newUserTestQuestionResult,
			userTestQuestionResults.get(
				newUserTestQuestionResult.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = UserTestQuestionResultLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<UserTestQuestionResult>() {
				@Override
				public void performAction(
					UserTestQuestionResult userTestQuestionResult) {
					Assert.assertNotNull(userTestQuestionResult);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		UserTestQuestionResult newUserTestQuestionResult = addUserTestQuestionResult();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(UserTestQuestionResult.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq(
				"userTestQuestionResultId",
				newUserTestQuestionResult.getUserTestQuestionResultId()));

		List<UserTestQuestionResult> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		UserTestQuestionResult existingUserTestQuestionResult = result.get(0);

		Assert.assertEquals(existingUserTestQuestionResult,
			newUserTestQuestionResult);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(UserTestQuestionResult.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq(
				"userTestQuestionResultId", RandomTestUtil.nextLong()));

		List<UserTestQuestionResult> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		UserTestQuestionResult newUserTestQuestionResult = addUserTestQuestionResult();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(UserTestQuestionResult.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property(
				"userTestQuestionResultId"));

		Object newUserTestQuestionResultId = newUserTestQuestionResult.getUserTestQuestionResultId();

		dynamicQuery.add(RestrictionsFactoryUtil.in(
				"userTestQuestionResultId",
				new Object[] { newUserTestQuestionResultId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingUserTestQuestionResultId = result.get(0);

		Assert.assertEquals(existingUserTestQuestionResultId,
			newUserTestQuestionResultId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(UserTestQuestionResult.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property(
				"userTestQuestionResultId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in(
				"userTestQuestionResultId",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	protected UserTestQuestionResult addUserTestQuestionResult()
		throws Exception {
		long pk = RandomTestUtil.nextLong();

		UserTestQuestionResult userTestQuestionResult = _persistence.create(pk);

		userTestQuestionResult.setQuestionId(RandomTestUtil.nextLong());

		userTestQuestionResult.setUserId(RandomTestUtil.nextLong());

		userTestQuestionResult.setAnswersByUser(RandomTestUtil.randomString());

		userTestQuestionResult.setCreateDate(RandomTestUtil.nextDate());

		_userTestQuestionResults.add(_persistence.update(userTestQuestionResult));

		return userTestQuestionResult;
	}

	private List<UserTestQuestionResult> _userTestQuestionResults = new ArrayList<UserTestQuestionResult>();
	private UserTestQuestionResultPersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}