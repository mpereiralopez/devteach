package com.devshaker.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class XpUltils {
	
	Log log = LogFactoryUtil.getLog(XpUltils.class);
	
	private static final int XP_SEED_FIRST_LEVEL = 30;
	private static final double XP_SEED_INDEX = 1.75;

	public static final double  BONUS_VARIETY_NOCHAGE = 1.0;
	public static final double  BONUS_VARIETY_SUBCATEGORY_CHANGE = 1.5;
	public static final double  BONUS_VARIETY_CATEGORY_CHANGE = 2.0;
	
	private static final double BONUS_SUCCESIVE_1 = 1.0;
	private static final double BONUS_SUCCESIVE_2 = 1.05;
	private static final double BONUS_SUCCESIVE_3 = 1.10;
	private static final double BONUS_SUCCESIVE_4 = 1.15;
	private static final double BONUS_SUCCESIVE_5 = 1.20;

	//given from 0 to 20% correct
	private static final int INDICE_LEVEL_10 = 10;
	//given from 20 to 40% correct
	private static final int INDICE_LEVEL_15 = 15;	
	//given from 40 to 60% correct
	private static final int INDICE_LEVEL_30 = 30;
	//given from 60 to 80% correct
	private static final int INDICE_LEVEL_45 = 45;
	//given from 80 to 100% correct
	private static final int INDICE_LEVEL_60 = 60;
	//given from 100% correct
	private static final int INDICE_LEVEL_75 = 75;


	public static long totalNewXpPoints(long previousPoint, int levelOfQuestion, int percentageOfCorrect ,double bonusVariety, int levelsInSameSession){
		long returnValue = previousPoint;
		
		System.out.println("previousPoint "+previousPoint+" "+levelOfQuestion+" "+percentageOfCorrect+" "+bonusVariety+" "+levelsInSameSession);
		
		int indiceLevel = INDICE_LEVEL_10;
		
		if(percentageOfCorrect < 20)indiceLevel = INDICE_LEVEL_10; 
		if(percentageOfCorrect >= 20 && percentageOfCorrect < 40)indiceLevel = INDICE_LEVEL_15; 
		if(percentageOfCorrect >= 40 && percentageOfCorrect < 60)indiceLevel = INDICE_LEVEL_30; 
		if(percentageOfCorrect >= 60 && percentageOfCorrect < 80)indiceLevel = INDICE_LEVEL_45; 
		if(percentageOfCorrect >= 80 && percentageOfCorrect < 100)indiceLevel = INDICE_LEVEL_60; 
		if(percentageOfCorrect == 100)indiceLevel = INDICE_LEVEL_75; 

		Double bonusSuccesive = BONUS_SUCCESIVE_1;
		
		switch (levelsInSameSession) {
		
		case 0:
			bonusSuccesive = BONUS_SUCCESIVE_1;
			break;
		case 1:
			bonusSuccesive = BONUS_SUCCESIVE_1;
			break;
		case 2:
			bonusSuccesive = BONUS_SUCCESIVE_2;
			break;
		case 3:
			bonusSuccesive = BONUS_SUCCESIVE_3;
			break;
		case 4:
			bonusSuccesive = BONUS_SUCCESIVE_4;
			break;
		default:
			bonusSuccesive = BONUS_SUCCESIVE_5;
			break;
		}
		
		
		Double newCalculation = (levelOfQuestion*indiceLevel*(bonusVariety*bonusSuccesive));
		returnValue =returnValue + newCalculation.intValue(); 
		return returnValue;
	}
	
	public static int calculateUserLevel (long newUserExperience){
		int levelOfUser = 0;

		if (newUserExperience>XP_SEED_FIRST_LEVEL){
			levelOfUser=1;
			double begin = XP_SEED_FIRST_LEVEL;
			while(begin*XP_SEED_INDEX < newUserExperience){
				levelOfUser = levelOfUser +1;
				begin = begin*XP_SEED_INDEX;
			}
		}
		
		return levelOfUser;
	}
	
}
