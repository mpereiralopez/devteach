/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.ReportedQuestions;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ReportedQuestions in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see ReportedQuestions
 * @generated
 */
@ProviderType
public class ReportedQuestionsCacheModel implements CacheModel<ReportedQuestions>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ReportedQuestionsCacheModel)) {
			return false;
		}

		ReportedQuestionsCacheModel reportedQuestionsCacheModel = (ReportedQuestionsCacheModel)obj;

		if (reportId == reportedQuestionsCacheModel.reportId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, reportId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{reportId=");
		sb.append(reportId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", questionId=");
		sb.append(questionId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", reportContent=");
		sb.append(reportContent);
		sb.append(", reportStatus=");
		sb.append(reportStatus);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ReportedQuestions toEntityModel() {
		ReportedQuestionsImpl reportedQuestionsImpl = new ReportedQuestionsImpl();

		reportedQuestionsImpl.setReportId(reportId);
		reportedQuestionsImpl.setCompanyId(companyId);

		if (createDate == Long.MIN_VALUE) {
			reportedQuestionsImpl.setCreateDate(null);
		}
		else {
			reportedQuestionsImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			reportedQuestionsImpl.setModifiedDate(null);
		}
		else {
			reportedQuestionsImpl.setModifiedDate(new Date(modifiedDate));
		}

		reportedQuestionsImpl.setQuestionId(questionId);
		reportedQuestionsImpl.setUserId(userId);

		if (reportContent == null) {
			reportedQuestionsImpl.setReportContent(StringPool.BLANK);
		}
		else {
			reportedQuestionsImpl.setReportContent(reportContent);
		}

		if (reportStatus == null) {
			reportedQuestionsImpl.setReportStatus(StringPool.BLANK);
		}
		else {
			reportedQuestionsImpl.setReportStatus(reportStatus);
		}

		reportedQuestionsImpl.resetOriginalValues();

		return reportedQuestionsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		reportId = objectInput.readLong();

		companyId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		questionId = objectInput.readLong();

		userId = objectInput.readLong();
		reportContent = objectInput.readUTF();
		reportStatus = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(reportId);

		objectOutput.writeLong(companyId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(questionId);

		objectOutput.writeLong(userId);

		if (reportContent == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(reportContent);
		}

		if (reportStatus == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(reportStatus);
		}
	}

	public long reportId;
	public long companyId;
	public long createDate;
	public long modifiedDate;
	public long questionId;
	public long userId;
	public String reportContent;
	public String reportStatus;
}