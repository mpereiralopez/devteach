/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.BadgeRule;

import com.devshaker.service.persistence.BadgeRulePK;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing BadgeRule in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see BadgeRule
 * @generated
 */
@ProviderType
public class BadgeRuleCacheModel implements CacheModel<BadgeRule>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BadgeRuleCacheModel)) {
			return false;
		}

		BadgeRuleCacheModel badgeRuleCacheModel = (BadgeRuleCacheModel)obj;

		if (badgeRulePK.equals(badgeRuleCacheModel.badgeRulePK)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, badgeRulePK);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{ruleId=");
		sb.append(ruleId);
		sb.append(", badgeId=");
		sb.append(badgeId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public BadgeRule toEntityModel() {
		BadgeRuleImpl badgeRuleImpl = new BadgeRuleImpl();

		badgeRuleImpl.setRuleId(ruleId);
		badgeRuleImpl.setBadgeId(badgeId);
		badgeRuleImpl.setCompanyId(companyId);

		badgeRuleImpl.resetOriginalValues();

		return badgeRuleImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ruleId = objectInput.readLong();

		badgeId = objectInput.readLong();

		companyId = objectInput.readLong();

		badgeRulePK = new BadgeRulePK(ruleId, badgeId);
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ruleId);

		objectOutput.writeLong(badgeId);

		objectOutput.writeLong(companyId);
	}

	public long ruleId;
	public long badgeId;
	public long companyId;
	public transient BadgeRulePK badgeRulePK;
}