/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.Level;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Level in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Level
 * @generated
 */
@ProviderType
public class LevelCacheModel implements CacheModel<Level>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LevelCacheModel)) {
			return false;
		}

		LevelCacheModel levelCacheModel = (LevelCacheModel)obj;

		if (levelId == levelCacheModel.levelId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, levelId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{levelId=");
		sb.append(levelId);
		sb.append(", categoryId=");
		sb.append(categoryId);
		sb.append(", subcategoryId=");
		sb.append(subcategoryId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", levelName=");
		sb.append(levelName);
		sb.append(", levelDescription=");
		sb.append(levelDescription);
		sb.append(", levelIcon=");
		sb.append(levelIcon);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Level toEntityModel() {
		LevelImpl levelImpl = new LevelImpl();

		levelImpl.setLevelId(levelId);
		levelImpl.setCategoryId(categoryId);
		levelImpl.setSubcategoryId(subcategoryId);
		levelImpl.setGroupId(groupId);
		levelImpl.setCompanyId(companyId);
		levelImpl.setUserId(userId);

		if (userName == null) {
			levelImpl.setUserName(StringPool.BLANK);
		}
		else {
			levelImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			levelImpl.setCreateDate(null);
		}
		else {
			levelImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			levelImpl.setModifiedDate(null);
		}
		else {
			levelImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (levelName == null) {
			levelImpl.setLevelName(StringPool.BLANK);
		}
		else {
			levelImpl.setLevelName(levelName);
		}

		if (levelDescription == null) {
			levelImpl.setLevelDescription(StringPool.BLANK);
		}
		else {
			levelImpl.setLevelDescription(levelDescription);
		}

		levelImpl.setLevelIcon(levelIcon);

		levelImpl.resetOriginalValues();

		return levelImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		levelId = objectInput.readLong();

		categoryId = objectInput.readLong();

		subcategoryId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		levelName = objectInput.readUTF();
		levelDescription = objectInput.readUTF();

		levelIcon = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(levelId);

		objectOutput.writeLong(categoryId);

		objectOutput.writeLong(subcategoryId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (levelName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(levelName);
		}

		if (levelDescription == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(levelDescription);
		}

		objectOutput.writeLong(levelIcon);
	}

	public long levelId;
	public long categoryId;
	public long subcategoryId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String levelName;
	public String levelDescription;
	public long levelIcon;
}