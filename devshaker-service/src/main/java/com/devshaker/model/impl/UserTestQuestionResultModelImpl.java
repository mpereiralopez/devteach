/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.UserTestQuestionResult;
import com.devshaker.model.UserTestQuestionResultModel;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.expando.kernel.util.ExpandoBridgeFactoryUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.impl.BaseModelImpl;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

import java.sql.Types;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the UserTestQuestionResult service. Represents a row in the &quot;devShaker_UserTestQuestionResult&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link UserTestQuestionResultModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link UserTestQuestionResultImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserTestQuestionResultImpl
 * @see UserTestQuestionResult
 * @see UserTestQuestionResultModel
 * @generated
 */
@ProviderType
public class UserTestQuestionResultModelImpl extends BaseModelImpl<UserTestQuestionResult>
	implements UserTestQuestionResultModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a user test question result model instance should use the {@link UserTestQuestionResult} interface instead.
	 */
	public static final String TABLE_NAME = "devShaker_UserTestQuestionResult";
	public static final Object[][] TABLE_COLUMNS = {
			{ "userTestQuestionResultId", Types.BIGINT },
			{ "questionId", Types.BIGINT },
			{ "userId", Types.BIGINT },
			{ "answersByUser", Types.VARCHAR },
			{ "createDate", Types.TIMESTAMP },
			{ "isCorrect", Types.BOOLEAN },
			{ "tokenData", Types.VARCHAR }
		};
	public static final Map<String, Integer> TABLE_COLUMNS_MAP = new HashMap<String, Integer>();

	static {
		TABLE_COLUMNS_MAP.put("userTestQuestionResultId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("questionId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("userId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("answersByUser", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("createDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("isCorrect", Types.BOOLEAN);
		TABLE_COLUMNS_MAP.put("tokenData", Types.VARCHAR);
	}

	public static final String TABLE_SQL_CREATE = "create table devShaker_UserTestQuestionResult (userTestQuestionResultId LONG not null primary key,questionId LONG,userId LONG,answersByUser VARCHAR(75) null,createDate DATE null,isCorrect BOOLEAN,tokenData VARCHAR(75) null)";
	public static final String TABLE_SQL_DROP = "drop table devShaker_UserTestQuestionResult";
	public static final String ORDER_BY_JPQL = " ORDER BY userTestQuestionResult.createDate DESC";
	public static final String ORDER_BY_SQL = " ORDER BY devShaker_UserTestQuestionResult.createDate DESC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.devshaker.service.util.PropsUtil.get(
				"value.object.entity.cache.enabled.com.devshaker.model.UserTestQuestionResult"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.devshaker.service.util.PropsUtil.get(
				"value.object.finder.cache.enabled.com.devshaker.model.UserTestQuestionResult"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.devshaker.service.util.PropsUtil.get(
				"value.object.column.bitmask.enabled.com.devshaker.model.UserTestQuestionResult"),
			true);
	public static final long ISCORRECT_COLUMN_BITMASK = 1L;
	public static final long QUESTIONID_COLUMN_BITMASK = 2L;
	public static final long TOKENDATA_COLUMN_BITMASK = 4L;
	public static final long USERID_COLUMN_BITMASK = 8L;
	public static final long CREATEDATE_COLUMN_BITMASK = 16L;
	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.devshaker.service.util.PropsUtil.get(
				"lock.expiration.time.com.devshaker.model.UserTestQuestionResult"));

	public UserTestQuestionResultModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _userTestQuestionResultId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setUserTestQuestionResultId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _userTestQuestionResultId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return UserTestQuestionResult.class;
	}

	@Override
	public String getModelClassName() {
		return UserTestQuestionResult.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("userTestQuestionResultId", getUserTestQuestionResultId());
		attributes.put("questionId", getQuestionId());
		attributes.put("userId", getUserId());
		attributes.put("answersByUser", getAnswersByUser());
		attributes.put("createDate", getCreateDate());
		attributes.put("isCorrect", getIsCorrect());
		attributes.put("tokenData", getTokenData());

		attributes.put("entityCacheEnabled", isEntityCacheEnabled());
		attributes.put("finderCacheEnabled", isFinderCacheEnabled());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long userTestQuestionResultId = (Long)attributes.get(
				"userTestQuestionResultId");

		if (userTestQuestionResultId != null) {
			setUserTestQuestionResultId(userTestQuestionResultId);
		}

		Long questionId = (Long)attributes.get("questionId");

		if (questionId != null) {
			setQuestionId(questionId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String answersByUser = (String)attributes.get("answersByUser");

		if (answersByUser != null) {
			setAnswersByUser(answersByUser);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Boolean isCorrect = (Boolean)attributes.get("isCorrect");

		if (isCorrect != null) {
			setIsCorrect(isCorrect);
		}

		String tokenData = (String)attributes.get("tokenData");

		if (tokenData != null) {
			setTokenData(tokenData);
		}
	}

	@Override
	public long getUserTestQuestionResultId() {
		return _userTestQuestionResultId;
	}

	@Override
	public void setUserTestQuestionResultId(long userTestQuestionResultId) {
		_userTestQuestionResultId = userTestQuestionResultId;
	}

	@Override
	public long getQuestionId() {
		return _questionId;
	}

	@Override
	public void setQuestionId(long questionId) {
		_columnBitmask |= QUESTIONID_COLUMN_BITMASK;

		if (!_setOriginalQuestionId) {
			_setOriginalQuestionId = true;

			_originalQuestionId = _questionId;
		}

		_questionId = questionId;
	}

	public long getOriginalQuestionId() {
		return _originalQuestionId;
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_columnBitmask |= USERID_COLUMN_BITMASK;

		if (!_setOriginalUserId) {
			_setOriginalUserId = true;

			_originalUserId = _userId;
		}

		_userId = userId;
	}

	@Override
	public String getUserUuid() {
		try {
			User user = UserLocalServiceUtil.getUserById(getUserId());

			return user.getUuid();
		}
		catch (PortalException pe) {
			return StringPool.BLANK;
		}
	}

	@Override
	public void setUserUuid(String userUuid) {
	}

	public long getOriginalUserId() {
		return _originalUserId;
	}

	@Override
	public String getAnswersByUser() {
		if (_answersByUser == null) {
			return StringPool.BLANK;
		}
		else {
			return _answersByUser;
		}
	}

	@Override
	public void setAnswersByUser(String answersByUser) {
		_answersByUser = answersByUser;
	}

	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		_columnBitmask = -1L;

		_createDate = createDate;
	}

	@Override
	public boolean getIsCorrect() {
		return _isCorrect;
	}

	@Override
	public boolean isIsCorrect() {
		return _isCorrect;
	}

	@Override
	public void setIsCorrect(boolean isCorrect) {
		_columnBitmask |= ISCORRECT_COLUMN_BITMASK;

		if (!_setOriginalIsCorrect) {
			_setOriginalIsCorrect = true;

			_originalIsCorrect = _isCorrect;
		}

		_isCorrect = isCorrect;
	}

	public boolean getOriginalIsCorrect() {
		return _originalIsCorrect;
	}

	@Override
	public String getTokenData() {
		if (_tokenData == null) {
			return StringPool.BLANK;
		}
		else {
			return _tokenData;
		}
	}

	@Override
	public void setTokenData(String tokenData) {
		_columnBitmask |= TOKENDATA_COLUMN_BITMASK;

		if (_originalTokenData == null) {
			_originalTokenData = _tokenData;
		}

		_tokenData = tokenData;
	}

	public String getOriginalTokenData() {
		return GetterUtil.getString(_originalTokenData);
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(0,
			UserTestQuestionResult.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public UserTestQuestionResult toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (UserTestQuestionResult)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		UserTestQuestionResultImpl userTestQuestionResultImpl = new UserTestQuestionResultImpl();

		userTestQuestionResultImpl.setUserTestQuestionResultId(getUserTestQuestionResultId());
		userTestQuestionResultImpl.setQuestionId(getQuestionId());
		userTestQuestionResultImpl.setUserId(getUserId());
		userTestQuestionResultImpl.setAnswersByUser(getAnswersByUser());
		userTestQuestionResultImpl.setCreateDate(getCreateDate());
		userTestQuestionResultImpl.setIsCorrect(getIsCorrect());
		userTestQuestionResultImpl.setTokenData(getTokenData());

		userTestQuestionResultImpl.resetOriginalValues();

		return userTestQuestionResultImpl;
	}

	@Override
	public int compareTo(UserTestQuestionResult userTestQuestionResult) {
		int value = 0;

		value = DateUtil.compareTo(getCreateDate(),
				userTestQuestionResult.getCreateDate());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserTestQuestionResult)) {
			return false;
		}

		UserTestQuestionResult userTestQuestionResult = (UserTestQuestionResult)obj;

		long primaryKey = userTestQuestionResult.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return ENTITY_CACHE_ENABLED;
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return FINDER_CACHE_ENABLED;
	}

	@Override
	public void resetOriginalValues() {
		UserTestQuestionResultModelImpl userTestQuestionResultModelImpl = this;

		userTestQuestionResultModelImpl._originalQuestionId = userTestQuestionResultModelImpl._questionId;

		userTestQuestionResultModelImpl._setOriginalQuestionId = false;

		userTestQuestionResultModelImpl._originalUserId = userTestQuestionResultModelImpl._userId;

		userTestQuestionResultModelImpl._setOriginalUserId = false;

		userTestQuestionResultModelImpl._originalIsCorrect = userTestQuestionResultModelImpl._isCorrect;

		userTestQuestionResultModelImpl._setOriginalIsCorrect = false;

		userTestQuestionResultModelImpl._originalTokenData = userTestQuestionResultModelImpl._tokenData;

		userTestQuestionResultModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<UserTestQuestionResult> toCacheModel() {
		UserTestQuestionResultCacheModel userTestQuestionResultCacheModel = new UserTestQuestionResultCacheModel();

		userTestQuestionResultCacheModel.userTestQuestionResultId = getUserTestQuestionResultId();

		userTestQuestionResultCacheModel.questionId = getQuestionId();

		userTestQuestionResultCacheModel.userId = getUserId();

		userTestQuestionResultCacheModel.answersByUser = getAnswersByUser();

		String answersByUser = userTestQuestionResultCacheModel.answersByUser;

		if ((answersByUser != null) && (answersByUser.length() == 0)) {
			userTestQuestionResultCacheModel.answersByUser = null;
		}

		Date createDate = getCreateDate();

		if (createDate != null) {
			userTestQuestionResultCacheModel.createDate = createDate.getTime();
		}
		else {
			userTestQuestionResultCacheModel.createDate = Long.MIN_VALUE;
		}

		userTestQuestionResultCacheModel.isCorrect = getIsCorrect();

		userTestQuestionResultCacheModel.tokenData = getTokenData();

		String tokenData = userTestQuestionResultCacheModel.tokenData;

		if ((tokenData != null) && (tokenData.length() == 0)) {
			userTestQuestionResultCacheModel.tokenData = null;
		}

		return userTestQuestionResultCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{userTestQuestionResultId=");
		sb.append(getUserTestQuestionResultId());
		sb.append(", questionId=");
		sb.append(getQuestionId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", answersByUser=");
		sb.append(getAnswersByUser());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", isCorrect=");
		sb.append(getIsCorrect());
		sb.append(", tokenData=");
		sb.append(getTokenData());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(25);

		sb.append("<model><model-name>");
		sb.append("com.devshaker.model.UserTestQuestionResult");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>userTestQuestionResultId</column-name><column-value><![CDATA[");
		sb.append(getUserTestQuestionResultId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>questionId</column-name><column-value><![CDATA[");
		sb.append(getQuestionId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>answersByUser</column-name><column-value><![CDATA[");
		sb.append(getAnswersByUser());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isCorrect</column-name><column-value><![CDATA[");
		sb.append(getIsCorrect());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tokenData</column-name><column-value><![CDATA[");
		sb.append(getTokenData());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static final ClassLoader _classLoader = UserTestQuestionResult.class.getClassLoader();
	private static final Class<?>[] _escapedModelInterfaces = new Class[] {
			UserTestQuestionResult.class
		};
	private long _userTestQuestionResultId;
	private long _questionId;
	private long _originalQuestionId;
	private boolean _setOriginalQuestionId;
	private long _userId;
	private long _originalUserId;
	private boolean _setOriginalUserId;
	private String _answersByUser;
	private Date _createDate;
	private boolean _isCorrect;
	private boolean _originalIsCorrect;
	private boolean _setOriginalIsCorrect;
	private String _tokenData;
	private String _originalTokenData;
	private long _columnBitmask;
	private UserTestQuestionResult _escapedModel;
}