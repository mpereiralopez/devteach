/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.Rule;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Rule in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Rule
 * @generated
 */
@ProviderType
public class RuleCacheModel implements CacheModel<Rule>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof RuleCacheModel)) {
			return false;
		}

		RuleCacheModel ruleCacheModel = (RuleCacheModel)obj;

		if (ruleId == ruleCacheModel.ruleId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, ruleId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{ruleId=");
		sb.append(ruleId);
		sb.append(", ruleDesc=");
		sb.append(ruleDesc);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userCreatorId=");
		sb.append(userCreatorId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Rule toEntityModel() {
		RuleImpl ruleImpl = new RuleImpl();

		ruleImpl.setRuleId(ruleId);

		if (ruleDesc == null) {
			ruleImpl.setRuleDesc(StringPool.BLANK);
		}
		else {
			ruleImpl.setRuleDesc(ruleDesc);
		}

		ruleImpl.setCompanyId(companyId);
		ruleImpl.setUserCreatorId(userCreatorId);

		if (userName == null) {
			ruleImpl.setUserName(StringPool.BLANK);
		}
		else {
			ruleImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			ruleImpl.setCreateDate(null);
		}
		else {
			ruleImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			ruleImpl.setModifiedDate(null);
		}
		else {
			ruleImpl.setModifiedDate(new Date(modifiedDate));
		}

		ruleImpl.resetOriginalValues();

		return ruleImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ruleId = objectInput.readLong();
		ruleDesc = objectInput.readUTF();

		companyId = objectInput.readLong();

		userCreatorId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ruleId);

		if (ruleDesc == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ruleDesc);
		}

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userCreatorId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
	}

	public long ruleId;
	public String ruleDesc;
	public long companyId;
	public long userCreatorId;
	public String userName;
	public long createDate;
	public long modifiedDate;
}