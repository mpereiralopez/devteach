/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.TestQuestion;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing TestQuestion in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see TestQuestion
 * @generated
 */
@ProviderType
public class TestQuestionCacheModel implements CacheModel<TestQuestion>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TestQuestionCacheModel)) {
			return false;
		}

		TestQuestionCacheModel testQuestionCacheModel = (TestQuestionCacheModel)obj;

		if (questionId == testQuestionCacheModel.questionId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, questionId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(45);

		sb.append("{questionId=");
		sb.append(questionId);
		sb.append(", categoryId=");
		sb.append(categoryId);
		sb.append(", subcategoryId=");
		sb.append(subcategoryId);
		sb.append(", levelId=");
		sb.append(levelId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userIdCreator=");
		sb.append(userIdCreator);
		sb.append(", userNameCreator=");
		sb.append(userNameCreator);
		sb.append(", userIdModified=");
		sb.append(userIdModified);
		sb.append(", userNameModified=");
		sb.append(userNameModified);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", questionText=");
		sb.append(questionText);
		sb.append(", status=");
		sb.append(status);
		sb.append(", type=");
		sb.append(type);
		sb.append(", answer0Text=");
		sb.append(answer0Text);
		sb.append(", answer1Text=");
		sb.append(answer1Text);
		sb.append(", answer2Text=");
		sb.append(answer2Text);
		sb.append(", answer3Text=");
		sb.append(answer3Text);
		sb.append(", correctAnswers=");
		sb.append(correctAnswers);
		sb.append(", reportContent=");
		sb.append(reportContent);
		sb.append(", reporterId=");
		sb.append(reporterId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public TestQuestion toEntityModel() {
		TestQuestionImpl testQuestionImpl = new TestQuestionImpl();

		testQuestionImpl.setQuestionId(questionId);
		testQuestionImpl.setCategoryId(categoryId);
		testQuestionImpl.setSubcategoryId(subcategoryId);
		testQuestionImpl.setLevelId(levelId);
		testQuestionImpl.setGroupId(groupId);
		testQuestionImpl.setCompanyId(companyId);
		testQuestionImpl.setUserIdCreator(userIdCreator);

		if (userNameCreator == null) {
			testQuestionImpl.setUserNameCreator(StringPool.BLANK);
		}
		else {
			testQuestionImpl.setUserNameCreator(userNameCreator);
		}

		testQuestionImpl.setUserIdModified(userIdModified);

		if (userNameModified == null) {
			testQuestionImpl.setUserNameModified(StringPool.BLANK);
		}
		else {
			testQuestionImpl.setUserNameModified(userNameModified);
		}

		if (createDate == Long.MIN_VALUE) {
			testQuestionImpl.setCreateDate(null);
		}
		else {
			testQuestionImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			testQuestionImpl.setModifiedDate(null);
		}
		else {
			testQuestionImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (questionText == null) {
			testQuestionImpl.setQuestionText(StringPool.BLANK);
		}
		else {
			testQuestionImpl.setQuestionText(questionText);
		}

		testQuestionImpl.setStatus(status);
		testQuestionImpl.setType(type);

		if (answer0Text == null) {
			testQuestionImpl.setAnswer0Text(StringPool.BLANK);
		}
		else {
			testQuestionImpl.setAnswer0Text(answer0Text);
		}

		if (answer1Text == null) {
			testQuestionImpl.setAnswer1Text(StringPool.BLANK);
		}
		else {
			testQuestionImpl.setAnswer1Text(answer1Text);
		}

		if (answer2Text == null) {
			testQuestionImpl.setAnswer2Text(StringPool.BLANK);
		}
		else {
			testQuestionImpl.setAnswer2Text(answer2Text);
		}

		if (answer3Text == null) {
			testQuestionImpl.setAnswer3Text(StringPool.BLANK);
		}
		else {
			testQuestionImpl.setAnswer3Text(answer3Text);
		}

		if (correctAnswers == null) {
			testQuestionImpl.setCorrectAnswers(StringPool.BLANK);
		}
		else {
			testQuestionImpl.setCorrectAnswers(correctAnswers);
		}

		if (reportContent == null) {
			testQuestionImpl.setReportContent(StringPool.BLANK);
		}
		else {
			testQuestionImpl.setReportContent(reportContent);
		}

		testQuestionImpl.setReporterId(reporterId);

		testQuestionImpl.resetOriginalValues();

		return testQuestionImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		questionId = objectInput.readLong();

		categoryId = objectInput.readLong();

		subcategoryId = objectInput.readLong();

		levelId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userIdCreator = objectInput.readLong();
		userNameCreator = objectInput.readUTF();

		userIdModified = objectInput.readLong();
		userNameModified = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		questionText = objectInput.readUTF();

		status = objectInput.readInt();

		type = objectInput.readInt();
		answer0Text = objectInput.readUTF();
		answer1Text = objectInput.readUTF();
		answer2Text = objectInput.readUTF();
		answer3Text = objectInput.readUTF();
		correctAnswers = objectInput.readUTF();
		reportContent = objectInput.readUTF();

		reporterId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(questionId);

		objectOutput.writeLong(categoryId);

		objectOutput.writeLong(subcategoryId);

		objectOutput.writeLong(levelId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userIdCreator);

		if (userNameCreator == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userNameCreator);
		}

		objectOutput.writeLong(userIdModified);

		if (userNameModified == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userNameModified);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (questionText == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(questionText);
		}

		objectOutput.writeInt(status);

		objectOutput.writeInt(type);

		if (answer0Text == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(answer0Text);
		}

		if (answer1Text == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(answer1Text);
		}

		if (answer2Text == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(answer2Text);
		}

		if (answer3Text == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(answer3Text);
		}

		if (correctAnswers == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(correctAnswers);
		}

		if (reportContent == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(reportContent);
		}

		objectOutput.writeLong(reporterId);
	}

	public long questionId;
	public long categoryId;
	public long subcategoryId;
	public long levelId;
	public long groupId;
	public long companyId;
	public long userIdCreator;
	public String userNameCreator;
	public long userIdModified;
	public String userNameModified;
	public long createDate;
	public long modifiedDate;
	public String questionText;
	public int status;
	public int type;
	public String answer0Text;
	public String answer1Text;
	public String answer2Text;
	public String answer3Text;
	public String correctAnswers;
	public String reportContent;
	public long reporterId;
}