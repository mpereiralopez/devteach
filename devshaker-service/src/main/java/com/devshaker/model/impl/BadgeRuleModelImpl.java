/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.BadgeRule;
import com.devshaker.model.BadgeRuleModel;

import com.devshaker.service.persistence.BadgeRulePK;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.impl.BaseModelImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;

import java.io.Serializable;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the BadgeRule service. Represents a row in the &quot;devShaker_BadgeRule&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link BadgeRuleModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link BadgeRuleImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BadgeRuleImpl
 * @see BadgeRule
 * @see BadgeRuleModel
 * @generated
 */
@ProviderType
public class BadgeRuleModelImpl extends BaseModelImpl<BadgeRule>
	implements BadgeRuleModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a badge rule model instance should use the {@link BadgeRule} interface instead.
	 */
	public static final String TABLE_NAME = "devShaker_BadgeRule";
	public static final Object[][] TABLE_COLUMNS = {
			{ "ruleId", Types.BIGINT },
			{ "badgeId", Types.BIGINT },
			{ "companyId", Types.BIGINT }
		};
	public static final Map<String, Integer> TABLE_COLUMNS_MAP = new HashMap<String, Integer>();

	static {
		TABLE_COLUMNS_MAP.put("ruleId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("badgeId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("companyId", Types.BIGINT);
	}

	public static final String TABLE_SQL_CREATE = "create table devShaker_BadgeRule (ruleId LONG not null,badgeId LONG not null,companyId LONG,primary key (ruleId, badgeId))";
	public static final String TABLE_SQL_DROP = "drop table devShaker_BadgeRule";
	public static final String ORDER_BY_JPQL = " ORDER BY badgeRule.id.ruleId ASC, badgeRule.id.badgeId ASC";
	public static final String ORDER_BY_SQL = " ORDER BY devShaker_BadgeRule.ruleId ASC, devShaker_BadgeRule.badgeId ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.devshaker.service.util.PropsUtil.get(
				"value.object.entity.cache.enabled.com.devshaker.model.BadgeRule"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.devshaker.service.util.PropsUtil.get(
				"value.object.finder.cache.enabled.com.devshaker.model.BadgeRule"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.devshaker.service.util.PropsUtil.get(
				"value.object.column.bitmask.enabled.com.devshaker.model.BadgeRule"),
			true);
	public static final long BADGEID_COLUMN_BITMASK = 1L;
	public static final long COMPANYID_COLUMN_BITMASK = 2L;
	public static final long RULEID_COLUMN_BITMASK = 4L;
	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.devshaker.service.util.PropsUtil.get(
				"lock.expiration.time.com.devshaker.model.BadgeRule"));

	public BadgeRuleModelImpl() {
	}

	@Override
	public BadgeRulePK getPrimaryKey() {
		return new BadgeRulePK(_ruleId, _badgeId);
	}

	@Override
	public void setPrimaryKey(BadgeRulePK primaryKey) {
		setRuleId(primaryKey.ruleId);
		setBadgeId(primaryKey.badgeId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new BadgeRulePK(_ruleId, _badgeId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((BadgeRulePK)primaryKeyObj);
	}

	@Override
	public Class<?> getModelClass() {
		return BadgeRule.class;
	}

	@Override
	public String getModelClassName() {
		return BadgeRule.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ruleId", getRuleId());
		attributes.put("badgeId", getBadgeId());
		attributes.put("companyId", getCompanyId());

		attributes.put("entityCacheEnabled", isEntityCacheEnabled());
		attributes.put("finderCacheEnabled", isFinderCacheEnabled());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ruleId = (Long)attributes.get("ruleId");

		if (ruleId != null) {
			setRuleId(ruleId);
		}

		Long badgeId = (Long)attributes.get("badgeId");

		if (badgeId != null) {
			setBadgeId(badgeId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}
	}

	@Override
	public long getRuleId() {
		return _ruleId;
	}

	@Override
	public void setRuleId(long ruleId) {
		_ruleId = ruleId;
	}

	@Override
	public long getBadgeId() {
		return _badgeId;
	}

	@Override
	public void setBadgeId(long badgeId) {
		_columnBitmask |= BADGEID_COLUMN_BITMASK;

		if (!_setOriginalBadgeId) {
			_setOriginalBadgeId = true;

			_originalBadgeId = _badgeId;
		}

		_badgeId = badgeId;
	}

	public long getOriginalBadgeId() {
		return _originalBadgeId;
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		_columnBitmask |= COMPANYID_COLUMN_BITMASK;

		if (!_setOriginalCompanyId) {
			_setOriginalCompanyId = true;

			_originalCompanyId = _companyId;
		}

		_companyId = companyId;
	}

	public long getOriginalCompanyId() {
		return _originalCompanyId;
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public BadgeRule toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (BadgeRule)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		BadgeRuleImpl badgeRuleImpl = new BadgeRuleImpl();

		badgeRuleImpl.setRuleId(getRuleId());
		badgeRuleImpl.setBadgeId(getBadgeId());
		badgeRuleImpl.setCompanyId(getCompanyId());

		badgeRuleImpl.resetOriginalValues();

		return badgeRuleImpl;
	}

	@Override
	public int compareTo(BadgeRule badgeRule) {
		BadgeRulePK primaryKey = badgeRule.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BadgeRule)) {
			return false;
		}

		BadgeRule badgeRule = (BadgeRule)obj;

		BadgeRulePK primaryKey = badgeRule.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return ENTITY_CACHE_ENABLED;
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return FINDER_CACHE_ENABLED;
	}

	@Override
	public void resetOriginalValues() {
		BadgeRuleModelImpl badgeRuleModelImpl = this;

		badgeRuleModelImpl._originalBadgeId = badgeRuleModelImpl._badgeId;

		badgeRuleModelImpl._setOriginalBadgeId = false;

		badgeRuleModelImpl._originalCompanyId = badgeRuleModelImpl._companyId;

		badgeRuleModelImpl._setOriginalCompanyId = false;

		badgeRuleModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<BadgeRule> toCacheModel() {
		BadgeRuleCacheModel badgeRuleCacheModel = new BadgeRuleCacheModel();

		badgeRuleCacheModel.badgeRulePK = getPrimaryKey();

		badgeRuleCacheModel.ruleId = getRuleId();

		badgeRuleCacheModel.badgeId = getBadgeId();

		badgeRuleCacheModel.companyId = getCompanyId();

		return badgeRuleCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{ruleId=");
		sb.append(getRuleId());
		sb.append(", badgeId=");
		sb.append(getBadgeId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("com.devshaker.model.BadgeRule");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>ruleId</column-name><column-value><![CDATA[");
		sb.append(getRuleId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>badgeId</column-name><column-value><![CDATA[");
		sb.append(getBadgeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static final ClassLoader _classLoader = BadgeRule.class.getClassLoader();
	private static final Class<?>[] _escapedModelInterfaces = new Class[] {
			BadgeRule.class
		};
	private long _ruleId;
	private long _badgeId;
	private long _originalBadgeId;
	private boolean _setOriginalBadgeId;
	private long _companyId;
	private long _originalCompanyId;
	private boolean _setOriginalCompanyId;
	private long _columnBitmask;
	private BadgeRule _escapedModel;
}