/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.UserBadge;

import com.devshaker.service.persistence.UserBadgePK;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing UserBadge in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see UserBadge
 * @generated
 */
@ProviderType
public class UserBadgeCacheModel implements CacheModel<UserBadge>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserBadgeCacheModel)) {
			return false;
		}

		UserBadgeCacheModel userBadgeCacheModel = (UserBadgeCacheModel)obj;

		if (userBadgePK.equals(userBadgeCacheModel.userBadgePK)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, userBadgePK);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{userId=");
		sb.append(userId);
		sb.append(", badgeId=");
		sb.append(badgeId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UserBadge toEntityModel() {
		UserBadgeImpl userBadgeImpl = new UserBadgeImpl();

		userBadgeImpl.setUserId(userId);
		userBadgeImpl.setBadgeId(badgeId);
		userBadgeImpl.setCompanyId(companyId);

		if (createDate == Long.MIN_VALUE) {
			userBadgeImpl.setCreateDate(null);
		}
		else {
			userBadgeImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			userBadgeImpl.setModifiedDate(null);
		}
		else {
			userBadgeImpl.setModifiedDate(new Date(modifiedDate));
		}

		userBadgeImpl.resetOriginalValues();

		return userBadgeImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		userId = objectInput.readLong();

		badgeId = objectInput.readLong();

		companyId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		userBadgePK = new UserBadgePK(userId, badgeId);
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(userId);

		objectOutput.writeLong(badgeId);

		objectOutput.writeLong(companyId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
	}

	public long userId;
	public long badgeId;
	public long companyId;
	public long createDate;
	public long modifiedDate;
	public transient UserBadgePK userBadgePK;
}