/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.UserTestQuestionResult;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing UserTestQuestionResult in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see UserTestQuestionResult
 * @generated
 */
@ProviderType
public class UserTestQuestionResultCacheModel implements CacheModel<UserTestQuestionResult>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserTestQuestionResultCacheModel)) {
			return false;
		}

		UserTestQuestionResultCacheModel userTestQuestionResultCacheModel = (UserTestQuestionResultCacheModel)obj;

		if (userTestQuestionResultId == userTestQuestionResultCacheModel.userTestQuestionResultId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, userTestQuestionResultId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{userTestQuestionResultId=");
		sb.append(userTestQuestionResultId);
		sb.append(", questionId=");
		sb.append(questionId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", answersByUser=");
		sb.append(answersByUser);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", isCorrect=");
		sb.append(isCorrect);
		sb.append(", tokenData=");
		sb.append(tokenData);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UserTestQuestionResult toEntityModel() {
		UserTestQuestionResultImpl userTestQuestionResultImpl = new UserTestQuestionResultImpl();

		userTestQuestionResultImpl.setUserTestQuestionResultId(userTestQuestionResultId);
		userTestQuestionResultImpl.setQuestionId(questionId);
		userTestQuestionResultImpl.setUserId(userId);

		if (answersByUser == null) {
			userTestQuestionResultImpl.setAnswersByUser(StringPool.BLANK);
		}
		else {
			userTestQuestionResultImpl.setAnswersByUser(answersByUser);
		}

		if (createDate == Long.MIN_VALUE) {
			userTestQuestionResultImpl.setCreateDate(null);
		}
		else {
			userTestQuestionResultImpl.setCreateDate(new Date(createDate));
		}

		userTestQuestionResultImpl.setIsCorrect(isCorrect);

		if (tokenData == null) {
			userTestQuestionResultImpl.setTokenData(StringPool.BLANK);
		}
		else {
			userTestQuestionResultImpl.setTokenData(tokenData);
		}

		userTestQuestionResultImpl.resetOriginalValues();

		return userTestQuestionResultImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		userTestQuestionResultId = objectInput.readLong();

		questionId = objectInput.readLong();

		userId = objectInput.readLong();
		answersByUser = objectInput.readUTF();
		createDate = objectInput.readLong();

		isCorrect = objectInput.readBoolean();
		tokenData = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(userTestQuestionResultId);

		objectOutput.writeLong(questionId);

		objectOutput.writeLong(userId);

		if (answersByUser == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(answersByUser);
		}

		objectOutput.writeLong(createDate);

		objectOutput.writeBoolean(isCorrect);

		if (tokenData == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tokenData);
		}
	}

	public long userTestQuestionResultId;
	public long questionId;
	public long userId;
	public String answersByUser;
	public long createDate;
	public boolean isCorrect;
	public String tokenData;
}