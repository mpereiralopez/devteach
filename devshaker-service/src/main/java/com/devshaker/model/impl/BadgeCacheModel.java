/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.Badge;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Badge in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Badge
 * @generated
 */
@ProviderType
public class BadgeCacheModel implements CacheModel<Badge>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BadgeCacheModel)) {
			return false;
		}

		BadgeCacheModel badgeCacheModel = (BadgeCacheModel)obj;

		if (badgeId == badgeCacheModel.badgeId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, badgeId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{badgeId=");
		sb.append(badgeId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userCreatorId=");
		sb.append(userCreatorId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", badgeTitle=");
		sb.append(badgeTitle);
		sb.append(", badgeDescription=");
		sb.append(badgeDescription);
		sb.append(", badgeIcon=");
		sb.append(badgeIcon);
		sb.append(", objPK=");
		sb.append(objPK);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Badge toEntityModel() {
		BadgeImpl badgeImpl = new BadgeImpl();

		badgeImpl.setBadgeId(badgeId);
		badgeImpl.setGroupId(groupId);
		badgeImpl.setCompanyId(companyId);
		badgeImpl.setUserCreatorId(userCreatorId);

		if (userName == null) {
			badgeImpl.setUserName(StringPool.BLANK);
		}
		else {
			badgeImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			badgeImpl.setCreateDate(null);
		}
		else {
			badgeImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			badgeImpl.setModifiedDate(null);
		}
		else {
			badgeImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (badgeTitle == null) {
			badgeImpl.setBadgeTitle(StringPool.BLANK);
		}
		else {
			badgeImpl.setBadgeTitle(badgeTitle);
		}

		if (badgeDescription == null) {
			badgeImpl.setBadgeDescription(StringPool.BLANK);
		}
		else {
			badgeImpl.setBadgeDescription(badgeDescription);
		}

		badgeImpl.setBadgeIcon(badgeIcon);
		badgeImpl.setObjPK(objPK);

		badgeImpl.resetOriginalValues();

		return badgeImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		badgeId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userCreatorId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		badgeTitle = objectInput.readUTF();
		badgeDescription = objectInput.readUTF();

		badgeIcon = objectInput.readLong();

		objPK = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(badgeId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userCreatorId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (badgeTitle == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(badgeTitle);
		}

		if (badgeDescription == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(badgeDescription);
		}

		objectOutput.writeLong(badgeIcon);

		objectOutput.writeLong(objPK);
	}

	public long badgeId;
	public long groupId;
	public long companyId;
	public long userCreatorId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String badgeTitle;
	public String badgeDescription;
	public long badgeIcon;
	public long objPK;
}