/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.Subcategory;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Subcategory in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Subcategory
 * @generated
 */
@ProviderType
public class SubcategoryCacheModel implements CacheModel<Subcategory>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SubcategoryCacheModel)) {
			return false;
		}

		SubcategoryCacheModel subcategoryCacheModel = (SubcategoryCacheModel)obj;

		if (subcategoryId == subcategoryCacheModel.subcategoryId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, subcategoryId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", categoryId=");
		sb.append(categoryId);
		sb.append(", subcategoryId=");
		sb.append(subcategoryId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", subcategoryName=");
		sb.append(subcategoryName);
		sb.append(", subcategoryDescription=");
		sb.append(subcategoryDescription);
		sb.append(", subcategoryIcon=");
		sb.append(subcategoryIcon);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Subcategory toEntityModel() {
		SubcategoryImpl subcategoryImpl = new SubcategoryImpl();

		if (uuid == null) {
			subcategoryImpl.setUuid(StringPool.BLANK);
		}
		else {
			subcategoryImpl.setUuid(uuid);
		}

		subcategoryImpl.setCategoryId(categoryId);
		subcategoryImpl.setSubcategoryId(subcategoryId);
		subcategoryImpl.setGroupId(groupId);
		subcategoryImpl.setCompanyId(companyId);
		subcategoryImpl.setUserId(userId);

		if (userName == null) {
			subcategoryImpl.setUserName(StringPool.BLANK);
		}
		else {
			subcategoryImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			subcategoryImpl.setCreateDate(null);
		}
		else {
			subcategoryImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			subcategoryImpl.setModifiedDate(null);
		}
		else {
			subcategoryImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (subcategoryName == null) {
			subcategoryImpl.setSubcategoryName(StringPool.BLANK);
		}
		else {
			subcategoryImpl.setSubcategoryName(subcategoryName);
		}

		if (subcategoryDescription == null) {
			subcategoryImpl.setSubcategoryDescription(StringPool.BLANK);
		}
		else {
			subcategoryImpl.setSubcategoryDescription(subcategoryDescription);
		}

		subcategoryImpl.setSubcategoryIcon(subcategoryIcon);

		subcategoryImpl.resetOriginalValues();

		return subcategoryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		categoryId = objectInput.readLong();

		subcategoryId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		subcategoryName = objectInput.readUTF();
		subcategoryDescription = objectInput.readUTF();

		subcategoryIcon = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(categoryId);

		objectOutput.writeLong(subcategoryId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (subcategoryName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(subcategoryName);
		}

		if (subcategoryDescription == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(subcategoryDescription);
		}

		objectOutput.writeLong(subcategoryIcon);
	}

	public String uuid;
	public long categoryId;
	public long subcategoryId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String subcategoryName;
	public String subcategoryDescription;
	public long subcategoryIcon;
}