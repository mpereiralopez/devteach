/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.impl;

import aQute.bnd.annotation.ProviderType;

import java.util.ArrayList;
import java.util.List;

import com.devshaker.model.BadgeRule;
import com.devshaker.model.Rule;
import com.devshaker.service.base.BadgeRuleLocalServiceBaseImpl;
import com.devshaker.service.persistence.BadgeRulePK;

/**
 * The implementation of the badge rule local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.devshaker.service.BadgeRuleLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BadgeRuleLocalServiceBaseImpl
 * @see com.devshaker.service.BadgeRuleLocalServiceUtil
 */
@ProviderType
public class BadgeRuleLocalServiceImpl extends BadgeRuleLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.devshaker.service.BadgeRuleLocalServiceUtil} to access the badge rule local service.
	 */
	
	public BadgeRule getBadgeRuleByBadgeIdAndRuleId(long ruleId, long badgeId){
		BadgeRulePK pk = new BadgeRulePK(ruleId, badgeId);
		return badgeRulePersistence.fetchByPrimaryKey(pk);
	}
	
	public List<BadgeRule> getBadgeRuleByCompanyId(long companyId){
		return badgeRulePersistence.findByCompanyId(companyId);
	}
	
	public List<Rule> getRulesOfBadge(long badgeId){
		List<BadgeRule> brs = badgeRulePersistence.findByBadgeId(badgeId);
		List <Rule> rules = new ArrayList<Rule>();
		for(BadgeRule br: brs){
			rules.add(rulePersistence.fetchByPrimaryKey(br.getRuleId()));
		}
		return rules;
	}
	
	
}