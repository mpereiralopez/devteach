/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.impl;

import aQute.bnd.annotation.ProviderType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import com.devshaker.model.Badge;
import com.devshaker.model.Category;
import com.devshaker.model.Level;
import com.devshaker.model.ReportedQuestions;
import com.devshaker.model.Subcategory;
import com.devshaker.model.TestQuestion;
import com.devshaker.model.UserTestQuestionResult;
import com.devshaker.service.BadgeLocalServiceUtil;
import com.devshaker.service.CategoryLocalServiceUtil;
import com.devshaker.service.LevelLocalServiceUtil;
import com.devshaker.service.ReportedQuestionsLocalServiceUtil;
import com.devshaker.service.SubcategoryLocalServiceUtil;
import com.devshaker.service.TestQuestionLocalServiceUtil;
import com.devshaker.service.UserBadgeLocalServiceUtil;
import com.devshaker.service.UserTestQuestionResultLocalServiceUtil;
import com.devshaker.service.base.ApiServiceBaseImpl;
import com.devshaker.service.persistence.UserBadgePK;
import com.devshaker.util.XpUltils;
import com.gamification.api.engine.RuleExecutor;
import com.gamification.api.util.Definitions;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.document.library.kernel.util.DLUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONSerializer;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.Country;
import com.liferay.portal.kernel.model.Image;
import com.liferay.portal.kernel.model.ListType;
import com.liferay.portal.kernel.model.ListTypeConstants;
import com.liferay.portal.kernel.model.Phone;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.service.AddressLocalServiceUtil;
import com.liferay.portal.kernel.service.CountryServiceUtil;
import com.liferay.portal.kernel.service.ImageLocalServiceUtil;
import com.liferay.portal.kernel.service.ListTypeLocalServiceUtil;
import com.liferay.portal.kernel.service.PhoneLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Base64;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

/**
 * The implementation of the api remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link com.devshaker.service.ApiService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ApiServiceBaseImpl
 * @see com.devshaker.service.ApiServiceUtil
 */
@ProviderType
public class ApiServiceImpl extends ApiServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link
	 * com.devshaker.service.ApiServiceUtil} to access the api remote service.
	 */
	
	private static final int NUM_QUESTION_TEST = 5;

	private static final String[] fieldsToExclude = { "descriptionCurrentValue", "columnBitmask", "originalUuid",
			"cachedModel", "description", "stagedModelType", "uuid", "modelClassName", "originalName",
			"nameCurrentLanguageId", "descriptionCurrentLanguageId", "originalUserId", "finderCacheEnabled",
			"modelClass", "entityCacheEnabled", "escapedModel", "primaryKeyObj", "modelAttributes", "userUuid",
			"primaryKey","answer2TextCurrentValue", "name", "defaultLanguageId","questionTextCurrentValue","questionTextCurrentLanguageId",
			"answer3TextCurrentLanguageId","answer2TextCurrentLanguageId","answer1TextCurrentLanguageId","answer3TextCurrentLanguageId","answer0TextCurrentValue",
			"answer0TextCurrentLanguageId","availableLanguageIds","questionTextMap","answer2TextMap","answer1TextMap","answer0TextMap","answer3TextMap","reporterId"};

	private static final String API_VERSION = "1.0";

	public String getApiVersion() {
		try {
			System.out.println(this.getGuestOrUserId());
			System.out.println(this.getUserId());
			System.out.println(UserLocalServiceUtil.getUser(this.getUserId()));

		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return API_VERSION;
	}

	public List<JSONObject> getCategoriesOfCompany() {
		List<JSONObject> jsonCategoryList = new ArrayList<JSONObject>();
		JSONSerializer jSONSerializer = JSONFactoryUtil.createJSONSerializer();
		jSONSerializer.exclude(fieldsToExclude);
		try {
			User user = UserLocalServiceUtil.getUser(this.getUserId());
			long companyIdOfUser = user.getCompanyId();
			List<Category> categoryList = CategoryLocalServiceUtil.getCategoriesOfCompany(companyIdOfUser);
			for (Category cat : categoryList) {
				JSONObject jSONObject = JSONFactoryUtil.createJSONObject(jSONSerializer.serializeDeep(cat));
				jsonCategoryList.add(jSONObject);
			}
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
			jsonObject.put("error", "unrecognized user");
			jsonCategoryList.add(jsonObject);
		}
		return jsonCategoryList;
	}

	public List<JSONObject> getSubCategoriesOfCategory(long categoryId) {
		List<JSONObject> jsonSubCategoryList = new ArrayList<JSONObject>();
		JSONSerializer jSONSerializer = JSONFactoryUtil.createJSONSerializer();
		jSONSerializer.exclude(fieldsToExclude);
		try {
			User user = UserLocalServiceUtil.getUser(this.getUserId());
			List<Subcategory> subcategoryList = SubcategoryLocalServiceUtil.getSubcategoriesOfCategory(categoryId);
			for (Subcategory subcat : subcategoryList) {
				JSONObject jSONObject = JSONFactoryUtil.createJSONObject(jSONSerializer.serializeDeep(subcat));
				jsonSubCategoryList.add(jSONObject);
			}
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
			jsonObject.put("error", "unrecognized user");
			jsonSubCategoryList.add(jsonObject);
		}
		return jsonSubCategoryList;
	}
	
	public List<JSONObject> getLevelOfSubcategory(long subcategoryId) {
		List<JSONObject> jsonLevelList = new ArrayList<JSONObject>();
		JSONSerializer jSONSerializer = JSONFactoryUtil.createJSONSerializer();
		jSONSerializer.exclude(fieldsToExclude);
		try {
			User user = UserLocalServiceUtil.getUser(this.getUserId());
			List<Level> levelList = LevelLocalServiceUtil.getLevelsOfSubcategory(subcategoryId);
			for (Level level : levelList) {
				JSONObject jSONObject = JSONFactoryUtil.createJSONObject(jSONSerializer.serializeDeep(level));
				jsonLevelList.add(jSONObject);
			}
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
			jsonObject.put("error", "unrecognized user");
			jsonLevelList.add(jsonObject);
		}
		return jsonLevelList;
	}
	
	
	public List<JSONObject> getTestQuestionOfLevel(long levelId){
		
		List<JSONObject> jsonTestQuestionList = new ArrayList<JSONObject>();
		JSONSerializer jSONSerializer = JSONFactoryUtil.createJSONSerializer();
		jSONSerializer.exclude(fieldsToExclude);
		try {
			User user = UserLocalServiceUtil.getUser(this.getUserId());
			List<TestQuestion> listaTestQuestions = TestQuestionLocalServiceUtil.getTestQuestionsBylevelId(levelId);
			List<Integer> indexs = getRandomQuestionPositions(listaTestQuestions.size());
			for(Integer i : indexs){
				JSONObject jSONObject = JSONFactoryUtil.createJSONObject(jSONSerializer.serializeDeep(listaTestQuestions.get(i)));
				jsonTestQuestionList.add(jSONObject);
				System.out.println(jSONObject);
			}
			
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
			jsonObject.put("error", "unrecognized user");
			jsonTestQuestionList.add(jsonObject);
		}
		return jsonTestQuestionList;
		
	}
	
	@JSONWebService (method="POST")
	public JSONObject sendTestResultOfUser(String jsonResponseStringify){
		JSONObject jSONObject = JSONFactoryUtil.createJSONObject();
		String tokenData = new String();
		List <Badge> badgesPendingToAttachToUser = new LinkedList<Badge>();

		try {
			User user = UserLocalServiceUtil.getUser(this.getUserId());
					
			JSONArray jsonResponse = JSONFactoryUtil.createJSONArray(jsonResponseStringify);
			long testQuestionId = 0;
			String response = new String();
			tokenData = jsonResponse.getJSONObject(0).getString("tokenData");
			boolean isCorrect = false;
			int correctAnswersObtained = 0;
			
			int levelsInSameSession = UserTestQuestionResultLocalServiceUtil.levelsInSameSession(user.getUserId(),tokenData);

			
			for(int i=0; i<jsonResponse.length();i++){
				JSONObject jsonObject = jsonResponse.getJSONObject(i);
				testQuestionId = jsonObject.getLong("id");
				response =  jsonObject.getString("response");
				
				isCorrect = checkIfAnswerIsCorrect(response, testQuestionId);
				UserTestQuestionResult userTestQuestionResult = UserTestQuestionResultLocalServiceUtil.createUserTestQuestionResult(CounterLocalServiceUtil.increment(UserTestQuestionResult.class.getName(), 1));
				
				userTestQuestionResult.setAnswersByUser(response);
				userTestQuestionResult.setCreateDate(Calendar.getInstance().getTime());
				userTestQuestionResult.setQuestionId(testQuestionId);
				userTestQuestionResult.setUserId(user.getUserId());
				userTestQuestionResult.setIsCorrect(isCorrect);
				userTestQuestionResult.setTokenData(tokenData);
				UserTestQuestionResultLocalServiceUtil.updateUserTestQuestionResult(userTestQuestionResult);
				if(isCorrect)correctAnswersObtained++;
			}
						
			TestQuestion lastTestQuestionOfUser = UserTestQuestionResultLocalServiceUtil.getLastTestQuestionOfUser(user.getUserId());
			Subcategory lastSubcategoryOfUser = null;
			if(lastTestQuestionOfUser!=null){
				lastSubcategoryOfUser = SubcategoryLocalServiceUtil.getSubcategory(lastTestQuestionOfUser.getSubcategoryId());
			}
			
			
			TestQuestion question = TestQuestionLocalServiceUtil.getTestQuestion(testQuestionId);

			Subcategory subcategoryActual = SubcategoryLocalServiceUtil.getSubcategory(question.getSubcategoryId());

			int percentageOfCorrect = (100*correctAnswersObtained) /jsonResponse.length();
			
			long prevPoints = 0;

			if(user.getExpandoBridge().getAttribute("userXp")!=null){
				prevPoints = (long)user.getExpandoBridge().getAttribute("userXp");
			}
			
			double bonusVariety = XpUltils.BONUS_VARIETY_NOCHAGE;
			
			if(lastTestQuestionOfUser!=null){
				if(subcategoryActual.getCategoryId() != lastSubcategoryOfUser.getCategoryId()){
					bonusVariety = XpUltils.BONUS_VARIETY_CATEGORY_CHANGE;
				}else{
					if(subcategoryActual.getSubcategoryId()!=lastSubcategoryOfUser.getSubcategoryId()){
						bonusVariety = XpUltils.BONUS_VARIETY_SUBCATEGORY_CHANGE;
					}
				}
			}
			
			Level level = LevelLocalServiceUtil.getLevel(question.getLevelId());
			int levelOfQuestion = Integer.parseInt(level.getLevelName(Locale.getDefault()));
			long newXp = XpUltils.totalNewXpPoints(prevPoints, levelOfQuestion, percentageOfCorrect, bonusVariety, levelsInSameSession);			
			int userLevel = XpUltils.calculateUserLevel(newXp);
						
			jSONObject.put("userXp", newXp);
			jSONObject.put("userLevel", userLevel);

			
			user.getExpandoBridge().setAttribute("userXp", newXp);
			user.getExpandoBridge().setAttribute("userLevel", userLevel);
			
			
			
			/** AQUI EMPIEZO CON LAS COMPROBACIONES PARA SABER SI TIENE BADGES **/
			List<Badge> badgesOfUser = UserBadgeLocalServiceUtil.getBadgesObtainedByUserId(user.getUserId());
			List <Badge> badgesOfCompany = BadgeLocalServiceUtil.getBadgesOfCompany(user.getCompanyId());

			for(Badge badge  : badgesOfCompany){
				if(!badgesOfUser.contains(badge))badgesPendingToAttachToUser.add(badge);
			}
			
			// Tengo que mandar lo acumulado entre niveles.
			int levelBottomLimit=0,levelTopLimit=0;
			if(levelOfQuestion<=3){
				levelBottomLimit = 1;
				levelTopLimit = 3;
			}else if(levelOfQuestion>3&&levelOfQuestion<=10){
				levelBottomLimit = 4;
				levelTopLimit = 10;
			}else if(levelOfQuestion>10&&levelOfQuestion<=16){
				levelBottomLimit = 11;
				levelTopLimit = 16;
			}else if(levelOfQuestion>16&&levelOfQuestion<=20){
				levelBottomLimit = 17;
				levelTopLimit = 20;
			}
			
			
			int accumulatedCorrectAnswerOfUser = UserTestQuestionResultLocalServiceUtil.getAccumulatedCorrectAnswersOfUserBetweenLevels(user.getUserId(), 
					subcategoryActual.getSubcategoryId(), levelBottomLimit, levelTopLimit);
			
			//-----------------------------------------------------------//
			
			System.out.println("Preguntas correctas en este nivel "+level.getLevelName(user.getLocale())+" de la subcategoria "
			+subcategoryActual.getSubcategoryName(user.getLocale())+" cond id "+subcategoryActual.getSubcategoryId()+": "+correctAnswersObtained+" Acumulado "+accumulatedCorrectAnswerOfUser);
			
			for(Badge badgePending: badgesPendingToAttachToUser){
				
				HashMap<String, Object> bind = new HashMap<>();
				bind.put(Definitions.QUANTIFICATOR_CORRECT_ANSWER, accumulatedCorrectAnswerOfUser);
				bind.put(Definitions.PREDICATE_LEVEL, levelOfQuestion);
				bind.put(Definitions.PREDICATE_SUBCATEGORY, (int)subcategoryActual.getSubcategoryId());
				bind.put(Definitions.PREDICATE_CATEGORY, categoryLocalService.getCategory(subcategoryActual.getCategoryId()).getName());
				bind.put("userXp", newXp);
				bind.put("userLevel", userLevel);
				
				RuleExecutor.executor(badgeRuleLocalService.getRulesOfBadge(badgePending.getBadgeId()), bind, badgePending.getBadgeId(), user.getUserId());
			}
			JSONArray badgesJSArray = JSONFactoryUtil.createJSONArray();
			List<Badge> badgesOfUserAux = UserBadgeLocalServiceUtil.getBadgesObtainedByUserId(user.getUserId());
			ThemeDisplay themeDisplay = ServiceContextThreadLocal.getServiceContext().getThemeDisplay();
			for(Badge badge : badgesOfUserAux){
				if(!badgesOfUser.contains(badge)){
					
					badgesJSArray.put(getBadgeJsonInformation(badge,user,themeDisplay ));
				}
			}
			System.out.println(badgesJSArray);
			jSONObject.put("badges", badgesJSArray);
			
		} catch ( PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jSONObject;
	}
	
	@JSONWebService (method="POST")
	public JSONObject reportQuestion(long questionId, String reason){
		
		JSONObject jSONObject = JSONFactoryUtil.createJSONObject();
		try {
			User user = UserLocalServiceUtil.getUser(this.getUserId());
		
		ReportedQuestions reportedQuestions = ReportedQuestionsLocalServiceUtil.createReportedQuestions(CounterLocalServiceUtil.increment(ReportedQuestions.class.getName(), 1));
		reportedQuestions.setCompanyId(user.getCompanyId());
		reportedQuestions.setCreateDate(Calendar.getInstance().getTime());
		reportedQuestions.setUserId(user.getUserId());
		reportedQuestions.setQuestionId(questionId);
		reportedQuestions.setReportContent(reason);
	
		ReportedQuestionsLocalServiceUtil.updateReportedQuestions(reportedQuestions);
		
		jSONObject.put("status", "ok");
		jSONObject.put("msg", "Report properly submited");
		
		} catch ( PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jSONObject;
	}
	
	
	
	private boolean checkIfAnswerIsCorrect (String response, long testQuestionId){
		boolean isCorrect = false;
		try {
			String correctAnswers = TestQuestionLocalServiceUtil.getTestQuestion(testQuestionId).getCorrectAnswers();
			if(correctAnswers.length()== response.length()){
				if(correctAnswers.length()==1){
					isCorrect = correctAnswers.equals(response);
				}else{
	
					List<String> correctResponsesList = Arrays.asList(correctAnswers.split("#"));
					List<String> userResponsesList = Arrays.asList(response.split("#"));

					for(String correctResponse: correctResponsesList){
						if(userResponsesList.contains(correctResponse)){
							isCorrect = true;
						}else{
							isCorrect = false;
							break;
						}
					}

				}
			}
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return isCorrect;
	}
	
	private List<Integer> getRandomQuestionPositions (int sizeOfListAsMaximun){
		int minimum = 0;
		int maximum = sizeOfListAsMaximun-1;
		List <Integer> uniqueList = new LinkedList<Integer>();
		
		while(uniqueList.size()<NUM_QUESTION_TEST){
			Random rand = new Random();
			int randomNum = minimum + rand.nextInt((maximum - minimum) + 1);
			if(!uniqueList.contains(randomNum)){
				uniqueList.add(randomNum);
			}
		}
		
		return uniqueList;
	}
	
	public JSONObject getUserExtraDataInformation(long userId){
		JSONObject obj = JSONFactoryUtil.createJSONObject();
		ThemeDisplay themeDisplay = ServiceContextThreadLocal.getServiceContext().getThemeDisplay();

		User u =null;
		try {
			u = UserLocalServiceUtil.getUser(userId);
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		obj.put("phone", "");
		obj.put("country", "");

		if(u.getPhones().size()>0)obj.put("phone", u.getPhones().get(0).getNumber());
		if(u.getAddresses().size()>0){
			String a3=new String();
			try {
				a3 = CountryServiceUtil.getCountry(u.getAddresses().get(0).getCountryId()).getA3();
			} catch (PortalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			obj.put("country",a3);
		}
		
		// We should give to user his/her list of badges
		
		try {
			List<Badge> badgesOfUser = UserBadgeLocalServiceUtil.getBadgesObtainedByUserId(userId);
			JSONArray badgesJSArray = JSONFactoryUtil.createJSONArray();

			for(Badge badge : badgesOfUser){
				badgesJSArray.put(getBadgeJsonInformation(badge,  u, themeDisplay));
			}
			obj.put("badges", badgesJSArray);

		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		
		return obj;
	}
	
	
	public User updateUserInfo(long userId, String screenName, String firstName, String emailAddress, 
			String phone, int year, int month, int day, String country){
		ServiceContext serviceCntext = ServiceContextThreadLocal.getServiceContext();
		User userToReturn = null;
		try {
			User user = UserLocalServiceUtil.getUser(userId);
			
			if(Validator.isNotNull(screenName))user.setScreenName(screenName);
			
			if(Validator.isNotNull(firstName))user.setFirstName(firstName);
			
			if(Validator.isNotNull(emailAddress))user.setEmailAddress(emailAddress);
						
			if(Validator.isNotNull(phone)){
				List<Phone> phonesOfUser =  user.getPhones();
				if(phonesOfUser.size()!=0){
					//System.out.println("AQUI");
					user.getPhones().get(0).setNumber(phone);
					Phone userPhone = user.getPhones().get(0);
					userPhone.setNumber(phone);
					PhoneLocalServiceUtil.updatePhone(userPhone);
				}else{
					System.out.println("CREO UNO");
					ListType type = ListTypeLocalServiceUtil.getListTypes(ListTypeConstants.CONTACT_PHONE).get(0);
					
					PhoneLocalServiceUtil.addPhone( userId, Contact.class.getName() , 
							user.getContactId(), phone, "", type.getListTypeId(), true, serviceCntext);
				}
				
			}
			
			Calendar cal = Calendar.getInstance();
			cal.set(year, month, day);
			user.getContact().setBirthday(cal.getTime());
			if(Validator.isNotNull(country)){
				//System.out.println(country);
				ListType type = ListTypeLocalServiceUtil.getListTypes(ListTypeConstants.CONTACT_ADDRESS).get(0);
				//System.out.println(type.getName());
				//System.out.println(type.getPrimaryKey());
				//System.out.println(type.getListTypeId());

				Country countryObj = CountryServiceUtil.getCountryByA3(country);
				//System.out.println(countryObj.getCountryId());
				//System.out.println(countryObj.getName());
				List <Address> userAddress = user.getAddresses();
				if(userAddress.size()!=0){
					//System.out.println("1");
					if(countryObj!=null) user.getAddresses().get(0).setCountryId(countryObj.getCountryId());
				}else{
					
					if(countryObj!=null) {
						//System.out.println("2");
						AddressLocalServiceUtil.addAddress(userId, Contact.class.getName(), 
								user.getContactId(), "street1", "", "", "city", "zip", 0L, countryObj.getCountryId(), type.getListTypeId(), true, true, serviceCntext);
					}

					
					
				}
				
			}
			
			
			userToReturn = UserLocalServiceUtil.updateUser(user);
			
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return userToReturn;
	}
	
	private JSONObject getBadgeJsonInformation(Badge badge, User user, ThemeDisplay themeDisplay) throws Exception{
		JSONObject obj = JSONFactoryUtil.createJSONObject();
		obj.put("id", badge.getBadgeId());
		obj.put("title", badge.getBadgeTitle(user.getLocale()));
		obj.put("description", badge.getBadgeDescription(user.getLocale()));
		obj.put("date", UserBadgeLocalServiceUtil.getUserBadge(new UserBadgePK(user.getUserId(), badge.getBadgeId())).getCreateDate().getTime());
		
		FileEntry image_ = DLAppLocalServiceUtil.getFileEntry(badge.getBadgeIcon());
		
		String url =DLUtil.getImagePreviewURL(image_, image_.getFileVersion(), themeDisplay);
		
		Subcategory subcat = SubcategoryLocalServiceUtil.getSubcategory(badge.getObjPK());
		
		obj.put("subCategoryName", subcat.getSubcategoryName(user.getLocale()));
		obj.put("categoryName", CategoryLocalServiceUtil.getCategory(subcat.getCategoryId()).getName(user.getLocale()));
		
		System.out.println("img URL "+url);
		obj.put("img", url);
		
		return obj;
	}
	
}