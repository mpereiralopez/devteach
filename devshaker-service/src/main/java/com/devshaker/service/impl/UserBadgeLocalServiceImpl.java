/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.impl;

import aQute.bnd.annotation.ProviderType;

import java.util.LinkedList;
import java.util.List;

import com.devshaker.model.Badge;
import com.devshaker.model.UserBadge;
import com.devshaker.service.BadgeLocalServiceUtil;
import com.devshaker.service.base.UserBadgeLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.PortalException;

/**
 * The implementation of the user badge local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.devshaker.service.UserBadgeLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserBadgeLocalServiceBaseImpl
 * @see com.devshaker.service.UserBadgeLocalServiceUtil
 */
@ProviderType
public class UserBadgeLocalServiceImpl extends UserBadgeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.devshaker.service.UserBadgeLocalServiceUtil} to access the user badge local service.
	 */
	
	public List<Badge> getBadgesObtainedByUserId(long userId) throws PortalException{
		List<UserBadge> lista = userBadgePersistence.findByUserId(userId);
		List <Badge> listaBadgesAttached = new LinkedList<Badge>();
		
		for(UserBadge ub : lista){
			listaBadgesAttached.add(BadgeLocalServiceUtil.getBadge(ub.getBadgeId()));
		}
		
		return listaBadgesAttached;
	}
}