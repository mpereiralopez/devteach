/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.impl;

import aQute.bnd.annotation.ProviderType;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.portlet.PortletRequest;

import com.devshaker.service.base.DevUtilsApiLocalServiceBaseImpl;
import com.gamification.api.util.Definitions;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.StringPool;

/**
 * The implementation of the dev utils api local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.devshaker.service.DevUtilsApiLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DevUtilsApiLocalServiceBaseImpl
 * @see com.devshaker.service.DevUtilsApiLocalServiceUtil
 */
@ProviderType
public class DevUtilsApiLocalServiceImpl extends DevUtilsApiLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.devshaker.service.DevUtilsApiLocalServiceUtil} to access the dev utils api local service.
	 */
	
	private static String IMAGEGALLERY_MAINFOLDER = "icons";
	private static String IMAGEGALLERY_PORTLETFOLDER = "category";
	private static String IMAGEGALLERY_MAINFOLDER_DESCRIPTION = "Category Image Uploads";
	private static String IMAGEGALLERY_PORTLETFOLDER_DESCRIPTION = StringPool.BLANK;

	
	public boolean validateFileSize(File file) {
		boolean valid = true;
		// Comprobar que el tamano del fichero no supere los 5mb
		long size = 5 * 1024 * 1024;
		if (file.length() > size) {
			valid = false;
		}
		return valid;
	}

	public long createIGFolders(PortletRequest request, long userId, long repositoryId)
			throws PortalException, SystemException {
		// Variables for folder ids
		Long igMainFolderId = 0L;
		Long igPortletFolderId = 0L;
		Long igRecordFolderId = 0L;
		// Search for folders
		boolean igMainFolderFound = false;
		boolean igPortletFolderFound = false;
		try {
			// Get the main folder
			Folder igMainFolder = DLAppLocalServiceUtil.getFolder(repositoryId,
					DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, IMAGEGALLERY_MAINFOLDER);
			igMainFolderId = igMainFolder.getFolderId();
			igMainFolderFound = true;
			// Get the portlet folder
			DLFolder igPortletFolder = DLFolderLocalServiceUtil.getFolder(repositoryId, igMainFolderId,
					IMAGEGALLERY_PORTLETFOLDER);
			igPortletFolderId = igPortletFolder.getFolderId();
			igPortletFolderFound = true;
		} catch (Exception ex) {
		}

		ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFolder.class.getName(), request);
		// Damos permisos al archivo para usuarios de comunidad.
		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(true);
		// Create main folder if not exist
		if (!igMainFolderFound) {
			Folder newImageMainFolder = DLAppLocalServiceUtil.addFolder(userId, repositoryId, 0,
					IMAGEGALLERY_MAINFOLDER, IMAGEGALLERY_MAINFOLDER_DESCRIPTION, serviceContext);
			igMainFolderId = newImageMainFolder.getFolderId();
			igMainFolderFound = true;
		}
		// Create portlet folder if not exist
		if (igMainFolderFound && !igPortletFolderFound) {
			Folder newImagePortletFolder = DLAppLocalServiceUtil.addFolder(userId, repositoryId, igMainFolderId,
					IMAGEGALLERY_PORTLETFOLDER, IMAGEGALLERY_PORTLETFOLDER_DESCRIPTION, serviceContext);
			igPortletFolderFound = true;
			igPortletFolderId = newImagePortletFolder.getFolderId();
		}
		// Create this record folder
		if (igPortletFolderFound) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			Date date = new Date();
			String igRecordFolderName = dateFormat.format(date) + StringPool.UNDERLINE + userId;
			Folder newImageRecordFolder = DLAppLocalServiceUtil.addFolder(userId, repositoryId, igPortletFolderId,
					igRecordFolderName, igRecordFolderName, serviceContext);
			igRecordFolderId = newImageRecordFolder.getFolderId();
		}
		return igRecordFolderId;
	}
	
	public  String [] getMetricArray(){
		return new String[]{Definitions.METRIC_SCORE};
	}
	
	public  String [] getConditionsArray(){
		return new String[]{Definitions.CONDITION_AND,Definitions.CONDITION_OR,
				Definitions.CONDITION_EQUAL,Definitions.CONDITION_GT,Definitions.CONDITION_LT,
				Definitions.CONDITION_LET ,Definitions.CONDITION_GET};
	}
	
	public  String [] getQuantificatorArray(){
		return new String[]{Definitions.QUANTIFICATOR_CORRECT_ANSWER,Definitions.QUANTIFICATOR_XP};
	}
	
	public  String [] getPredicateArray(){
		return new String[]{Definitions.PREDICATE_LEVEL,Definitions.PREDICATE_SUBCATEGORY,Definitions.PREDICATE_CATEGORY};
	}
	
}