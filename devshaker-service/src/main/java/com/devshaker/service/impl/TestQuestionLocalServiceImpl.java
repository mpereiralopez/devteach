/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.impl;

import aQute.bnd.annotation.ProviderType;

import java.util.List;

import com.devshaker.model.TestQuestion;
import com.devshaker.service.base.TestQuestionLocalServiceBaseImpl;

/**
 * The implementation of the test question local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.devshaker.service.TestQuestionLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TestQuestionLocalServiceBaseImpl
 * @see com.devshaker.service.TestQuestionLocalServiceUtil
 */
@ProviderType
public class TestQuestionLocalServiceImpl
	extends TestQuestionLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.devshaker.service.TestQuestionLocalServiceUtil} to access the test question local service.
	 */
	
	public List<TestQuestion> getTestQuestionsByCompanyId(long companyId){
		return testQuestionPersistence.findByCompanyId(companyId);
	}
	
	public List <TestQuestion> getTestQuestionsByCategoryId(long categoryId){
		return testQuestionPersistence.findByCategoryId(categoryId);
	}
	
	public List <TestQuestion> getTestQuestionsBySubcategoryId(long subcategoryId){
		return testQuestionPersistence.findBySubcategoryId(subcategoryId);
	}
	
	public List <TestQuestion> getTestQuestionsBylevelId(long levelId){
		return testQuestionPersistence.findBylevelId(levelId);
	}
}