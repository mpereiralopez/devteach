/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.impl;

import aQute.bnd.annotation.ProviderType;

import java.util.ArrayList;
import java.util.List;

import com.devshaker.exception.NoSuchUserTestQuestionResultException;
import com.devshaker.model.Level;
import com.devshaker.model.TestQuestion;
import com.devshaker.model.UserTestQuestionResult;
import com.devshaker.service.LevelLocalServiceUtil;
import com.devshaker.service.TestQuestionLocalServiceUtil;
import com.devshaker.service.UserTestQuestionResultLocalServiceUtil;
import com.devshaker.service.base.UserTestQuestionResultLocalServiceBaseImpl;
import com.liferay.document.library.kernel.exception.DirectoryNameException;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.jsonwebservice.JSONWebServiceMode;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;

/**
 * The implementation of the user test question result local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.devshaker.service.UserTestQuestionResultLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserTestQuestionResultLocalServiceBaseImpl
 * @see com.devshaker.service.UserTestQuestionResultLocalServiceUtil
 */
@ProviderType
public class UserTestQuestionResultLocalServiceImpl
	extends UserTestQuestionResultLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.devshaker.service.UserTestQuestionResultLocalServiceUtil} to access the user test question result local service.
	 */
	
	public int getQuestionTimesFailed(long questionId){
		return userTestQuestionResultPersistence.countByQuestionIdAndStatus(questionId, false);

		
	}
	
	public int getQuestionTimesPassed(long questionId){
		return userTestQuestionResultPersistence.countByQuestionIdAndStatus(questionId, true);
	}
	
	public int levelsInSameSession (long userId,String tokenData){
		int levelsInARow = 1;
		List<UserTestQuestionResult> list = userTestQuestionResultPersistence.findByUserIdAndTokenDate(userId, tokenData);
		if(list != null && list.size()>0) levelsInARow = list.size();
			
		return levelsInARow;
	}
	
	public TestQuestion getLastTestQuestionOfUser(long userId){
		TestQuestion testQuestion = null;
		
		/*DynamicQuery consulta = DynamicQueryFactoryUtil.forClass(UserTestQuestionResult.class)
				.add(PropertyFactoryUtil.forName("userId").eq(userId))
				.addOrder(PropertyFactoryUtil.forName("createDate").desc());*/
		
		UserTestQuestionResult result = null;
		try {
			result = userTestQuestionResultPersistence.findByUserId_Last(userId, null);
			System.out.println(result);
			testQuestion = TestQuestionLocalServiceUtil.getTestQuestion(result.getQuestionId());

		} catch (NoSuchUserTestQuestionResultException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			//Es la primera vez que el usuario postea una solución
			testQuestion = null;
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return testQuestion;
	}
	
	public int getAccumulatedCorrectAnswersOfUserBetweenLevels(long userId, long subcategoryId ,int levelBottomLimit, int levelTopLimit) throws PortalException{
		User u = UserLocalServiceUtil.getUser(userId);
		List<UserTestQuestionResult> userTestQuestionCorrect = userTestQuestionResultPersistence.findByUserIdAndStatus(userId, true);
		TestQuestion q = null;
		int counter = 0;
		for(UserTestQuestionResult testResult : userTestQuestionCorrect){
			q = TestQuestionLocalServiceUtil.getTestQuestion(testResult.getQuestionId());
			if(q.getSubcategoryId()==subcategoryId){
				Level l = LevelLocalServiceUtil.getLevel(q.getLevelId());
				int levelNameInt = Integer.parseInt(l.getLevelName(u.getLocale()));
				if(levelNameInt>= levelBottomLimit && levelNameInt<=levelTopLimit){
					counter++;
				}
			}
		}
		
		return counter;
	}
	
	
	public List<UserTestQuestionResult> getTestQuestionResults(){
		return userTestQuestionResultPersistence.findAll();
	}
	
	public List<UserTestQuestionResult> getTestQuestionResultsOfCategory(long categoryId){
		List<TestQuestion> listOfCategoryQuestions = testQuestionLocalService.getTestQuestionsByCategoryId(categoryId);
		List<UserTestQuestionResult> all = userTestQuestionResultPersistence.findAll();
		
		List<UserTestQuestionResult> filtered = new ArrayList<UserTestQuestionResult> ();
		
		for(UserTestQuestionResult res: all){
			for(TestQuestion q: listOfCategoryQuestions){
				if(q.getQuestionId() == res.getQuestionId()){
					filtered.add(res);
					break;
				}
			}
		}
		
		return filtered;
	}
	
	public List<UserTestQuestionResult> getTestQuestionResultsOfSubcategory(long subcategoryId){
		List<TestQuestion> listOfCategoryQuestions = testQuestionLocalService.getTestQuestionsBySubcategoryId(subcategoryId);
		List<UserTestQuestionResult> all = userTestQuestionResultPersistence.findAll();
		
		List<UserTestQuestionResult> filtered = new ArrayList<UserTestQuestionResult> ();
		
		for(UserTestQuestionResult res: all){
			for(TestQuestion q: listOfCategoryQuestions){
				if(q.getQuestionId() == res.getQuestionId()){
					filtered.add(res);
					break;
				}
			}
		}
		
		return filtered;
	}
	
	public List<UserTestQuestionResult> getTestQuestionResultsOfLevel(long levelId){
		List<TestQuestion> listOfCategoryQuestions = testQuestionLocalService.getTestQuestionsBylevelId(levelId);
		List<UserTestQuestionResult> all = userTestQuestionResultPersistence.findAll();
		
		List<UserTestQuestionResult> filtered = new ArrayList<UserTestQuestionResult> ();
		
		for(UserTestQuestionResult res: all){
			for(TestQuestion q: listOfCategoryQuestions){
				if(q.getQuestionId() == res.getQuestionId()){
					filtered.add(res);
					break;
				}
			}
		}
		
		return filtered;
	}
}