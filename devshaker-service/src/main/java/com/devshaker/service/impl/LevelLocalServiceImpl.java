/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.impl;

import aQute.bnd.annotation.ProviderType;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import com.devshaker.model.Level;
import com.devshaker.service.base.LevelLocalServiceBaseImpl;

/**
 * The implementation of the level local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.devshaker.service.LevelLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LevelLocalServiceBaseImpl
 * @see com.devshaker.service.LevelLocalServiceUtil
 */
@ProviderType
public class LevelLocalServiceImpl extends LevelLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.devshaker.service.LevelLocalServiceUtil} to access the level local service.
	 */
	
	public List<Level> getLevelsOfSubcategory(long subcategoryId){
		return levelPersistence.findBySubcategoryId(subcategoryId);
	}
	
	
	public Level getLevelByNameAndSubcategoryId (String name, Locale locale, long subcategoryId){
		Level level = null;
		List <Level> levelOfCompany = levelPersistence.findBySubcategoryId(subcategoryId);
		Iterator <Level> it = levelOfCompany.iterator();
		while (it.hasNext()){
			Level levelAux = it.next();
			if(levelAux.getLevelName(locale).equals(name)){
				level=levelAux;
				break;
			}
		}
		
		return level;
	}
}