/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.impl;

import aQute.bnd.annotation.ProviderType;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import com.devshaker.exception.NoSuchCategoryException;
import com.devshaker.model.Category;
import com.devshaker.service.base.CategoryLocalServiceBaseImpl;

/**
 * The implementation of the category local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.devshaker.service.CategoryLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CategoryLocalServiceBaseImpl
 * @see com.devshaker.service.CategoryLocalServiceUtil
 */
@ProviderType
public class CategoryLocalServiceImpl extends CategoryLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.devshaker.service.CategoryLocalServiceUtil} to access the category local service.
	 */
	
	public List<Category> getCategoriesOfCompany(long companyId){
		return categoryPersistence.findByCompanyId(companyId);
	}
	
	public Category getCategoryByName (String name, Locale locale, long companyId){
		Category cat = null;
		List <Category> categoryOfCompany = categoryPersistence.findByCompanyId(companyId);
		Iterator <Category> it = categoryOfCompany.iterator();
		while (it.hasNext()){
			Category catAux = it.next();
			if(catAux.getName(locale).equals(name)){
				cat=catAux;
				break;
			}
		}
		
		return cat;
	}
	

}