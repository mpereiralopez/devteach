/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.impl;

import aQute.bnd.annotation.ProviderType;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import com.devshaker.model.Subcategory;
import com.devshaker.service.base.SubcategoryLocalServiceBaseImpl;

/**
 * The implementation of the subcategory local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.devshaker.service.SubcategoryLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SubcategoryLocalServiceBaseImpl
 * @see com.devshaker.service.SubcategoryLocalServiceUtil
 */
@ProviderType
public class SubcategoryLocalServiceImpl extends SubcategoryLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.devshaker.service.SubcategoryLocalServiceUtil} to access the subcategory local service.
	 */
	
	public List<Subcategory> getSubcategoriesOfCompany(long companyId){
		return subcategoryPersistence.findByCompanyId(companyId);
	}
	
	public List<Subcategory> getSubcategoriesOfCategory(long categoryId){
		return subcategoryPersistence.findByCategoryId(categoryId);
	}
	
	
	public Subcategory getSubcategoryByNameAndCategoryId (String name, Locale locale, long categoryId){
		Subcategory subcat = null;
		List <Subcategory> subcategoryOfCompany = subcategoryPersistence.findByCategoryId(categoryId);
		Iterator <Subcategory> it = subcategoryOfCompany.iterator();
		while (it.hasNext()){
			Subcategory subcatAux = it.next();
			if(subcatAux.getSubcategoryName(locale).equals(name)){
				subcat=subcatAux;
				break;
			}
		}
		
		return subcat;
	}
}