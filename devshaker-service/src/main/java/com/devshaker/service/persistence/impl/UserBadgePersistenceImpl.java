/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.exception.NoSuchUserBadgeException;

import com.devshaker.model.UserBadge;
import com.devshaker.model.impl.UserBadgeImpl;
import com.devshaker.model.impl.UserBadgeModelImpl;

import com.devshaker.service.persistence.UserBadgePK;
import com.devshaker.service.persistence.UserBadgePersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the user badge service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserBadgePersistence
 * @see com.devshaker.service.persistence.UserBadgeUtil
 * @generated
 */
@ProviderType
public class UserBadgePersistenceImpl extends BasePersistenceImpl<UserBadge>
	implements UserBadgePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UserBadgeUtil} to access the user badge persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UserBadgeImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
			UserBadgeModelImpl.FINDER_CACHE_ENABLED, UserBadgeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
			UserBadgeModelImpl.FINDER_CACHE_ENABLED, UserBadgeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
			UserBadgeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
			UserBadgeModelImpl.FINDER_CACHE_ENABLED, UserBadgeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
			UserBadgeModelImpl.FINDER_CACHE_ENABLED, UserBadgeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserId",
			new String[] { Long.class.getName() },
			UserBadgeModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
			UserBadgeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the user badges where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching user badges
	 */
	@Override
	public List<UserBadge> findByUserId(long userId) {
		return findByUserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user badges where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user badges
	 * @param end the upper bound of the range of user badges (not inclusive)
	 * @return the range of matching user badges
	 */
	@Override
	public List<UserBadge> findByUserId(long userId, int start, int end) {
		return findByUserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user badges where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user badges
	 * @param end the upper bound of the range of user badges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user badges
	 */
	@Override
	public List<UserBadge> findByUserId(long userId, int start, int end,
		OrderByComparator<UserBadge> orderByComparator) {
		return findByUserId(userId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user badges where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user badges
	 * @param end the upper bound of the range of user badges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching user badges
	 */
	@Override
	public List<UserBadge> findByUserId(long userId, int start, int end,
		OrderByComparator<UserBadge> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<UserBadge> list = null;

		if (retrieveFromCache) {
			list = (List<UserBadge>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (UserBadge userBadge : list) {
					if ((userId != userBadge.getUserId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERBADGE_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserBadgeModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<UserBadge>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserBadge>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user badge in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user badge
	 * @throws NoSuchUserBadgeException if a matching user badge could not be found
	 */
	@Override
	public UserBadge findByUserId_First(long userId,
		OrderByComparator<UserBadge> orderByComparator)
		throws NoSuchUserBadgeException {
		UserBadge userBadge = fetchByUserId_First(userId, orderByComparator);

		if (userBadge != null) {
			return userBadge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserBadgeException(msg.toString());
	}

	/**
	 * Returns the first user badge in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user badge, or <code>null</code> if a matching user badge could not be found
	 */
	@Override
	public UserBadge fetchByUserId_First(long userId,
		OrderByComparator<UserBadge> orderByComparator) {
		List<UserBadge> list = findByUserId(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user badge in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user badge
	 * @throws NoSuchUserBadgeException if a matching user badge could not be found
	 */
	@Override
	public UserBadge findByUserId_Last(long userId,
		OrderByComparator<UserBadge> orderByComparator)
		throws NoSuchUserBadgeException {
		UserBadge userBadge = fetchByUserId_Last(userId, orderByComparator);

		if (userBadge != null) {
			return userBadge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserBadgeException(msg.toString());
	}

	/**
	 * Returns the last user badge in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user badge, or <code>null</code> if a matching user badge could not be found
	 */
	@Override
	public UserBadge fetchByUserId_Last(long userId,
		OrderByComparator<UserBadge> orderByComparator) {
		int count = countByUserId(userId);

		if (count == 0) {
			return null;
		}

		List<UserBadge> list = findByUserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user badges before and after the current user badge in the ordered set where userId = &#63;.
	 *
	 * @param userBadgePK the primary key of the current user badge
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user badge
	 * @throws NoSuchUserBadgeException if a user badge with the primary key could not be found
	 */
	@Override
	public UserBadge[] findByUserId_PrevAndNext(UserBadgePK userBadgePK,
		long userId, OrderByComparator<UserBadge> orderByComparator)
		throws NoSuchUserBadgeException {
		UserBadge userBadge = findByPrimaryKey(userBadgePK);

		Session session = null;

		try {
			session = openSession();

			UserBadge[] array = new UserBadgeImpl[3];

			array[0] = getByUserId_PrevAndNext(session, userBadge, userId,
					orderByComparator, true);

			array[1] = userBadge;

			array[2] = getByUserId_PrevAndNext(session, userBadge, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserBadge getByUserId_PrevAndNext(Session session,
		UserBadge userBadge, long userId,
		OrderByComparator<UserBadge> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERBADGE_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserBadgeModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userBadge);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserBadge> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user badges where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	@Override
	public void removeByUserId(long userId) {
		for (UserBadge userBadge : findByUserId(userId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(userBadge);
		}
	}

	/**
	 * Returns the number of user badges where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching user badges
	 */
	@Override
	public int countByUserId(long userId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERBADGE_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "userBadge.id.userId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BADGEID = new FinderPath(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
			UserBadgeModelImpl.FINDER_CACHE_ENABLED, UserBadgeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBadgeId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BADGEID =
		new FinderPath(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
			UserBadgeModelImpl.FINDER_CACHE_ENABLED, UserBadgeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBadgeId",
			new String[] { Long.class.getName() },
			UserBadgeModelImpl.BADGEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BADGEID = new FinderPath(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
			UserBadgeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBadgeId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the user badges where badgeId = &#63;.
	 *
	 * @param badgeId the badge ID
	 * @return the matching user badges
	 */
	@Override
	public List<UserBadge> findByBadgeId(long badgeId) {
		return findByBadgeId(badgeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user badges where badgeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param badgeId the badge ID
	 * @param start the lower bound of the range of user badges
	 * @param end the upper bound of the range of user badges (not inclusive)
	 * @return the range of matching user badges
	 */
	@Override
	public List<UserBadge> findByBadgeId(long badgeId, int start, int end) {
		return findByBadgeId(badgeId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user badges where badgeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param badgeId the badge ID
	 * @param start the lower bound of the range of user badges
	 * @param end the upper bound of the range of user badges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user badges
	 */
	@Override
	public List<UserBadge> findByBadgeId(long badgeId, int start, int end,
		OrderByComparator<UserBadge> orderByComparator) {
		return findByBadgeId(badgeId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user badges where badgeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param badgeId the badge ID
	 * @param start the lower bound of the range of user badges
	 * @param end the upper bound of the range of user badges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching user badges
	 */
	@Override
	public List<UserBadge> findByBadgeId(long badgeId, int start, int end,
		OrderByComparator<UserBadge> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BADGEID;
			finderArgs = new Object[] { badgeId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BADGEID;
			finderArgs = new Object[] { badgeId, start, end, orderByComparator };
		}

		List<UserBadge> list = null;

		if (retrieveFromCache) {
			list = (List<UserBadge>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (UserBadge userBadge : list) {
					if ((badgeId != userBadge.getBadgeId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERBADGE_WHERE);

			query.append(_FINDER_COLUMN_BADGEID_BADGEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserBadgeModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(badgeId);

				if (!pagination) {
					list = (List<UserBadge>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserBadge>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user badge in the ordered set where badgeId = &#63;.
	 *
	 * @param badgeId the badge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user badge
	 * @throws NoSuchUserBadgeException if a matching user badge could not be found
	 */
	@Override
	public UserBadge findByBadgeId_First(long badgeId,
		OrderByComparator<UserBadge> orderByComparator)
		throws NoSuchUserBadgeException {
		UserBadge userBadge = fetchByBadgeId_First(badgeId, orderByComparator);

		if (userBadge != null) {
			return userBadge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("badgeId=");
		msg.append(badgeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserBadgeException(msg.toString());
	}

	/**
	 * Returns the first user badge in the ordered set where badgeId = &#63;.
	 *
	 * @param badgeId the badge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user badge, or <code>null</code> if a matching user badge could not be found
	 */
	@Override
	public UserBadge fetchByBadgeId_First(long badgeId,
		OrderByComparator<UserBadge> orderByComparator) {
		List<UserBadge> list = findByBadgeId(badgeId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user badge in the ordered set where badgeId = &#63;.
	 *
	 * @param badgeId the badge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user badge
	 * @throws NoSuchUserBadgeException if a matching user badge could not be found
	 */
	@Override
	public UserBadge findByBadgeId_Last(long badgeId,
		OrderByComparator<UserBadge> orderByComparator)
		throws NoSuchUserBadgeException {
		UserBadge userBadge = fetchByBadgeId_Last(badgeId, orderByComparator);

		if (userBadge != null) {
			return userBadge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("badgeId=");
		msg.append(badgeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserBadgeException(msg.toString());
	}

	/**
	 * Returns the last user badge in the ordered set where badgeId = &#63;.
	 *
	 * @param badgeId the badge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user badge, or <code>null</code> if a matching user badge could not be found
	 */
	@Override
	public UserBadge fetchByBadgeId_Last(long badgeId,
		OrderByComparator<UserBadge> orderByComparator) {
		int count = countByBadgeId(badgeId);

		if (count == 0) {
			return null;
		}

		List<UserBadge> list = findByBadgeId(badgeId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user badges before and after the current user badge in the ordered set where badgeId = &#63;.
	 *
	 * @param userBadgePK the primary key of the current user badge
	 * @param badgeId the badge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user badge
	 * @throws NoSuchUserBadgeException if a user badge with the primary key could not be found
	 */
	@Override
	public UserBadge[] findByBadgeId_PrevAndNext(UserBadgePK userBadgePK,
		long badgeId, OrderByComparator<UserBadge> orderByComparator)
		throws NoSuchUserBadgeException {
		UserBadge userBadge = findByPrimaryKey(userBadgePK);

		Session session = null;

		try {
			session = openSession();

			UserBadge[] array = new UserBadgeImpl[3];

			array[0] = getByBadgeId_PrevAndNext(session, userBadge, badgeId,
					orderByComparator, true);

			array[1] = userBadge;

			array[2] = getByBadgeId_PrevAndNext(session, userBadge, badgeId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserBadge getByBadgeId_PrevAndNext(Session session,
		UserBadge userBadge, long badgeId,
		OrderByComparator<UserBadge> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERBADGE_WHERE);

		query.append(_FINDER_COLUMN_BADGEID_BADGEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserBadgeModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(badgeId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userBadge);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserBadge> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user badges where badgeId = &#63; from the database.
	 *
	 * @param badgeId the badge ID
	 */
	@Override
	public void removeByBadgeId(long badgeId) {
		for (UserBadge userBadge : findByBadgeId(badgeId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(userBadge);
		}
	}

	/**
	 * Returns the number of user badges where badgeId = &#63;.
	 *
	 * @param badgeId the badge ID
	 * @return the number of matching user badges
	 */
	@Override
	public int countByBadgeId(long badgeId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_BADGEID;

		Object[] finderArgs = new Object[] { badgeId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERBADGE_WHERE);

			query.append(_FINDER_COLUMN_BADGEID_BADGEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(badgeId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_BADGEID_BADGEID_2 = "userBadge.id.badgeId = ?";

	public UserBadgePersistenceImpl() {
		setModelClass(UserBadge.class);
	}

	/**
	 * Caches the user badge in the entity cache if it is enabled.
	 *
	 * @param userBadge the user badge
	 */
	@Override
	public void cacheResult(UserBadge userBadge) {
		entityCache.putResult(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
			UserBadgeImpl.class, userBadge.getPrimaryKey(), userBadge);

		userBadge.resetOriginalValues();
	}

	/**
	 * Caches the user badges in the entity cache if it is enabled.
	 *
	 * @param userBadges the user badges
	 */
	@Override
	public void cacheResult(List<UserBadge> userBadges) {
		for (UserBadge userBadge : userBadges) {
			if (entityCache.getResult(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
						UserBadgeImpl.class, userBadge.getPrimaryKey()) == null) {
				cacheResult(userBadge);
			}
			else {
				userBadge.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all user badges.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(UserBadgeImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the user badge.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UserBadge userBadge) {
		entityCache.removeResult(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
			UserBadgeImpl.class, userBadge.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<UserBadge> userBadges) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UserBadge userBadge : userBadges) {
			entityCache.removeResult(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
				UserBadgeImpl.class, userBadge.getPrimaryKey());
		}
	}

	/**
	 * Creates a new user badge with the primary key. Does not add the user badge to the database.
	 *
	 * @param userBadgePK the primary key for the new user badge
	 * @return the new user badge
	 */
	@Override
	public UserBadge create(UserBadgePK userBadgePK) {
		UserBadge userBadge = new UserBadgeImpl();

		userBadge.setNew(true);
		userBadge.setPrimaryKey(userBadgePK);

		userBadge.setCompanyId(companyProvider.getCompanyId());

		return userBadge;
	}

	/**
	 * Removes the user badge with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userBadgePK the primary key of the user badge
	 * @return the user badge that was removed
	 * @throws NoSuchUserBadgeException if a user badge with the primary key could not be found
	 */
	@Override
	public UserBadge remove(UserBadgePK userBadgePK)
		throws NoSuchUserBadgeException {
		return remove((Serializable)userBadgePK);
	}

	/**
	 * Removes the user badge with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the user badge
	 * @return the user badge that was removed
	 * @throws NoSuchUserBadgeException if a user badge with the primary key could not be found
	 */
	@Override
	public UserBadge remove(Serializable primaryKey)
		throws NoSuchUserBadgeException {
		Session session = null;

		try {
			session = openSession();

			UserBadge userBadge = (UserBadge)session.get(UserBadgeImpl.class,
					primaryKey);

			if (userBadge == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUserBadgeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(userBadge);
		}
		catch (NoSuchUserBadgeException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UserBadge removeImpl(UserBadge userBadge) {
		userBadge = toUnwrappedModel(userBadge);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(userBadge)) {
				userBadge = (UserBadge)session.get(UserBadgeImpl.class,
						userBadge.getPrimaryKeyObj());
			}

			if (userBadge != null) {
				session.delete(userBadge);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (userBadge != null) {
			clearCache(userBadge);
		}

		return userBadge;
	}

	@Override
	public UserBadge updateImpl(UserBadge userBadge) {
		userBadge = toUnwrappedModel(userBadge);

		boolean isNew = userBadge.isNew();

		UserBadgeModelImpl userBadgeModelImpl = (UserBadgeModelImpl)userBadge;

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (userBadge.getCreateDate() == null)) {
			if (serviceContext == null) {
				userBadge.setCreateDate(now);
			}
			else {
				userBadge.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!userBadgeModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				userBadge.setModifiedDate(now);
			}
			else {
				userBadge.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (userBadge.isNew()) {
				session.save(userBadge);

				userBadge.setNew(false);
			}
			else {
				userBadge = (UserBadge)session.merge(userBadge);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !UserBadgeModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((userBadgeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userBadgeModelImpl.getOriginalUserId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { userBadgeModelImpl.getUserId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}

			if ((userBadgeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BADGEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userBadgeModelImpl.getOriginalBadgeId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_BADGEID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BADGEID,
					args);

				args = new Object[] { userBadgeModelImpl.getBadgeId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_BADGEID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BADGEID,
					args);
			}
		}

		entityCache.putResult(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
			UserBadgeImpl.class, userBadge.getPrimaryKey(), userBadge, false);

		userBadge.resetOriginalValues();

		return userBadge;
	}

	protected UserBadge toUnwrappedModel(UserBadge userBadge) {
		if (userBadge instanceof UserBadgeImpl) {
			return userBadge;
		}

		UserBadgeImpl userBadgeImpl = new UserBadgeImpl();

		userBadgeImpl.setNew(userBadge.isNew());
		userBadgeImpl.setPrimaryKey(userBadge.getPrimaryKey());

		userBadgeImpl.setUserId(userBadge.getUserId());
		userBadgeImpl.setBadgeId(userBadge.getBadgeId());
		userBadgeImpl.setCompanyId(userBadge.getCompanyId());
		userBadgeImpl.setCreateDate(userBadge.getCreateDate());
		userBadgeImpl.setModifiedDate(userBadge.getModifiedDate());

		return userBadgeImpl;
	}

	/**
	 * Returns the user badge with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the user badge
	 * @return the user badge
	 * @throws NoSuchUserBadgeException if a user badge with the primary key could not be found
	 */
	@Override
	public UserBadge findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUserBadgeException {
		UserBadge userBadge = fetchByPrimaryKey(primaryKey);

		if (userBadge == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUserBadgeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return userBadge;
	}

	/**
	 * Returns the user badge with the primary key or throws a {@link NoSuchUserBadgeException} if it could not be found.
	 *
	 * @param userBadgePK the primary key of the user badge
	 * @return the user badge
	 * @throws NoSuchUserBadgeException if a user badge with the primary key could not be found
	 */
	@Override
	public UserBadge findByPrimaryKey(UserBadgePK userBadgePK)
		throws NoSuchUserBadgeException {
		return findByPrimaryKey((Serializable)userBadgePK);
	}

	/**
	 * Returns the user badge with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the user badge
	 * @return the user badge, or <code>null</code> if a user badge with the primary key could not be found
	 */
	@Override
	public UserBadge fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
				UserBadgeImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		UserBadge userBadge = (UserBadge)serializable;

		if (userBadge == null) {
			Session session = null;

			try {
				session = openSession();

				userBadge = (UserBadge)session.get(UserBadgeImpl.class,
						primaryKey);

				if (userBadge != null) {
					cacheResult(userBadge);
				}
				else {
					entityCache.putResult(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
						UserBadgeImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(UserBadgeModelImpl.ENTITY_CACHE_ENABLED,
					UserBadgeImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return userBadge;
	}

	/**
	 * Returns the user badge with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userBadgePK the primary key of the user badge
	 * @return the user badge, or <code>null</code> if a user badge with the primary key could not be found
	 */
	@Override
	public UserBadge fetchByPrimaryKey(UserBadgePK userBadgePK) {
		return fetchByPrimaryKey((Serializable)userBadgePK);
	}

	@Override
	public Map<Serializable, UserBadge> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, UserBadge> map = new HashMap<Serializable, UserBadge>();

		for (Serializable primaryKey : primaryKeys) {
			UserBadge userBadge = fetchByPrimaryKey(primaryKey);

			if (userBadge != null) {
				map.put(primaryKey, userBadge);
			}
		}

		return map;
	}

	/**
	 * Returns all the user badges.
	 *
	 * @return the user badges
	 */
	@Override
	public List<UserBadge> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user badges.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user badges
	 * @param end the upper bound of the range of user badges (not inclusive)
	 * @return the range of user badges
	 */
	@Override
	public List<UserBadge> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the user badges.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user badges
	 * @param end the upper bound of the range of user badges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of user badges
	 */
	@Override
	public List<UserBadge> findAll(int start, int end,
		OrderByComparator<UserBadge> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user badges.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user badges
	 * @param end the upper bound of the range of user badges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of user badges
	 */
	@Override
	public List<UserBadge> findAll(int start, int end,
		OrderByComparator<UserBadge> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UserBadge> list = null;

		if (retrieveFromCache) {
			list = (List<UserBadge>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_USERBADGE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_USERBADGE;

				if (pagination) {
					sql = sql.concat(UserBadgeModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UserBadge>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserBadge>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the user badges from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (UserBadge userBadge : findAll()) {
			remove(userBadge);
		}
	}

	/**
	 * Returns the number of user badges.
	 *
	 * @return the number of user badges
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_USERBADGE);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return UserBadgeModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the user badge persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(UserBadgeImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_USERBADGE = "SELECT userBadge FROM UserBadge userBadge";
	private static final String _SQL_SELECT_USERBADGE_WHERE = "SELECT userBadge FROM UserBadge userBadge WHERE ";
	private static final String _SQL_COUNT_USERBADGE = "SELECT COUNT(userBadge) FROM UserBadge userBadge";
	private static final String _SQL_COUNT_USERBADGE_WHERE = "SELECT COUNT(userBadge) FROM UserBadge userBadge WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "userBadge.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UserBadge exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No UserBadge exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(UserBadgePersistenceImpl.class);
}