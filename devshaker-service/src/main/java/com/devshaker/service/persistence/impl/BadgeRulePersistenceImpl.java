/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.exception.NoSuchBadgeRuleException;

import com.devshaker.model.BadgeRule;
import com.devshaker.model.impl.BadgeRuleImpl;
import com.devshaker.model.impl.BadgeRuleModelImpl;

import com.devshaker.service.persistence.BadgeRulePK;
import com.devshaker.service.persistence.BadgeRulePersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the badge rule service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BadgeRulePersistence
 * @see com.devshaker.service.persistence.BadgeRuleUtil
 * @generated
 */
@ProviderType
public class BadgeRulePersistenceImpl extends BasePersistenceImpl<BadgeRule>
	implements BadgeRulePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link BadgeRuleUtil} to access the badge rule persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = BadgeRuleImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
			BadgeRuleModelImpl.FINDER_CACHE_ENABLED, BadgeRuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
			BadgeRuleModelImpl.FINDER_CACHE_ENABLED, BadgeRuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
			BadgeRuleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BADGEID = new FinderPath(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
			BadgeRuleModelImpl.FINDER_CACHE_ENABLED, BadgeRuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBadgeId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BADGEID =
		new FinderPath(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
			BadgeRuleModelImpl.FINDER_CACHE_ENABLED, BadgeRuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBadgeId",
			new String[] { Long.class.getName() },
			BadgeRuleModelImpl.BADGEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BADGEID = new FinderPath(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
			BadgeRuleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBadgeId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the badge rules where badgeId = &#63;.
	 *
	 * @param badgeId the badge ID
	 * @return the matching badge rules
	 */
	@Override
	public List<BadgeRule> findByBadgeId(long badgeId) {
		return findByBadgeId(badgeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the badge rules where badgeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param badgeId the badge ID
	 * @param start the lower bound of the range of badge rules
	 * @param end the upper bound of the range of badge rules (not inclusive)
	 * @return the range of matching badge rules
	 */
	@Override
	public List<BadgeRule> findByBadgeId(long badgeId, int start, int end) {
		return findByBadgeId(badgeId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the badge rules where badgeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param badgeId the badge ID
	 * @param start the lower bound of the range of badge rules
	 * @param end the upper bound of the range of badge rules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching badge rules
	 */
	@Override
	public List<BadgeRule> findByBadgeId(long badgeId, int start, int end,
		OrderByComparator<BadgeRule> orderByComparator) {
		return findByBadgeId(badgeId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the badge rules where badgeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param badgeId the badge ID
	 * @param start the lower bound of the range of badge rules
	 * @param end the upper bound of the range of badge rules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching badge rules
	 */
	@Override
	public List<BadgeRule> findByBadgeId(long badgeId, int start, int end,
		OrderByComparator<BadgeRule> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BADGEID;
			finderArgs = new Object[] { badgeId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BADGEID;
			finderArgs = new Object[] { badgeId, start, end, orderByComparator };
		}

		List<BadgeRule> list = null;

		if (retrieveFromCache) {
			list = (List<BadgeRule>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (BadgeRule badgeRule : list) {
					if ((badgeId != badgeRule.getBadgeId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BADGERULE_WHERE);

			query.append(_FINDER_COLUMN_BADGEID_BADGEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(BadgeRuleModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(badgeId);

				if (!pagination) {
					list = (List<BadgeRule>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<BadgeRule>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first badge rule in the ordered set where badgeId = &#63;.
	 *
	 * @param badgeId the badge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching badge rule
	 * @throws NoSuchBadgeRuleException if a matching badge rule could not be found
	 */
	@Override
	public BadgeRule findByBadgeId_First(long badgeId,
		OrderByComparator<BadgeRule> orderByComparator)
		throws NoSuchBadgeRuleException {
		BadgeRule badgeRule = fetchByBadgeId_First(badgeId, orderByComparator);

		if (badgeRule != null) {
			return badgeRule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("badgeId=");
		msg.append(badgeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBadgeRuleException(msg.toString());
	}

	/**
	 * Returns the first badge rule in the ordered set where badgeId = &#63;.
	 *
	 * @param badgeId the badge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching badge rule, or <code>null</code> if a matching badge rule could not be found
	 */
	@Override
	public BadgeRule fetchByBadgeId_First(long badgeId,
		OrderByComparator<BadgeRule> orderByComparator) {
		List<BadgeRule> list = findByBadgeId(badgeId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last badge rule in the ordered set where badgeId = &#63;.
	 *
	 * @param badgeId the badge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching badge rule
	 * @throws NoSuchBadgeRuleException if a matching badge rule could not be found
	 */
	@Override
	public BadgeRule findByBadgeId_Last(long badgeId,
		OrderByComparator<BadgeRule> orderByComparator)
		throws NoSuchBadgeRuleException {
		BadgeRule badgeRule = fetchByBadgeId_Last(badgeId, orderByComparator);

		if (badgeRule != null) {
			return badgeRule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("badgeId=");
		msg.append(badgeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBadgeRuleException(msg.toString());
	}

	/**
	 * Returns the last badge rule in the ordered set where badgeId = &#63;.
	 *
	 * @param badgeId the badge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching badge rule, or <code>null</code> if a matching badge rule could not be found
	 */
	@Override
	public BadgeRule fetchByBadgeId_Last(long badgeId,
		OrderByComparator<BadgeRule> orderByComparator) {
		int count = countByBadgeId(badgeId);

		if (count == 0) {
			return null;
		}

		List<BadgeRule> list = findByBadgeId(badgeId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the badge rules before and after the current badge rule in the ordered set where badgeId = &#63;.
	 *
	 * @param badgeRulePK the primary key of the current badge rule
	 * @param badgeId the badge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next badge rule
	 * @throws NoSuchBadgeRuleException if a badge rule with the primary key could not be found
	 */
	@Override
	public BadgeRule[] findByBadgeId_PrevAndNext(BadgeRulePK badgeRulePK,
		long badgeId, OrderByComparator<BadgeRule> orderByComparator)
		throws NoSuchBadgeRuleException {
		BadgeRule badgeRule = findByPrimaryKey(badgeRulePK);

		Session session = null;

		try {
			session = openSession();

			BadgeRule[] array = new BadgeRuleImpl[3];

			array[0] = getByBadgeId_PrevAndNext(session, badgeRule, badgeId,
					orderByComparator, true);

			array[1] = badgeRule;

			array[2] = getByBadgeId_PrevAndNext(session, badgeRule, badgeId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BadgeRule getByBadgeId_PrevAndNext(Session session,
		BadgeRule badgeRule, long badgeId,
		OrderByComparator<BadgeRule> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BADGERULE_WHERE);

		query.append(_FINDER_COLUMN_BADGEID_BADGEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(BadgeRuleModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(badgeId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(badgeRule);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BadgeRule> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the badge rules where badgeId = &#63; from the database.
	 *
	 * @param badgeId the badge ID
	 */
	@Override
	public void removeByBadgeId(long badgeId) {
		for (BadgeRule badgeRule : findByBadgeId(badgeId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(badgeRule);
		}
	}

	/**
	 * Returns the number of badge rules where badgeId = &#63;.
	 *
	 * @param badgeId the badge ID
	 * @return the number of matching badge rules
	 */
	@Override
	public int countByBadgeId(long badgeId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_BADGEID;

		Object[] finderArgs = new Object[] { badgeId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BADGERULE_WHERE);

			query.append(_FINDER_COLUMN_BADGEID_BADGEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(badgeId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_BADGEID_BADGEID_2 = "badgeRule.id.badgeId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
			BadgeRuleModelImpl.FINDER_CACHE_ENABLED, BadgeRuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompanyId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
			BadgeRuleModelImpl.FINDER_CACHE_ENABLED, BadgeRuleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompanyId",
			new String[] { Long.class.getName() },
			BadgeRuleModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYID = new FinderPath(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
			BadgeRuleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the badge rules where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching badge rules
	 */
	@Override
	public List<BadgeRule> findByCompanyId(long companyId) {
		return findByCompanyId(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the badge rules where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of badge rules
	 * @param end the upper bound of the range of badge rules (not inclusive)
	 * @return the range of matching badge rules
	 */
	@Override
	public List<BadgeRule> findByCompanyId(long companyId, int start, int end) {
		return findByCompanyId(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the badge rules where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of badge rules
	 * @param end the upper bound of the range of badge rules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching badge rules
	 */
	@Override
	public List<BadgeRule> findByCompanyId(long companyId, int start, int end,
		OrderByComparator<BadgeRule> orderByComparator) {
		return findByCompanyId(companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the badge rules where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of badge rules
	 * @param end the upper bound of the range of badge rules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching badge rules
	 */
	@Override
	public List<BadgeRule> findByCompanyId(long companyId, int start, int end,
		OrderByComparator<BadgeRule> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<BadgeRule> list = null;

		if (retrieveFromCache) {
			list = (List<BadgeRule>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (BadgeRule badgeRule : list) {
					if ((companyId != badgeRule.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BADGERULE_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(BadgeRuleModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (!pagination) {
					list = (List<BadgeRule>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<BadgeRule>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first badge rule in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching badge rule
	 * @throws NoSuchBadgeRuleException if a matching badge rule could not be found
	 */
	@Override
	public BadgeRule findByCompanyId_First(long companyId,
		OrderByComparator<BadgeRule> orderByComparator)
		throws NoSuchBadgeRuleException {
		BadgeRule badgeRule = fetchByCompanyId_First(companyId,
				orderByComparator);

		if (badgeRule != null) {
			return badgeRule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBadgeRuleException(msg.toString());
	}

	/**
	 * Returns the first badge rule in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching badge rule, or <code>null</code> if a matching badge rule could not be found
	 */
	@Override
	public BadgeRule fetchByCompanyId_First(long companyId,
		OrderByComparator<BadgeRule> orderByComparator) {
		List<BadgeRule> list = findByCompanyId(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last badge rule in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching badge rule
	 * @throws NoSuchBadgeRuleException if a matching badge rule could not be found
	 */
	@Override
	public BadgeRule findByCompanyId_Last(long companyId,
		OrderByComparator<BadgeRule> orderByComparator)
		throws NoSuchBadgeRuleException {
		BadgeRule badgeRule = fetchByCompanyId_Last(companyId, orderByComparator);

		if (badgeRule != null) {
			return badgeRule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBadgeRuleException(msg.toString());
	}

	/**
	 * Returns the last badge rule in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching badge rule, or <code>null</code> if a matching badge rule could not be found
	 */
	@Override
	public BadgeRule fetchByCompanyId_Last(long companyId,
		OrderByComparator<BadgeRule> orderByComparator) {
		int count = countByCompanyId(companyId);

		if (count == 0) {
			return null;
		}

		List<BadgeRule> list = findByCompanyId(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the badge rules before and after the current badge rule in the ordered set where companyId = &#63;.
	 *
	 * @param badgeRulePK the primary key of the current badge rule
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next badge rule
	 * @throws NoSuchBadgeRuleException if a badge rule with the primary key could not be found
	 */
	@Override
	public BadgeRule[] findByCompanyId_PrevAndNext(BadgeRulePK badgeRulePK,
		long companyId, OrderByComparator<BadgeRule> orderByComparator)
		throws NoSuchBadgeRuleException {
		BadgeRule badgeRule = findByPrimaryKey(badgeRulePK);

		Session session = null;

		try {
			session = openSession();

			BadgeRule[] array = new BadgeRuleImpl[3];

			array[0] = getByCompanyId_PrevAndNext(session, badgeRule,
					companyId, orderByComparator, true);

			array[1] = badgeRule;

			array[2] = getByCompanyId_PrevAndNext(session, badgeRule,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BadgeRule getByCompanyId_PrevAndNext(Session session,
		BadgeRule badgeRule, long companyId,
		OrderByComparator<BadgeRule> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BADGERULE_WHERE);

		query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(BadgeRuleModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(badgeRule);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BadgeRule> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the badge rules where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 */
	@Override
	public void removeByCompanyId(long companyId) {
		for (BadgeRule badgeRule : findByCompanyId(companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(badgeRule);
		}
	}

	/**
	 * Returns the number of badge rules where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching badge rules
	 */
	@Override
	public int countByCompanyId(long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYID;

		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BADGERULE_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 = "badgeRule.companyId = ?";

	public BadgeRulePersistenceImpl() {
		setModelClass(BadgeRule.class);
	}

	/**
	 * Caches the badge rule in the entity cache if it is enabled.
	 *
	 * @param badgeRule the badge rule
	 */
	@Override
	public void cacheResult(BadgeRule badgeRule) {
		entityCache.putResult(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
			BadgeRuleImpl.class, badgeRule.getPrimaryKey(), badgeRule);

		badgeRule.resetOriginalValues();
	}

	/**
	 * Caches the badge rules in the entity cache if it is enabled.
	 *
	 * @param badgeRules the badge rules
	 */
	@Override
	public void cacheResult(List<BadgeRule> badgeRules) {
		for (BadgeRule badgeRule : badgeRules) {
			if (entityCache.getResult(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
						BadgeRuleImpl.class, badgeRule.getPrimaryKey()) == null) {
				cacheResult(badgeRule);
			}
			else {
				badgeRule.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all badge rules.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(BadgeRuleImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the badge rule.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(BadgeRule badgeRule) {
		entityCache.removeResult(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
			BadgeRuleImpl.class, badgeRule.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<BadgeRule> badgeRules) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (BadgeRule badgeRule : badgeRules) {
			entityCache.removeResult(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
				BadgeRuleImpl.class, badgeRule.getPrimaryKey());
		}
	}

	/**
	 * Creates a new badge rule with the primary key. Does not add the badge rule to the database.
	 *
	 * @param badgeRulePK the primary key for the new badge rule
	 * @return the new badge rule
	 */
	@Override
	public BadgeRule create(BadgeRulePK badgeRulePK) {
		BadgeRule badgeRule = new BadgeRuleImpl();

		badgeRule.setNew(true);
		badgeRule.setPrimaryKey(badgeRulePK);

		badgeRule.setCompanyId(companyProvider.getCompanyId());

		return badgeRule;
	}

	/**
	 * Removes the badge rule with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param badgeRulePK the primary key of the badge rule
	 * @return the badge rule that was removed
	 * @throws NoSuchBadgeRuleException if a badge rule with the primary key could not be found
	 */
	@Override
	public BadgeRule remove(BadgeRulePK badgeRulePK)
		throws NoSuchBadgeRuleException {
		return remove((Serializable)badgeRulePK);
	}

	/**
	 * Removes the badge rule with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the badge rule
	 * @return the badge rule that was removed
	 * @throws NoSuchBadgeRuleException if a badge rule with the primary key could not be found
	 */
	@Override
	public BadgeRule remove(Serializable primaryKey)
		throws NoSuchBadgeRuleException {
		Session session = null;

		try {
			session = openSession();

			BadgeRule badgeRule = (BadgeRule)session.get(BadgeRuleImpl.class,
					primaryKey);

			if (badgeRule == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchBadgeRuleException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(badgeRule);
		}
		catch (NoSuchBadgeRuleException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected BadgeRule removeImpl(BadgeRule badgeRule) {
		badgeRule = toUnwrappedModel(badgeRule);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(badgeRule)) {
				badgeRule = (BadgeRule)session.get(BadgeRuleImpl.class,
						badgeRule.getPrimaryKeyObj());
			}

			if (badgeRule != null) {
				session.delete(badgeRule);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (badgeRule != null) {
			clearCache(badgeRule);
		}

		return badgeRule;
	}

	@Override
	public BadgeRule updateImpl(BadgeRule badgeRule) {
		badgeRule = toUnwrappedModel(badgeRule);

		boolean isNew = badgeRule.isNew();

		BadgeRuleModelImpl badgeRuleModelImpl = (BadgeRuleModelImpl)badgeRule;

		Session session = null;

		try {
			session = openSession();

			if (badgeRule.isNew()) {
				session.save(badgeRule);

				badgeRule.setNew(false);
			}
			else {
				badgeRule = (BadgeRule)session.merge(badgeRule);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !BadgeRuleModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((badgeRuleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BADGEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						badgeRuleModelImpl.getOriginalBadgeId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_BADGEID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BADGEID,
					args);

				args = new Object[] { badgeRuleModelImpl.getBadgeId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_BADGEID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BADGEID,
					args);
			}

			if ((badgeRuleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						badgeRuleModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);

				args = new Object[] { badgeRuleModelImpl.getCompanyId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);
			}
		}

		entityCache.putResult(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
			BadgeRuleImpl.class, badgeRule.getPrimaryKey(), badgeRule, false);

		badgeRule.resetOriginalValues();

		return badgeRule;
	}

	protected BadgeRule toUnwrappedModel(BadgeRule badgeRule) {
		if (badgeRule instanceof BadgeRuleImpl) {
			return badgeRule;
		}

		BadgeRuleImpl badgeRuleImpl = new BadgeRuleImpl();

		badgeRuleImpl.setNew(badgeRule.isNew());
		badgeRuleImpl.setPrimaryKey(badgeRule.getPrimaryKey());

		badgeRuleImpl.setRuleId(badgeRule.getRuleId());
		badgeRuleImpl.setBadgeId(badgeRule.getBadgeId());
		badgeRuleImpl.setCompanyId(badgeRule.getCompanyId());

		return badgeRuleImpl;
	}

	/**
	 * Returns the badge rule with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the badge rule
	 * @return the badge rule
	 * @throws NoSuchBadgeRuleException if a badge rule with the primary key could not be found
	 */
	@Override
	public BadgeRule findByPrimaryKey(Serializable primaryKey)
		throws NoSuchBadgeRuleException {
		BadgeRule badgeRule = fetchByPrimaryKey(primaryKey);

		if (badgeRule == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchBadgeRuleException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return badgeRule;
	}

	/**
	 * Returns the badge rule with the primary key or throws a {@link NoSuchBadgeRuleException} if it could not be found.
	 *
	 * @param badgeRulePK the primary key of the badge rule
	 * @return the badge rule
	 * @throws NoSuchBadgeRuleException if a badge rule with the primary key could not be found
	 */
	@Override
	public BadgeRule findByPrimaryKey(BadgeRulePK badgeRulePK)
		throws NoSuchBadgeRuleException {
		return findByPrimaryKey((Serializable)badgeRulePK);
	}

	/**
	 * Returns the badge rule with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the badge rule
	 * @return the badge rule, or <code>null</code> if a badge rule with the primary key could not be found
	 */
	@Override
	public BadgeRule fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
				BadgeRuleImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		BadgeRule badgeRule = (BadgeRule)serializable;

		if (badgeRule == null) {
			Session session = null;

			try {
				session = openSession();

				badgeRule = (BadgeRule)session.get(BadgeRuleImpl.class,
						primaryKey);

				if (badgeRule != null) {
					cacheResult(badgeRule);
				}
				else {
					entityCache.putResult(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
						BadgeRuleImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(BadgeRuleModelImpl.ENTITY_CACHE_ENABLED,
					BadgeRuleImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return badgeRule;
	}

	/**
	 * Returns the badge rule with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param badgeRulePK the primary key of the badge rule
	 * @return the badge rule, or <code>null</code> if a badge rule with the primary key could not be found
	 */
	@Override
	public BadgeRule fetchByPrimaryKey(BadgeRulePK badgeRulePK) {
		return fetchByPrimaryKey((Serializable)badgeRulePK);
	}

	@Override
	public Map<Serializable, BadgeRule> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, BadgeRule> map = new HashMap<Serializable, BadgeRule>();

		for (Serializable primaryKey : primaryKeys) {
			BadgeRule badgeRule = fetchByPrimaryKey(primaryKey);

			if (badgeRule != null) {
				map.put(primaryKey, badgeRule);
			}
		}

		return map;
	}

	/**
	 * Returns all the badge rules.
	 *
	 * @return the badge rules
	 */
	@Override
	public List<BadgeRule> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the badge rules.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of badge rules
	 * @param end the upper bound of the range of badge rules (not inclusive)
	 * @return the range of badge rules
	 */
	@Override
	public List<BadgeRule> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the badge rules.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of badge rules
	 * @param end the upper bound of the range of badge rules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of badge rules
	 */
	@Override
	public List<BadgeRule> findAll(int start, int end,
		OrderByComparator<BadgeRule> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the badge rules.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of badge rules
	 * @param end the upper bound of the range of badge rules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of badge rules
	 */
	@Override
	public List<BadgeRule> findAll(int start, int end,
		OrderByComparator<BadgeRule> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<BadgeRule> list = null;

		if (retrieveFromCache) {
			list = (List<BadgeRule>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_BADGERULE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_BADGERULE;

				if (pagination) {
					sql = sql.concat(BadgeRuleModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<BadgeRule>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<BadgeRule>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the badge rules from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (BadgeRule badgeRule : findAll()) {
			remove(badgeRule);
		}
	}

	/**
	 * Returns the number of badge rules.
	 *
	 * @return the number of badge rules
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_BADGERULE);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return BadgeRuleModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the badge rule persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(BadgeRuleImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_BADGERULE = "SELECT badgeRule FROM BadgeRule badgeRule";
	private static final String _SQL_SELECT_BADGERULE_WHERE = "SELECT badgeRule FROM BadgeRule badgeRule WHERE ";
	private static final String _SQL_COUNT_BADGERULE = "SELECT COUNT(badgeRule) FROM BadgeRule badgeRule";
	private static final String _SQL_COUNT_BADGERULE_WHERE = "SELECT COUNT(badgeRule) FROM BadgeRule badgeRule WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "badgeRule.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No BadgeRule exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No BadgeRule exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(BadgeRulePersistenceImpl.class);
}