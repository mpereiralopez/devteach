/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.exception.NoSuchReportedQuestionsException;

import com.devshaker.model.ReportedQuestions;
import com.devshaker.model.impl.ReportedQuestionsImpl;
import com.devshaker.model.impl.ReportedQuestionsModelImpl;

import com.devshaker.service.persistence.ReportedQuestionsPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the reported questions service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ReportedQuestionsPersistence
 * @see com.devshaker.service.persistence.ReportedQuestionsUtil
 * @generated
 */
@ProviderType
public class ReportedQuestionsPersistenceImpl extends BasePersistenceImpl<ReportedQuestions>
	implements ReportedQuestionsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ReportedQuestionsUtil} to access the reported questions persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ReportedQuestionsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsModelImpl.FINDER_CACHE_ENABLED,
			ReportedQuestionsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsModelImpl.FINDER_CACHE_ENABLED,
			ReportedQuestionsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsModelImpl.FINDER_CACHE_ENABLED,
			ReportedQuestionsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsModelImpl.FINDER_CACHE_ENABLED,
			ReportedQuestionsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserId",
			new String[] { Long.class.getName() },
			ReportedQuestionsModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the reported questionses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching reported questionses
	 */
	@Override
	public List<ReportedQuestions> findByUserId(long userId) {
		return findByUserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the reported questionses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of reported questionses
	 * @param end the upper bound of the range of reported questionses (not inclusive)
	 * @return the range of matching reported questionses
	 */
	@Override
	public List<ReportedQuestions> findByUserId(long userId, int start, int end) {
		return findByUserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the reported questionses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of reported questionses
	 * @param end the upper bound of the range of reported questionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching reported questionses
	 */
	@Override
	public List<ReportedQuestions> findByUserId(long userId, int start,
		int end, OrderByComparator<ReportedQuestions> orderByComparator) {
		return findByUserId(userId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the reported questionses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of reported questionses
	 * @param end the upper bound of the range of reported questionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching reported questionses
	 */
	@Override
	public List<ReportedQuestions> findByUserId(long userId, int start,
		int end, OrderByComparator<ReportedQuestions> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<ReportedQuestions> list = null;

		if (retrieveFromCache) {
			list = (List<ReportedQuestions>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ReportedQuestions reportedQuestions : list) {
					if ((userId != reportedQuestions.getUserId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_REPORTEDQUESTIONS_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ReportedQuestionsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<ReportedQuestions>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ReportedQuestions>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first reported questions in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching reported questions
	 * @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	 */
	@Override
	public ReportedQuestions findByUserId_First(long userId,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException {
		ReportedQuestions reportedQuestions = fetchByUserId_First(userId,
				orderByComparator);

		if (reportedQuestions != null) {
			return reportedQuestions;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReportedQuestionsException(msg.toString());
	}

	/**
	 * Returns the first reported questions in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching reported questions, or <code>null</code> if a matching reported questions could not be found
	 */
	@Override
	public ReportedQuestions fetchByUserId_First(long userId,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		List<ReportedQuestions> list = findByUserId(userId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last reported questions in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching reported questions
	 * @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	 */
	@Override
	public ReportedQuestions findByUserId_Last(long userId,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException {
		ReportedQuestions reportedQuestions = fetchByUserId_Last(userId,
				orderByComparator);

		if (reportedQuestions != null) {
			return reportedQuestions;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReportedQuestionsException(msg.toString());
	}

	/**
	 * Returns the last reported questions in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching reported questions, or <code>null</code> if a matching reported questions could not be found
	 */
	@Override
	public ReportedQuestions fetchByUserId_Last(long userId,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		int count = countByUserId(userId);

		if (count == 0) {
			return null;
		}

		List<ReportedQuestions> list = findByUserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the reported questionses before and after the current reported questions in the ordered set where userId = &#63;.
	 *
	 * @param reportId the primary key of the current reported questions
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next reported questions
	 * @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	 */
	@Override
	public ReportedQuestions[] findByUserId_PrevAndNext(long reportId,
		long userId, OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException {
		ReportedQuestions reportedQuestions = findByPrimaryKey(reportId);

		Session session = null;

		try {
			session = openSession();

			ReportedQuestions[] array = new ReportedQuestionsImpl[3];

			array[0] = getByUserId_PrevAndNext(session, reportedQuestions,
					userId, orderByComparator, true);

			array[1] = reportedQuestions;

			array[2] = getByUserId_PrevAndNext(session, reportedQuestions,
					userId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ReportedQuestions getByUserId_PrevAndNext(Session session,
		ReportedQuestions reportedQuestions, long userId,
		OrderByComparator<ReportedQuestions> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_REPORTEDQUESTIONS_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ReportedQuestionsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(reportedQuestions);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ReportedQuestions> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the reported questionses where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	@Override
	public void removeByUserId(long userId) {
		for (ReportedQuestions reportedQuestions : findByUserId(userId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(reportedQuestions);
		}
	}

	/**
	 * Returns the number of reported questionses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching reported questionses
	 */
	@Override
	public int countByUserId(long userId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_REPORTEDQUESTIONS_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "reportedQuestions.userId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUESTIONID =
		new FinderPath(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsModelImpl.FINDER_CACHE_ENABLED,
			ReportedQuestionsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByquestionId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONID =
		new FinderPath(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsModelImpl.FINDER_CACHE_ENABLED,
			ReportedQuestionsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByquestionId",
			new String[] { Long.class.getName() },
			ReportedQuestionsModelImpl.QUESTIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_QUESTIONID = new FinderPath(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByquestionId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the reported questionses where questionId = &#63;.
	 *
	 * @param questionId the question ID
	 * @return the matching reported questionses
	 */
	@Override
	public List<ReportedQuestions> findByquestionId(long questionId) {
		return findByquestionId(questionId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the reported questionses where questionId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param questionId the question ID
	 * @param start the lower bound of the range of reported questionses
	 * @param end the upper bound of the range of reported questionses (not inclusive)
	 * @return the range of matching reported questionses
	 */
	@Override
	public List<ReportedQuestions> findByquestionId(long questionId, int start,
		int end) {
		return findByquestionId(questionId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the reported questionses where questionId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param questionId the question ID
	 * @param start the lower bound of the range of reported questionses
	 * @param end the upper bound of the range of reported questionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching reported questionses
	 */
	@Override
	public List<ReportedQuestions> findByquestionId(long questionId, int start,
		int end, OrderByComparator<ReportedQuestions> orderByComparator) {
		return findByquestionId(questionId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the reported questionses where questionId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param questionId the question ID
	 * @param start the lower bound of the range of reported questionses
	 * @param end the upper bound of the range of reported questionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching reported questionses
	 */
	@Override
	public List<ReportedQuestions> findByquestionId(long questionId, int start,
		int end, OrderByComparator<ReportedQuestions> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONID;
			finderArgs = new Object[] { questionId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUESTIONID;
			finderArgs = new Object[] { questionId, start, end, orderByComparator };
		}

		List<ReportedQuestions> list = null;

		if (retrieveFromCache) {
			list = (List<ReportedQuestions>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ReportedQuestions reportedQuestions : list) {
					if ((questionId != reportedQuestions.getQuestionId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_REPORTEDQUESTIONS_WHERE);

			query.append(_FINDER_COLUMN_QUESTIONID_QUESTIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ReportedQuestionsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(questionId);

				if (!pagination) {
					list = (List<ReportedQuestions>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ReportedQuestions>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first reported questions in the ordered set where questionId = &#63;.
	 *
	 * @param questionId the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching reported questions
	 * @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	 */
	@Override
	public ReportedQuestions findByquestionId_First(long questionId,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException {
		ReportedQuestions reportedQuestions = fetchByquestionId_First(questionId,
				orderByComparator);

		if (reportedQuestions != null) {
			return reportedQuestions;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("questionId=");
		msg.append(questionId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReportedQuestionsException(msg.toString());
	}

	/**
	 * Returns the first reported questions in the ordered set where questionId = &#63;.
	 *
	 * @param questionId the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching reported questions, or <code>null</code> if a matching reported questions could not be found
	 */
	@Override
	public ReportedQuestions fetchByquestionId_First(long questionId,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		List<ReportedQuestions> list = findByquestionId(questionId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last reported questions in the ordered set where questionId = &#63;.
	 *
	 * @param questionId the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching reported questions
	 * @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	 */
	@Override
	public ReportedQuestions findByquestionId_Last(long questionId,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException {
		ReportedQuestions reportedQuestions = fetchByquestionId_Last(questionId,
				orderByComparator);

		if (reportedQuestions != null) {
			return reportedQuestions;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("questionId=");
		msg.append(questionId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReportedQuestionsException(msg.toString());
	}

	/**
	 * Returns the last reported questions in the ordered set where questionId = &#63;.
	 *
	 * @param questionId the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching reported questions, or <code>null</code> if a matching reported questions could not be found
	 */
	@Override
	public ReportedQuestions fetchByquestionId_Last(long questionId,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		int count = countByquestionId(questionId);

		if (count == 0) {
			return null;
		}

		List<ReportedQuestions> list = findByquestionId(questionId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the reported questionses before and after the current reported questions in the ordered set where questionId = &#63;.
	 *
	 * @param reportId the primary key of the current reported questions
	 * @param questionId the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next reported questions
	 * @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	 */
	@Override
	public ReportedQuestions[] findByquestionId_PrevAndNext(long reportId,
		long questionId, OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException {
		ReportedQuestions reportedQuestions = findByPrimaryKey(reportId);

		Session session = null;

		try {
			session = openSession();

			ReportedQuestions[] array = new ReportedQuestionsImpl[3];

			array[0] = getByquestionId_PrevAndNext(session, reportedQuestions,
					questionId, orderByComparator, true);

			array[1] = reportedQuestions;

			array[2] = getByquestionId_PrevAndNext(session, reportedQuestions,
					questionId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ReportedQuestions getByquestionId_PrevAndNext(Session session,
		ReportedQuestions reportedQuestions, long questionId,
		OrderByComparator<ReportedQuestions> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_REPORTEDQUESTIONS_WHERE);

		query.append(_FINDER_COLUMN_QUESTIONID_QUESTIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ReportedQuestionsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(questionId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(reportedQuestions);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ReportedQuestions> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the reported questionses where questionId = &#63; from the database.
	 *
	 * @param questionId the question ID
	 */
	@Override
	public void removeByquestionId(long questionId) {
		for (ReportedQuestions reportedQuestions : findByquestionId(
				questionId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(reportedQuestions);
		}
	}

	/**
	 * Returns the number of reported questionses where questionId = &#63;.
	 *
	 * @param questionId the question ID
	 * @return the number of matching reported questionses
	 */
	@Override
	public int countByquestionId(long questionId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_QUESTIONID;

		Object[] finderArgs = new Object[] { questionId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_REPORTEDQUESTIONS_WHERE);

			query.append(_FINDER_COLUMN_QUESTIONID_QUESTIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(questionId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_QUESTIONID_QUESTIONID_2 = "reportedQuestions.questionId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_REPORTSTATUS =
		new FinderPath(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsModelImpl.FINDER_CACHE_ENABLED,
			ReportedQuestionsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByreportStatus",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REPORTSTATUS =
		new FinderPath(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsModelImpl.FINDER_CACHE_ENABLED,
			ReportedQuestionsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByreportStatus",
			new String[] { String.class.getName() },
			ReportedQuestionsModelImpl.REPORTSTATUS_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_REPORTSTATUS = new FinderPath(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByreportStatus",
			new String[] { String.class.getName() });

	/**
	 * Returns all the reported questionses where reportStatus = &#63;.
	 *
	 * @param reportStatus the report status
	 * @return the matching reported questionses
	 */
	@Override
	public List<ReportedQuestions> findByreportStatus(String reportStatus) {
		return findByreportStatus(reportStatus, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the reported questionses where reportStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param reportStatus the report status
	 * @param start the lower bound of the range of reported questionses
	 * @param end the upper bound of the range of reported questionses (not inclusive)
	 * @return the range of matching reported questionses
	 */
	@Override
	public List<ReportedQuestions> findByreportStatus(String reportStatus,
		int start, int end) {
		return findByreportStatus(reportStatus, start, end, null);
	}

	/**
	 * Returns an ordered range of all the reported questionses where reportStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param reportStatus the report status
	 * @param start the lower bound of the range of reported questionses
	 * @param end the upper bound of the range of reported questionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching reported questionses
	 */
	@Override
	public List<ReportedQuestions> findByreportStatus(String reportStatus,
		int start, int end,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		return findByreportStatus(reportStatus, start, end, orderByComparator,
			true);
	}

	/**
	 * Returns an ordered range of all the reported questionses where reportStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param reportStatus the report status
	 * @param start the lower bound of the range of reported questionses
	 * @param end the upper bound of the range of reported questionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching reported questionses
	 */
	@Override
	public List<ReportedQuestions> findByreportStatus(String reportStatus,
		int start, int end,
		OrderByComparator<ReportedQuestions> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REPORTSTATUS;
			finderArgs = new Object[] { reportStatus };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_REPORTSTATUS;
			finderArgs = new Object[] {
					reportStatus,
					
					start, end, orderByComparator
				};
		}

		List<ReportedQuestions> list = null;

		if (retrieveFromCache) {
			list = (List<ReportedQuestions>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ReportedQuestions reportedQuestions : list) {
					if (!Objects.equals(reportStatus,
								reportedQuestions.getReportStatus())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_REPORTEDQUESTIONS_WHERE);

			boolean bindReportStatus = false;

			if (reportStatus == null) {
				query.append(_FINDER_COLUMN_REPORTSTATUS_REPORTSTATUS_1);
			}
			else if (reportStatus.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_REPORTSTATUS_REPORTSTATUS_3);
			}
			else {
				bindReportStatus = true;

				query.append(_FINDER_COLUMN_REPORTSTATUS_REPORTSTATUS_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ReportedQuestionsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindReportStatus) {
					qPos.add(reportStatus);
				}

				if (!pagination) {
					list = (List<ReportedQuestions>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ReportedQuestions>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first reported questions in the ordered set where reportStatus = &#63;.
	 *
	 * @param reportStatus the report status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching reported questions
	 * @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	 */
	@Override
	public ReportedQuestions findByreportStatus_First(String reportStatus,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException {
		ReportedQuestions reportedQuestions = fetchByreportStatus_First(reportStatus,
				orderByComparator);

		if (reportedQuestions != null) {
			return reportedQuestions;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("reportStatus=");
		msg.append(reportStatus);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReportedQuestionsException(msg.toString());
	}

	/**
	 * Returns the first reported questions in the ordered set where reportStatus = &#63;.
	 *
	 * @param reportStatus the report status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching reported questions, or <code>null</code> if a matching reported questions could not be found
	 */
	@Override
	public ReportedQuestions fetchByreportStatus_First(String reportStatus,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		List<ReportedQuestions> list = findByreportStatus(reportStatus, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last reported questions in the ordered set where reportStatus = &#63;.
	 *
	 * @param reportStatus the report status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching reported questions
	 * @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	 */
	@Override
	public ReportedQuestions findByreportStatus_Last(String reportStatus,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException {
		ReportedQuestions reportedQuestions = fetchByreportStatus_Last(reportStatus,
				orderByComparator);

		if (reportedQuestions != null) {
			return reportedQuestions;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("reportStatus=");
		msg.append(reportStatus);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReportedQuestionsException(msg.toString());
	}

	/**
	 * Returns the last reported questions in the ordered set where reportStatus = &#63;.
	 *
	 * @param reportStatus the report status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching reported questions, or <code>null</code> if a matching reported questions could not be found
	 */
	@Override
	public ReportedQuestions fetchByreportStatus_Last(String reportStatus,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		int count = countByreportStatus(reportStatus);

		if (count == 0) {
			return null;
		}

		List<ReportedQuestions> list = findByreportStatus(reportStatus,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the reported questionses before and after the current reported questions in the ordered set where reportStatus = &#63;.
	 *
	 * @param reportId the primary key of the current reported questions
	 * @param reportStatus the report status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next reported questions
	 * @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	 */
	@Override
	public ReportedQuestions[] findByreportStatus_PrevAndNext(long reportId,
		String reportStatus,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException {
		ReportedQuestions reportedQuestions = findByPrimaryKey(reportId);

		Session session = null;

		try {
			session = openSession();

			ReportedQuestions[] array = new ReportedQuestionsImpl[3];

			array[0] = getByreportStatus_PrevAndNext(session,
					reportedQuestions, reportStatus, orderByComparator, true);

			array[1] = reportedQuestions;

			array[2] = getByreportStatus_PrevAndNext(session,
					reportedQuestions, reportStatus, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ReportedQuestions getByreportStatus_PrevAndNext(Session session,
		ReportedQuestions reportedQuestions, String reportStatus,
		OrderByComparator<ReportedQuestions> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_REPORTEDQUESTIONS_WHERE);

		boolean bindReportStatus = false;

		if (reportStatus == null) {
			query.append(_FINDER_COLUMN_REPORTSTATUS_REPORTSTATUS_1);
		}
		else if (reportStatus.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_REPORTSTATUS_REPORTSTATUS_3);
		}
		else {
			bindReportStatus = true;

			query.append(_FINDER_COLUMN_REPORTSTATUS_REPORTSTATUS_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ReportedQuestionsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindReportStatus) {
			qPos.add(reportStatus);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(reportedQuestions);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ReportedQuestions> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the reported questionses where reportStatus = &#63; from the database.
	 *
	 * @param reportStatus the report status
	 */
	@Override
	public void removeByreportStatus(String reportStatus) {
		for (ReportedQuestions reportedQuestions : findByreportStatus(
				reportStatus, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(reportedQuestions);
		}
	}

	/**
	 * Returns the number of reported questionses where reportStatus = &#63;.
	 *
	 * @param reportStatus the report status
	 * @return the number of matching reported questionses
	 */
	@Override
	public int countByreportStatus(String reportStatus) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_REPORTSTATUS;

		Object[] finderArgs = new Object[] { reportStatus };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_REPORTEDQUESTIONS_WHERE);

			boolean bindReportStatus = false;

			if (reportStatus == null) {
				query.append(_FINDER_COLUMN_REPORTSTATUS_REPORTSTATUS_1);
			}
			else if (reportStatus.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_REPORTSTATUS_REPORTSTATUS_3);
			}
			else {
				bindReportStatus = true;

				query.append(_FINDER_COLUMN_REPORTSTATUS_REPORTSTATUS_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindReportStatus) {
					qPos.add(reportStatus);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_REPORTSTATUS_REPORTSTATUS_1 = "reportedQuestions.reportStatus IS NULL";
	private static final String _FINDER_COLUMN_REPORTSTATUS_REPORTSTATUS_2 = "reportedQuestions.reportStatus = ?";
	private static final String _FINDER_COLUMN_REPORTSTATUS_REPORTSTATUS_3 = "(reportedQuestions.reportStatus IS NULL OR reportedQuestions.reportStatus = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERANDQUESTION =
		new FinderPath(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsModelImpl.FINDER_CACHE_ENABLED,
			ReportedQuestionsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserAndQuestion",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERANDQUESTION =
		new FinderPath(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsModelImpl.FINDER_CACHE_ENABLED,
			ReportedQuestionsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserAndQuestion",
			new String[] { Long.class.getName(), Long.class.getName() },
			ReportedQuestionsModelImpl.USERID_COLUMN_BITMASK |
			ReportedQuestionsModelImpl.QUESTIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERANDQUESTION = new FinderPath(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUserAndQuestion",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns all the reported questionses where userId = &#63; and questionId = &#63;.
	 *
	 * @param userId the user ID
	 * @param questionId the question ID
	 * @return the matching reported questionses
	 */
	@Override
	public List<ReportedQuestions> findByUserAndQuestion(long userId,
		long questionId) {
		return findByUserAndQuestion(userId, questionId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the reported questionses where userId = &#63; and questionId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param questionId the question ID
	 * @param start the lower bound of the range of reported questionses
	 * @param end the upper bound of the range of reported questionses (not inclusive)
	 * @return the range of matching reported questionses
	 */
	@Override
	public List<ReportedQuestions> findByUserAndQuestion(long userId,
		long questionId, int start, int end) {
		return findByUserAndQuestion(userId, questionId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the reported questionses where userId = &#63; and questionId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param questionId the question ID
	 * @param start the lower bound of the range of reported questionses
	 * @param end the upper bound of the range of reported questionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching reported questionses
	 */
	@Override
	public List<ReportedQuestions> findByUserAndQuestion(long userId,
		long questionId, int start, int end,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		return findByUserAndQuestion(userId, questionId, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the reported questionses where userId = &#63; and questionId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param questionId the question ID
	 * @param start the lower bound of the range of reported questionses
	 * @param end the upper bound of the range of reported questionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching reported questionses
	 */
	@Override
	public List<ReportedQuestions> findByUserAndQuestion(long userId,
		long questionId, int start, int end,
		OrderByComparator<ReportedQuestions> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERANDQUESTION;
			finderArgs = new Object[] { userId, questionId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERANDQUESTION;
			finderArgs = new Object[] {
					userId, questionId,
					
					start, end, orderByComparator
				};
		}

		List<ReportedQuestions> list = null;

		if (retrieveFromCache) {
			list = (List<ReportedQuestions>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ReportedQuestions reportedQuestions : list) {
					if ((userId != reportedQuestions.getUserId()) ||
							(questionId != reportedQuestions.getQuestionId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_REPORTEDQUESTIONS_WHERE);

			query.append(_FINDER_COLUMN_USERANDQUESTION_USERID_2);

			query.append(_FINDER_COLUMN_USERANDQUESTION_QUESTIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ReportedQuestionsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(questionId);

				if (!pagination) {
					list = (List<ReportedQuestions>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ReportedQuestions>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first reported questions in the ordered set where userId = &#63; and questionId = &#63;.
	 *
	 * @param userId the user ID
	 * @param questionId the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching reported questions
	 * @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	 */
	@Override
	public ReportedQuestions findByUserAndQuestion_First(long userId,
		long questionId, OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException {
		ReportedQuestions reportedQuestions = fetchByUserAndQuestion_First(userId,
				questionId, orderByComparator);

		if (reportedQuestions != null) {
			return reportedQuestions;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", questionId=");
		msg.append(questionId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReportedQuestionsException(msg.toString());
	}

	/**
	 * Returns the first reported questions in the ordered set where userId = &#63; and questionId = &#63;.
	 *
	 * @param userId the user ID
	 * @param questionId the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching reported questions, or <code>null</code> if a matching reported questions could not be found
	 */
	@Override
	public ReportedQuestions fetchByUserAndQuestion_First(long userId,
		long questionId, OrderByComparator<ReportedQuestions> orderByComparator) {
		List<ReportedQuestions> list = findByUserAndQuestion(userId,
				questionId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last reported questions in the ordered set where userId = &#63; and questionId = &#63;.
	 *
	 * @param userId the user ID
	 * @param questionId the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching reported questions
	 * @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	 */
	@Override
	public ReportedQuestions findByUserAndQuestion_Last(long userId,
		long questionId, OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException {
		ReportedQuestions reportedQuestions = fetchByUserAndQuestion_Last(userId,
				questionId, orderByComparator);

		if (reportedQuestions != null) {
			return reportedQuestions;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", questionId=");
		msg.append(questionId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReportedQuestionsException(msg.toString());
	}

	/**
	 * Returns the last reported questions in the ordered set where userId = &#63; and questionId = &#63;.
	 *
	 * @param userId the user ID
	 * @param questionId the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching reported questions, or <code>null</code> if a matching reported questions could not be found
	 */
	@Override
	public ReportedQuestions fetchByUserAndQuestion_Last(long userId,
		long questionId, OrderByComparator<ReportedQuestions> orderByComparator) {
		int count = countByUserAndQuestion(userId, questionId);

		if (count == 0) {
			return null;
		}

		List<ReportedQuestions> list = findByUserAndQuestion(userId,
				questionId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the reported questionses before and after the current reported questions in the ordered set where userId = &#63; and questionId = &#63;.
	 *
	 * @param reportId the primary key of the current reported questions
	 * @param userId the user ID
	 * @param questionId the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next reported questions
	 * @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	 */
	@Override
	public ReportedQuestions[] findByUserAndQuestion_PrevAndNext(
		long reportId, long userId, long questionId,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException {
		ReportedQuestions reportedQuestions = findByPrimaryKey(reportId);

		Session session = null;

		try {
			session = openSession();

			ReportedQuestions[] array = new ReportedQuestionsImpl[3];

			array[0] = getByUserAndQuestion_PrevAndNext(session,
					reportedQuestions, userId, questionId, orderByComparator,
					true);

			array[1] = reportedQuestions;

			array[2] = getByUserAndQuestion_PrevAndNext(session,
					reportedQuestions, userId, questionId, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ReportedQuestions getByUserAndQuestion_PrevAndNext(
		Session session, ReportedQuestions reportedQuestions, long userId,
		long questionId,
		OrderByComparator<ReportedQuestions> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_REPORTEDQUESTIONS_WHERE);

		query.append(_FINDER_COLUMN_USERANDQUESTION_USERID_2);

		query.append(_FINDER_COLUMN_USERANDQUESTION_QUESTIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ReportedQuestionsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		qPos.add(questionId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(reportedQuestions);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ReportedQuestions> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the reported questionses where userId = &#63; and questionId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param questionId the question ID
	 */
	@Override
	public void removeByUserAndQuestion(long userId, long questionId) {
		for (ReportedQuestions reportedQuestions : findByUserAndQuestion(
				userId, questionId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(reportedQuestions);
		}
	}

	/**
	 * Returns the number of reported questionses where userId = &#63; and questionId = &#63;.
	 *
	 * @param userId the user ID
	 * @param questionId the question ID
	 * @return the number of matching reported questionses
	 */
	@Override
	public int countByUserAndQuestion(long userId, long questionId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERANDQUESTION;

		Object[] finderArgs = new Object[] { userId, questionId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_REPORTEDQUESTIONS_WHERE);

			query.append(_FINDER_COLUMN_USERANDQUESTION_USERID_2);

			query.append(_FINDER_COLUMN_USERANDQUESTION_QUESTIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(questionId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERANDQUESTION_USERID_2 = "reportedQuestions.userId = ? AND ";
	private static final String _FINDER_COLUMN_USERANDQUESTION_QUESTIONID_2 = "reportedQuestions.questionId = ?";

	public ReportedQuestionsPersistenceImpl() {
		setModelClass(ReportedQuestions.class);
	}

	/**
	 * Caches the reported questions in the entity cache if it is enabled.
	 *
	 * @param reportedQuestions the reported questions
	 */
	@Override
	public void cacheResult(ReportedQuestions reportedQuestions) {
		entityCache.putResult(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsImpl.class, reportedQuestions.getPrimaryKey(),
			reportedQuestions);

		reportedQuestions.resetOriginalValues();
	}

	/**
	 * Caches the reported questionses in the entity cache if it is enabled.
	 *
	 * @param reportedQuestionses the reported questionses
	 */
	@Override
	public void cacheResult(List<ReportedQuestions> reportedQuestionses) {
		for (ReportedQuestions reportedQuestions : reportedQuestionses) {
			if (entityCache.getResult(
						ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
						ReportedQuestionsImpl.class,
						reportedQuestions.getPrimaryKey()) == null) {
				cacheResult(reportedQuestions);
			}
			else {
				reportedQuestions.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all reported questionses.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ReportedQuestionsImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the reported questions.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ReportedQuestions reportedQuestions) {
		entityCache.removeResult(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsImpl.class, reportedQuestions.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ReportedQuestions> reportedQuestionses) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ReportedQuestions reportedQuestions : reportedQuestionses) {
			entityCache.removeResult(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
				ReportedQuestionsImpl.class, reportedQuestions.getPrimaryKey());
		}
	}

	/**
	 * Creates a new reported questions with the primary key. Does not add the reported questions to the database.
	 *
	 * @param reportId the primary key for the new reported questions
	 * @return the new reported questions
	 */
	@Override
	public ReportedQuestions create(long reportId) {
		ReportedQuestions reportedQuestions = new ReportedQuestionsImpl();

		reportedQuestions.setNew(true);
		reportedQuestions.setPrimaryKey(reportId);

		reportedQuestions.setCompanyId(companyProvider.getCompanyId());

		return reportedQuestions;
	}

	/**
	 * Removes the reported questions with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param reportId the primary key of the reported questions
	 * @return the reported questions that was removed
	 * @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	 */
	@Override
	public ReportedQuestions remove(long reportId)
		throws NoSuchReportedQuestionsException {
		return remove((Serializable)reportId);
	}

	/**
	 * Removes the reported questions with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the reported questions
	 * @return the reported questions that was removed
	 * @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	 */
	@Override
	public ReportedQuestions remove(Serializable primaryKey)
		throws NoSuchReportedQuestionsException {
		Session session = null;

		try {
			session = openSession();

			ReportedQuestions reportedQuestions = (ReportedQuestions)session.get(ReportedQuestionsImpl.class,
					primaryKey);

			if (reportedQuestions == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchReportedQuestionsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(reportedQuestions);
		}
		catch (NoSuchReportedQuestionsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ReportedQuestions removeImpl(ReportedQuestions reportedQuestions) {
		reportedQuestions = toUnwrappedModel(reportedQuestions);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(reportedQuestions)) {
				reportedQuestions = (ReportedQuestions)session.get(ReportedQuestionsImpl.class,
						reportedQuestions.getPrimaryKeyObj());
			}

			if (reportedQuestions != null) {
				session.delete(reportedQuestions);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (reportedQuestions != null) {
			clearCache(reportedQuestions);
		}

		return reportedQuestions;
	}

	@Override
	public ReportedQuestions updateImpl(ReportedQuestions reportedQuestions) {
		reportedQuestions = toUnwrappedModel(reportedQuestions);

		boolean isNew = reportedQuestions.isNew();

		ReportedQuestionsModelImpl reportedQuestionsModelImpl = (ReportedQuestionsModelImpl)reportedQuestions;

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (reportedQuestions.getCreateDate() == null)) {
			if (serviceContext == null) {
				reportedQuestions.setCreateDate(now);
			}
			else {
				reportedQuestions.setCreateDate(serviceContext.getCreateDate(
						now));
			}
		}

		if (!reportedQuestionsModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				reportedQuestions.setModifiedDate(now);
			}
			else {
				reportedQuestions.setModifiedDate(serviceContext.getModifiedDate(
						now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (reportedQuestions.isNew()) {
				session.save(reportedQuestions);

				reportedQuestions.setNew(false);
			}
			else {
				reportedQuestions = (ReportedQuestions)session.merge(reportedQuestions);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ReportedQuestionsModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((reportedQuestionsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						reportedQuestionsModelImpl.getOriginalUserId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { reportedQuestionsModelImpl.getUserId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}

			if ((reportedQuestionsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						reportedQuestionsModelImpl.getOriginalQuestionId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_QUESTIONID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONID,
					args);

				args = new Object[] { reportedQuestionsModelImpl.getQuestionId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_QUESTIONID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONID,
					args);
			}

			if ((reportedQuestionsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REPORTSTATUS.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						reportedQuestionsModelImpl.getOriginalReportStatus()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_REPORTSTATUS, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REPORTSTATUS,
					args);

				args = new Object[] { reportedQuestionsModelImpl.getReportStatus() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_REPORTSTATUS, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REPORTSTATUS,
					args);
			}

			if ((reportedQuestionsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERANDQUESTION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						reportedQuestionsModelImpl.getOriginalUserId(),
						reportedQuestionsModelImpl.getOriginalQuestionId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERANDQUESTION,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERANDQUESTION,
					args);

				args = new Object[] {
						reportedQuestionsModelImpl.getUserId(),
						reportedQuestionsModelImpl.getQuestionId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERANDQUESTION,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERANDQUESTION,
					args);
			}
		}

		entityCache.putResult(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
			ReportedQuestionsImpl.class, reportedQuestions.getPrimaryKey(),
			reportedQuestions, false);

		reportedQuestions.resetOriginalValues();

		return reportedQuestions;
	}

	protected ReportedQuestions toUnwrappedModel(
		ReportedQuestions reportedQuestions) {
		if (reportedQuestions instanceof ReportedQuestionsImpl) {
			return reportedQuestions;
		}

		ReportedQuestionsImpl reportedQuestionsImpl = new ReportedQuestionsImpl();

		reportedQuestionsImpl.setNew(reportedQuestions.isNew());
		reportedQuestionsImpl.setPrimaryKey(reportedQuestions.getPrimaryKey());

		reportedQuestionsImpl.setReportId(reportedQuestions.getReportId());
		reportedQuestionsImpl.setCompanyId(reportedQuestions.getCompanyId());
		reportedQuestionsImpl.setCreateDate(reportedQuestions.getCreateDate());
		reportedQuestionsImpl.setModifiedDate(reportedQuestions.getModifiedDate());
		reportedQuestionsImpl.setQuestionId(reportedQuestions.getQuestionId());
		reportedQuestionsImpl.setUserId(reportedQuestions.getUserId());
		reportedQuestionsImpl.setReportContent(reportedQuestions.getReportContent());
		reportedQuestionsImpl.setReportStatus(reportedQuestions.getReportStatus());

		return reportedQuestionsImpl;
	}

	/**
	 * Returns the reported questions with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the reported questions
	 * @return the reported questions
	 * @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	 */
	@Override
	public ReportedQuestions findByPrimaryKey(Serializable primaryKey)
		throws NoSuchReportedQuestionsException {
		ReportedQuestions reportedQuestions = fetchByPrimaryKey(primaryKey);

		if (reportedQuestions == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchReportedQuestionsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return reportedQuestions;
	}

	/**
	 * Returns the reported questions with the primary key or throws a {@link NoSuchReportedQuestionsException} if it could not be found.
	 *
	 * @param reportId the primary key of the reported questions
	 * @return the reported questions
	 * @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	 */
	@Override
	public ReportedQuestions findByPrimaryKey(long reportId)
		throws NoSuchReportedQuestionsException {
		return findByPrimaryKey((Serializable)reportId);
	}

	/**
	 * Returns the reported questions with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the reported questions
	 * @return the reported questions, or <code>null</code> if a reported questions with the primary key could not be found
	 */
	@Override
	public ReportedQuestions fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
				ReportedQuestionsImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		ReportedQuestions reportedQuestions = (ReportedQuestions)serializable;

		if (reportedQuestions == null) {
			Session session = null;

			try {
				session = openSession();

				reportedQuestions = (ReportedQuestions)session.get(ReportedQuestionsImpl.class,
						primaryKey);

				if (reportedQuestions != null) {
					cacheResult(reportedQuestions);
				}
				else {
					entityCache.putResult(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
						ReportedQuestionsImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
					ReportedQuestionsImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return reportedQuestions;
	}

	/**
	 * Returns the reported questions with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param reportId the primary key of the reported questions
	 * @return the reported questions, or <code>null</code> if a reported questions with the primary key could not be found
	 */
	@Override
	public ReportedQuestions fetchByPrimaryKey(long reportId) {
		return fetchByPrimaryKey((Serializable)reportId);
	}

	@Override
	public Map<Serializable, ReportedQuestions> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, ReportedQuestions> map = new HashMap<Serializable, ReportedQuestions>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			ReportedQuestions reportedQuestions = fetchByPrimaryKey(primaryKey);

			if (reportedQuestions != null) {
				map.put(primaryKey, reportedQuestions);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
					ReportedQuestionsImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (ReportedQuestions)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_REPORTEDQUESTIONS_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (ReportedQuestions reportedQuestions : (List<ReportedQuestions>)q.list()) {
				map.put(reportedQuestions.getPrimaryKeyObj(), reportedQuestions);

				cacheResult(reportedQuestions);

				uncachedPrimaryKeys.remove(reportedQuestions.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ReportedQuestionsModelImpl.ENTITY_CACHE_ENABLED,
					ReportedQuestionsImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the reported questionses.
	 *
	 * @return the reported questionses
	 */
	@Override
	public List<ReportedQuestions> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the reported questionses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of reported questionses
	 * @param end the upper bound of the range of reported questionses (not inclusive)
	 * @return the range of reported questionses
	 */
	@Override
	public List<ReportedQuestions> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the reported questionses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of reported questionses
	 * @param end the upper bound of the range of reported questionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of reported questionses
	 */
	@Override
	public List<ReportedQuestions> findAll(int start, int end,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the reported questionses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of reported questionses
	 * @param end the upper bound of the range of reported questionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of reported questionses
	 */
	@Override
	public List<ReportedQuestions> findAll(int start, int end,
		OrderByComparator<ReportedQuestions> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ReportedQuestions> list = null;

		if (retrieveFromCache) {
			list = (List<ReportedQuestions>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_REPORTEDQUESTIONS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_REPORTEDQUESTIONS;

				if (pagination) {
					sql = sql.concat(ReportedQuestionsModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ReportedQuestions>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ReportedQuestions>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the reported questionses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ReportedQuestions reportedQuestions : findAll()) {
			remove(reportedQuestions);
		}
	}

	/**
	 * Returns the number of reported questionses.
	 *
	 * @return the number of reported questionses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_REPORTEDQUESTIONS);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ReportedQuestionsModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the reported questions persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ReportedQuestionsImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_REPORTEDQUESTIONS = "SELECT reportedQuestions FROM ReportedQuestions reportedQuestions";
	private static final String _SQL_SELECT_REPORTEDQUESTIONS_WHERE_PKS_IN = "SELECT reportedQuestions FROM ReportedQuestions reportedQuestions WHERE reportId IN (";
	private static final String _SQL_SELECT_REPORTEDQUESTIONS_WHERE = "SELECT reportedQuestions FROM ReportedQuestions reportedQuestions WHERE ";
	private static final String _SQL_COUNT_REPORTEDQUESTIONS = "SELECT COUNT(reportedQuestions) FROM ReportedQuestions reportedQuestions";
	private static final String _SQL_COUNT_REPORTEDQUESTIONS_WHERE = "SELECT COUNT(reportedQuestions) FROM ReportedQuestions reportedQuestions WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "reportedQuestions.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ReportedQuestions exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ReportedQuestions exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ReportedQuestionsPersistenceImpl.class);
}