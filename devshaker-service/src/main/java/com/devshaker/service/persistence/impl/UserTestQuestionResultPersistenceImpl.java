/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.exception.NoSuchUserTestQuestionResultException;

import com.devshaker.model.UserTestQuestionResult;
import com.devshaker.model.impl.UserTestQuestionResultImpl;
import com.devshaker.model.impl.UserTestQuestionResultModelImpl;

import com.devshaker.service.persistence.UserTestQuestionResultPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the user test question result service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserTestQuestionResultPersistence
 * @see com.devshaker.service.persistence.UserTestQuestionResultUtil
 * @generated
 */
@ProviderType
public class UserTestQuestionResultPersistenceImpl extends BasePersistenceImpl<UserTestQuestionResult>
	implements UserTestQuestionResultPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UserTestQuestionResultUtil} to access the user test question result persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UserTestQuestionResultImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED,
			UserTestQuestionResultImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED,
			UserTestQuestionResultImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED,
			UserTestQuestionResultImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED,
			UserTestQuestionResultImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserId",
			new String[] { Long.class.getName() },
			UserTestQuestionResultModelImpl.USERID_COLUMN_BITMASK |
			UserTestQuestionResultModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the user test question results where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByUserId(long userId) {
		return findByUserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user test question results where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @return the range of matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByUserId(long userId, int start,
		int end) {
		return findByUserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user test question results where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByUserId(long userId, int start,
		int end, OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return findByUserId(userId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user test question results where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByUserId(long userId, int start,
		int end, OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<UserTestQuestionResult> list = null;

		if (retrieveFromCache) {
			list = (List<UserTestQuestionResult>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (UserTestQuestionResult userTestQuestionResult : list) {
					if ((userId != userTestQuestionResult.getUserId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERTESTQUESTIONRESULT_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserTestQuestionResultModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<UserTestQuestionResult>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserTestQuestionResult>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user test question result in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user test question result
	 * @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult findByUserId_First(long userId,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException {
		UserTestQuestionResult userTestQuestionResult = fetchByUserId_First(userId,
				orderByComparator);

		if (userTestQuestionResult != null) {
			return userTestQuestionResult;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserTestQuestionResultException(msg.toString());
	}

	/**
	 * Returns the first user test question result in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user test question result, or <code>null</code> if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult fetchByUserId_First(long userId,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		List<UserTestQuestionResult> list = findByUserId(userId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user test question result in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user test question result
	 * @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult findByUserId_Last(long userId,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException {
		UserTestQuestionResult userTestQuestionResult = fetchByUserId_Last(userId,
				orderByComparator);

		if (userTestQuestionResult != null) {
			return userTestQuestionResult;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserTestQuestionResultException(msg.toString());
	}

	/**
	 * Returns the last user test question result in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user test question result, or <code>null</code> if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult fetchByUserId_Last(long userId,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		int count = countByUserId(userId);

		if (count == 0) {
			return null;
		}

		List<UserTestQuestionResult> list = findByUserId(userId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user test question results before and after the current user test question result in the ordered set where userId = &#63;.
	 *
	 * @param userTestQuestionResultId the primary key of the current user test question result
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user test question result
	 * @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	 */
	@Override
	public UserTestQuestionResult[] findByUserId_PrevAndNext(
		long userTestQuestionResultId, long userId,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException {
		UserTestQuestionResult userTestQuestionResult = findByPrimaryKey(userTestQuestionResultId);

		Session session = null;

		try {
			session = openSession();

			UserTestQuestionResult[] array = new UserTestQuestionResultImpl[3];

			array[0] = getByUserId_PrevAndNext(session, userTestQuestionResult,
					userId, orderByComparator, true);

			array[1] = userTestQuestionResult;

			array[2] = getByUserId_PrevAndNext(session, userTestQuestionResult,
					userId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserTestQuestionResult getByUserId_PrevAndNext(Session session,
		UserTestQuestionResult userTestQuestionResult, long userId,
		OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERTESTQUESTIONRESULT_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserTestQuestionResultModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userTestQuestionResult);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserTestQuestionResult> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user test question results where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	@Override
	public void removeByUserId(long userId) {
		for (UserTestQuestionResult userTestQuestionResult : findByUserId(
				userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(userTestQuestionResult);
		}
	}

	/**
	 * Returns the number of user test question results where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching user test question results
	 */
	@Override
	public int countByUserId(long userId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERTESTQUESTIONRESULT_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "userTestQuestionResult.userId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERIDANDSTATUS =
		new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED,
			UserTestQuestionResultImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserIdAndStatus",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDANDSTATUS =
		new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED,
			UserTestQuestionResultImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserIdAndStatus",
			new String[] { Long.class.getName(), Boolean.class.getName() },
			UserTestQuestionResultModelImpl.USERID_COLUMN_BITMASK |
			UserTestQuestionResultModelImpl.ISCORRECT_COLUMN_BITMASK |
			UserTestQuestionResultModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERIDANDSTATUS = new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUserIdAndStatus",
			new String[] { Long.class.getName(), Boolean.class.getName() });

	/**
	 * Returns all the user test question results where userId = &#63; and isCorrect = &#63;.
	 *
	 * @param userId the user ID
	 * @param isCorrect the is correct
	 * @return the matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByUserIdAndStatus(long userId,
		boolean isCorrect) {
		return findByUserIdAndStatus(userId, isCorrect, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user test question results where userId = &#63; and isCorrect = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param isCorrect the is correct
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @return the range of matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByUserIdAndStatus(long userId,
		boolean isCorrect, int start, int end) {
		return findByUserIdAndStatus(userId, isCorrect, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user test question results where userId = &#63; and isCorrect = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param isCorrect the is correct
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByUserIdAndStatus(long userId,
		boolean isCorrect, int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return findByUserIdAndStatus(userId, isCorrect, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user test question results where userId = &#63; and isCorrect = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param isCorrect the is correct
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByUserIdAndStatus(long userId,
		boolean isCorrect, int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDANDSTATUS;
			finderArgs = new Object[] { userId, isCorrect };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERIDANDSTATUS;
			finderArgs = new Object[] {
					userId, isCorrect,
					
					start, end, orderByComparator
				};
		}

		List<UserTestQuestionResult> list = null;

		if (retrieveFromCache) {
			list = (List<UserTestQuestionResult>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (UserTestQuestionResult userTestQuestionResult : list) {
					if ((userId != userTestQuestionResult.getUserId()) ||
							(isCorrect != userTestQuestionResult.getIsCorrect())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_USERTESTQUESTIONRESULT_WHERE);

			query.append(_FINDER_COLUMN_USERIDANDSTATUS_USERID_2);

			query.append(_FINDER_COLUMN_USERIDANDSTATUS_ISCORRECT_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserTestQuestionResultModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(isCorrect);

				if (!pagination) {
					list = (List<UserTestQuestionResult>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserTestQuestionResult>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user test question result in the ordered set where userId = &#63; and isCorrect = &#63;.
	 *
	 * @param userId the user ID
	 * @param isCorrect the is correct
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user test question result
	 * @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult findByUserIdAndStatus_First(long userId,
		boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException {
		UserTestQuestionResult userTestQuestionResult = fetchByUserIdAndStatus_First(userId,
				isCorrect, orderByComparator);

		if (userTestQuestionResult != null) {
			return userTestQuestionResult;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", isCorrect=");
		msg.append(isCorrect);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserTestQuestionResultException(msg.toString());
	}

	/**
	 * Returns the first user test question result in the ordered set where userId = &#63; and isCorrect = &#63;.
	 *
	 * @param userId the user ID
	 * @param isCorrect the is correct
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user test question result, or <code>null</code> if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult fetchByUserIdAndStatus_First(long userId,
		boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		List<UserTestQuestionResult> list = findByUserIdAndStatus(userId,
				isCorrect, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user test question result in the ordered set where userId = &#63; and isCorrect = &#63;.
	 *
	 * @param userId the user ID
	 * @param isCorrect the is correct
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user test question result
	 * @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult findByUserIdAndStatus_Last(long userId,
		boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException {
		UserTestQuestionResult userTestQuestionResult = fetchByUserIdAndStatus_Last(userId,
				isCorrect, orderByComparator);

		if (userTestQuestionResult != null) {
			return userTestQuestionResult;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", isCorrect=");
		msg.append(isCorrect);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserTestQuestionResultException(msg.toString());
	}

	/**
	 * Returns the last user test question result in the ordered set where userId = &#63; and isCorrect = &#63;.
	 *
	 * @param userId the user ID
	 * @param isCorrect the is correct
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user test question result, or <code>null</code> if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult fetchByUserIdAndStatus_Last(long userId,
		boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		int count = countByUserIdAndStatus(userId, isCorrect);

		if (count == 0) {
			return null;
		}

		List<UserTestQuestionResult> list = findByUserIdAndStatus(userId,
				isCorrect, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user test question results before and after the current user test question result in the ordered set where userId = &#63; and isCorrect = &#63;.
	 *
	 * @param userTestQuestionResultId the primary key of the current user test question result
	 * @param userId the user ID
	 * @param isCorrect the is correct
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user test question result
	 * @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	 */
	@Override
	public UserTestQuestionResult[] findByUserIdAndStatus_PrevAndNext(
		long userTestQuestionResultId, long userId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException {
		UserTestQuestionResult userTestQuestionResult = findByPrimaryKey(userTestQuestionResultId);

		Session session = null;

		try {
			session = openSession();

			UserTestQuestionResult[] array = new UserTestQuestionResultImpl[3];

			array[0] = getByUserIdAndStatus_PrevAndNext(session,
					userTestQuestionResult, userId, isCorrect,
					orderByComparator, true);

			array[1] = userTestQuestionResult;

			array[2] = getByUserIdAndStatus_PrevAndNext(session,
					userTestQuestionResult, userId, isCorrect,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserTestQuestionResult getByUserIdAndStatus_PrevAndNext(
		Session session, UserTestQuestionResult userTestQuestionResult,
		long userId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_USERTESTQUESTIONRESULT_WHERE);

		query.append(_FINDER_COLUMN_USERIDANDSTATUS_USERID_2);

		query.append(_FINDER_COLUMN_USERIDANDSTATUS_ISCORRECT_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserTestQuestionResultModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		qPos.add(isCorrect);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userTestQuestionResult);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserTestQuestionResult> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user test question results where userId = &#63; and isCorrect = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param isCorrect the is correct
	 */
	@Override
	public void removeByUserIdAndStatus(long userId, boolean isCorrect) {
		for (UserTestQuestionResult userTestQuestionResult : findByUserIdAndStatus(
				userId, isCorrect, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(userTestQuestionResult);
		}
	}

	/**
	 * Returns the number of user test question results where userId = &#63; and isCorrect = &#63;.
	 *
	 * @param userId the user ID
	 * @param isCorrect the is correct
	 * @return the number of matching user test question results
	 */
	@Override
	public int countByUserIdAndStatus(long userId, boolean isCorrect) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERIDANDSTATUS;

		Object[] finderArgs = new Object[] { userId, isCorrect };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_USERTESTQUESTIONRESULT_WHERE);

			query.append(_FINDER_COLUMN_USERIDANDSTATUS_USERID_2);

			query.append(_FINDER_COLUMN_USERIDANDSTATUS_ISCORRECT_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(isCorrect);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERIDANDSTATUS_USERID_2 = "userTestQuestionResult.userId = ? AND ";
	private static final String _FINDER_COLUMN_USERIDANDSTATUS_ISCORRECT_2 = "userTestQuestionResult.isCorrect = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUESTIONID =
		new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED,
			UserTestQuestionResultImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByQuestionId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONID =
		new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED,
			UserTestQuestionResultImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByQuestionId",
			new String[] { Long.class.getName() },
			UserTestQuestionResultModelImpl.QUESTIONID_COLUMN_BITMASK |
			UserTestQuestionResultModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_QUESTIONID = new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByQuestionId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the user test question results where questionId = &#63;.
	 *
	 * @param questionId the question ID
	 * @return the matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByQuestionId(long questionId) {
		return findByQuestionId(questionId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user test question results where questionId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param questionId the question ID
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @return the range of matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByQuestionId(long questionId,
		int start, int end) {
		return findByQuestionId(questionId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user test question results where questionId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param questionId the question ID
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByQuestionId(long questionId,
		int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return findByQuestionId(questionId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user test question results where questionId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param questionId the question ID
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByQuestionId(long questionId,
		int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONID;
			finderArgs = new Object[] { questionId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUESTIONID;
			finderArgs = new Object[] { questionId, start, end, orderByComparator };
		}

		List<UserTestQuestionResult> list = null;

		if (retrieveFromCache) {
			list = (List<UserTestQuestionResult>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (UserTestQuestionResult userTestQuestionResult : list) {
					if ((questionId != userTestQuestionResult.getQuestionId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERTESTQUESTIONRESULT_WHERE);

			query.append(_FINDER_COLUMN_QUESTIONID_QUESTIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserTestQuestionResultModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(questionId);

				if (!pagination) {
					list = (List<UserTestQuestionResult>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserTestQuestionResult>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user test question result in the ordered set where questionId = &#63;.
	 *
	 * @param questionId the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user test question result
	 * @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult findByQuestionId_First(long questionId,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException {
		UserTestQuestionResult userTestQuestionResult = fetchByQuestionId_First(questionId,
				orderByComparator);

		if (userTestQuestionResult != null) {
			return userTestQuestionResult;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("questionId=");
		msg.append(questionId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserTestQuestionResultException(msg.toString());
	}

	/**
	 * Returns the first user test question result in the ordered set where questionId = &#63;.
	 *
	 * @param questionId the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user test question result, or <code>null</code> if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult fetchByQuestionId_First(long questionId,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		List<UserTestQuestionResult> list = findByQuestionId(questionId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user test question result in the ordered set where questionId = &#63;.
	 *
	 * @param questionId the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user test question result
	 * @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult findByQuestionId_Last(long questionId,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException {
		UserTestQuestionResult userTestQuestionResult = fetchByQuestionId_Last(questionId,
				orderByComparator);

		if (userTestQuestionResult != null) {
			return userTestQuestionResult;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("questionId=");
		msg.append(questionId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserTestQuestionResultException(msg.toString());
	}

	/**
	 * Returns the last user test question result in the ordered set where questionId = &#63;.
	 *
	 * @param questionId the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user test question result, or <code>null</code> if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult fetchByQuestionId_Last(long questionId,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		int count = countByQuestionId(questionId);

		if (count == 0) {
			return null;
		}

		List<UserTestQuestionResult> list = findByQuestionId(questionId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user test question results before and after the current user test question result in the ordered set where questionId = &#63;.
	 *
	 * @param userTestQuestionResultId the primary key of the current user test question result
	 * @param questionId the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user test question result
	 * @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	 */
	@Override
	public UserTestQuestionResult[] findByQuestionId_PrevAndNext(
		long userTestQuestionResultId, long questionId,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException {
		UserTestQuestionResult userTestQuestionResult = findByPrimaryKey(userTestQuestionResultId);

		Session session = null;

		try {
			session = openSession();

			UserTestQuestionResult[] array = new UserTestQuestionResultImpl[3];

			array[0] = getByQuestionId_PrevAndNext(session,
					userTestQuestionResult, questionId, orderByComparator, true);

			array[1] = userTestQuestionResult;

			array[2] = getByQuestionId_PrevAndNext(session,
					userTestQuestionResult, questionId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserTestQuestionResult getByQuestionId_PrevAndNext(
		Session session, UserTestQuestionResult userTestQuestionResult,
		long questionId,
		OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERTESTQUESTIONRESULT_WHERE);

		query.append(_FINDER_COLUMN_QUESTIONID_QUESTIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserTestQuestionResultModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(questionId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userTestQuestionResult);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserTestQuestionResult> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user test question results where questionId = &#63; from the database.
	 *
	 * @param questionId the question ID
	 */
	@Override
	public void removeByQuestionId(long questionId) {
		for (UserTestQuestionResult userTestQuestionResult : findByQuestionId(
				questionId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(userTestQuestionResult);
		}
	}

	/**
	 * Returns the number of user test question results where questionId = &#63;.
	 *
	 * @param questionId the question ID
	 * @return the number of matching user test question results
	 */
	@Override
	public int countByQuestionId(long questionId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_QUESTIONID;

		Object[] finderArgs = new Object[] { questionId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERTESTQUESTIONRESULT_WHERE);

			query.append(_FINDER_COLUMN_QUESTIONID_QUESTIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(questionId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_QUESTIONID_QUESTIONID_2 = "userTestQuestionResult.questionId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUESTIONIDANDSTATUS =
		new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED,
			UserTestQuestionResultImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByQuestionIdAndStatus",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONIDANDSTATUS =
		new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED,
			UserTestQuestionResultImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByQuestionIdAndStatus",
			new String[] { Long.class.getName(), Boolean.class.getName() },
			UserTestQuestionResultModelImpl.QUESTIONID_COLUMN_BITMASK |
			UserTestQuestionResultModelImpl.ISCORRECT_COLUMN_BITMASK |
			UserTestQuestionResultModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_QUESTIONIDANDSTATUS = new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByQuestionIdAndStatus",
			new String[] { Long.class.getName(), Boolean.class.getName() });

	/**
	 * Returns all the user test question results where questionId = &#63; and isCorrect = &#63;.
	 *
	 * @param questionId the question ID
	 * @param isCorrect the is correct
	 * @return the matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByQuestionIdAndStatus(
		long questionId, boolean isCorrect) {
		return findByQuestionIdAndStatus(questionId, isCorrect,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user test question results where questionId = &#63; and isCorrect = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param questionId the question ID
	 * @param isCorrect the is correct
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @return the range of matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByQuestionIdAndStatus(
		long questionId, boolean isCorrect, int start, int end) {
		return findByQuestionIdAndStatus(questionId, isCorrect, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user test question results where questionId = &#63; and isCorrect = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param questionId the question ID
	 * @param isCorrect the is correct
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByQuestionIdAndStatus(
		long questionId, boolean isCorrect, int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return findByQuestionIdAndStatus(questionId, isCorrect, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user test question results where questionId = &#63; and isCorrect = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param questionId the question ID
	 * @param isCorrect the is correct
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByQuestionIdAndStatus(
		long questionId, boolean isCorrect, int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONIDANDSTATUS;
			finderArgs = new Object[] { questionId, isCorrect };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUESTIONIDANDSTATUS;
			finderArgs = new Object[] {
					questionId, isCorrect,
					
					start, end, orderByComparator
				};
		}

		List<UserTestQuestionResult> list = null;

		if (retrieveFromCache) {
			list = (List<UserTestQuestionResult>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (UserTestQuestionResult userTestQuestionResult : list) {
					if ((questionId != userTestQuestionResult.getQuestionId()) ||
							(isCorrect != userTestQuestionResult.getIsCorrect())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_USERTESTQUESTIONRESULT_WHERE);

			query.append(_FINDER_COLUMN_QUESTIONIDANDSTATUS_QUESTIONID_2);

			query.append(_FINDER_COLUMN_QUESTIONIDANDSTATUS_ISCORRECT_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserTestQuestionResultModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(questionId);

				qPos.add(isCorrect);

				if (!pagination) {
					list = (List<UserTestQuestionResult>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserTestQuestionResult>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user test question result in the ordered set where questionId = &#63; and isCorrect = &#63;.
	 *
	 * @param questionId the question ID
	 * @param isCorrect the is correct
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user test question result
	 * @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult findByQuestionIdAndStatus_First(
		long questionId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException {
		UserTestQuestionResult userTestQuestionResult = fetchByQuestionIdAndStatus_First(questionId,
				isCorrect, orderByComparator);

		if (userTestQuestionResult != null) {
			return userTestQuestionResult;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("questionId=");
		msg.append(questionId);

		msg.append(", isCorrect=");
		msg.append(isCorrect);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserTestQuestionResultException(msg.toString());
	}

	/**
	 * Returns the first user test question result in the ordered set where questionId = &#63; and isCorrect = &#63;.
	 *
	 * @param questionId the question ID
	 * @param isCorrect the is correct
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user test question result, or <code>null</code> if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult fetchByQuestionIdAndStatus_First(
		long questionId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		List<UserTestQuestionResult> list = findByQuestionIdAndStatus(questionId,
				isCorrect, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user test question result in the ordered set where questionId = &#63; and isCorrect = &#63;.
	 *
	 * @param questionId the question ID
	 * @param isCorrect the is correct
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user test question result
	 * @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult findByQuestionIdAndStatus_Last(
		long questionId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException {
		UserTestQuestionResult userTestQuestionResult = fetchByQuestionIdAndStatus_Last(questionId,
				isCorrect, orderByComparator);

		if (userTestQuestionResult != null) {
			return userTestQuestionResult;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("questionId=");
		msg.append(questionId);

		msg.append(", isCorrect=");
		msg.append(isCorrect);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserTestQuestionResultException(msg.toString());
	}

	/**
	 * Returns the last user test question result in the ordered set where questionId = &#63; and isCorrect = &#63;.
	 *
	 * @param questionId the question ID
	 * @param isCorrect the is correct
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user test question result, or <code>null</code> if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult fetchByQuestionIdAndStatus_Last(
		long questionId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		int count = countByQuestionIdAndStatus(questionId, isCorrect);

		if (count == 0) {
			return null;
		}

		List<UserTestQuestionResult> list = findByQuestionIdAndStatus(questionId,
				isCorrect, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user test question results before and after the current user test question result in the ordered set where questionId = &#63; and isCorrect = &#63;.
	 *
	 * @param userTestQuestionResultId the primary key of the current user test question result
	 * @param questionId the question ID
	 * @param isCorrect the is correct
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user test question result
	 * @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	 */
	@Override
	public UserTestQuestionResult[] findByQuestionIdAndStatus_PrevAndNext(
		long userTestQuestionResultId, long questionId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException {
		UserTestQuestionResult userTestQuestionResult = findByPrimaryKey(userTestQuestionResultId);

		Session session = null;

		try {
			session = openSession();

			UserTestQuestionResult[] array = new UserTestQuestionResultImpl[3];

			array[0] = getByQuestionIdAndStatus_PrevAndNext(session,
					userTestQuestionResult, questionId, isCorrect,
					orderByComparator, true);

			array[1] = userTestQuestionResult;

			array[2] = getByQuestionIdAndStatus_PrevAndNext(session,
					userTestQuestionResult, questionId, isCorrect,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserTestQuestionResult getByQuestionIdAndStatus_PrevAndNext(
		Session session, UserTestQuestionResult userTestQuestionResult,
		long questionId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_USERTESTQUESTIONRESULT_WHERE);

		query.append(_FINDER_COLUMN_QUESTIONIDANDSTATUS_QUESTIONID_2);

		query.append(_FINDER_COLUMN_QUESTIONIDANDSTATUS_ISCORRECT_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserTestQuestionResultModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(questionId);

		qPos.add(isCorrect);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userTestQuestionResult);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserTestQuestionResult> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user test question results where questionId = &#63; and isCorrect = &#63; from the database.
	 *
	 * @param questionId the question ID
	 * @param isCorrect the is correct
	 */
	@Override
	public void removeByQuestionIdAndStatus(long questionId, boolean isCorrect) {
		for (UserTestQuestionResult userTestQuestionResult : findByQuestionIdAndStatus(
				questionId, isCorrect, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
				null)) {
			remove(userTestQuestionResult);
		}
	}

	/**
	 * Returns the number of user test question results where questionId = &#63; and isCorrect = &#63;.
	 *
	 * @param questionId the question ID
	 * @param isCorrect the is correct
	 * @return the number of matching user test question results
	 */
	@Override
	public int countByQuestionIdAndStatus(long questionId, boolean isCorrect) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_QUESTIONIDANDSTATUS;

		Object[] finderArgs = new Object[] { questionId, isCorrect };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_USERTESTQUESTIONRESULT_WHERE);

			query.append(_FINDER_COLUMN_QUESTIONIDANDSTATUS_QUESTIONID_2);

			query.append(_FINDER_COLUMN_QUESTIONIDANDSTATUS_ISCORRECT_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(questionId);

				qPos.add(isCorrect);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_QUESTIONIDANDSTATUS_QUESTIONID_2 = "userTestQuestionResult.questionId = ? AND ";
	private static final String _FINDER_COLUMN_QUESTIONIDANDSTATUS_ISCORRECT_2 = "userTestQuestionResult.isCorrect = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERIDANDTOKENDATE =
		new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED,
			UserTestQuestionResultImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserIdAndTokenDate",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDANDTOKENDATE =
		new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED,
			UserTestQuestionResultImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUserIdAndTokenDate",
			new String[] { Long.class.getName(), String.class.getName() },
			UserTestQuestionResultModelImpl.USERID_COLUMN_BITMASK |
			UserTestQuestionResultModelImpl.TOKENDATA_COLUMN_BITMASK |
			UserTestQuestionResultModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERIDANDTOKENDATE = new FinderPath(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUserIdAndTokenDate",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the user test question results where userId = &#63; and tokenData = &#63;.
	 *
	 * @param userId the user ID
	 * @param tokenData the token data
	 * @return the matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByUserIdAndTokenDate(long userId,
		String tokenData) {
		return findByUserIdAndTokenDate(userId, tokenData, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user test question results where userId = &#63; and tokenData = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param tokenData the token data
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @return the range of matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByUserIdAndTokenDate(long userId,
		String tokenData, int start, int end) {
		return findByUserIdAndTokenDate(userId, tokenData, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user test question results where userId = &#63; and tokenData = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param tokenData the token data
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByUserIdAndTokenDate(long userId,
		String tokenData, int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return findByUserIdAndTokenDate(userId, tokenData, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user test question results where userId = &#63; and tokenData = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param tokenData the token data
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findByUserIdAndTokenDate(long userId,
		String tokenData, int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDANDTOKENDATE;
			finderArgs = new Object[] { userId, tokenData };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERIDANDTOKENDATE;
			finderArgs = new Object[] {
					userId, tokenData,
					
					start, end, orderByComparator
				};
		}

		List<UserTestQuestionResult> list = null;

		if (retrieveFromCache) {
			list = (List<UserTestQuestionResult>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (UserTestQuestionResult userTestQuestionResult : list) {
					if ((userId != userTestQuestionResult.getUserId()) ||
							!Objects.equals(tokenData,
								userTestQuestionResult.getTokenData())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_USERTESTQUESTIONRESULT_WHERE);

			query.append(_FINDER_COLUMN_USERIDANDTOKENDATE_USERID_2);

			boolean bindTokenData = false;

			if (tokenData == null) {
				query.append(_FINDER_COLUMN_USERIDANDTOKENDATE_TOKENDATA_1);
			}
			else if (tokenData.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_USERIDANDTOKENDATE_TOKENDATA_3);
			}
			else {
				bindTokenData = true;

				query.append(_FINDER_COLUMN_USERIDANDTOKENDATE_TOKENDATA_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserTestQuestionResultModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (bindTokenData) {
					qPos.add(tokenData);
				}

				if (!pagination) {
					list = (List<UserTestQuestionResult>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserTestQuestionResult>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user test question result in the ordered set where userId = &#63; and tokenData = &#63;.
	 *
	 * @param userId the user ID
	 * @param tokenData the token data
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user test question result
	 * @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult findByUserIdAndTokenDate_First(long userId,
		String tokenData,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException {
		UserTestQuestionResult userTestQuestionResult = fetchByUserIdAndTokenDate_First(userId,
				tokenData, orderByComparator);

		if (userTestQuestionResult != null) {
			return userTestQuestionResult;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", tokenData=");
		msg.append(tokenData);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserTestQuestionResultException(msg.toString());
	}

	/**
	 * Returns the first user test question result in the ordered set where userId = &#63; and tokenData = &#63;.
	 *
	 * @param userId the user ID
	 * @param tokenData the token data
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user test question result, or <code>null</code> if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult fetchByUserIdAndTokenDate_First(long userId,
		String tokenData,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		List<UserTestQuestionResult> list = findByUserIdAndTokenDate(userId,
				tokenData, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user test question result in the ordered set where userId = &#63; and tokenData = &#63;.
	 *
	 * @param userId the user ID
	 * @param tokenData the token data
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user test question result
	 * @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult findByUserIdAndTokenDate_Last(long userId,
		String tokenData,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException {
		UserTestQuestionResult userTestQuestionResult = fetchByUserIdAndTokenDate_Last(userId,
				tokenData, orderByComparator);

		if (userTestQuestionResult != null) {
			return userTestQuestionResult;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", tokenData=");
		msg.append(tokenData);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUserTestQuestionResultException(msg.toString());
	}

	/**
	 * Returns the last user test question result in the ordered set where userId = &#63; and tokenData = &#63;.
	 *
	 * @param userId the user ID
	 * @param tokenData the token data
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user test question result, or <code>null</code> if a matching user test question result could not be found
	 */
	@Override
	public UserTestQuestionResult fetchByUserIdAndTokenDate_Last(long userId,
		String tokenData,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		int count = countByUserIdAndTokenDate(userId, tokenData);

		if (count == 0) {
			return null;
		}

		List<UserTestQuestionResult> list = findByUserIdAndTokenDate(userId,
				tokenData, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user test question results before and after the current user test question result in the ordered set where userId = &#63; and tokenData = &#63;.
	 *
	 * @param userTestQuestionResultId the primary key of the current user test question result
	 * @param userId the user ID
	 * @param tokenData the token data
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user test question result
	 * @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	 */
	@Override
	public UserTestQuestionResult[] findByUserIdAndTokenDate_PrevAndNext(
		long userTestQuestionResultId, long userId, String tokenData,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException {
		UserTestQuestionResult userTestQuestionResult = findByPrimaryKey(userTestQuestionResultId);

		Session session = null;

		try {
			session = openSession();

			UserTestQuestionResult[] array = new UserTestQuestionResultImpl[3];

			array[0] = getByUserIdAndTokenDate_PrevAndNext(session,
					userTestQuestionResult, userId, tokenData,
					orderByComparator, true);

			array[1] = userTestQuestionResult;

			array[2] = getByUserIdAndTokenDate_PrevAndNext(session,
					userTestQuestionResult, userId, tokenData,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserTestQuestionResult getByUserIdAndTokenDate_PrevAndNext(
		Session session, UserTestQuestionResult userTestQuestionResult,
		long userId, String tokenData,
		OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_USERTESTQUESTIONRESULT_WHERE);

		query.append(_FINDER_COLUMN_USERIDANDTOKENDATE_USERID_2);

		boolean bindTokenData = false;

		if (tokenData == null) {
			query.append(_FINDER_COLUMN_USERIDANDTOKENDATE_TOKENDATA_1);
		}
		else if (tokenData.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_USERIDANDTOKENDATE_TOKENDATA_3);
		}
		else {
			bindTokenData = true;

			query.append(_FINDER_COLUMN_USERIDANDTOKENDATE_TOKENDATA_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserTestQuestionResultModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (bindTokenData) {
			qPos.add(tokenData);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userTestQuestionResult);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserTestQuestionResult> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user test question results where userId = &#63; and tokenData = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param tokenData the token data
	 */
	@Override
	public void removeByUserIdAndTokenDate(long userId, String tokenData) {
		for (UserTestQuestionResult userTestQuestionResult : findByUserIdAndTokenDate(
				userId, tokenData, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(userTestQuestionResult);
		}
	}

	/**
	 * Returns the number of user test question results where userId = &#63; and tokenData = &#63;.
	 *
	 * @param userId the user ID
	 * @param tokenData the token data
	 * @return the number of matching user test question results
	 */
	@Override
	public int countByUserIdAndTokenDate(long userId, String tokenData) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERIDANDTOKENDATE;

		Object[] finderArgs = new Object[] { userId, tokenData };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_USERTESTQUESTIONRESULT_WHERE);

			query.append(_FINDER_COLUMN_USERIDANDTOKENDATE_USERID_2);

			boolean bindTokenData = false;

			if (tokenData == null) {
				query.append(_FINDER_COLUMN_USERIDANDTOKENDATE_TOKENDATA_1);
			}
			else if (tokenData.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_USERIDANDTOKENDATE_TOKENDATA_3);
			}
			else {
				bindTokenData = true;

				query.append(_FINDER_COLUMN_USERIDANDTOKENDATE_TOKENDATA_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (bindTokenData) {
					qPos.add(tokenData);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERIDANDTOKENDATE_USERID_2 = "userTestQuestionResult.userId = ? AND ";
	private static final String _FINDER_COLUMN_USERIDANDTOKENDATE_TOKENDATA_1 = "userTestQuestionResult.tokenData IS NULL";
	private static final String _FINDER_COLUMN_USERIDANDTOKENDATE_TOKENDATA_2 = "userTestQuestionResult.tokenData = ?";
	private static final String _FINDER_COLUMN_USERIDANDTOKENDATE_TOKENDATA_3 = "(userTestQuestionResult.tokenData IS NULL OR userTestQuestionResult.tokenData = '')";

	public UserTestQuestionResultPersistenceImpl() {
		setModelClass(UserTestQuestionResult.class);
	}

	/**
	 * Caches the user test question result in the entity cache if it is enabled.
	 *
	 * @param userTestQuestionResult the user test question result
	 */
	@Override
	public void cacheResult(UserTestQuestionResult userTestQuestionResult) {
		entityCache.putResult(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultImpl.class,
			userTestQuestionResult.getPrimaryKey(), userTestQuestionResult);

		userTestQuestionResult.resetOriginalValues();
	}

	/**
	 * Caches the user test question results in the entity cache if it is enabled.
	 *
	 * @param userTestQuestionResults the user test question results
	 */
	@Override
	public void cacheResult(
		List<UserTestQuestionResult> userTestQuestionResults) {
		for (UserTestQuestionResult userTestQuestionResult : userTestQuestionResults) {
			if (entityCache.getResult(
						UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
						UserTestQuestionResultImpl.class,
						userTestQuestionResult.getPrimaryKey()) == null) {
				cacheResult(userTestQuestionResult);
			}
			else {
				userTestQuestionResult.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all user test question results.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(UserTestQuestionResultImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the user test question result.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UserTestQuestionResult userTestQuestionResult) {
		entityCache.removeResult(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultImpl.class,
			userTestQuestionResult.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<UserTestQuestionResult> userTestQuestionResults) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UserTestQuestionResult userTestQuestionResult : userTestQuestionResults) {
			entityCache.removeResult(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
				UserTestQuestionResultImpl.class,
				userTestQuestionResult.getPrimaryKey());
		}
	}

	/**
	 * Creates a new user test question result with the primary key. Does not add the user test question result to the database.
	 *
	 * @param userTestQuestionResultId the primary key for the new user test question result
	 * @return the new user test question result
	 */
	@Override
	public UserTestQuestionResult create(long userTestQuestionResultId) {
		UserTestQuestionResult userTestQuestionResult = new UserTestQuestionResultImpl();

		userTestQuestionResult.setNew(true);
		userTestQuestionResult.setPrimaryKey(userTestQuestionResultId);

		return userTestQuestionResult;
	}

	/**
	 * Removes the user test question result with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userTestQuestionResultId the primary key of the user test question result
	 * @return the user test question result that was removed
	 * @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	 */
	@Override
	public UserTestQuestionResult remove(long userTestQuestionResultId)
		throws NoSuchUserTestQuestionResultException {
		return remove((Serializable)userTestQuestionResultId);
	}

	/**
	 * Removes the user test question result with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the user test question result
	 * @return the user test question result that was removed
	 * @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	 */
	@Override
	public UserTestQuestionResult remove(Serializable primaryKey)
		throws NoSuchUserTestQuestionResultException {
		Session session = null;

		try {
			session = openSession();

			UserTestQuestionResult userTestQuestionResult = (UserTestQuestionResult)session.get(UserTestQuestionResultImpl.class,
					primaryKey);

			if (userTestQuestionResult == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUserTestQuestionResultException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(userTestQuestionResult);
		}
		catch (NoSuchUserTestQuestionResultException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UserTestQuestionResult removeImpl(
		UserTestQuestionResult userTestQuestionResult) {
		userTestQuestionResult = toUnwrappedModel(userTestQuestionResult);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(userTestQuestionResult)) {
				userTestQuestionResult = (UserTestQuestionResult)session.get(UserTestQuestionResultImpl.class,
						userTestQuestionResult.getPrimaryKeyObj());
			}

			if (userTestQuestionResult != null) {
				session.delete(userTestQuestionResult);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (userTestQuestionResult != null) {
			clearCache(userTestQuestionResult);
		}

		return userTestQuestionResult;
	}

	@Override
	public UserTestQuestionResult updateImpl(
		UserTestQuestionResult userTestQuestionResult) {
		userTestQuestionResult = toUnwrappedModel(userTestQuestionResult);

		boolean isNew = userTestQuestionResult.isNew();

		UserTestQuestionResultModelImpl userTestQuestionResultModelImpl = (UserTestQuestionResultModelImpl)userTestQuestionResult;

		Session session = null;

		try {
			session = openSession();

			if (userTestQuestionResult.isNew()) {
				session.save(userTestQuestionResult);

				userTestQuestionResult.setNew(false);
			}
			else {
				userTestQuestionResult = (UserTestQuestionResult)session.merge(userTestQuestionResult);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !UserTestQuestionResultModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((userTestQuestionResultModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userTestQuestionResultModelImpl.getOriginalUserId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { userTestQuestionResultModelImpl.getUserId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}

			if ((userTestQuestionResultModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDANDSTATUS.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userTestQuestionResultModelImpl.getOriginalUserId(),
						userTestQuestionResultModelImpl.getOriginalIsCorrect()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERIDANDSTATUS,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDANDSTATUS,
					args);

				args = new Object[] {
						userTestQuestionResultModelImpl.getUserId(),
						userTestQuestionResultModelImpl.getIsCorrect()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERIDANDSTATUS,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDANDSTATUS,
					args);
			}

			if ((userTestQuestionResultModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userTestQuestionResultModelImpl.getOriginalQuestionId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_QUESTIONID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONID,
					args);

				args = new Object[] {
						userTestQuestionResultModelImpl.getQuestionId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_QUESTIONID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONID,
					args);
			}

			if ((userTestQuestionResultModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONIDANDSTATUS.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userTestQuestionResultModelImpl.getOriginalQuestionId(),
						userTestQuestionResultModelImpl.getOriginalIsCorrect()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_QUESTIONIDANDSTATUS,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONIDANDSTATUS,
					args);

				args = new Object[] {
						userTestQuestionResultModelImpl.getQuestionId(),
						userTestQuestionResultModelImpl.getIsCorrect()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_QUESTIONIDANDSTATUS,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESTIONIDANDSTATUS,
					args);
			}

			if ((userTestQuestionResultModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDANDTOKENDATE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userTestQuestionResultModelImpl.getOriginalUserId(),
						userTestQuestionResultModelImpl.getOriginalTokenData()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERIDANDTOKENDATE,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDANDTOKENDATE,
					args);

				args = new Object[] {
						userTestQuestionResultModelImpl.getUserId(),
						userTestQuestionResultModelImpl.getTokenData()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERIDANDTOKENDATE,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDANDTOKENDATE,
					args);
			}
		}

		entityCache.putResult(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
			UserTestQuestionResultImpl.class,
			userTestQuestionResult.getPrimaryKey(), userTestQuestionResult,
			false);

		userTestQuestionResult.resetOriginalValues();

		return userTestQuestionResult;
	}

	protected UserTestQuestionResult toUnwrappedModel(
		UserTestQuestionResult userTestQuestionResult) {
		if (userTestQuestionResult instanceof UserTestQuestionResultImpl) {
			return userTestQuestionResult;
		}

		UserTestQuestionResultImpl userTestQuestionResultImpl = new UserTestQuestionResultImpl();

		userTestQuestionResultImpl.setNew(userTestQuestionResult.isNew());
		userTestQuestionResultImpl.setPrimaryKey(userTestQuestionResult.getPrimaryKey());

		userTestQuestionResultImpl.setUserTestQuestionResultId(userTestQuestionResult.getUserTestQuestionResultId());
		userTestQuestionResultImpl.setQuestionId(userTestQuestionResult.getQuestionId());
		userTestQuestionResultImpl.setUserId(userTestQuestionResult.getUserId());
		userTestQuestionResultImpl.setAnswersByUser(userTestQuestionResult.getAnswersByUser());
		userTestQuestionResultImpl.setCreateDate(userTestQuestionResult.getCreateDate());
		userTestQuestionResultImpl.setIsCorrect(userTestQuestionResult.isIsCorrect());
		userTestQuestionResultImpl.setTokenData(userTestQuestionResult.getTokenData());

		return userTestQuestionResultImpl;
	}

	/**
	 * Returns the user test question result with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the user test question result
	 * @return the user test question result
	 * @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	 */
	@Override
	public UserTestQuestionResult findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUserTestQuestionResultException {
		UserTestQuestionResult userTestQuestionResult = fetchByPrimaryKey(primaryKey);

		if (userTestQuestionResult == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUserTestQuestionResultException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return userTestQuestionResult;
	}

	/**
	 * Returns the user test question result with the primary key or throws a {@link NoSuchUserTestQuestionResultException} if it could not be found.
	 *
	 * @param userTestQuestionResultId the primary key of the user test question result
	 * @return the user test question result
	 * @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	 */
	@Override
	public UserTestQuestionResult findByPrimaryKey(
		long userTestQuestionResultId)
		throws NoSuchUserTestQuestionResultException {
		return findByPrimaryKey((Serializable)userTestQuestionResultId);
	}

	/**
	 * Returns the user test question result with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the user test question result
	 * @return the user test question result, or <code>null</code> if a user test question result with the primary key could not be found
	 */
	@Override
	public UserTestQuestionResult fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
				UserTestQuestionResultImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		UserTestQuestionResult userTestQuestionResult = (UserTestQuestionResult)serializable;

		if (userTestQuestionResult == null) {
			Session session = null;

			try {
				session = openSession();

				userTestQuestionResult = (UserTestQuestionResult)session.get(UserTestQuestionResultImpl.class,
						primaryKey);

				if (userTestQuestionResult != null) {
					cacheResult(userTestQuestionResult);
				}
				else {
					entityCache.putResult(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
						UserTestQuestionResultImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
					UserTestQuestionResultImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return userTestQuestionResult;
	}

	/**
	 * Returns the user test question result with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userTestQuestionResultId the primary key of the user test question result
	 * @return the user test question result, or <code>null</code> if a user test question result with the primary key could not be found
	 */
	@Override
	public UserTestQuestionResult fetchByPrimaryKey(
		long userTestQuestionResultId) {
		return fetchByPrimaryKey((Serializable)userTestQuestionResultId);
	}

	@Override
	public Map<Serializable, UserTestQuestionResult> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, UserTestQuestionResult> map = new HashMap<Serializable, UserTestQuestionResult>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			UserTestQuestionResult userTestQuestionResult = fetchByPrimaryKey(primaryKey);

			if (userTestQuestionResult != null) {
				map.put(primaryKey, userTestQuestionResult);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
					UserTestQuestionResultImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (UserTestQuestionResult)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_USERTESTQUESTIONRESULT_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (UserTestQuestionResult userTestQuestionResult : (List<UserTestQuestionResult>)q.list()) {
				map.put(userTestQuestionResult.getPrimaryKeyObj(),
					userTestQuestionResult);

				cacheResult(userTestQuestionResult);

				uncachedPrimaryKeys.remove(userTestQuestionResult.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(UserTestQuestionResultModelImpl.ENTITY_CACHE_ENABLED,
					UserTestQuestionResultImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the user test question results.
	 *
	 * @return the user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user test question results.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @return the range of user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the user test question results.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findAll(int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user test question results.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of user test question results
	 */
	@Override
	public List<UserTestQuestionResult> findAll(int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UserTestQuestionResult> list = null;

		if (retrieveFromCache) {
			list = (List<UserTestQuestionResult>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_USERTESTQUESTIONRESULT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_USERTESTQUESTIONRESULT;

				if (pagination) {
					sql = sql.concat(UserTestQuestionResultModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UserTestQuestionResult>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserTestQuestionResult>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the user test question results from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (UserTestQuestionResult userTestQuestionResult : findAll()) {
			remove(userTestQuestionResult);
		}
	}

	/**
	 * Returns the number of user test question results.
	 *
	 * @return the number of user test question results
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_USERTESTQUESTIONRESULT);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return UserTestQuestionResultModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the user test question result persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(UserTestQuestionResultImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_USERTESTQUESTIONRESULT = "SELECT userTestQuestionResult FROM UserTestQuestionResult userTestQuestionResult";
	private static final String _SQL_SELECT_USERTESTQUESTIONRESULT_WHERE_PKS_IN = "SELECT userTestQuestionResult FROM UserTestQuestionResult userTestQuestionResult WHERE userTestQuestionResultId IN (";
	private static final String _SQL_SELECT_USERTESTQUESTIONRESULT_WHERE = "SELECT userTestQuestionResult FROM UserTestQuestionResult userTestQuestionResult WHERE ";
	private static final String _SQL_COUNT_USERTESTQUESTIONRESULT = "SELECT COUNT(userTestQuestionResult) FROM UserTestQuestionResult userTestQuestionResult";
	private static final String _SQL_COUNT_USERTESTQUESTIONRESULT_WHERE = "SELECT COUNT(userTestQuestionResult) FROM UserTestQuestionResult userTestQuestionResult WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "userTestQuestionResult.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UserTestQuestionResult exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No UserTestQuestionResult exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(UserTestQuestionResultPersistenceImpl.class);
}