/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.exception.NoSuchTestQuestionException;

import com.devshaker.model.TestQuestion;
import com.devshaker.model.impl.TestQuestionImpl;
import com.devshaker.model.impl.TestQuestionModelImpl;

import com.devshaker.service.persistence.TestQuestionPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the test question service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TestQuestionPersistence
 * @see com.devshaker.service.persistence.TestQuestionUtil
 * @generated
 */
@ProviderType
public class TestQuestionPersistenceImpl extends BasePersistenceImpl<TestQuestion>
	implements TestQuestionPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link TestQuestionUtil} to access the test question persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = TestQuestionImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, TestQuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, TestQuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, TestQuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompanyId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, TestQuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompanyId",
			new String[] { Long.class.getName() },
			TestQuestionModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYID = new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the test questions where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching test questions
	 */
	@Override
	public List<TestQuestion> findByCompanyId(long companyId) {
		return findByCompanyId(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the test questions where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @return the range of matching test questions
	 */
	@Override
	public List<TestQuestion> findByCompanyId(long companyId, int start, int end) {
		return findByCompanyId(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the test questions where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching test questions
	 */
	@Override
	public List<TestQuestion> findByCompanyId(long companyId, int start,
		int end, OrderByComparator<TestQuestion> orderByComparator) {
		return findByCompanyId(companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the test questions where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching test questions
	 */
	@Override
	public List<TestQuestion> findByCompanyId(long companyId, int start,
		int end, OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<TestQuestion> list = null;

		if (retrieveFromCache) {
			list = (List<TestQuestion>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (TestQuestion testQuestion : list) {
					if ((companyId != testQuestion.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_TESTQUESTION_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TestQuestionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (!pagination) {
					list = (List<TestQuestion>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<TestQuestion>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first test question in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching test question
	 * @throws NoSuchTestQuestionException if a matching test question could not be found
	 */
	@Override
	public TestQuestion findByCompanyId_First(long companyId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = fetchByCompanyId_First(companyId,
				orderByComparator);

		if (testQuestion != null) {
			return testQuestion;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTestQuestionException(msg.toString());
	}

	/**
	 * Returns the first test question in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching test question, or <code>null</code> if a matching test question could not be found
	 */
	@Override
	public TestQuestion fetchByCompanyId_First(long companyId,
		OrderByComparator<TestQuestion> orderByComparator) {
		List<TestQuestion> list = findByCompanyId(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last test question in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching test question
	 * @throws NoSuchTestQuestionException if a matching test question could not be found
	 */
	@Override
	public TestQuestion findByCompanyId_Last(long companyId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = fetchByCompanyId_Last(companyId,
				orderByComparator);

		if (testQuestion != null) {
			return testQuestion;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTestQuestionException(msg.toString());
	}

	/**
	 * Returns the last test question in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching test question, or <code>null</code> if a matching test question could not be found
	 */
	@Override
	public TestQuestion fetchByCompanyId_Last(long companyId,
		OrderByComparator<TestQuestion> orderByComparator) {
		int count = countByCompanyId(companyId);

		if (count == 0) {
			return null;
		}

		List<TestQuestion> list = findByCompanyId(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the test questions before and after the current test question in the ordered set where companyId = &#63;.
	 *
	 * @param questionId the primary key of the current test question
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next test question
	 * @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	 */
	@Override
	public TestQuestion[] findByCompanyId_PrevAndNext(long questionId,
		long companyId, OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = findByPrimaryKey(questionId);

		Session session = null;

		try {
			session = openSession();

			TestQuestion[] array = new TestQuestionImpl[3];

			array[0] = getByCompanyId_PrevAndNext(session, testQuestion,
					companyId, orderByComparator, true);

			array[1] = testQuestion;

			array[2] = getByCompanyId_PrevAndNext(session, testQuestion,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TestQuestion getByCompanyId_PrevAndNext(Session session,
		TestQuestion testQuestion, long companyId,
		OrderByComparator<TestQuestion> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TESTQUESTION_WHERE);

		query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TestQuestionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(testQuestion);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TestQuestion> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the test questions where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 */
	@Override
	public void removeByCompanyId(long companyId) {
		for (TestQuestion testQuestion : findByCompanyId(companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(testQuestion);
		}
	}

	/**
	 * Returns the number of test questions where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching test questions
	 */
	@Override
	public int countByCompanyId(long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYID;

		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TESTQUESTION_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 = "testQuestion.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERIDCREATOR =
		new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, TestQuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserIdCreator",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDCREATOR =
		new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, TestQuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserIdCreator",
			new String[] { Long.class.getName() },
			TestQuestionModelImpl.USERIDCREATOR_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERIDCREATOR = new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserIdCreator",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the test questions where userIdCreator = &#63;.
	 *
	 * @param userIdCreator the user ID creator
	 * @return the matching test questions
	 */
	@Override
	public List<TestQuestion> findByUserIdCreator(long userIdCreator) {
		return findByUserIdCreator(userIdCreator, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the test questions where userIdCreator = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userIdCreator the user ID creator
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @return the range of matching test questions
	 */
	@Override
	public List<TestQuestion> findByUserIdCreator(long userIdCreator,
		int start, int end) {
		return findByUserIdCreator(userIdCreator, start, end, null);
	}

	/**
	 * Returns an ordered range of all the test questions where userIdCreator = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userIdCreator the user ID creator
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching test questions
	 */
	@Override
	public List<TestQuestion> findByUserIdCreator(long userIdCreator,
		int start, int end, OrderByComparator<TestQuestion> orderByComparator) {
		return findByUserIdCreator(userIdCreator, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the test questions where userIdCreator = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userIdCreator the user ID creator
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching test questions
	 */
	@Override
	public List<TestQuestion> findByUserIdCreator(long userIdCreator,
		int start, int end, OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDCREATOR;
			finderArgs = new Object[] { userIdCreator };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERIDCREATOR;
			finderArgs = new Object[] {
					userIdCreator,
					
					start, end, orderByComparator
				};
		}

		List<TestQuestion> list = null;

		if (retrieveFromCache) {
			list = (List<TestQuestion>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (TestQuestion testQuestion : list) {
					if ((userIdCreator != testQuestion.getUserIdCreator())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_TESTQUESTION_WHERE);

			query.append(_FINDER_COLUMN_USERIDCREATOR_USERIDCREATOR_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TestQuestionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userIdCreator);

				if (!pagination) {
					list = (List<TestQuestion>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<TestQuestion>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first test question in the ordered set where userIdCreator = &#63;.
	 *
	 * @param userIdCreator the user ID creator
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching test question
	 * @throws NoSuchTestQuestionException if a matching test question could not be found
	 */
	@Override
	public TestQuestion findByUserIdCreator_First(long userIdCreator,
		OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = fetchByUserIdCreator_First(userIdCreator,
				orderByComparator);

		if (testQuestion != null) {
			return testQuestion;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userIdCreator=");
		msg.append(userIdCreator);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTestQuestionException(msg.toString());
	}

	/**
	 * Returns the first test question in the ordered set where userIdCreator = &#63;.
	 *
	 * @param userIdCreator the user ID creator
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching test question, or <code>null</code> if a matching test question could not be found
	 */
	@Override
	public TestQuestion fetchByUserIdCreator_First(long userIdCreator,
		OrderByComparator<TestQuestion> orderByComparator) {
		List<TestQuestion> list = findByUserIdCreator(userIdCreator, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last test question in the ordered set where userIdCreator = &#63;.
	 *
	 * @param userIdCreator the user ID creator
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching test question
	 * @throws NoSuchTestQuestionException if a matching test question could not be found
	 */
	@Override
	public TestQuestion findByUserIdCreator_Last(long userIdCreator,
		OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = fetchByUserIdCreator_Last(userIdCreator,
				orderByComparator);

		if (testQuestion != null) {
			return testQuestion;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userIdCreator=");
		msg.append(userIdCreator);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTestQuestionException(msg.toString());
	}

	/**
	 * Returns the last test question in the ordered set where userIdCreator = &#63;.
	 *
	 * @param userIdCreator the user ID creator
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching test question, or <code>null</code> if a matching test question could not be found
	 */
	@Override
	public TestQuestion fetchByUserIdCreator_Last(long userIdCreator,
		OrderByComparator<TestQuestion> orderByComparator) {
		int count = countByUserIdCreator(userIdCreator);

		if (count == 0) {
			return null;
		}

		List<TestQuestion> list = findByUserIdCreator(userIdCreator, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the test questions before and after the current test question in the ordered set where userIdCreator = &#63;.
	 *
	 * @param questionId the primary key of the current test question
	 * @param userIdCreator the user ID creator
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next test question
	 * @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	 */
	@Override
	public TestQuestion[] findByUserIdCreator_PrevAndNext(long questionId,
		long userIdCreator, OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = findByPrimaryKey(questionId);

		Session session = null;

		try {
			session = openSession();

			TestQuestion[] array = new TestQuestionImpl[3];

			array[0] = getByUserIdCreator_PrevAndNext(session, testQuestion,
					userIdCreator, orderByComparator, true);

			array[1] = testQuestion;

			array[2] = getByUserIdCreator_PrevAndNext(session, testQuestion,
					userIdCreator, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TestQuestion getByUserIdCreator_PrevAndNext(Session session,
		TestQuestion testQuestion, long userIdCreator,
		OrderByComparator<TestQuestion> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TESTQUESTION_WHERE);

		query.append(_FINDER_COLUMN_USERIDCREATOR_USERIDCREATOR_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TestQuestionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userIdCreator);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(testQuestion);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TestQuestion> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the test questions where userIdCreator = &#63; from the database.
	 *
	 * @param userIdCreator the user ID creator
	 */
	@Override
	public void removeByUserIdCreator(long userIdCreator) {
		for (TestQuestion testQuestion : findByUserIdCreator(userIdCreator,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(testQuestion);
		}
	}

	/**
	 * Returns the number of test questions where userIdCreator = &#63;.
	 *
	 * @param userIdCreator the user ID creator
	 * @return the number of matching test questions
	 */
	@Override
	public int countByUserIdCreator(long userIdCreator) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERIDCREATOR;

		Object[] finderArgs = new Object[] { userIdCreator };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TESTQUESTION_WHERE);

			query.append(_FINDER_COLUMN_USERIDCREATOR_USERIDCREATOR_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userIdCreator);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERIDCREATOR_USERIDCREATOR_2 = "testQuestion.userIdCreator = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORYID =
		new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, TestQuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCategoryId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORYID =
		new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, TestQuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCategoryId",
			new String[] { Long.class.getName() },
			TestQuestionModelImpl.CATEGORYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CATEGORYID = new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCategoryId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the test questions where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the matching test questions
	 */
	@Override
	public List<TestQuestion> findByCategoryId(long categoryId) {
		return findByCategoryId(categoryId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the test questions where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @return the range of matching test questions
	 */
	@Override
	public List<TestQuestion> findByCategoryId(long categoryId, int start,
		int end) {
		return findByCategoryId(categoryId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the test questions where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching test questions
	 */
	@Override
	public List<TestQuestion> findByCategoryId(long categoryId, int start,
		int end, OrderByComparator<TestQuestion> orderByComparator) {
		return findByCategoryId(categoryId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the test questions where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching test questions
	 */
	@Override
	public List<TestQuestion> findByCategoryId(long categoryId, int start,
		int end, OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORYID;
			finderArgs = new Object[] { categoryId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORYID;
			finderArgs = new Object[] { categoryId, start, end, orderByComparator };
		}

		List<TestQuestion> list = null;

		if (retrieveFromCache) {
			list = (List<TestQuestion>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (TestQuestion testQuestion : list) {
					if ((categoryId != testQuestion.getCategoryId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_TESTQUESTION_WHERE);

			query.append(_FINDER_COLUMN_CATEGORYID_CATEGORYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TestQuestionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(categoryId);

				if (!pagination) {
					list = (List<TestQuestion>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<TestQuestion>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first test question in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching test question
	 * @throws NoSuchTestQuestionException if a matching test question could not be found
	 */
	@Override
	public TestQuestion findByCategoryId_First(long categoryId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = fetchByCategoryId_First(categoryId,
				orderByComparator);

		if (testQuestion != null) {
			return testQuestion;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("categoryId=");
		msg.append(categoryId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTestQuestionException(msg.toString());
	}

	/**
	 * Returns the first test question in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching test question, or <code>null</code> if a matching test question could not be found
	 */
	@Override
	public TestQuestion fetchByCategoryId_First(long categoryId,
		OrderByComparator<TestQuestion> orderByComparator) {
		List<TestQuestion> list = findByCategoryId(categoryId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last test question in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching test question
	 * @throws NoSuchTestQuestionException if a matching test question could not be found
	 */
	@Override
	public TestQuestion findByCategoryId_Last(long categoryId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = fetchByCategoryId_Last(categoryId,
				orderByComparator);

		if (testQuestion != null) {
			return testQuestion;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("categoryId=");
		msg.append(categoryId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTestQuestionException(msg.toString());
	}

	/**
	 * Returns the last test question in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching test question, or <code>null</code> if a matching test question could not be found
	 */
	@Override
	public TestQuestion fetchByCategoryId_Last(long categoryId,
		OrderByComparator<TestQuestion> orderByComparator) {
		int count = countByCategoryId(categoryId);

		if (count == 0) {
			return null;
		}

		List<TestQuestion> list = findByCategoryId(categoryId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the test questions before and after the current test question in the ordered set where categoryId = &#63;.
	 *
	 * @param questionId the primary key of the current test question
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next test question
	 * @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	 */
	@Override
	public TestQuestion[] findByCategoryId_PrevAndNext(long questionId,
		long categoryId, OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = findByPrimaryKey(questionId);

		Session session = null;

		try {
			session = openSession();

			TestQuestion[] array = new TestQuestionImpl[3];

			array[0] = getByCategoryId_PrevAndNext(session, testQuestion,
					categoryId, orderByComparator, true);

			array[1] = testQuestion;

			array[2] = getByCategoryId_PrevAndNext(session, testQuestion,
					categoryId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TestQuestion getByCategoryId_PrevAndNext(Session session,
		TestQuestion testQuestion, long categoryId,
		OrderByComparator<TestQuestion> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TESTQUESTION_WHERE);

		query.append(_FINDER_COLUMN_CATEGORYID_CATEGORYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TestQuestionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(categoryId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(testQuestion);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TestQuestion> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the test questions where categoryId = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 */
	@Override
	public void removeByCategoryId(long categoryId) {
		for (TestQuestion testQuestion : findByCategoryId(categoryId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(testQuestion);
		}
	}

	/**
	 * Returns the number of test questions where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the number of matching test questions
	 */
	@Override
	public int countByCategoryId(long categoryId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CATEGORYID;

		Object[] finderArgs = new Object[] { categoryId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TESTQUESTION_WHERE);

			query.append(_FINDER_COLUMN_CATEGORYID_CATEGORYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(categoryId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CATEGORYID_CATEGORYID_2 = "testQuestion.categoryId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SUBCATEGORYID =
		new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, TestQuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBySubcategoryId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBCATEGORYID =
		new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, TestQuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBySubcategoryId",
			new String[] { Long.class.getName() },
			TestQuestionModelImpl.SUBCATEGORYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SUBCATEGORYID = new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBySubcategoryId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the test questions where subcategoryId = &#63;.
	 *
	 * @param subcategoryId the subcategory ID
	 * @return the matching test questions
	 */
	@Override
	public List<TestQuestion> findBySubcategoryId(long subcategoryId) {
		return findBySubcategoryId(subcategoryId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the test questions where subcategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param subcategoryId the subcategory ID
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @return the range of matching test questions
	 */
	@Override
	public List<TestQuestion> findBySubcategoryId(long subcategoryId,
		int start, int end) {
		return findBySubcategoryId(subcategoryId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the test questions where subcategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param subcategoryId the subcategory ID
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching test questions
	 */
	@Override
	public List<TestQuestion> findBySubcategoryId(long subcategoryId,
		int start, int end, OrderByComparator<TestQuestion> orderByComparator) {
		return findBySubcategoryId(subcategoryId, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the test questions where subcategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param subcategoryId the subcategory ID
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching test questions
	 */
	@Override
	public List<TestQuestion> findBySubcategoryId(long subcategoryId,
		int start, int end, OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBCATEGORYID;
			finderArgs = new Object[] { subcategoryId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SUBCATEGORYID;
			finderArgs = new Object[] {
					subcategoryId,
					
					start, end, orderByComparator
				};
		}

		List<TestQuestion> list = null;

		if (retrieveFromCache) {
			list = (List<TestQuestion>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (TestQuestion testQuestion : list) {
					if ((subcategoryId != testQuestion.getSubcategoryId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_TESTQUESTION_WHERE);

			query.append(_FINDER_COLUMN_SUBCATEGORYID_SUBCATEGORYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TestQuestionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subcategoryId);

				if (!pagination) {
					list = (List<TestQuestion>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<TestQuestion>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first test question in the ordered set where subcategoryId = &#63;.
	 *
	 * @param subcategoryId the subcategory ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching test question
	 * @throws NoSuchTestQuestionException if a matching test question could not be found
	 */
	@Override
	public TestQuestion findBySubcategoryId_First(long subcategoryId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = fetchBySubcategoryId_First(subcategoryId,
				orderByComparator);

		if (testQuestion != null) {
			return testQuestion;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("subcategoryId=");
		msg.append(subcategoryId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTestQuestionException(msg.toString());
	}

	/**
	 * Returns the first test question in the ordered set where subcategoryId = &#63;.
	 *
	 * @param subcategoryId the subcategory ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching test question, or <code>null</code> if a matching test question could not be found
	 */
	@Override
	public TestQuestion fetchBySubcategoryId_First(long subcategoryId,
		OrderByComparator<TestQuestion> orderByComparator) {
		List<TestQuestion> list = findBySubcategoryId(subcategoryId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last test question in the ordered set where subcategoryId = &#63;.
	 *
	 * @param subcategoryId the subcategory ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching test question
	 * @throws NoSuchTestQuestionException if a matching test question could not be found
	 */
	@Override
	public TestQuestion findBySubcategoryId_Last(long subcategoryId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = fetchBySubcategoryId_Last(subcategoryId,
				orderByComparator);

		if (testQuestion != null) {
			return testQuestion;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("subcategoryId=");
		msg.append(subcategoryId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTestQuestionException(msg.toString());
	}

	/**
	 * Returns the last test question in the ordered set where subcategoryId = &#63;.
	 *
	 * @param subcategoryId the subcategory ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching test question, or <code>null</code> if a matching test question could not be found
	 */
	@Override
	public TestQuestion fetchBySubcategoryId_Last(long subcategoryId,
		OrderByComparator<TestQuestion> orderByComparator) {
		int count = countBySubcategoryId(subcategoryId);

		if (count == 0) {
			return null;
		}

		List<TestQuestion> list = findBySubcategoryId(subcategoryId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the test questions before and after the current test question in the ordered set where subcategoryId = &#63;.
	 *
	 * @param questionId the primary key of the current test question
	 * @param subcategoryId the subcategory ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next test question
	 * @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	 */
	@Override
	public TestQuestion[] findBySubcategoryId_PrevAndNext(long questionId,
		long subcategoryId, OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = findByPrimaryKey(questionId);

		Session session = null;

		try {
			session = openSession();

			TestQuestion[] array = new TestQuestionImpl[3];

			array[0] = getBySubcategoryId_PrevAndNext(session, testQuestion,
					subcategoryId, orderByComparator, true);

			array[1] = testQuestion;

			array[2] = getBySubcategoryId_PrevAndNext(session, testQuestion,
					subcategoryId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TestQuestion getBySubcategoryId_PrevAndNext(Session session,
		TestQuestion testQuestion, long subcategoryId,
		OrderByComparator<TestQuestion> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TESTQUESTION_WHERE);

		query.append(_FINDER_COLUMN_SUBCATEGORYID_SUBCATEGORYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TestQuestionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(subcategoryId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(testQuestion);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TestQuestion> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the test questions where subcategoryId = &#63; from the database.
	 *
	 * @param subcategoryId the subcategory ID
	 */
	@Override
	public void removeBySubcategoryId(long subcategoryId) {
		for (TestQuestion testQuestion : findBySubcategoryId(subcategoryId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(testQuestion);
		}
	}

	/**
	 * Returns the number of test questions where subcategoryId = &#63;.
	 *
	 * @param subcategoryId the subcategory ID
	 * @return the number of matching test questions
	 */
	@Override
	public int countBySubcategoryId(long subcategoryId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SUBCATEGORYID;

		Object[] finderArgs = new Object[] { subcategoryId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TESTQUESTION_WHERE);

			query.append(_FINDER_COLUMN_SUBCATEGORYID_SUBCATEGORYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subcategoryId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SUBCATEGORYID_SUBCATEGORYID_2 = "testQuestion.subcategoryId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LEVELID = new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, TestQuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBylevelId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LEVELID =
		new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, TestQuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBylevelId",
			new String[] { Long.class.getName() },
			TestQuestionModelImpl.LEVELID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LEVELID = new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBylevelId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the test questions where levelId = &#63;.
	 *
	 * @param levelId the level ID
	 * @return the matching test questions
	 */
	@Override
	public List<TestQuestion> findBylevelId(long levelId) {
		return findBylevelId(levelId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the test questions where levelId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param levelId the level ID
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @return the range of matching test questions
	 */
	@Override
	public List<TestQuestion> findBylevelId(long levelId, int start, int end) {
		return findBylevelId(levelId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the test questions where levelId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param levelId the level ID
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching test questions
	 */
	@Override
	public List<TestQuestion> findBylevelId(long levelId, int start, int end,
		OrderByComparator<TestQuestion> orderByComparator) {
		return findBylevelId(levelId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the test questions where levelId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param levelId the level ID
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching test questions
	 */
	@Override
	public List<TestQuestion> findBylevelId(long levelId, int start, int end,
		OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LEVELID;
			finderArgs = new Object[] { levelId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LEVELID;
			finderArgs = new Object[] { levelId, start, end, orderByComparator };
		}

		List<TestQuestion> list = null;

		if (retrieveFromCache) {
			list = (List<TestQuestion>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (TestQuestion testQuestion : list) {
					if ((levelId != testQuestion.getLevelId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_TESTQUESTION_WHERE);

			query.append(_FINDER_COLUMN_LEVELID_LEVELID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TestQuestionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(levelId);

				if (!pagination) {
					list = (List<TestQuestion>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<TestQuestion>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first test question in the ordered set where levelId = &#63;.
	 *
	 * @param levelId the level ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching test question
	 * @throws NoSuchTestQuestionException if a matching test question could not be found
	 */
	@Override
	public TestQuestion findBylevelId_First(long levelId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = fetchBylevelId_First(levelId,
				orderByComparator);

		if (testQuestion != null) {
			return testQuestion;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("levelId=");
		msg.append(levelId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTestQuestionException(msg.toString());
	}

	/**
	 * Returns the first test question in the ordered set where levelId = &#63;.
	 *
	 * @param levelId the level ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching test question, or <code>null</code> if a matching test question could not be found
	 */
	@Override
	public TestQuestion fetchBylevelId_First(long levelId,
		OrderByComparator<TestQuestion> orderByComparator) {
		List<TestQuestion> list = findBylevelId(levelId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last test question in the ordered set where levelId = &#63;.
	 *
	 * @param levelId the level ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching test question
	 * @throws NoSuchTestQuestionException if a matching test question could not be found
	 */
	@Override
	public TestQuestion findBylevelId_Last(long levelId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = fetchBylevelId_Last(levelId,
				orderByComparator);

		if (testQuestion != null) {
			return testQuestion;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("levelId=");
		msg.append(levelId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTestQuestionException(msg.toString());
	}

	/**
	 * Returns the last test question in the ordered set where levelId = &#63;.
	 *
	 * @param levelId the level ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching test question, or <code>null</code> if a matching test question could not be found
	 */
	@Override
	public TestQuestion fetchBylevelId_Last(long levelId,
		OrderByComparator<TestQuestion> orderByComparator) {
		int count = countBylevelId(levelId);

		if (count == 0) {
			return null;
		}

		List<TestQuestion> list = findBylevelId(levelId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the test questions before and after the current test question in the ordered set where levelId = &#63;.
	 *
	 * @param questionId the primary key of the current test question
	 * @param levelId the level ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next test question
	 * @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	 */
	@Override
	public TestQuestion[] findBylevelId_PrevAndNext(long questionId,
		long levelId, OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = findByPrimaryKey(questionId);

		Session session = null;

		try {
			session = openSession();

			TestQuestion[] array = new TestQuestionImpl[3];

			array[0] = getBylevelId_PrevAndNext(session, testQuestion, levelId,
					orderByComparator, true);

			array[1] = testQuestion;

			array[2] = getBylevelId_PrevAndNext(session, testQuestion, levelId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TestQuestion getBylevelId_PrevAndNext(Session session,
		TestQuestion testQuestion, long levelId,
		OrderByComparator<TestQuestion> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TESTQUESTION_WHERE);

		query.append(_FINDER_COLUMN_LEVELID_LEVELID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TestQuestionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(levelId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(testQuestion);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TestQuestion> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the test questions where levelId = &#63; from the database.
	 *
	 * @param levelId the level ID
	 */
	@Override
	public void removeBylevelId(long levelId) {
		for (TestQuestion testQuestion : findBylevelId(levelId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(testQuestion);
		}
	}

	/**
	 * Returns the number of test questions where levelId = &#63;.
	 *
	 * @param levelId the level ID
	 * @return the number of matching test questions
	 */
	@Override
	public int countBylevelId(long levelId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LEVELID;

		Object[] finderArgs = new Object[] { levelId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TESTQUESTION_WHERE);

			query.append(_FINDER_COLUMN_LEVELID_LEVELID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(levelId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LEVELID_LEVELID_2 = "testQuestion.levelId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STATUS = new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, TestQuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByStatus",
			new String[] {
				Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUS =
		new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, TestQuestionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByStatus",
			new String[] { Integer.class.getName() },
			TestQuestionModelImpl.STATUS_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STATUS = new FinderPath(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByStatus",
			new String[] { Integer.class.getName() });

	/**
	 * Returns all the test questions where status = &#63;.
	 *
	 * @param status the status
	 * @return the matching test questions
	 */
	@Override
	public List<TestQuestion> findByStatus(int status) {
		return findByStatus(status, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the test questions where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @return the range of matching test questions
	 */
	@Override
	public List<TestQuestion> findByStatus(int status, int start, int end) {
		return findByStatus(status, start, end, null);
	}

	/**
	 * Returns an ordered range of all the test questions where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching test questions
	 */
	@Override
	public List<TestQuestion> findByStatus(int status, int start, int end,
		OrderByComparator<TestQuestion> orderByComparator) {
		return findByStatus(status, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the test questions where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching test questions
	 */
	@Override
	public List<TestQuestion> findByStatus(int status, int start, int end,
		OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUS;
			finderArgs = new Object[] { status };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STATUS;
			finderArgs = new Object[] { status, start, end, orderByComparator };
		}

		List<TestQuestion> list = null;

		if (retrieveFromCache) {
			list = (List<TestQuestion>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (TestQuestion testQuestion : list) {
					if ((status != testQuestion.getStatus())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_TESTQUESTION_WHERE);

			query.append(_FINDER_COLUMN_STATUS_STATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TestQuestionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(status);

				if (!pagination) {
					list = (List<TestQuestion>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<TestQuestion>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first test question in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching test question
	 * @throws NoSuchTestQuestionException if a matching test question could not be found
	 */
	@Override
	public TestQuestion findByStatus_First(int status,
		OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = fetchByStatus_First(status,
				orderByComparator);

		if (testQuestion != null) {
			return testQuestion;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTestQuestionException(msg.toString());
	}

	/**
	 * Returns the first test question in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching test question, or <code>null</code> if a matching test question could not be found
	 */
	@Override
	public TestQuestion fetchByStatus_First(int status,
		OrderByComparator<TestQuestion> orderByComparator) {
		List<TestQuestion> list = findByStatus(status, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last test question in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching test question
	 * @throws NoSuchTestQuestionException if a matching test question could not be found
	 */
	@Override
	public TestQuestion findByStatus_Last(int status,
		OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = fetchByStatus_Last(status, orderByComparator);

		if (testQuestion != null) {
			return testQuestion;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTestQuestionException(msg.toString());
	}

	/**
	 * Returns the last test question in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching test question, or <code>null</code> if a matching test question could not be found
	 */
	@Override
	public TestQuestion fetchByStatus_Last(int status,
		OrderByComparator<TestQuestion> orderByComparator) {
		int count = countByStatus(status);

		if (count == 0) {
			return null;
		}

		List<TestQuestion> list = findByStatus(status, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the test questions before and after the current test question in the ordered set where status = &#63;.
	 *
	 * @param questionId the primary key of the current test question
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next test question
	 * @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	 */
	@Override
	public TestQuestion[] findByStatus_PrevAndNext(long questionId, int status,
		OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = findByPrimaryKey(questionId);

		Session session = null;

		try {
			session = openSession();

			TestQuestion[] array = new TestQuestionImpl[3];

			array[0] = getByStatus_PrevAndNext(session, testQuestion, status,
					orderByComparator, true);

			array[1] = testQuestion;

			array[2] = getByStatus_PrevAndNext(session, testQuestion, status,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TestQuestion getByStatus_PrevAndNext(Session session,
		TestQuestion testQuestion, int status,
		OrderByComparator<TestQuestion> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TESTQUESTION_WHERE);

		query.append(_FINDER_COLUMN_STATUS_STATUS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TestQuestionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(status);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(testQuestion);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TestQuestion> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the test questions where status = &#63; from the database.
	 *
	 * @param status the status
	 */
	@Override
	public void removeByStatus(int status) {
		for (TestQuestion testQuestion : findByStatus(status,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(testQuestion);
		}
	}

	/**
	 * Returns the number of test questions where status = &#63;.
	 *
	 * @param status the status
	 * @return the number of matching test questions
	 */
	@Override
	public int countByStatus(int status) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_STATUS;

		Object[] finderArgs = new Object[] { status };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TESTQUESTION_WHERE);

			query.append(_FINDER_COLUMN_STATUS_STATUS_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(status);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_STATUS_STATUS_2 = "testQuestion.status = ?";

	public TestQuestionPersistenceImpl() {
		setModelClass(TestQuestion.class);
	}

	/**
	 * Caches the test question in the entity cache if it is enabled.
	 *
	 * @param testQuestion the test question
	 */
	@Override
	public void cacheResult(TestQuestion testQuestion) {
		entityCache.putResult(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionImpl.class, testQuestion.getPrimaryKey(), testQuestion);

		testQuestion.resetOriginalValues();
	}

	/**
	 * Caches the test questions in the entity cache if it is enabled.
	 *
	 * @param testQuestions the test questions
	 */
	@Override
	public void cacheResult(List<TestQuestion> testQuestions) {
		for (TestQuestion testQuestion : testQuestions) {
			if (entityCache.getResult(
						TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
						TestQuestionImpl.class, testQuestion.getPrimaryKey()) == null) {
				cacheResult(testQuestion);
			}
			else {
				testQuestion.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all test questions.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(TestQuestionImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the test question.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(TestQuestion testQuestion) {
		entityCache.removeResult(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionImpl.class, testQuestion.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<TestQuestion> testQuestions) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (TestQuestion testQuestion : testQuestions) {
			entityCache.removeResult(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
				TestQuestionImpl.class, testQuestion.getPrimaryKey());
		}
	}

	/**
	 * Creates a new test question with the primary key. Does not add the test question to the database.
	 *
	 * @param questionId the primary key for the new test question
	 * @return the new test question
	 */
	@Override
	public TestQuestion create(long questionId) {
		TestQuestion testQuestion = new TestQuestionImpl();

		testQuestion.setNew(true);
		testQuestion.setPrimaryKey(questionId);

		testQuestion.setCompanyId(companyProvider.getCompanyId());

		return testQuestion;
	}

	/**
	 * Removes the test question with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param questionId the primary key of the test question
	 * @return the test question that was removed
	 * @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	 */
	@Override
	public TestQuestion remove(long questionId)
		throws NoSuchTestQuestionException {
		return remove((Serializable)questionId);
	}

	/**
	 * Removes the test question with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the test question
	 * @return the test question that was removed
	 * @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	 */
	@Override
	public TestQuestion remove(Serializable primaryKey)
		throws NoSuchTestQuestionException {
		Session session = null;

		try {
			session = openSession();

			TestQuestion testQuestion = (TestQuestion)session.get(TestQuestionImpl.class,
					primaryKey);

			if (testQuestion == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTestQuestionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(testQuestion);
		}
		catch (NoSuchTestQuestionException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected TestQuestion removeImpl(TestQuestion testQuestion) {
		testQuestion = toUnwrappedModel(testQuestion);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(testQuestion)) {
				testQuestion = (TestQuestion)session.get(TestQuestionImpl.class,
						testQuestion.getPrimaryKeyObj());
			}

			if (testQuestion != null) {
				session.delete(testQuestion);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (testQuestion != null) {
			clearCache(testQuestion);
		}

		return testQuestion;
	}

	@Override
	public TestQuestion updateImpl(TestQuestion testQuestion) {
		testQuestion = toUnwrappedModel(testQuestion);

		boolean isNew = testQuestion.isNew();

		TestQuestionModelImpl testQuestionModelImpl = (TestQuestionModelImpl)testQuestion;

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (testQuestion.getCreateDate() == null)) {
			if (serviceContext == null) {
				testQuestion.setCreateDate(now);
			}
			else {
				testQuestion.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!testQuestionModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				testQuestion.setModifiedDate(now);
			}
			else {
				testQuestion.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (testQuestion.isNew()) {
				session.save(testQuestion);

				testQuestion.setNew(false);
			}
			else {
				testQuestion = (TestQuestion)session.merge(testQuestion);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !TestQuestionModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((testQuestionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						testQuestionModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);

				args = new Object[] { testQuestionModelImpl.getCompanyId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);
			}

			if ((testQuestionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDCREATOR.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						testQuestionModelImpl.getOriginalUserIdCreator()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERIDCREATOR,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDCREATOR,
					args);

				args = new Object[] { testQuestionModelImpl.getUserIdCreator() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERIDCREATOR,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDCREATOR,
					args);
			}

			if ((testQuestionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						testQuestionModelImpl.getOriginalCategoryId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_CATEGORYID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORYID,
					args);

				args = new Object[] { testQuestionModelImpl.getCategoryId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_CATEGORYID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORYID,
					args);
			}

			if ((testQuestionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBCATEGORYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						testQuestionModelImpl.getOriginalSubcategoryId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SUBCATEGORYID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBCATEGORYID,
					args);

				args = new Object[] { testQuestionModelImpl.getSubcategoryId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SUBCATEGORYID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBCATEGORYID,
					args);
			}

			if ((testQuestionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LEVELID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						testQuestionModelImpl.getOriginalLevelId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_LEVELID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LEVELID,
					args);

				args = new Object[] { testQuestionModelImpl.getLevelId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_LEVELID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LEVELID,
					args);
			}

			if ((testQuestionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUS.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						testQuestionModelImpl.getOriginalStatus()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_STATUS, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUS,
					args);

				args = new Object[] { testQuestionModelImpl.getStatus() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_STATUS, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATUS,
					args);
			}
		}

		entityCache.putResult(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
			TestQuestionImpl.class, testQuestion.getPrimaryKey(), testQuestion,
			false);

		testQuestion.resetOriginalValues();

		return testQuestion;
	}

	protected TestQuestion toUnwrappedModel(TestQuestion testQuestion) {
		if (testQuestion instanceof TestQuestionImpl) {
			return testQuestion;
		}

		TestQuestionImpl testQuestionImpl = new TestQuestionImpl();

		testQuestionImpl.setNew(testQuestion.isNew());
		testQuestionImpl.setPrimaryKey(testQuestion.getPrimaryKey());

		testQuestionImpl.setQuestionId(testQuestion.getQuestionId());
		testQuestionImpl.setCategoryId(testQuestion.getCategoryId());
		testQuestionImpl.setSubcategoryId(testQuestion.getSubcategoryId());
		testQuestionImpl.setLevelId(testQuestion.getLevelId());
		testQuestionImpl.setGroupId(testQuestion.getGroupId());
		testQuestionImpl.setCompanyId(testQuestion.getCompanyId());
		testQuestionImpl.setUserIdCreator(testQuestion.getUserIdCreator());
		testQuestionImpl.setUserNameCreator(testQuestion.getUserNameCreator());
		testQuestionImpl.setUserIdModified(testQuestion.getUserIdModified());
		testQuestionImpl.setUserNameModified(testQuestion.getUserNameModified());
		testQuestionImpl.setCreateDate(testQuestion.getCreateDate());
		testQuestionImpl.setModifiedDate(testQuestion.getModifiedDate());
		testQuestionImpl.setQuestionText(testQuestion.getQuestionText());
		testQuestionImpl.setStatus(testQuestion.getStatus());
		testQuestionImpl.setType(testQuestion.getType());
		testQuestionImpl.setAnswer0Text(testQuestion.getAnswer0Text());
		testQuestionImpl.setAnswer1Text(testQuestion.getAnswer1Text());
		testQuestionImpl.setAnswer2Text(testQuestion.getAnswer2Text());
		testQuestionImpl.setAnswer3Text(testQuestion.getAnswer3Text());
		testQuestionImpl.setCorrectAnswers(testQuestion.getCorrectAnswers());
		testQuestionImpl.setReportContent(testQuestion.getReportContent());
		testQuestionImpl.setReporterId(testQuestion.getReporterId());

		return testQuestionImpl;
	}

	/**
	 * Returns the test question with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the test question
	 * @return the test question
	 * @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	 */
	@Override
	public TestQuestion findByPrimaryKey(Serializable primaryKey)
		throws NoSuchTestQuestionException {
		TestQuestion testQuestion = fetchByPrimaryKey(primaryKey);

		if (testQuestion == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchTestQuestionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return testQuestion;
	}

	/**
	 * Returns the test question with the primary key or throws a {@link NoSuchTestQuestionException} if it could not be found.
	 *
	 * @param questionId the primary key of the test question
	 * @return the test question
	 * @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	 */
	@Override
	public TestQuestion findByPrimaryKey(long questionId)
		throws NoSuchTestQuestionException {
		return findByPrimaryKey((Serializable)questionId);
	}

	/**
	 * Returns the test question with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the test question
	 * @return the test question, or <code>null</code> if a test question with the primary key could not be found
	 */
	@Override
	public TestQuestion fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
				TestQuestionImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		TestQuestion testQuestion = (TestQuestion)serializable;

		if (testQuestion == null) {
			Session session = null;

			try {
				session = openSession();

				testQuestion = (TestQuestion)session.get(TestQuestionImpl.class,
						primaryKey);

				if (testQuestion != null) {
					cacheResult(testQuestion);
				}
				else {
					entityCache.putResult(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
						TestQuestionImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
					TestQuestionImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return testQuestion;
	}

	/**
	 * Returns the test question with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param questionId the primary key of the test question
	 * @return the test question, or <code>null</code> if a test question with the primary key could not be found
	 */
	@Override
	public TestQuestion fetchByPrimaryKey(long questionId) {
		return fetchByPrimaryKey((Serializable)questionId);
	}

	@Override
	public Map<Serializable, TestQuestion> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, TestQuestion> map = new HashMap<Serializable, TestQuestion>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			TestQuestion testQuestion = fetchByPrimaryKey(primaryKey);

			if (testQuestion != null) {
				map.put(primaryKey, testQuestion);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
					TestQuestionImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (TestQuestion)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_TESTQUESTION_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (TestQuestion testQuestion : (List<TestQuestion>)q.list()) {
				map.put(testQuestion.getPrimaryKeyObj(), testQuestion);

				cacheResult(testQuestion);

				uncachedPrimaryKeys.remove(testQuestion.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(TestQuestionModelImpl.ENTITY_CACHE_ENABLED,
					TestQuestionImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the test questions.
	 *
	 * @return the test questions
	 */
	@Override
	public List<TestQuestion> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the test questions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @return the range of test questions
	 */
	@Override
	public List<TestQuestion> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the test questions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of test questions
	 */
	@Override
	public List<TestQuestion> findAll(int start, int end,
		OrderByComparator<TestQuestion> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the test questions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of test questions
	 * @param end the upper bound of the range of test questions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of test questions
	 */
	@Override
	public List<TestQuestion> findAll(int start, int end,
		OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<TestQuestion> list = null;

		if (retrieveFromCache) {
			list = (List<TestQuestion>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_TESTQUESTION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TESTQUESTION;

				if (pagination) {
					sql = sql.concat(TestQuestionModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<TestQuestion>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<TestQuestion>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the test questions from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (TestQuestion testQuestion : findAll()) {
			remove(testQuestion);
		}
	}

	/**
	 * Returns the number of test questions.
	 *
	 * @return the number of test questions
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TESTQUESTION);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return TestQuestionModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the test question persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(TestQuestionImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_TESTQUESTION = "SELECT testQuestion FROM TestQuestion testQuestion";
	private static final String _SQL_SELECT_TESTQUESTION_WHERE_PKS_IN = "SELECT testQuestion FROM TestQuestion testQuestion WHERE questionId IN (";
	private static final String _SQL_SELECT_TESTQUESTION_WHERE = "SELECT testQuestion FROM TestQuestion testQuestion WHERE ";
	private static final String _SQL_COUNT_TESTQUESTION = "SELECT COUNT(testQuestion) FROM TestQuestion testQuestion";
	private static final String _SQL_COUNT_TESTQUESTION_WHERE = "SELECT COUNT(testQuestion) FROM TestQuestion testQuestion WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "testQuestion.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No TestQuestion exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No TestQuestion exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(TestQuestionPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"type"
			});
}