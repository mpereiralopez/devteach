/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.http;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.service.ApiServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link ApiServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ApiServiceHttp
 * @see ApiServiceUtil
 * @generated
 */
@ProviderType
public class ApiServiceSoap {
	public static java.lang.String getApiVersion() throws RemoteException {
		try {
			java.lang.String returnValue = ApiServiceUtil.getApiVersion();

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject[] getCategoriesOfCompany()
		throws RemoteException {
		try {
			java.util.List<com.liferay.portal.kernel.json.JSONObject> returnValue =
				ApiServiceUtil.getCategoriesOfCompany();

			return returnValue.toArray(new com.liferay.portal.kernel.json.JSONObject[returnValue.size()]);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject[] getSubCategoriesOfCategory(
		long categoryId) throws RemoteException {
		try {
			java.util.List<com.liferay.portal.kernel.json.JSONObject> returnValue =
				ApiServiceUtil.getSubCategoriesOfCategory(categoryId);

			return returnValue.toArray(new com.liferay.portal.kernel.json.JSONObject[returnValue.size()]);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject[] getLevelOfSubcategory(
		long subcategoryId) throws RemoteException {
		try {
			java.util.List<com.liferay.portal.kernel.json.JSONObject> returnValue =
				ApiServiceUtil.getLevelOfSubcategory(subcategoryId);

			return returnValue.toArray(new com.liferay.portal.kernel.json.JSONObject[returnValue.size()]);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject[] getTestQuestionOfLevel(
		long levelId) throws RemoteException {
		try {
			java.util.List<com.liferay.portal.kernel.json.JSONObject> returnValue =
				ApiServiceUtil.getTestQuestionOfLevel(levelId);

			return returnValue.toArray(new com.liferay.portal.kernel.json.JSONObject[returnValue.size()]);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String sendTestResultOfUser(
		java.lang.String jsonResponseStringify) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = ApiServiceUtil.sendTestResultOfUser(jsonResponseStringify);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String reportQuestion(long questionId,
		java.lang.String reason) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = ApiServiceUtil.reportQuestion(questionId,
					reason);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getUserExtraDataInformation(long userId)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = ApiServiceUtil.getUserExtraDataInformation(userId);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.liferay.portal.kernel.model.User updateUserInfo(
		long userId, java.lang.String screenName, java.lang.String firstName,
		java.lang.String emailAddress, java.lang.String phone, int year,
		int month, int day, java.lang.String country) throws RemoteException {
		try {
			com.liferay.portal.kernel.model.User returnValue = ApiServiceUtil.updateUserInfo(userId,
					screenName, firstName, emailAddress, phone, year, month,
					day, country);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(ApiServiceSoap.class);
}