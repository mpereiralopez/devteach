/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.http;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.service.ApiServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.HttpPrincipal;
import com.liferay.portal.kernel.service.http.TunnelUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;

/**
 * Provides the HTTP utility for the
 * {@link ApiServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it requires an additional
 * {@link HttpPrincipal} parameter.
 *
 * <p>
 * The benefits of using the HTTP utility is that it is fast and allows for
 * tunneling without the cost of serializing to text. The drawback is that it
 * only works with Java.
 * </p>
 *
 * <p>
 * Set the property <b>tunnel.servlet.hosts.allowed</b> in portal.properties to
 * configure security.
 * </p>
 *
 * <p>
 * The HTTP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ApiServiceSoap
 * @see HttpPrincipal
 * @see ApiServiceUtil
 * @generated
 */
@ProviderType
public class ApiServiceHttp {
	public static java.lang.String getApiVersion(HttpPrincipal httpPrincipal) {
		try {
			MethodKey methodKey = new MethodKey(ApiServiceUtil.class,
					"getApiVersion", _getApiVersionParameterTypes0);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.util.List<com.liferay.portal.kernel.json.JSONObject> getCategoriesOfCompany(
		HttpPrincipal httpPrincipal) {
		try {
			MethodKey methodKey = new MethodKey(ApiServiceUtil.class,
					"getCategoriesOfCompany",
					_getCategoriesOfCompanyParameterTypes1);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.util.List<com.liferay.portal.kernel.json.JSONObject>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.util.List<com.liferay.portal.kernel.json.JSONObject> getSubCategoriesOfCategory(
		HttpPrincipal httpPrincipal, long categoryId) {
		try {
			MethodKey methodKey = new MethodKey(ApiServiceUtil.class,
					"getSubCategoriesOfCategory",
					_getSubCategoriesOfCategoryParameterTypes2);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					categoryId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.util.List<com.liferay.portal.kernel.json.JSONObject>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.util.List<com.liferay.portal.kernel.json.JSONObject> getLevelOfSubcategory(
		HttpPrincipal httpPrincipal, long subcategoryId) {
		try {
			MethodKey methodKey = new MethodKey(ApiServiceUtil.class,
					"getLevelOfSubcategory",
					_getLevelOfSubcategoryParameterTypes3);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					subcategoryId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.util.List<com.liferay.portal.kernel.json.JSONObject>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.util.List<com.liferay.portal.kernel.json.JSONObject> getTestQuestionOfLevel(
		HttpPrincipal httpPrincipal, long levelId) {
		try {
			MethodKey methodKey = new MethodKey(ApiServiceUtil.class,
					"getTestQuestionOfLevel",
					_getTestQuestionOfLevelParameterTypes4);

			MethodHandler methodHandler = new MethodHandler(methodKey, levelId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.util.List<com.liferay.portal.kernel.json.JSONObject>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject sendTestResultOfUser(
		HttpPrincipal httpPrincipal, java.lang.String jsonResponseStringify) {
		try {
			MethodKey methodKey = new MethodKey(ApiServiceUtil.class,
					"sendTestResultOfUser", _sendTestResultOfUserParameterTypes5);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					jsonResponseStringify);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject reportQuestion(
		HttpPrincipal httpPrincipal, long questionId, java.lang.String reason) {
		try {
			MethodKey methodKey = new MethodKey(ApiServiceUtil.class,
					"reportQuestion", _reportQuestionParameterTypes6);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					questionId, reason);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject getUserExtraDataInformation(
		HttpPrincipal httpPrincipal, long userId) {
		try {
			MethodKey methodKey = new MethodKey(ApiServiceUtil.class,
					"getUserExtraDataInformation",
					_getUserExtraDataInformationParameterTypes9);

			MethodHandler methodHandler = new MethodHandler(methodKey, userId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.model.User updateUserInfo(
		HttpPrincipal httpPrincipal, long userId, java.lang.String screenName,
		java.lang.String firstName, java.lang.String emailAddress,
		java.lang.String phone, int year, int month, int day,
		java.lang.String country) {
		try {
			MethodKey methodKey = new MethodKey(ApiServiceUtil.class,
					"updateUserInfo", _updateUserInfoParameterTypes10);

			MethodHandler methodHandler = new MethodHandler(methodKey, userId,
					screenName, firstName, emailAddress, phone, year, month,
					day, country);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.model.User)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(ApiServiceHttp.class);
	private static final Class<?>[] _getApiVersionParameterTypes0 = new Class[] {  };
	private static final Class<?>[] _getCategoriesOfCompanyParameterTypes1 = new Class[] {
			
		};
	private static final Class<?>[] _getSubCategoriesOfCategoryParameterTypes2 = new Class[] {
			long.class
		};
	private static final Class<?>[] _getLevelOfSubcategoryParameterTypes3 = new Class[] {
			long.class
		};
	private static final Class<?>[] _getTestQuestionOfLevelParameterTypes4 = new Class[] {
			long.class
		};
	private static final Class<?>[] _sendTestResultOfUserParameterTypes5 = new Class[] {
			java.lang.String.class
		};
	private static final Class<?>[] _reportQuestionParameterTypes6 = new Class[] {
			long.class, java.lang.String.class
		};
	private static final Class<?>[] _getUserExtraDataInformationParameterTypes9 = new Class[] {
			long.class
		};
	private static final Class<?>[] _updateUserInfoParameterTypes10 = new Class[] {
			long.class, java.lang.String.class, java.lang.String.class,
			java.lang.String.class, java.lang.String.class, int.class, int.class,
			int.class, java.lang.String.class
		};
}