/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.base;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.UserTestQuestionResult;

import com.devshaker.service.UserTestQuestionResultLocalService;
import com.devshaker.service.persistence.BadgePersistence;
import com.devshaker.service.persistence.BadgeRulePersistence;
import com.devshaker.service.persistence.CategoryPersistence;
import com.devshaker.service.persistence.LevelPersistence;
import com.devshaker.service.persistence.ReportedQuestionsPersistence;
import com.devshaker.service.persistence.RulePersistence;
import com.devshaker.service.persistence.SubcategoryPersistence;
import com.devshaker.service.persistence.TestQuestionPersistence;
import com.devshaker.service.persistence.UserBadgePersistence;
import com.devshaker.service.persistence.UserTestQuestionResultPersistence;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.dao.db.DB;
import com.liferay.portal.kernel.dao.db.DBManagerUtil;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DefaultActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.module.framework.service.IdentifiableOSGiService;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalServiceImpl;
import com.liferay.portal.kernel.service.PersistedModelLocalServiceRegistry;
import com.liferay.portal.kernel.service.persistence.ClassNamePersistence;
import com.liferay.portal.kernel.service.persistence.UserPersistence;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.List;

import javax.sql.DataSource;

/**
 * Provides the base implementation for the user test question result local service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link com.devshaker.service.impl.UserTestQuestionResultLocalServiceImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.devshaker.service.impl.UserTestQuestionResultLocalServiceImpl
 * @see com.devshaker.service.UserTestQuestionResultLocalServiceUtil
 * @generated
 */
@ProviderType
public abstract class UserTestQuestionResultLocalServiceBaseImpl
	extends BaseLocalServiceImpl implements UserTestQuestionResultLocalService,
		IdentifiableOSGiService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link com.devshaker.service.UserTestQuestionResultLocalServiceUtil} to access the user test question result local service.
	 */

	/**
	 * Adds the user test question result to the database. Also notifies the appropriate model listeners.
	 *
	 * @param userTestQuestionResult the user test question result
	 * @return the user test question result that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public UserTestQuestionResult addUserTestQuestionResult(
		UserTestQuestionResult userTestQuestionResult) {
		userTestQuestionResult.setNew(true);

		return userTestQuestionResultPersistence.update(userTestQuestionResult);
	}

	/**
	 * Creates a new user test question result with the primary key. Does not add the user test question result to the database.
	 *
	 * @param userTestQuestionResultId the primary key for the new user test question result
	 * @return the new user test question result
	 */
	@Override
	public UserTestQuestionResult createUserTestQuestionResult(
		long userTestQuestionResultId) {
		return userTestQuestionResultPersistence.create(userTestQuestionResultId);
	}

	/**
	 * Deletes the user test question result with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userTestQuestionResultId the primary key of the user test question result
	 * @return the user test question result that was removed
	 * @throws PortalException if a user test question result with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public UserTestQuestionResult deleteUserTestQuestionResult(
		long userTestQuestionResultId) throws PortalException {
		return userTestQuestionResultPersistence.remove(userTestQuestionResultId);
	}

	/**
	 * Deletes the user test question result from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userTestQuestionResult the user test question result
	 * @return the user test question result that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public UserTestQuestionResult deleteUserTestQuestionResult(
		UserTestQuestionResult userTestQuestionResult) {
		return userTestQuestionResultPersistence.remove(userTestQuestionResult);
	}

	@Override
	public DynamicQuery dynamicQuery() {
		Class<?> clazz = getClass();

		return DynamicQueryFactoryUtil.forClass(UserTestQuestionResult.class,
			clazz.getClassLoader());
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return userTestQuestionResultPersistence.findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end) {
		return userTestQuestionResultPersistence.findWithDynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator) {
		return userTestQuestionResultPersistence.findWithDynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return userTestQuestionResultPersistence.countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection) {
		return userTestQuestionResultPersistence.countWithDynamicQuery(dynamicQuery,
			projection);
	}

	@Override
	public UserTestQuestionResult fetchUserTestQuestionResult(
		long userTestQuestionResultId) {
		return userTestQuestionResultPersistence.fetchByPrimaryKey(userTestQuestionResultId);
	}

	/**
	 * Returns the user test question result with the primary key.
	 *
	 * @param userTestQuestionResultId the primary key of the user test question result
	 * @return the user test question result
	 * @throws PortalException if a user test question result with the primary key could not be found
	 */
	@Override
	public UserTestQuestionResult getUserTestQuestionResult(
		long userTestQuestionResultId) throws PortalException {
		return userTestQuestionResultPersistence.findByPrimaryKey(userTestQuestionResultId);
	}

	@Override
	public ActionableDynamicQuery getActionableDynamicQuery() {
		ActionableDynamicQuery actionableDynamicQuery = new DefaultActionableDynamicQuery();

		actionableDynamicQuery.setBaseLocalService(userTestQuestionResultLocalService);
		actionableDynamicQuery.setClassLoader(getClassLoader());
		actionableDynamicQuery.setModelClass(UserTestQuestionResult.class);

		actionableDynamicQuery.setPrimaryKeyPropertyName(
			"userTestQuestionResultId");

		return actionableDynamicQuery;
	}

	@Override
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		IndexableActionableDynamicQuery indexableActionableDynamicQuery = new IndexableActionableDynamicQuery();

		indexableActionableDynamicQuery.setBaseLocalService(userTestQuestionResultLocalService);
		indexableActionableDynamicQuery.setClassLoader(getClassLoader());
		indexableActionableDynamicQuery.setModelClass(UserTestQuestionResult.class);

		indexableActionableDynamicQuery.setPrimaryKeyPropertyName(
			"userTestQuestionResultId");

		return indexableActionableDynamicQuery;
	}

	protected void initActionableDynamicQuery(
		ActionableDynamicQuery actionableDynamicQuery) {
		actionableDynamicQuery.setBaseLocalService(userTestQuestionResultLocalService);
		actionableDynamicQuery.setClassLoader(getClassLoader());
		actionableDynamicQuery.setModelClass(UserTestQuestionResult.class);

		actionableDynamicQuery.setPrimaryKeyPropertyName(
			"userTestQuestionResultId");
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException {
		return userTestQuestionResultLocalService.deleteUserTestQuestionResult((UserTestQuestionResult)persistedModel);
	}

	@Override
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {
		return userTestQuestionResultPersistence.findByPrimaryKey(primaryKeyObj);
	}

	/**
	 * Returns a range of all the user test question results.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user test question results
	 * @param end the upper bound of the range of user test question results (not inclusive)
	 * @return the range of user test question results
	 */
	@Override
	public List<UserTestQuestionResult> getUserTestQuestionResults(int start,
		int end) {
		return userTestQuestionResultPersistence.findAll(start, end);
	}

	/**
	 * Returns the number of user test question results.
	 *
	 * @return the number of user test question results
	 */
	@Override
	public int getUserTestQuestionResultsCount() {
		return userTestQuestionResultPersistence.countAll();
	}

	/**
	 * Updates the user test question result in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param userTestQuestionResult the user test question result
	 * @return the user test question result that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public UserTestQuestionResult updateUserTestQuestionResult(
		UserTestQuestionResult userTestQuestionResult) {
		return userTestQuestionResultPersistence.update(userTestQuestionResult);
	}

	/**
	 * Returns the api local service.
	 *
	 * @return the api local service
	 */
	public com.devshaker.service.ApiLocalService getApiLocalService() {
		return apiLocalService;
	}

	/**
	 * Sets the api local service.
	 *
	 * @param apiLocalService the api local service
	 */
	public void setApiLocalService(
		com.devshaker.service.ApiLocalService apiLocalService) {
		this.apiLocalService = apiLocalService;
	}

	/**
	 * Returns the badge local service.
	 *
	 * @return the badge local service
	 */
	public com.devshaker.service.BadgeLocalService getBadgeLocalService() {
		return badgeLocalService;
	}

	/**
	 * Sets the badge local service.
	 *
	 * @param badgeLocalService the badge local service
	 */
	public void setBadgeLocalService(
		com.devshaker.service.BadgeLocalService badgeLocalService) {
		this.badgeLocalService = badgeLocalService;
	}

	/**
	 * Returns the badge persistence.
	 *
	 * @return the badge persistence
	 */
	public BadgePersistence getBadgePersistence() {
		return badgePersistence;
	}

	/**
	 * Sets the badge persistence.
	 *
	 * @param badgePersistence the badge persistence
	 */
	public void setBadgePersistence(BadgePersistence badgePersistence) {
		this.badgePersistence = badgePersistence;
	}

	/**
	 * Returns the badge rule local service.
	 *
	 * @return the badge rule local service
	 */
	public com.devshaker.service.BadgeRuleLocalService getBadgeRuleLocalService() {
		return badgeRuleLocalService;
	}

	/**
	 * Sets the badge rule local service.
	 *
	 * @param badgeRuleLocalService the badge rule local service
	 */
	public void setBadgeRuleLocalService(
		com.devshaker.service.BadgeRuleLocalService badgeRuleLocalService) {
		this.badgeRuleLocalService = badgeRuleLocalService;
	}

	/**
	 * Returns the badge rule persistence.
	 *
	 * @return the badge rule persistence
	 */
	public BadgeRulePersistence getBadgeRulePersistence() {
		return badgeRulePersistence;
	}

	/**
	 * Sets the badge rule persistence.
	 *
	 * @param badgeRulePersistence the badge rule persistence
	 */
	public void setBadgeRulePersistence(
		BadgeRulePersistence badgeRulePersistence) {
		this.badgeRulePersistence = badgeRulePersistence;
	}

	/**
	 * Returns the category local service.
	 *
	 * @return the category local service
	 */
	public com.devshaker.service.CategoryLocalService getCategoryLocalService() {
		return categoryLocalService;
	}

	/**
	 * Sets the category local service.
	 *
	 * @param categoryLocalService the category local service
	 */
	public void setCategoryLocalService(
		com.devshaker.service.CategoryLocalService categoryLocalService) {
		this.categoryLocalService = categoryLocalService;
	}

	/**
	 * Returns the category persistence.
	 *
	 * @return the category persistence
	 */
	public CategoryPersistence getCategoryPersistence() {
		return categoryPersistence;
	}

	/**
	 * Sets the category persistence.
	 *
	 * @param categoryPersistence the category persistence
	 */
	public void setCategoryPersistence(CategoryPersistence categoryPersistence) {
		this.categoryPersistence = categoryPersistence;
	}

	/**
	 * Returns the dev utils api local service.
	 *
	 * @return the dev utils api local service
	 */
	public com.devshaker.service.DevUtilsApiLocalService getDevUtilsApiLocalService() {
		return devUtilsApiLocalService;
	}

	/**
	 * Sets the dev utils api local service.
	 *
	 * @param devUtilsApiLocalService the dev utils api local service
	 */
	public void setDevUtilsApiLocalService(
		com.devshaker.service.DevUtilsApiLocalService devUtilsApiLocalService) {
		this.devUtilsApiLocalService = devUtilsApiLocalService;
	}

	/**
	 * Returns the level local service.
	 *
	 * @return the level local service
	 */
	public com.devshaker.service.LevelLocalService getLevelLocalService() {
		return levelLocalService;
	}

	/**
	 * Sets the level local service.
	 *
	 * @param levelLocalService the level local service
	 */
	public void setLevelLocalService(
		com.devshaker.service.LevelLocalService levelLocalService) {
		this.levelLocalService = levelLocalService;
	}

	/**
	 * Returns the level persistence.
	 *
	 * @return the level persistence
	 */
	public LevelPersistence getLevelPersistence() {
		return levelPersistence;
	}

	/**
	 * Sets the level persistence.
	 *
	 * @param levelPersistence the level persistence
	 */
	public void setLevelPersistence(LevelPersistence levelPersistence) {
		this.levelPersistence = levelPersistence;
	}

	/**
	 * Returns the reported questions local service.
	 *
	 * @return the reported questions local service
	 */
	public com.devshaker.service.ReportedQuestionsLocalService getReportedQuestionsLocalService() {
		return reportedQuestionsLocalService;
	}

	/**
	 * Sets the reported questions local service.
	 *
	 * @param reportedQuestionsLocalService the reported questions local service
	 */
	public void setReportedQuestionsLocalService(
		com.devshaker.service.ReportedQuestionsLocalService reportedQuestionsLocalService) {
		this.reportedQuestionsLocalService = reportedQuestionsLocalService;
	}

	/**
	 * Returns the reported questions persistence.
	 *
	 * @return the reported questions persistence
	 */
	public ReportedQuestionsPersistence getReportedQuestionsPersistence() {
		return reportedQuestionsPersistence;
	}

	/**
	 * Sets the reported questions persistence.
	 *
	 * @param reportedQuestionsPersistence the reported questions persistence
	 */
	public void setReportedQuestionsPersistence(
		ReportedQuestionsPersistence reportedQuestionsPersistence) {
		this.reportedQuestionsPersistence = reportedQuestionsPersistence;
	}

	/**
	 * Returns the rule local service.
	 *
	 * @return the rule local service
	 */
	public com.devshaker.service.RuleLocalService getRuleLocalService() {
		return ruleLocalService;
	}

	/**
	 * Sets the rule local service.
	 *
	 * @param ruleLocalService the rule local service
	 */
	public void setRuleLocalService(
		com.devshaker.service.RuleLocalService ruleLocalService) {
		this.ruleLocalService = ruleLocalService;
	}

	/**
	 * Returns the rule persistence.
	 *
	 * @return the rule persistence
	 */
	public RulePersistence getRulePersistence() {
		return rulePersistence;
	}

	/**
	 * Sets the rule persistence.
	 *
	 * @param rulePersistence the rule persistence
	 */
	public void setRulePersistence(RulePersistence rulePersistence) {
		this.rulePersistence = rulePersistence;
	}

	/**
	 * Returns the subcategory local service.
	 *
	 * @return the subcategory local service
	 */
	public com.devshaker.service.SubcategoryLocalService getSubcategoryLocalService() {
		return subcategoryLocalService;
	}

	/**
	 * Sets the subcategory local service.
	 *
	 * @param subcategoryLocalService the subcategory local service
	 */
	public void setSubcategoryLocalService(
		com.devshaker.service.SubcategoryLocalService subcategoryLocalService) {
		this.subcategoryLocalService = subcategoryLocalService;
	}

	/**
	 * Returns the subcategory persistence.
	 *
	 * @return the subcategory persistence
	 */
	public SubcategoryPersistence getSubcategoryPersistence() {
		return subcategoryPersistence;
	}

	/**
	 * Sets the subcategory persistence.
	 *
	 * @param subcategoryPersistence the subcategory persistence
	 */
	public void setSubcategoryPersistence(
		SubcategoryPersistence subcategoryPersistence) {
		this.subcategoryPersistence = subcategoryPersistence;
	}

	/**
	 * Returns the test question local service.
	 *
	 * @return the test question local service
	 */
	public com.devshaker.service.TestQuestionLocalService getTestQuestionLocalService() {
		return testQuestionLocalService;
	}

	/**
	 * Sets the test question local service.
	 *
	 * @param testQuestionLocalService the test question local service
	 */
	public void setTestQuestionLocalService(
		com.devshaker.service.TestQuestionLocalService testQuestionLocalService) {
		this.testQuestionLocalService = testQuestionLocalService;
	}

	/**
	 * Returns the test question persistence.
	 *
	 * @return the test question persistence
	 */
	public TestQuestionPersistence getTestQuestionPersistence() {
		return testQuestionPersistence;
	}

	/**
	 * Sets the test question persistence.
	 *
	 * @param testQuestionPersistence the test question persistence
	 */
	public void setTestQuestionPersistence(
		TestQuestionPersistence testQuestionPersistence) {
		this.testQuestionPersistence = testQuestionPersistence;
	}

	/**
	 * Returns the user badge local service.
	 *
	 * @return the user badge local service
	 */
	public com.devshaker.service.UserBadgeLocalService getUserBadgeLocalService() {
		return userBadgeLocalService;
	}

	/**
	 * Sets the user badge local service.
	 *
	 * @param userBadgeLocalService the user badge local service
	 */
	public void setUserBadgeLocalService(
		com.devshaker.service.UserBadgeLocalService userBadgeLocalService) {
		this.userBadgeLocalService = userBadgeLocalService;
	}

	/**
	 * Returns the user badge persistence.
	 *
	 * @return the user badge persistence
	 */
	public UserBadgePersistence getUserBadgePersistence() {
		return userBadgePersistence;
	}

	/**
	 * Sets the user badge persistence.
	 *
	 * @param userBadgePersistence the user badge persistence
	 */
	public void setUserBadgePersistence(
		UserBadgePersistence userBadgePersistence) {
		this.userBadgePersistence = userBadgePersistence;
	}

	/**
	 * Returns the user test question result local service.
	 *
	 * @return the user test question result local service
	 */
	public UserTestQuestionResultLocalService getUserTestQuestionResultLocalService() {
		return userTestQuestionResultLocalService;
	}

	/**
	 * Sets the user test question result local service.
	 *
	 * @param userTestQuestionResultLocalService the user test question result local service
	 */
	public void setUserTestQuestionResultLocalService(
		UserTestQuestionResultLocalService userTestQuestionResultLocalService) {
		this.userTestQuestionResultLocalService = userTestQuestionResultLocalService;
	}

	/**
	 * Returns the user test question result persistence.
	 *
	 * @return the user test question result persistence
	 */
	public UserTestQuestionResultPersistence getUserTestQuestionResultPersistence() {
		return userTestQuestionResultPersistence;
	}

	/**
	 * Sets the user test question result persistence.
	 *
	 * @param userTestQuestionResultPersistence the user test question result persistence
	 */
	public void setUserTestQuestionResultPersistence(
		UserTestQuestionResultPersistence userTestQuestionResultPersistence) {
		this.userTestQuestionResultPersistence = userTestQuestionResultPersistence;
	}

	/**
	 * Returns the counter local service.
	 *
	 * @return the counter local service
	 */
	public com.liferay.counter.kernel.service.CounterLocalService getCounterLocalService() {
		return counterLocalService;
	}

	/**
	 * Sets the counter local service.
	 *
	 * @param counterLocalService the counter local service
	 */
	public void setCounterLocalService(
		com.liferay.counter.kernel.service.CounterLocalService counterLocalService) {
		this.counterLocalService = counterLocalService;
	}

	/**
	 * Returns the class name local service.
	 *
	 * @return the class name local service
	 */
	public com.liferay.portal.kernel.service.ClassNameLocalService getClassNameLocalService() {
		return classNameLocalService;
	}

	/**
	 * Sets the class name local service.
	 *
	 * @param classNameLocalService the class name local service
	 */
	public void setClassNameLocalService(
		com.liferay.portal.kernel.service.ClassNameLocalService classNameLocalService) {
		this.classNameLocalService = classNameLocalService;
	}

	/**
	 * Returns the class name persistence.
	 *
	 * @return the class name persistence
	 */
	public ClassNamePersistence getClassNamePersistence() {
		return classNamePersistence;
	}

	/**
	 * Sets the class name persistence.
	 *
	 * @param classNamePersistence the class name persistence
	 */
	public void setClassNamePersistence(
		ClassNamePersistence classNamePersistence) {
		this.classNamePersistence = classNamePersistence;
	}

	/**
	 * Returns the resource local service.
	 *
	 * @return the resource local service
	 */
	public com.liferay.portal.kernel.service.ResourceLocalService getResourceLocalService() {
		return resourceLocalService;
	}

	/**
	 * Sets the resource local service.
	 *
	 * @param resourceLocalService the resource local service
	 */
	public void setResourceLocalService(
		com.liferay.portal.kernel.service.ResourceLocalService resourceLocalService) {
		this.resourceLocalService = resourceLocalService;
	}

	/**
	 * Returns the user local service.
	 *
	 * @return the user local service
	 */
	public com.liferay.portal.kernel.service.UserLocalService getUserLocalService() {
		return userLocalService;
	}

	/**
	 * Sets the user local service.
	 *
	 * @param userLocalService the user local service
	 */
	public void setUserLocalService(
		com.liferay.portal.kernel.service.UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	/**
	 * Returns the user persistence.
	 *
	 * @return the user persistence
	 */
	public UserPersistence getUserPersistence() {
		return userPersistence;
	}

	/**
	 * Sets the user persistence.
	 *
	 * @param userPersistence the user persistence
	 */
	public void setUserPersistence(UserPersistence userPersistence) {
		this.userPersistence = userPersistence;
	}

	public void afterPropertiesSet() {
		persistedModelLocalServiceRegistry.register("com.devshaker.model.UserTestQuestionResult",
			userTestQuestionResultLocalService);
	}

	public void destroy() {
		persistedModelLocalServiceRegistry.unregister(
			"com.devshaker.model.UserTestQuestionResult");
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return UserTestQuestionResultLocalService.class.getName();
	}

	protected Class<?> getModelClass() {
		return UserTestQuestionResult.class;
	}

	protected String getModelClassName() {
		return UserTestQuestionResult.class.getName();
	}

	/**
	 * Performs a SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) {
		try {
			DataSource dataSource = userTestQuestionResultPersistence.getDataSource();

			DB db = DBManagerUtil.getDB();

			sql = db.buildSQL(sql);
			sql = PortalUtil.transformSQL(sql);

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(dataSource,
					sql);

			sqlUpdate.update();
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@BeanReference(type = com.devshaker.service.ApiLocalService.class)
	protected com.devshaker.service.ApiLocalService apiLocalService;
	@BeanReference(type = com.devshaker.service.BadgeLocalService.class)
	protected com.devshaker.service.BadgeLocalService badgeLocalService;
	@BeanReference(type = BadgePersistence.class)
	protected BadgePersistence badgePersistence;
	@BeanReference(type = com.devshaker.service.BadgeRuleLocalService.class)
	protected com.devshaker.service.BadgeRuleLocalService badgeRuleLocalService;
	@BeanReference(type = BadgeRulePersistence.class)
	protected BadgeRulePersistence badgeRulePersistence;
	@BeanReference(type = com.devshaker.service.CategoryLocalService.class)
	protected com.devshaker.service.CategoryLocalService categoryLocalService;
	@BeanReference(type = CategoryPersistence.class)
	protected CategoryPersistence categoryPersistence;
	@BeanReference(type = com.devshaker.service.DevUtilsApiLocalService.class)
	protected com.devshaker.service.DevUtilsApiLocalService devUtilsApiLocalService;
	@BeanReference(type = com.devshaker.service.LevelLocalService.class)
	protected com.devshaker.service.LevelLocalService levelLocalService;
	@BeanReference(type = LevelPersistence.class)
	protected LevelPersistence levelPersistence;
	@BeanReference(type = com.devshaker.service.ReportedQuestionsLocalService.class)
	protected com.devshaker.service.ReportedQuestionsLocalService reportedQuestionsLocalService;
	@BeanReference(type = ReportedQuestionsPersistence.class)
	protected ReportedQuestionsPersistence reportedQuestionsPersistence;
	@BeanReference(type = com.devshaker.service.RuleLocalService.class)
	protected com.devshaker.service.RuleLocalService ruleLocalService;
	@BeanReference(type = RulePersistence.class)
	protected RulePersistence rulePersistence;
	@BeanReference(type = com.devshaker.service.SubcategoryLocalService.class)
	protected com.devshaker.service.SubcategoryLocalService subcategoryLocalService;
	@BeanReference(type = SubcategoryPersistence.class)
	protected SubcategoryPersistence subcategoryPersistence;
	@BeanReference(type = com.devshaker.service.TestQuestionLocalService.class)
	protected com.devshaker.service.TestQuestionLocalService testQuestionLocalService;
	@BeanReference(type = TestQuestionPersistence.class)
	protected TestQuestionPersistence testQuestionPersistence;
	@BeanReference(type = com.devshaker.service.UserBadgeLocalService.class)
	protected com.devshaker.service.UserBadgeLocalService userBadgeLocalService;
	@BeanReference(type = UserBadgePersistence.class)
	protected UserBadgePersistence userBadgePersistence;
	@BeanReference(type = UserTestQuestionResultLocalService.class)
	protected UserTestQuestionResultLocalService userTestQuestionResultLocalService;
	@BeanReference(type = UserTestQuestionResultPersistence.class)
	protected UserTestQuestionResultPersistence userTestQuestionResultPersistence;
	@ServiceReference(type = com.liferay.counter.kernel.service.CounterLocalService.class)
	protected com.liferay.counter.kernel.service.CounterLocalService counterLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.ClassNameLocalService.class)
	protected com.liferay.portal.kernel.service.ClassNameLocalService classNameLocalService;
	@ServiceReference(type = ClassNamePersistence.class)
	protected ClassNamePersistence classNamePersistence;
	@ServiceReference(type = com.liferay.portal.kernel.service.ResourceLocalService.class)
	protected com.liferay.portal.kernel.service.ResourceLocalService resourceLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.UserLocalService.class)
	protected com.liferay.portal.kernel.service.UserLocalService userLocalService;
	@ServiceReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	@ServiceReference(type = PersistedModelLocalServiceRegistry.class)
	protected PersistedModelLocalServiceRegistry persistedModelLocalServiceRegistry;
}