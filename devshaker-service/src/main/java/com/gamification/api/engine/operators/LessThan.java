package com.gamification.api.engine.operators;
import java.util.Map;
import java.util.Stack;

import com.gamification.api.engine.BaseType;
import com.gamification.api.engine.Expression;
import com.gamification.api.engine.Variable;



public class LessThan extends Operation {

	public LessThan() {
		super("<");
	}

	@Override
	public boolean interpret(Map<String, ?> bindings) {
		// TODO Auto-generated method stub
		Variable v = (Variable) this.leftOperand;
		Object obj = bindings.get(v.getName());
		if (obj == null)
			return false;
		BaseType<?> type = (BaseType<?>) this.rightOperand;
		if (type.getType().equals(obj.getClass())) {			
			if(obj instanceof Integer){
				if ((int) obj < Integer.parseInt(type.getValue().toString()))
					return true;
			}
		}
		return false;
	}

	@Override
	public Operation copy() {
		// TODO Auto-generated method stub
		return new LessThan();
	}

	@Override
	public int parse(String[] tokens, int pos, Stack<Expression> stack) {
		// TODO Auto-generated method stub
		if (pos - 1 >= 0 && tokens.length >= pos + 1) {
			String var = tokens[pos - 1];

			this.leftOperand = new Variable(var);
			this.rightOperand = BaseType.getBaseType(tokens[pos + 1]);
			stack.push(this);

			return pos + 1;
		}
		throw new IllegalArgumentException("Cannot assign value to variable");
	}

}
