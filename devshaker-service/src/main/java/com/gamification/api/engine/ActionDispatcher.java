package com.gamification.api.engine;

public interface ActionDispatcher {
    public void fire();

}
