package com.gamification.api.engine;

import java.util.Calendar;

import com.devshaker.model.UserBadge;
import com.devshaker.service.UserBadgeLocalServiceUtil;
import com.devshaker.service.persistence.UserBadgePK;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;

public class BadgeAssociationActionDispatcher implements ActionDispatcher
{
	
	private long  userId;
	private long badgeId;

	@Override
	public void fire() {
		// TODO Auto-generated method stub
        System.out.println("Asociando badgeId "+badgeId+" a usuario "+userId);
        UserBadgePK ubPk = new UserBadgePK(userId, badgeId);
        User u;
		try {
			u = UserLocalServiceUtil.getUser(userId);
			UserBadge ub =  UserBadgeLocalServiceUtil.createUserBadge(ubPk);
	        ub.setCreateDate(Calendar.getInstance().getTime());
	        ub.setCompanyId(u.getCompanyId());
	        UserBadgeLocalServiceUtil.updateUserBadge(ub);
	        System.out.println("FINALLY ASSOCIATED ");
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	
	public BadgeAssociationActionDispatcher(long badgeId, long userId){
		this.badgeId = badgeId;
		this.userId = userId;
	}

}
