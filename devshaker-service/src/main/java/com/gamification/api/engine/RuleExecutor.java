package com.gamification.api.engine;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.gamification.api.engine.operators.And;
import com.gamification.api.engine.operators.Equals;
import com.gamification.api.engine.operators.GreatEqualsThan;
import com.gamification.api.engine.operators.GreatThan;
import com.gamification.api.engine.operators.LessEqualsThan;
import com.gamification.api.engine.operators.LessThan;
import com.gamification.api.engine.operators.Not;
import com.gamification.api.engine.operators.Operations;



public class RuleExecutor{

	public static void executor(List<com.devshaker.model.Rule> rulesOfBadge, HashMap<String,Object> bind, long badgeIdOfRules, long userId) {
		// TODO Auto-generated method stub
		// create a singleton container for operations
        Operations operations = Operations.INSTANCE;
        System.out.println("DENTRO DE excutor");
        //CORRECT_ANSWER == 3 AND LEVEL > 1 AND LEVEL < 3
        // register new operations with the previously created container
        operations.registerOperation(new And());
        operations.registerOperation(new Equals());
        operations.registerOperation(new Not());
        operations.registerOperation(new GreatThan());
        operations.registerOperation(new LessThan());
        operations.registerOperation(new GreatEqualsThan());
        operations.registerOperation(new LessEqualsThan());
        
        StringBuilder ruleStr = new StringBuilder();
        for(com.devshaker.model.Rule rule : rulesOfBadge){
        	String aux [] = rule.getRuleDesc().split("@");
        	ruleStr.append(aux[1]+" AND ");
        }
        
        String ruleResult = ruleStr.substring(0, ruleStr.length()-5);
        
        System.out.println("ruleResult "+ruleResult);
        
        
        Expression ex1 = ExpressionParser.fromString(ruleResult);
        // define the possible actions for rules that fire
        ActionDispatcher inPatient = new BadgeAssociationActionDispatcher(badgeIdOfRules,userId);
        Rule rule1 = new Rule.Builder().withExpression(ex1).withDispatcher(inPatient).build();
        // for test purpose define a variable binding ...
        /*Map<String, Object> bindings = new HashMap<>();
        bindings.put("CORRECT_ANSWER", 5);
        bindings.put("LEVEL", 3);*/
        // ... and evaluate the defined rules with the specified bindings
        boolean triggered = rule1.eval(bind);
        System.out.println("Action triggered: "+triggered);
	}  
	

}
