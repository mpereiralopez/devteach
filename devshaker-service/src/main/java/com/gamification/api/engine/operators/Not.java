package com.gamification.api.engine.operators;
import java.util.Map;
import java.util.Stack;

import com.gamification.api.engine.Expression;

public class Not extends Operation {

	public Not() {
		super("NOT");
	}

	@Override
	public boolean interpret(Map<String, ?> bindings) {
		// TODO Auto-generated method stub
		return !this.rightOperand.interpret(bindings);
	}

	@Override
	public Operation copy() {
		// TODO Auto-generated method stub
		return new Not();
	}

	@Override
	public int parse(String[] tokens, int pos, Stack<Expression> stack) {
		// TODO Auto-generated method stub
		int i = findNextExpression(tokens, pos + 1, stack);
		Expression right = stack.pop();

		this.rightOperand = right;
		stack.push(this);

		return i;
	}

}
