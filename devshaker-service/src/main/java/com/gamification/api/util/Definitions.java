package com.gamification.api.util;

public class Definitions {
	
	public static final String PREDICATE_CATEGORY = "CATEGORY";
	public static final String PREDICATE_SUBCATEGORY = "SUBCATEGORY";
	public static final String PREDICATE_LEVEL = "LEVEL";
	public static final String PREDICATE_CHALLENGE = "CHALLENGE";
	public static final String PREDICATE_FRIEND = "FRIEND";
	public static final String PREDICATE_RECRUITER = "RECRUITER";
	public static final String PREDICATE_PROFILE = "PROFILE";
	public static final String PREDICATE_BADGE = "BADGE";
	public static final String PREDICATE_ACHIEVEMENT = "ACHIEVEMENT";
	public static final String PREDICATE_QUESTION = "QUESTION";
	
	public static final String QUANTIFICATOR_CORRECT_ANSWER = "CORRECT_ANSWER";
	public static final String QUANTIFICATOR_XP = "XP";

	public static final String CONDITION_AND = "AND";
	public static final String CONDITION_OR = "OR";
	public static final String CONDITION_EQUAL = "==";
	public static final String CONDITION_LT = "<";
	public static final String CONDITION_GT = ">";
	public static final String CONDITION_LET = "<=";
	public static final String CONDITION_GET = ">=";

	public static final String PREREQUISITE = "PRERULE";
	public static final String REPEAT = "REPEAT";

	public static final String ACTION_START = "START";
	public static final String ACTION_COMPLETE = "COMPLETE";
	public static final String ACTION_SEE = "SEE";
	public static final String ACTION_UNLOCK = "UNLOCK";
	public static final String ACTION_UPLOAD = "UPLOAD";
	public static final String ACTION_ACCEPT = "ACCEPT";
	public static final String ACTION_INVITE = "INVITE";
	public static final String ACTION_RANKED = "RANKED";
	public static final String ACTION_SEEN = "BEEN_SEEN";
	
	public static final String METRIC_SCORE = "SCORE";

	
	
}
