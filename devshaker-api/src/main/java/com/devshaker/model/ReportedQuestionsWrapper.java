/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ReportedQuestions}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ReportedQuestions
 * @generated
 */
@ProviderType
public class ReportedQuestionsWrapper implements ReportedQuestions,
	ModelWrapper<ReportedQuestions> {
	public ReportedQuestionsWrapper(ReportedQuestions reportedQuestions) {
		_reportedQuestions = reportedQuestions;
	}

	@Override
	public Class<?> getModelClass() {
		return ReportedQuestions.class;
	}

	@Override
	public String getModelClassName() {
		return ReportedQuestions.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("reportId", getReportId());
		attributes.put("companyId", getCompanyId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("questionId", getQuestionId());
		attributes.put("userId", getUserId());
		attributes.put("reportContent", getReportContent());
		attributes.put("reportStatus", getReportStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long reportId = (Long)attributes.get("reportId");

		if (reportId != null) {
			setReportId(reportId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long questionId = (Long)attributes.get("questionId");

		if (questionId != null) {
			setQuestionId(questionId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String reportContent = (String)attributes.get("reportContent");

		if (reportContent != null) {
			setReportContent(reportContent);
		}

		String reportStatus = (String)attributes.get("reportStatus");

		if (reportStatus != null) {
			setReportStatus(reportStatus);
		}
	}

	@Override
	public ReportedQuestions toEscapedModel() {
		return new ReportedQuestionsWrapper(_reportedQuestions.toEscapedModel());
	}

	@Override
	public ReportedQuestions toUnescapedModel() {
		return new ReportedQuestionsWrapper(_reportedQuestions.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _reportedQuestions.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _reportedQuestions.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _reportedQuestions.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _reportedQuestions.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ReportedQuestions> toCacheModel() {
		return _reportedQuestions.toCacheModel();
	}

	@Override
	public int compareTo(ReportedQuestions reportedQuestions) {
		return _reportedQuestions.compareTo(reportedQuestions);
	}

	@Override
	public int hashCode() {
		return _reportedQuestions.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _reportedQuestions.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ReportedQuestionsWrapper((ReportedQuestions)_reportedQuestions.clone());
	}

	/**
	* Returns the report content of this reported questions.
	*
	* @return the report content of this reported questions
	*/
	@Override
	public java.lang.String getReportContent() {
		return _reportedQuestions.getReportContent();
	}

	/**
	* Returns the report status of this reported questions.
	*
	* @return the report status of this reported questions
	*/
	@Override
	public java.lang.String getReportStatus() {
		return _reportedQuestions.getReportStatus();
	}

	/**
	* Returns the user uuid of this reported questions.
	*
	* @return the user uuid of this reported questions
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _reportedQuestions.getUserUuid();
	}

	@Override
	public java.lang.String toString() {
		return _reportedQuestions.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _reportedQuestions.toXmlString();
	}

	/**
	* Returns the create date of this reported questions.
	*
	* @return the create date of this reported questions
	*/
	@Override
	public Date getCreateDate() {
		return _reportedQuestions.getCreateDate();
	}

	/**
	* Returns the modified date of this reported questions.
	*
	* @return the modified date of this reported questions
	*/
	@Override
	public Date getModifiedDate() {
		return _reportedQuestions.getModifiedDate();
	}

	/**
	* Returns the company ID of this reported questions.
	*
	* @return the company ID of this reported questions
	*/
	@Override
	public long getCompanyId() {
		return _reportedQuestions.getCompanyId();
	}

	/**
	* Returns the primary key of this reported questions.
	*
	* @return the primary key of this reported questions
	*/
	@Override
	public long getPrimaryKey() {
		return _reportedQuestions.getPrimaryKey();
	}

	/**
	* Returns the question ID of this reported questions.
	*
	* @return the question ID of this reported questions
	*/
	@Override
	public long getQuestionId() {
		return _reportedQuestions.getQuestionId();
	}

	/**
	* Returns the report ID of this reported questions.
	*
	* @return the report ID of this reported questions
	*/
	@Override
	public long getReportId() {
		return _reportedQuestions.getReportId();
	}

	/**
	* Returns the user ID of this reported questions.
	*
	* @return the user ID of this reported questions
	*/
	@Override
	public long getUserId() {
		return _reportedQuestions.getUserId();
	}

	@Override
	public void persist() {
		_reportedQuestions.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_reportedQuestions.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this reported questions.
	*
	* @param companyId the company ID of this reported questions
	*/
	@Override
	public void setCompanyId(long companyId) {
		_reportedQuestions.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this reported questions.
	*
	* @param createDate the create date of this reported questions
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_reportedQuestions.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_reportedQuestions.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_reportedQuestions.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_reportedQuestions.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the modified date of this reported questions.
	*
	* @param modifiedDate the modified date of this reported questions
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_reportedQuestions.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_reportedQuestions.setNew(n);
	}

	/**
	* Sets the primary key of this reported questions.
	*
	* @param primaryKey the primary key of this reported questions
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_reportedQuestions.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_reportedQuestions.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the question ID of this reported questions.
	*
	* @param questionId the question ID of this reported questions
	*/
	@Override
	public void setQuestionId(long questionId) {
		_reportedQuestions.setQuestionId(questionId);
	}

	/**
	* Sets the report content of this reported questions.
	*
	* @param reportContent the report content of this reported questions
	*/
	@Override
	public void setReportContent(java.lang.String reportContent) {
		_reportedQuestions.setReportContent(reportContent);
	}

	/**
	* Sets the report ID of this reported questions.
	*
	* @param reportId the report ID of this reported questions
	*/
	@Override
	public void setReportId(long reportId) {
		_reportedQuestions.setReportId(reportId);
	}

	/**
	* Sets the report status of this reported questions.
	*
	* @param reportStatus the report status of this reported questions
	*/
	@Override
	public void setReportStatus(java.lang.String reportStatus) {
		_reportedQuestions.setReportStatus(reportStatus);
	}

	/**
	* Sets the user ID of this reported questions.
	*
	* @param userId the user ID of this reported questions
	*/
	@Override
	public void setUserId(long userId) {
		_reportedQuestions.setUserId(userId);
	}

	/**
	* Sets the user uuid of this reported questions.
	*
	* @param userUuid the user uuid of this reported questions
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_reportedQuestions.setUserUuid(userUuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ReportedQuestionsWrapper)) {
			return false;
		}

		ReportedQuestionsWrapper reportedQuestionsWrapper = (ReportedQuestionsWrapper)obj;

		if (Objects.equals(_reportedQuestions,
					reportedQuestionsWrapper._reportedQuestions)) {
			return true;
		}

		return false;
	}

	@Override
	public ReportedQuestions getWrappedModel() {
		return _reportedQuestions;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _reportedQuestions.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _reportedQuestions.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_reportedQuestions.resetOriginalValues();
	}

	private final ReportedQuestions _reportedQuestions;
}