/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link UserBadge}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserBadge
 * @generated
 */
@ProviderType
public class UserBadgeWrapper implements UserBadge, ModelWrapper<UserBadge> {
	public UserBadgeWrapper(UserBadge userBadge) {
		_userBadge = userBadge;
	}

	@Override
	public Class<?> getModelClass() {
		return UserBadge.class;
	}

	@Override
	public String getModelClassName() {
		return UserBadge.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("userId", getUserId());
		attributes.put("badgeId", getBadgeId());
		attributes.put("companyId", getCompanyId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long badgeId = (Long)attributes.get("badgeId");

		if (badgeId != null) {
			setBadgeId(badgeId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}
	}

	@Override
	public UserBadge toEscapedModel() {
		return new UserBadgeWrapper(_userBadge.toEscapedModel());
	}

	@Override
	public UserBadge toUnescapedModel() {
		return new UserBadgeWrapper(_userBadge.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _userBadge.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _userBadge.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _userBadge.isNew();
	}

	/**
	* Returns the primary key of this user badge.
	*
	* @return the primary key of this user badge
	*/
	@Override
	public com.devshaker.service.persistence.UserBadgePK getPrimaryKey() {
		return _userBadge.getPrimaryKey();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _userBadge.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<UserBadge> toCacheModel() {
		return _userBadge.toCacheModel();
	}

	@Override
	public int compareTo(UserBadge userBadge) {
		return _userBadge.compareTo(userBadge);
	}

	@Override
	public int hashCode() {
		return _userBadge.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _userBadge.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new UserBadgeWrapper((UserBadge)_userBadge.clone());
	}

	/**
	* Returns the user uuid of this user badge.
	*
	* @return the user uuid of this user badge
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _userBadge.getUserUuid();
	}

	@Override
	public java.lang.String toString() {
		return _userBadge.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _userBadge.toXmlString();
	}

	/**
	* Returns the create date of this user badge.
	*
	* @return the create date of this user badge
	*/
	@Override
	public Date getCreateDate() {
		return _userBadge.getCreateDate();
	}

	/**
	* Returns the modified date of this user badge.
	*
	* @return the modified date of this user badge
	*/
	@Override
	public Date getModifiedDate() {
		return _userBadge.getModifiedDate();
	}

	/**
	* Returns the badge ID of this user badge.
	*
	* @return the badge ID of this user badge
	*/
	@Override
	public long getBadgeId() {
		return _userBadge.getBadgeId();
	}

	/**
	* Returns the company ID of this user badge.
	*
	* @return the company ID of this user badge
	*/
	@Override
	public long getCompanyId() {
		return _userBadge.getCompanyId();
	}

	/**
	* Returns the user ID of this user badge.
	*
	* @return the user ID of this user badge
	*/
	@Override
	public long getUserId() {
		return _userBadge.getUserId();
	}

	@Override
	public void persist() {
		_userBadge.persist();
	}

	/**
	* Sets the badge ID of this user badge.
	*
	* @param badgeId the badge ID of this user badge
	*/
	@Override
	public void setBadgeId(long badgeId) {
		_userBadge.setBadgeId(badgeId);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_userBadge.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this user badge.
	*
	* @param companyId the company ID of this user badge
	*/
	@Override
	public void setCompanyId(long companyId) {
		_userBadge.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this user badge.
	*
	* @param createDate the create date of this user badge
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_userBadge.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_userBadge.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_userBadge.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_userBadge.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the modified date of this user badge.
	*
	* @param modifiedDate the modified date of this user badge
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_userBadge.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_userBadge.setNew(n);
	}

	/**
	* Sets the primary key of this user badge.
	*
	* @param primaryKey the primary key of this user badge
	*/
	@Override
	public void setPrimaryKey(
		com.devshaker.service.persistence.UserBadgePK primaryKey) {
		_userBadge.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_userBadge.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the user ID of this user badge.
	*
	* @param userId the user ID of this user badge
	*/
	@Override
	public void setUserId(long userId) {
		_userBadge.setUserId(userId);
	}

	/**
	* Sets the user uuid of this user badge.
	*
	* @param userUuid the user uuid of this user badge
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_userBadge.setUserUuid(userUuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserBadgeWrapper)) {
			return false;
		}

		UserBadgeWrapper userBadgeWrapper = (UserBadgeWrapper)obj;

		if (Objects.equals(_userBadge, userBadgeWrapper._userBadge)) {
			return true;
		}

		return false;
	}

	@Override
	public UserBadge getWrappedModel() {
		return _userBadge;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _userBadge.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _userBadge.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_userBadge.resetOriginalValues();
	}

	private final UserBadge _userBadge;
}