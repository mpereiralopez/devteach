/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Subcategory}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Subcategory
 * @generated
 */
@ProviderType
public class SubcategoryWrapper implements Subcategory,
	ModelWrapper<Subcategory> {
	public SubcategoryWrapper(Subcategory subcategory) {
		_subcategory = subcategory;
	}

	@Override
	public Class<?> getModelClass() {
		return Subcategory.class;
	}

	@Override
	public String getModelClassName() {
		return Subcategory.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("categoryId", getCategoryId());
		attributes.put("subcategoryId", getSubcategoryId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("subcategoryName", getSubcategoryName());
		attributes.put("subcategoryDescription", getSubcategoryDescription());
		attributes.put("subcategoryIcon", getSubcategoryIcon());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long categoryId = (Long)attributes.get("categoryId");

		if (categoryId != null) {
			setCategoryId(categoryId);
		}

		Long subcategoryId = (Long)attributes.get("subcategoryId");

		if (subcategoryId != null) {
			setSubcategoryId(subcategoryId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String subcategoryName = (String)attributes.get("subcategoryName");

		if (subcategoryName != null) {
			setSubcategoryName(subcategoryName);
		}

		String subcategoryDescription = (String)attributes.get(
				"subcategoryDescription");

		if (subcategoryDescription != null) {
			setSubcategoryDescription(subcategoryDescription);
		}

		Long subcategoryIcon = (Long)attributes.get("subcategoryIcon");

		if (subcategoryIcon != null) {
			setSubcategoryIcon(subcategoryIcon);
		}
	}

	@Override
	public Subcategory toEscapedModel() {
		return new SubcategoryWrapper(_subcategory.toEscapedModel());
	}

	@Override
	public Subcategory toUnescapedModel() {
		return new SubcategoryWrapper(_subcategory.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _subcategory.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _subcategory.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _subcategory.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _subcategory.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Subcategory> toCacheModel() {
		return _subcategory.toCacheModel();
	}

	@Override
	public int compareTo(Subcategory subcategory) {
		return _subcategory.compareTo(subcategory);
	}

	@Override
	public int hashCode() {
		return _subcategory.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _subcategory.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new SubcategoryWrapper((Subcategory)_subcategory.clone());
	}

	@Override
	public java.lang.String getDefaultLanguageId() {
		return _subcategory.getDefaultLanguageId();
	}

	/**
	* Returns the subcategory description of this subcategory.
	*
	* @return the subcategory description of this subcategory
	*/
	@Override
	public java.lang.String getSubcategoryDescription() {
		return _subcategory.getSubcategoryDescription();
	}

	/**
	* Returns the localized subcategory description of this subcategory in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @return the localized subcategory description of this subcategory
	*/
	@Override
	public java.lang.String getSubcategoryDescription(
		java.lang.String languageId) {
		return _subcategory.getSubcategoryDescription(languageId);
	}

	/**
	* Returns the localized subcategory description of this subcategory in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized subcategory description of this subcategory
	*/
	@Override
	public java.lang.String getSubcategoryDescription(
		java.lang.String languageId, boolean useDefault) {
		return _subcategory.getSubcategoryDescription(languageId, useDefault);
	}

	/**
	* Returns the localized subcategory description of this subcategory in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param locale the locale of the language
	* @return the localized subcategory description of this subcategory
	*/
	@Override
	public java.lang.String getSubcategoryDescription(java.util.Locale locale) {
		return _subcategory.getSubcategoryDescription(locale);
	}

	/**
	* Returns the localized subcategory description of this subcategory in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param locale the local of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized subcategory description of this subcategory. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	*/
	@Override
	public java.lang.String getSubcategoryDescription(java.util.Locale locale,
		boolean useDefault) {
		return _subcategory.getSubcategoryDescription(locale, useDefault);
	}

	@Override
	public java.lang.String getSubcategoryDescriptionCurrentLanguageId() {
		return _subcategory.getSubcategoryDescriptionCurrentLanguageId();
	}

	@Override
	public java.lang.String getSubcategoryDescriptionCurrentValue() {
		return _subcategory.getSubcategoryDescriptionCurrentValue();
	}

	/**
	* Returns the subcategory name of this subcategory.
	*
	* @return the subcategory name of this subcategory
	*/
	@Override
	public java.lang.String getSubcategoryName() {
		return _subcategory.getSubcategoryName();
	}

	/**
	* Returns the localized subcategory name of this subcategory in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @return the localized subcategory name of this subcategory
	*/
	@Override
	public java.lang.String getSubcategoryName(java.lang.String languageId) {
		return _subcategory.getSubcategoryName(languageId);
	}

	/**
	* Returns the localized subcategory name of this subcategory in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized subcategory name of this subcategory
	*/
	@Override
	public java.lang.String getSubcategoryName(java.lang.String languageId,
		boolean useDefault) {
		return _subcategory.getSubcategoryName(languageId, useDefault);
	}

	/**
	* Returns the localized subcategory name of this subcategory in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param locale the locale of the language
	* @return the localized subcategory name of this subcategory
	*/
	@Override
	public java.lang.String getSubcategoryName(java.util.Locale locale) {
		return _subcategory.getSubcategoryName(locale);
	}

	/**
	* Returns the localized subcategory name of this subcategory in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param locale the local of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized subcategory name of this subcategory. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	*/
	@Override
	public java.lang.String getSubcategoryName(java.util.Locale locale,
		boolean useDefault) {
		return _subcategory.getSubcategoryName(locale, useDefault);
	}

	@Override
	public java.lang.String getSubcategoryNameCurrentLanguageId() {
		return _subcategory.getSubcategoryNameCurrentLanguageId();
	}

	@Override
	public java.lang.String getSubcategoryNameCurrentValue() {
		return _subcategory.getSubcategoryNameCurrentValue();
	}

	/**
	* Returns the user name of this subcategory.
	*
	* @return the user name of this subcategory
	*/
	@Override
	public java.lang.String getUserName() {
		return _subcategory.getUserName();
	}

	/**
	* Returns the user uuid of this subcategory.
	*
	* @return the user uuid of this subcategory
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _subcategory.getUserUuid();
	}

	/**
	* Returns the uuid of this subcategory.
	*
	* @return the uuid of this subcategory
	*/
	@Override
	public java.lang.String getUuid() {
		return _subcategory.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _subcategory.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _subcategory.toXmlString();
	}

	@Override
	public java.lang.String[] getAvailableLanguageIds() {
		return _subcategory.getAvailableLanguageIds();
	}

	/**
	* Returns the create date of this subcategory.
	*
	* @return the create date of this subcategory
	*/
	@Override
	public Date getCreateDate() {
		return _subcategory.getCreateDate();
	}

	/**
	* Returns the modified date of this subcategory.
	*
	* @return the modified date of this subcategory
	*/
	@Override
	public Date getModifiedDate() {
		return _subcategory.getModifiedDate();
	}

	/**
	* Returns a map of the locales and localized subcategory descriptions of this subcategory.
	*
	* @return the locales and localized subcategory descriptions of this subcategory
	*/
	@Override
	public Map<java.util.Locale, java.lang.String> getSubcategoryDescriptionMap() {
		return _subcategory.getSubcategoryDescriptionMap();
	}

	/**
	* Returns a map of the locales and localized subcategory names of this subcategory.
	*
	* @return the locales and localized subcategory names of this subcategory
	*/
	@Override
	public Map<java.util.Locale, java.lang.String> getSubcategoryNameMap() {
		return _subcategory.getSubcategoryNameMap();
	}

	/**
	* Returns the category ID of this subcategory.
	*
	* @return the category ID of this subcategory
	*/
	@Override
	public long getCategoryId() {
		return _subcategory.getCategoryId();
	}

	/**
	* Returns the company ID of this subcategory.
	*
	* @return the company ID of this subcategory
	*/
	@Override
	public long getCompanyId() {
		return _subcategory.getCompanyId();
	}

	/**
	* Returns the group ID of this subcategory.
	*
	* @return the group ID of this subcategory
	*/
	@Override
	public long getGroupId() {
		return _subcategory.getGroupId();
	}

	/**
	* Returns the primary key of this subcategory.
	*
	* @return the primary key of this subcategory
	*/
	@Override
	public long getPrimaryKey() {
		return _subcategory.getPrimaryKey();
	}

	/**
	* Returns the subcategory icon of this subcategory.
	*
	* @return the subcategory icon of this subcategory
	*/
	@Override
	public long getSubcategoryIcon() {
		return _subcategory.getSubcategoryIcon();
	}

	/**
	* Returns the subcategory ID of this subcategory.
	*
	* @return the subcategory ID of this subcategory
	*/
	@Override
	public long getSubcategoryId() {
		return _subcategory.getSubcategoryId();
	}

	/**
	* Returns the user ID of this subcategory.
	*
	* @return the user ID of this subcategory
	*/
	@Override
	public long getUserId() {
		return _subcategory.getUserId();
	}

	@Override
	public void persist() {
		_subcategory.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {
		_subcategory.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
		java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {
		_subcategory.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_subcategory.setCachedModel(cachedModel);
	}

	/**
	* Sets the category ID of this subcategory.
	*
	* @param categoryId the category ID of this subcategory
	*/
	@Override
	public void setCategoryId(long categoryId) {
		_subcategory.setCategoryId(categoryId);
	}

	/**
	* Sets the company ID of this subcategory.
	*
	* @param companyId the company ID of this subcategory
	*/
	@Override
	public void setCompanyId(long companyId) {
		_subcategory.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this subcategory.
	*
	* @param createDate the create date of this subcategory
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_subcategory.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_subcategory.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_subcategory.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_subcategory.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this subcategory.
	*
	* @param groupId the group ID of this subcategory
	*/
	@Override
	public void setGroupId(long groupId) {
		_subcategory.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this subcategory.
	*
	* @param modifiedDate the modified date of this subcategory
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_subcategory.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_subcategory.setNew(n);
	}

	/**
	* Sets the primary key of this subcategory.
	*
	* @param primaryKey the primary key of this subcategory
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_subcategory.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_subcategory.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the subcategory description of this subcategory.
	*
	* @param subcategoryDescription the subcategory description of this subcategory
	*/
	@Override
	public void setSubcategoryDescription(
		java.lang.String subcategoryDescription) {
		_subcategory.setSubcategoryDescription(subcategoryDescription);
	}

	/**
	* Sets the localized subcategory description of this subcategory in the language.
	*
	* @param subcategoryDescription the localized subcategory description of this subcategory
	* @param locale the locale of the language
	*/
	@Override
	public void setSubcategoryDescription(
		java.lang.String subcategoryDescription, java.util.Locale locale) {
		_subcategory.setSubcategoryDescription(subcategoryDescription, locale);
	}

	/**
	* Sets the localized subcategory description of this subcategory in the language, and sets the default locale.
	*
	* @param subcategoryDescription the localized subcategory description of this subcategory
	* @param locale the locale of the language
	* @param defaultLocale the default locale
	*/
	@Override
	public void setSubcategoryDescription(
		java.lang.String subcategoryDescription, java.util.Locale locale,
		java.util.Locale defaultLocale) {
		_subcategory.setSubcategoryDescription(subcategoryDescription, locale,
			defaultLocale);
	}

	@Override
	public void setSubcategoryDescriptionCurrentLanguageId(
		java.lang.String languageId) {
		_subcategory.setSubcategoryDescriptionCurrentLanguageId(languageId);
	}

	/**
	* Sets the localized subcategory descriptions of this subcategory from the map of locales and localized subcategory descriptions.
	*
	* @param subcategoryDescriptionMap the locales and localized subcategory descriptions of this subcategory
	*/
	@Override
	public void setSubcategoryDescriptionMap(
		Map<java.util.Locale, java.lang.String> subcategoryDescriptionMap) {
		_subcategory.setSubcategoryDescriptionMap(subcategoryDescriptionMap);
	}

	/**
	* Sets the localized subcategory descriptions of this subcategory from the map of locales and localized subcategory descriptions, and sets the default locale.
	*
	* @param subcategoryDescriptionMap the locales and localized subcategory descriptions of this subcategory
	* @param defaultLocale the default locale
	*/
	@Override
	public void setSubcategoryDescriptionMap(
		Map<java.util.Locale, java.lang.String> subcategoryDescriptionMap,
		java.util.Locale defaultLocale) {
		_subcategory.setSubcategoryDescriptionMap(subcategoryDescriptionMap,
			defaultLocale);
	}

	/**
	* Sets the subcategory icon of this subcategory.
	*
	* @param subcategoryIcon the subcategory icon of this subcategory
	*/
	@Override
	public void setSubcategoryIcon(long subcategoryIcon) {
		_subcategory.setSubcategoryIcon(subcategoryIcon);
	}

	/**
	* Sets the subcategory ID of this subcategory.
	*
	* @param subcategoryId the subcategory ID of this subcategory
	*/
	@Override
	public void setSubcategoryId(long subcategoryId) {
		_subcategory.setSubcategoryId(subcategoryId);
	}

	/**
	* Sets the subcategory name of this subcategory.
	*
	* @param subcategoryName the subcategory name of this subcategory
	*/
	@Override
	public void setSubcategoryName(java.lang.String subcategoryName) {
		_subcategory.setSubcategoryName(subcategoryName);
	}

	/**
	* Sets the localized subcategory name of this subcategory in the language.
	*
	* @param subcategoryName the localized subcategory name of this subcategory
	* @param locale the locale of the language
	*/
	@Override
	public void setSubcategoryName(java.lang.String subcategoryName,
		java.util.Locale locale) {
		_subcategory.setSubcategoryName(subcategoryName, locale);
	}

	/**
	* Sets the localized subcategory name of this subcategory in the language, and sets the default locale.
	*
	* @param subcategoryName the localized subcategory name of this subcategory
	* @param locale the locale of the language
	* @param defaultLocale the default locale
	*/
	@Override
	public void setSubcategoryName(java.lang.String subcategoryName,
		java.util.Locale locale, java.util.Locale defaultLocale) {
		_subcategory.setSubcategoryName(subcategoryName, locale, defaultLocale);
	}

	@Override
	public void setSubcategoryNameCurrentLanguageId(java.lang.String languageId) {
		_subcategory.setSubcategoryNameCurrentLanguageId(languageId);
	}

	/**
	* Sets the localized subcategory names of this subcategory from the map of locales and localized subcategory names.
	*
	* @param subcategoryNameMap the locales and localized subcategory names of this subcategory
	*/
	@Override
	public void setSubcategoryNameMap(
		Map<java.util.Locale, java.lang.String> subcategoryNameMap) {
		_subcategory.setSubcategoryNameMap(subcategoryNameMap);
	}

	/**
	* Sets the localized subcategory names of this subcategory from the map of locales and localized subcategory names, and sets the default locale.
	*
	* @param subcategoryNameMap the locales and localized subcategory names of this subcategory
	* @param defaultLocale the default locale
	*/
	@Override
	public void setSubcategoryNameMap(
		Map<java.util.Locale, java.lang.String> subcategoryNameMap,
		java.util.Locale defaultLocale) {
		_subcategory.setSubcategoryNameMap(subcategoryNameMap, defaultLocale);
	}

	/**
	* Sets the user ID of this subcategory.
	*
	* @param userId the user ID of this subcategory
	*/
	@Override
	public void setUserId(long userId) {
		_subcategory.setUserId(userId);
	}

	/**
	* Sets the user name of this subcategory.
	*
	* @param userName the user name of this subcategory
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_subcategory.setUserName(userName);
	}

	/**
	* Sets the user uuid of this subcategory.
	*
	* @param userUuid the user uuid of this subcategory
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_subcategory.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this subcategory.
	*
	* @param uuid the uuid of this subcategory
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_subcategory.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SubcategoryWrapper)) {
			return false;
		}

		SubcategoryWrapper subcategoryWrapper = (SubcategoryWrapper)obj;

		if (Objects.equals(_subcategory, subcategoryWrapper._subcategory)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _subcategory.getStagedModelType();
	}

	@Override
	public Subcategory getWrappedModel() {
		return _subcategory;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _subcategory.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _subcategory.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_subcategory.resetOriginalValues();
	}

	private final Subcategory _subcategory;
}