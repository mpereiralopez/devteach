/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the UserBadge service. Represents a row in the &quot;devShaker_UserBadge&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see UserBadgeModel
 * @see com.devshaker.model.impl.UserBadgeImpl
 * @see com.devshaker.model.impl.UserBadgeModelImpl
 * @generated
 */
@ImplementationClassName("com.devshaker.model.impl.UserBadgeImpl")
@ProviderType
public interface UserBadge extends UserBadgeModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link com.devshaker.model.impl.UserBadgeImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<UserBadge, Long> USER_ID_ACCESSOR = new Accessor<UserBadge, Long>() {
			@Override
			public Long get(UserBadge userBadge) {
				return userBadge.getUserId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<UserBadge> getTypeClass() {
				return UserBadge.class;
			}
		};

	public static final Accessor<UserBadge, Long> BADGE_ID_ACCESSOR = new Accessor<UserBadge, Long>() {
			@Override
			public Long get(UserBadge userBadge) {
				return userBadge.getBadgeId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<UserBadge> getTypeClass() {
				return UserBadge.class;
			}
		};
}