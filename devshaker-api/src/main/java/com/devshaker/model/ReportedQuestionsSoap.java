/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.devshaker.service.http.ReportedQuestionsServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see com.devshaker.service.http.ReportedQuestionsServiceSoap
 * @generated
 */
@ProviderType
public class ReportedQuestionsSoap implements Serializable {
	public static ReportedQuestionsSoap toSoapModel(ReportedQuestions model) {
		ReportedQuestionsSoap soapModel = new ReportedQuestionsSoap();

		soapModel.setReportId(model.getReportId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setQuestionId(model.getQuestionId());
		soapModel.setUserId(model.getUserId());
		soapModel.setReportContent(model.getReportContent());
		soapModel.setReportStatus(model.getReportStatus());

		return soapModel;
	}

	public static ReportedQuestionsSoap[] toSoapModels(
		ReportedQuestions[] models) {
		ReportedQuestionsSoap[] soapModels = new ReportedQuestionsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ReportedQuestionsSoap[][] toSoapModels(
		ReportedQuestions[][] models) {
		ReportedQuestionsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ReportedQuestionsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ReportedQuestionsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ReportedQuestionsSoap[] toSoapModels(
		List<ReportedQuestions> models) {
		List<ReportedQuestionsSoap> soapModels = new ArrayList<ReportedQuestionsSoap>(models.size());

		for (ReportedQuestions model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ReportedQuestionsSoap[soapModels.size()]);
	}

	public ReportedQuestionsSoap() {
	}

	public long getPrimaryKey() {
		return _reportId;
	}

	public void setPrimaryKey(long pk) {
		setReportId(pk);
	}

	public long getReportId() {
		return _reportId;
	}

	public void setReportId(long reportId) {
		_reportId = reportId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getQuestionId() {
		return _questionId;
	}

	public void setQuestionId(long questionId) {
		_questionId = questionId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getReportContent() {
		return _reportContent;
	}

	public void setReportContent(String reportContent) {
		_reportContent = reportContent;
	}

	public String getReportStatus() {
		return _reportStatus;
	}

	public void setReportStatus(String reportStatus) {
		_reportStatus = reportStatus;
	}

	private long _reportId;
	private long _companyId;
	private Date _createDate;
	private Date _modifiedDate;
	private long _questionId;
	private long _userId;
	private String _reportContent;
	private String _reportStatus;
}