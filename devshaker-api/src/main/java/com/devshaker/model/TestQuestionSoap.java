/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class TestQuestionSoap implements Serializable {
	public static TestQuestionSoap toSoapModel(TestQuestion model) {
		TestQuestionSoap soapModel = new TestQuestionSoap();

		soapModel.setQuestionId(model.getQuestionId());
		soapModel.setCategoryId(model.getCategoryId());
		soapModel.setSubcategoryId(model.getSubcategoryId());
		soapModel.setLevelId(model.getLevelId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserIdCreator(model.getUserIdCreator());
		soapModel.setUserNameCreator(model.getUserNameCreator());
		soapModel.setUserIdModified(model.getUserIdModified());
		soapModel.setUserNameModified(model.getUserNameModified());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setQuestionText(model.getQuestionText());
		soapModel.setStatus(model.getStatus());
		soapModel.setType(model.getType());
		soapModel.setAnswer0Text(model.getAnswer0Text());
		soapModel.setAnswer1Text(model.getAnswer1Text());
		soapModel.setAnswer2Text(model.getAnswer2Text());
		soapModel.setAnswer3Text(model.getAnswer3Text());
		soapModel.setCorrectAnswers(model.getCorrectAnswers());
		soapModel.setReportContent(model.getReportContent());
		soapModel.setReporterId(model.getReporterId());

		return soapModel;
	}

	public static TestQuestionSoap[] toSoapModels(TestQuestion[] models) {
		TestQuestionSoap[] soapModels = new TestQuestionSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TestQuestionSoap[][] toSoapModels(TestQuestion[][] models) {
		TestQuestionSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TestQuestionSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TestQuestionSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TestQuestionSoap[] toSoapModels(List<TestQuestion> models) {
		List<TestQuestionSoap> soapModels = new ArrayList<TestQuestionSoap>(models.size());

		for (TestQuestion model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TestQuestionSoap[soapModels.size()]);
	}

	public TestQuestionSoap() {
	}

	public long getPrimaryKey() {
		return _questionId;
	}

	public void setPrimaryKey(long pk) {
		setQuestionId(pk);
	}

	public long getQuestionId() {
		return _questionId;
	}

	public void setQuestionId(long questionId) {
		_questionId = questionId;
	}

	public long getCategoryId() {
		return _categoryId;
	}

	public void setCategoryId(long categoryId) {
		_categoryId = categoryId;
	}

	public long getSubcategoryId() {
		return _subcategoryId;
	}

	public void setSubcategoryId(long subcategoryId) {
		_subcategoryId = subcategoryId;
	}

	public long getLevelId() {
		return _levelId;
	}

	public void setLevelId(long levelId) {
		_levelId = levelId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserIdCreator() {
		return _userIdCreator;
	}

	public void setUserIdCreator(long userIdCreator) {
		_userIdCreator = userIdCreator;
	}

	public String getUserNameCreator() {
		return _userNameCreator;
	}

	public void setUserNameCreator(String userNameCreator) {
		_userNameCreator = userNameCreator;
	}

	public long getUserIdModified() {
		return _userIdModified;
	}

	public void setUserIdModified(long userIdModified) {
		_userIdModified = userIdModified;
	}

	public String getUserNameModified() {
		return _userNameModified;
	}

	public void setUserNameModified(String userNameModified) {
		_userNameModified = userNameModified;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getQuestionText() {
		return _questionText;
	}

	public void setQuestionText(String questionText) {
		_questionText = questionText;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public int getType() {
		return _type;
	}

	public void setType(int type) {
		_type = type;
	}

	public String getAnswer0Text() {
		return _answer0Text;
	}

	public void setAnswer0Text(String answer0Text) {
		_answer0Text = answer0Text;
	}

	public String getAnswer1Text() {
		return _answer1Text;
	}

	public void setAnswer1Text(String answer1Text) {
		_answer1Text = answer1Text;
	}

	public String getAnswer2Text() {
		return _answer2Text;
	}

	public void setAnswer2Text(String answer2Text) {
		_answer2Text = answer2Text;
	}

	public String getAnswer3Text() {
		return _answer3Text;
	}

	public void setAnswer3Text(String answer3Text) {
		_answer3Text = answer3Text;
	}

	public String getCorrectAnswers() {
		return _correctAnswers;
	}

	public void setCorrectAnswers(String correctAnswers) {
		_correctAnswers = correctAnswers;
	}

	public String getReportContent() {
		return _reportContent;
	}

	public void setReportContent(String reportContent) {
		_reportContent = reportContent;
	}

	public long getReporterId() {
		return _reporterId;
	}

	public void setReporterId(long reporterId) {
		_reporterId = reporterId;
	}

	private long _questionId;
	private long _categoryId;
	private long _subcategoryId;
	private long _levelId;
	private long _groupId;
	private long _companyId;
	private long _userIdCreator;
	private String _userNameCreator;
	private long _userIdModified;
	private String _userNameModified;
	private Date _createDate;
	private Date _modifiedDate;
	private String _questionText;
	private int _status;
	private int _type;
	private String _answer0Text;
	private String _answer1Text;
	private String _answer2Text;
	private String _answer3Text;
	private String _correctAnswers;
	private String _reportContent;
	private long _reporterId;
}