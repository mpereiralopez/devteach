/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Category}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Category
 * @generated
 */
@ProviderType
public class CategoryWrapper implements Category, ModelWrapper<Category> {
	public CategoryWrapper(Category category) {
		_category = category;
	}

	@Override
	public Class<?> getModelClass() {
		return Category.class;
	}

	@Override
	public String getModelClassName() {
		return Category.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("categoryId", getCategoryId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("name", getName());
		attributes.put("description", getDescription());
		attributes.put("icon", getIcon());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long categoryId = (Long)attributes.get("categoryId");

		if (categoryId != null) {
			setCategoryId(categoryId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		Long icon = (Long)attributes.get("icon");

		if (icon != null) {
			setIcon(icon);
		}
	}

	@Override
	public Category toEscapedModel() {
		return new CategoryWrapper(_category.toEscapedModel());
	}

	@Override
	public Category toUnescapedModel() {
		return new CategoryWrapper(_category.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _category.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _category.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _category.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _category.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Category> toCacheModel() {
		return _category.toCacheModel();
	}

	@Override
	public int compareTo(Category category) {
		return _category.compareTo(category);
	}

	@Override
	public int hashCode() {
		return _category.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _category.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new CategoryWrapper((Category)_category.clone());
	}

	@Override
	public java.lang.String getDefaultLanguageId() {
		return _category.getDefaultLanguageId();
	}

	/**
	* Returns the description of this category.
	*
	* @return the description of this category
	*/
	@Override
	public java.lang.String getDescription() {
		return _category.getDescription();
	}

	/**
	* Returns the localized description of this category in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @return the localized description of this category
	*/
	@Override
	public java.lang.String getDescription(java.lang.String languageId) {
		return _category.getDescription(languageId);
	}

	/**
	* Returns the localized description of this category in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized description of this category
	*/
	@Override
	public java.lang.String getDescription(java.lang.String languageId,
		boolean useDefault) {
		return _category.getDescription(languageId, useDefault);
	}

	/**
	* Returns the localized description of this category in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param locale the locale of the language
	* @return the localized description of this category
	*/
	@Override
	public java.lang.String getDescription(java.util.Locale locale) {
		return _category.getDescription(locale);
	}

	/**
	* Returns the localized description of this category in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param locale the local of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized description of this category. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	*/
	@Override
	public java.lang.String getDescription(java.util.Locale locale,
		boolean useDefault) {
		return _category.getDescription(locale, useDefault);
	}

	@Override
	public java.lang.String getDescriptionCurrentLanguageId() {
		return _category.getDescriptionCurrentLanguageId();
	}

	@Override
	public java.lang.String getDescriptionCurrentValue() {
		return _category.getDescriptionCurrentValue();
	}

	/**
	* Returns the name of this category.
	*
	* @return the name of this category
	*/
	@Override
	public java.lang.String getName() {
		return _category.getName();
	}

	/**
	* Returns the localized name of this category in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @return the localized name of this category
	*/
	@Override
	public java.lang.String getName(java.lang.String languageId) {
		return _category.getName(languageId);
	}

	/**
	* Returns the localized name of this category in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized name of this category
	*/
	@Override
	public java.lang.String getName(java.lang.String languageId,
		boolean useDefault) {
		return _category.getName(languageId, useDefault);
	}

	/**
	* Returns the localized name of this category in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param locale the locale of the language
	* @return the localized name of this category
	*/
	@Override
	public java.lang.String getName(java.util.Locale locale) {
		return _category.getName(locale);
	}

	/**
	* Returns the localized name of this category in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param locale the local of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized name of this category. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	*/
	@Override
	public java.lang.String getName(java.util.Locale locale, boolean useDefault) {
		return _category.getName(locale, useDefault);
	}

	@Override
	public java.lang.String getNameCurrentLanguageId() {
		return _category.getNameCurrentLanguageId();
	}

	@Override
	public java.lang.String getNameCurrentValue() {
		return _category.getNameCurrentValue();
	}

	/**
	* Returns the user name of this category.
	*
	* @return the user name of this category
	*/
	@Override
	public java.lang.String getUserName() {
		return _category.getUserName();
	}

	/**
	* Returns the user uuid of this category.
	*
	* @return the user uuid of this category
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _category.getUserUuid();
	}

	/**
	* Returns the uuid of this category.
	*
	* @return the uuid of this category
	*/
	@Override
	public java.lang.String getUuid() {
		return _category.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _category.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _category.toXmlString();
	}

	@Override
	public java.lang.String[] getAvailableLanguageIds() {
		return _category.getAvailableLanguageIds();
	}

	/**
	* Returns the create date of this category.
	*
	* @return the create date of this category
	*/
	@Override
	public Date getCreateDate() {
		return _category.getCreateDate();
	}

	/**
	* Returns the modified date of this category.
	*
	* @return the modified date of this category
	*/
	@Override
	public Date getModifiedDate() {
		return _category.getModifiedDate();
	}

	/**
	* Returns a map of the locales and localized descriptions of this category.
	*
	* @return the locales and localized descriptions of this category
	*/
	@Override
	public Map<java.util.Locale, java.lang.String> getDescriptionMap() {
		return _category.getDescriptionMap();
	}

	/**
	* Returns a map of the locales and localized names of this category.
	*
	* @return the locales and localized names of this category
	*/
	@Override
	public Map<java.util.Locale, java.lang.String> getNameMap() {
		return _category.getNameMap();
	}

	/**
	* Returns the category ID of this category.
	*
	* @return the category ID of this category
	*/
	@Override
	public long getCategoryId() {
		return _category.getCategoryId();
	}

	/**
	* Returns the company ID of this category.
	*
	* @return the company ID of this category
	*/
	@Override
	public long getCompanyId() {
		return _category.getCompanyId();
	}

	/**
	* Returns the group ID of this category.
	*
	* @return the group ID of this category
	*/
	@Override
	public long getGroupId() {
		return _category.getGroupId();
	}

	/**
	* Returns the icon of this category.
	*
	* @return the icon of this category
	*/
	@Override
	public long getIcon() {
		return _category.getIcon();
	}

	/**
	* Returns the primary key of this category.
	*
	* @return the primary key of this category
	*/
	@Override
	public long getPrimaryKey() {
		return _category.getPrimaryKey();
	}

	/**
	* Returns the user ID of this category.
	*
	* @return the user ID of this category
	*/
	@Override
	public long getUserId() {
		return _category.getUserId();
	}

	@Override
	public void persist() {
		_category.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {
		_category.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
		java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {
		_category.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_category.setCachedModel(cachedModel);
	}

	/**
	* Sets the category ID of this category.
	*
	* @param categoryId the category ID of this category
	*/
	@Override
	public void setCategoryId(long categoryId) {
		_category.setCategoryId(categoryId);
	}

	/**
	* Sets the company ID of this category.
	*
	* @param companyId the company ID of this category
	*/
	@Override
	public void setCompanyId(long companyId) {
		_category.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this category.
	*
	* @param createDate the create date of this category
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_category.setCreateDate(createDate);
	}

	/**
	* Sets the description of this category.
	*
	* @param description the description of this category
	*/
	@Override
	public void setDescription(java.lang.String description) {
		_category.setDescription(description);
	}

	/**
	* Sets the localized description of this category in the language.
	*
	* @param description the localized description of this category
	* @param locale the locale of the language
	*/
	@Override
	public void setDescription(java.lang.String description,
		java.util.Locale locale) {
		_category.setDescription(description, locale);
	}

	/**
	* Sets the localized description of this category in the language, and sets the default locale.
	*
	* @param description the localized description of this category
	* @param locale the locale of the language
	* @param defaultLocale the default locale
	*/
	@Override
	public void setDescription(java.lang.String description,
		java.util.Locale locale, java.util.Locale defaultLocale) {
		_category.setDescription(description, locale, defaultLocale);
	}

	@Override
	public void setDescriptionCurrentLanguageId(java.lang.String languageId) {
		_category.setDescriptionCurrentLanguageId(languageId);
	}

	/**
	* Sets the localized descriptions of this category from the map of locales and localized descriptions.
	*
	* @param descriptionMap the locales and localized descriptions of this category
	*/
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, java.lang.String> descriptionMap) {
		_category.setDescriptionMap(descriptionMap);
	}

	/**
	* Sets the localized descriptions of this category from the map of locales and localized descriptions, and sets the default locale.
	*
	* @param descriptionMap the locales and localized descriptions of this category
	* @param defaultLocale the default locale
	*/
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, java.lang.String> descriptionMap,
		java.util.Locale defaultLocale) {
		_category.setDescriptionMap(descriptionMap, defaultLocale);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_category.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_category.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_category.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this category.
	*
	* @param groupId the group ID of this category
	*/
	@Override
	public void setGroupId(long groupId) {
		_category.setGroupId(groupId);
	}

	/**
	* Sets the icon of this category.
	*
	* @param icon the icon of this category
	*/
	@Override
	public void setIcon(long icon) {
		_category.setIcon(icon);
	}

	/**
	* Sets the modified date of this category.
	*
	* @param modifiedDate the modified date of this category
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_category.setModifiedDate(modifiedDate);
	}

	/**
	* Sets the name of this category.
	*
	* @param name the name of this category
	*/
	@Override
	public void setName(java.lang.String name) {
		_category.setName(name);
	}

	/**
	* Sets the localized name of this category in the language.
	*
	* @param name the localized name of this category
	* @param locale the locale of the language
	*/
	@Override
	public void setName(java.lang.String name, java.util.Locale locale) {
		_category.setName(name, locale);
	}

	/**
	* Sets the localized name of this category in the language, and sets the default locale.
	*
	* @param name the localized name of this category
	* @param locale the locale of the language
	* @param defaultLocale the default locale
	*/
	@Override
	public void setName(java.lang.String name, java.util.Locale locale,
		java.util.Locale defaultLocale) {
		_category.setName(name, locale, defaultLocale);
	}

	@Override
	public void setNameCurrentLanguageId(java.lang.String languageId) {
		_category.setNameCurrentLanguageId(languageId);
	}

	/**
	* Sets the localized names of this category from the map of locales and localized names.
	*
	* @param nameMap the locales and localized names of this category
	*/
	@Override
	public void setNameMap(Map<java.util.Locale, java.lang.String> nameMap) {
		_category.setNameMap(nameMap);
	}

	/**
	* Sets the localized names of this category from the map of locales and localized names, and sets the default locale.
	*
	* @param nameMap the locales and localized names of this category
	* @param defaultLocale the default locale
	*/
	@Override
	public void setNameMap(Map<java.util.Locale, java.lang.String> nameMap,
		java.util.Locale defaultLocale) {
		_category.setNameMap(nameMap, defaultLocale);
	}

	@Override
	public void setNew(boolean n) {
		_category.setNew(n);
	}

	/**
	* Sets the primary key of this category.
	*
	* @param primaryKey the primary key of this category
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_category.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_category.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the user ID of this category.
	*
	* @param userId the user ID of this category
	*/
	@Override
	public void setUserId(long userId) {
		_category.setUserId(userId);
	}

	/**
	* Sets the user name of this category.
	*
	* @param userName the user name of this category
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_category.setUserName(userName);
	}

	/**
	* Sets the user uuid of this category.
	*
	* @param userUuid the user uuid of this category
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_category.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this category.
	*
	* @param uuid the uuid of this category
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_category.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CategoryWrapper)) {
			return false;
		}

		CategoryWrapper categoryWrapper = (CategoryWrapper)obj;

		if (Objects.equals(_category, categoryWrapper._category)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _category.getStagedModelType();
	}

	@Override
	public Category getWrappedModel() {
		return _category;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _category.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _category.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_category.resetOriginalValues();
	}

	private final Category _category;
}