/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link TestQuestion}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TestQuestion
 * @generated
 */
@ProviderType
public class TestQuestionWrapper implements TestQuestion,
	ModelWrapper<TestQuestion> {
	public TestQuestionWrapper(TestQuestion testQuestion) {
		_testQuestion = testQuestion;
	}

	@Override
	public Class<?> getModelClass() {
		return TestQuestion.class;
	}

	@Override
	public String getModelClassName() {
		return TestQuestion.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("questionId", getQuestionId());
		attributes.put("categoryId", getCategoryId());
		attributes.put("subcategoryId", getSubcategoryId());
		attributes.put("levelId", getLevelId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userIdCreator", getUserIdCreator());
		attributes.put("userNameCreator", getUserNameCreator());
		attributes.put("userIdModified", getUserIdModified());
		attributes.put("userNameModified", getUserNameModified());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("questionText", getQuestionText());
		attributes.put("status", getStatus());
		attributes.put("type", getType());
		attributes.put("answer0Text", getAnswer0Text());
		attributes.put("answer1Text", getAnswer1Text());
		attributes.put("answer2Text", getAnswer2Text());
		attributes.put("answer3Text", getAnswer3Text());
		attributes.put("correctAnswers", getCorrectAnswers());
		attributes.put("reportContent", getReportContent());
		attributes.put("reporterId", getReporterId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long questionId = (Long)attributes.get("questionId");

		if (questionId != null) {
			setQuestionId(questionId);
		}

		Long categoryId = (Long)attributes.get("categoryId");

		if (categoryId != null) {
			setCategoryId(categoryId);
		}

		Long subcategoryId = (Long)attributes.get("subcategoryId");

		if (subcategoryId != null) {
			setSubcategoryId(subcategoryId);
		}

		Long levelId = (Long)attributes.get("levelId");

		if (levelId != null) {
			setLevelId(levelId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userIdCreator = (Long)attributes.get("userIdCreator");

		if (userIdCreator != null) {
			setUserIdCreator(userIdCreator);
		}

		String userNameCreator = (String)attributes.get("userNameCreator");

		if (userNameCreator != null) {
			setUserNameCreator(userNameCreator);
		}

		Long userIdModified = (Long)attributes.get("userIdModified");

		if (userIdModified != null) {
			setUserIdModified(userIdModified);
		}

		String userNameModified = (String)attributes.get("userNameModified");

		if (userNameModified != null) {
			setUserNameModified(userNameModified);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String questionText = (String)attributes.get("questionText");

		if (questionText != null) {
			setQuestionText(questionText);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		String answer0Text = (String)attributes.get("answer0Text");

		if (answer0Text != null) {
			setAnswer0Text(answer0Text);
		}

		String answer1Text = (String)attributes.get("answer1Text");

		if (answer1Text != null) {
			setAnswer1Text(answer1Text);
		}

		String answer2Text = (String)attributes.get("answer2Text");

		if (answer2Text != null) {
			setAnswer2Text(answer2Text);
		}

		String answer3Text = (String)attributes.get("answer3Text");

		if (answer3Text != null) {
			setAnswer3Text(answer3Text);
		}

		String correctAnswers = (String)attributes.get("correctAnswers");

		if (correctAnswers != null) {
			setCorrectAnswers(correctAnswers);
		}

		String reportContent = (String)attributes.get("reportContent");

		if (reportContent != null) {
			setReportContent(reportContent);
		}

		Long reporterId = (Long)attributes.get("reporterId");

		if (reporterId != null) {
			setReporterId(reporterId);
		}
	}

	@Override
	public TestQuestion toEscapedModel() {
		return new TestQuestionWrapper(_testQuestion.toEscapedModel());
	}

	@Override
	public TestQuestion toUnescapedModel() {
		return new TestQuestionWrapper(_testQuestion.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _testQuestion.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _testQuestion.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _testQuestion.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _testQuestion.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<TestQuestion> toCacheModel() {
		return _testQuestion.toCacheModel();
	}

	@Override
	public int compareTo(TestQuestion testQuestion) {
		return _testQuestion.compareTo(testQuestion);
	}

	/**
	* Returns the status of this test question.
	*
	* @return the status of this test question
	*/
	@Override
	public int getStatus() {
		return _testQuestion.getStatus();
	}

	/**
	* Returns the type of this test question.
	*
	* @return the type of this test question
	*/
	@Override
	public int getType() {
		return _testQuestion.getType();
	}

	@Override
	public int hashCode() {
		return _testQuestion.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _testQuestion.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new TestQuestionWrapper((TestQuestion)_testQuestion.clone());
	}

	/**
	* Returns the answer0 text of this test question.
	*
	* @return the answer0 text of this test question
	*/
	@Override
	public java.lang.String getAnswer0Text() {
		return _testQuestion.getAnswer0Text();
	}

	/**
	* Returns the localized answer0 text of this test question in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @return the localized answer0 text of this test question
	*/
	@Override
	public java.lang.String getAnswer0Text(java.lang.String languageId) {
		return _testQuestion.getAnswer0Text(languageId);
	}

	/**
	* Returns the localized answer0 text of this test question in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized answer0 text of this test question
	*/
	@Override
	public java.lang.String getAnswer0Text(java.lang.String languageId,
		boolean useDefault) {
		return _testQuestion.getAnswer0Text(languageId, useDefault);
	}

	/**
	* Returns the localized answer0 text of this test question in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param locale the locale of the language
	* @return the localized answer0 text of this test question
	*/
	@Override
	public java.lang.String getAnswer0Text(java.util.Locale locale) {
		return _testQuestion.getAnswer0Text(locale);
	}

	/**
	* Returns the localized answer0 text of this test question in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param locale the local of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized answer0 text of this test question. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	*/
	@Override
	public java.lang.String getAnswer0Text(java.util.Locale locale,
		boolean useDefault) {
		return _testQuestion.getAnswer0Text(locale, useDefault);
	}

	@Override
	public java.lang.String getAnswer0TextCurrentLanguageId() {
		return _testQuestion.getAnswer0TextCurrentLanguageId();
	}

	@Override
	public java.lang.String getAnswer0TextCurrentValue() {
		return _testQuestion.getAnswer0TextCurrentValue();
	}

	/**
	* Returns the answer1 text of this test question.
	*
	* @return the answer1 text of this test question
	*/
	@Override
	public java.lang.String getAnswer1Text() {
		return _testQuestion.getAnswer1Text();
	}

	/**
	* Returns the localized answer1 text of this test question in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @return the localized answer1 text of this test question
	*/
	@Override
	public java.lang.String getAnswer1Text(java.lang.String languageId) {
		return _testQuestion.getAnswer1Text(languageId);
	}

	/**
	* Returns the localized answer1 text of this test question in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized answer1 text of this test question
	*/
	@Override
	public java.lang.String getAnswer1Text(java.lang.String languageId,
		boolean useDefault) {
		return _testQuestion.getAnswer1Text(languageId, useDefault);
	}

	/**
	* Returns the localized answer1 text of this test question in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param locale the locale of the language
	* @return the localized answer1 text of this test question
	*/
	@Override
	public java.lang.String getAnswer1Text(java.util.Locale locale) {
		return _testQuestion.getAnswer1Text(locale);
	}

	/**
	* Returns the localized answer1 text of this test question in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param locale the local of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized answer1 text of this test question. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	*/
	@Override
	public java.lang.String getAnswer1Text(java.util.Locale locale,
		boolean useDefault) {
		return _testQuestion.getAnswer1Text(locale, useDefault);
	}

	@Override
	public java.lang.String getAnswer1TextCurrentLanguageId() {
		return _testQuestion.getAnswer1TextCurrentLanguageId();
	}

	@Override
	public java.lang.String getAnswer1TextCurrentValue() {
		return _testQuestion.getAnswer1TextCurrentValue();
	}

	/**
	* Returns the answer2 text of this test question.
	*
	* @return the answer2 text of this test question
	*/
	@Override
	public java.lang.String getAnswer2Text() {
		return _testQuestion.getAnswer2Text();
	}

	/**
	* Returns the localized answer2 text of this test question in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @return the localized answer2 text of this test question
	*/
	@Override
	public java.lang.String getAnswer2Text(java.lang.String languageId) {
		return _testQuestion.getAnswer2Text(languageId);
	}

	/**
	* Returns the localized answer2 text of this test question in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized answer2 text of this test question
	*/
	@Override
	public java.lang.String getAnswer2Text(java.lang.String languageId,
		boolean useDefault) {
		return _testQuestion.getAnswer2Text(languageId, useDefault);
	}

	/**
	* Returns the localized answer2 text of this test question in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param locale the locale of the language
	* @return the localized answer2 text of this test question
	*/
	@Override
	public java.lang.String getAnswer2Text(java.util.Locale locale) {
		return _testQuestion.getAnswer2Text(locale);
	}

	/**
	* Returns the localized answer2 text of this test question in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param locale the local of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized answer2 text of this test question. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	*/
	@Override
	public java.lang.String getAnswer2Text(java.util.Locale locale,
		boolean useDefault) {
		return _testQuestion.getAnswer2Text(locale, useDefault);
	}

	@Override
	public java.lang.String getAnswer2TextCurrentLanguageId() {
		return _testQuestion.getAnswer2TextCurrentLanguageId();
	}

	@Override
	public java.lang.String getAnswer2TextCurrentValue() {
		return _testQuestion.getAnswer2TextCurrentValue();
	}

	/**
	* Returns the answer3 text of this test question.
	*
	* @return the answer3 text of this test question
	*/
	@Override
	public java.lang.String getAnswer3Text() {
		return _testQuestion.getAnswer3Text();
	}

	/**
	* Returns the localized answer3 text of this test question in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @return the localized answer3 text of this test question
	*/
	@Override
	public java.lang.String getAnswer3Text(java.lang.String languageId) {
		return _testQuestion.getAnswer3Text(languageId);
	}

	/**
	* Returns the localized answer3 text of this test question in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized answer3 text of this test question
	*/
	@Override
	public java.lang.String getAnswer3Text(java.lang.String languageId,
		boolean useDefault) {
		return _testQuestion.getAnswer3Text(languageId, useDefault);
	}

	/**
	* Returns the localized answer3 text of this test question in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param locale the locale of the language
	* @return the localized answer3 text of this test question
	*/
	@Override
	public java.lang.String getAnswer3Text(java.util.Locale locale) {
		return _testQuestion.getAnswer3Text(locale);
	}

	/**
	* Returns the localized answer3 text of this test question in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param locale the local of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized answer3 text of this test question. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	*/
	@Override
	public java.lang.String getAnswer3Text(java.util.Locale locale,
		boolean useDefault) {
		return _testQuestion.getAnswer3Text(locale, useDefault);
	}

	@Override
	public java.lang.String getAnswer3TextCurrentLanguageId() {
		return _testQuestion.getAnswer3TextCurrentLanguageId();
	}

	@Override
	public java.lang.String getAnswer3TextCurrentValue() {
		return _testQuestion.getAnswer3TextCurrentValue();
	}

	/**
	* Returns the correct answers of this test question.
	*
	* @return the correct answers of this test question
	*/
	@Override
	public java.lang.String getCorrectAnswers() {
		return _testQuestion.getCorrectAnswers();
	}

	@Override
	public java.lang.String getDefaultLanguageId() {
		return _testQuestion.getDefaultLanguageId();
	}

	/**
	* Returns the question text of this test question.
	*
	* @return the question text of this test question
	*/
	@Override
	public java.lang.String getQuestionText() {
		return _testQuestion.getQuestionText();
	}

	/**
	* Returns the localized question text of this test question in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @return the localized question text of this test question
	*/
	@Override
	public java.lang.String getQuestionText(java.lang.String languageId) {
		return _testQuestion.getQuestionText(languageId);
	}

	/**
	* Returns the localized question text of this test question in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized question text of this test question
	*/
	@Override
	public java.lang.String getQuestionText(java.lang.String languageId,
		boolean useDefault) {
		return _testQuestion.getQuestionText(languageId, useDefault);
	}

	/**
	* Returns the localized question text of this test question in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param locale the locale of the language
	* @return the localized question text of this test question
	*/
	@Override
	public java.lang.String getQuestionText(java.util.Locale locale) {
		return _testQuestion.getQuestionText(locale);
	}

	/**
	* Returns the localized question text of this test question in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param locale the local of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized question text of this test question. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	*/
	@Override
	public java.lang.String getQuestionText(java.util.Locale locale,
		boolean useDefault) {
		return _testQuestion.getQuestionText(locale, useDefault);
	}

	@Override
	public java.lang.String getQuestionTextCurrentLanguageId() {
		return _testQuestion.getQuestionTextCurrentLanguageId();
	}

	@Override
	public java.lang.String getQuestionTextCurrentValue() {
		return _testQuestion.getQuestionTextCurrentValue();
	}

	/**
	* Returns the report content of this test question.
	*
	* @return the report content of this test question
	*/
	@Override
	public java.lang.String getReportContent() {
		return _testQuestion.getReportContent();
	}

	/**
	* Returns the user name creator of this test question.
	*
	* @return the user name creator of this test question
	*/
	@Override
	public java.lang.String getUserNameCreator() {
		return _testQuestion.getUserNameCreator();
	}

	/**
	* Returns the user name modified of this test question.
	*
	* @return the user name modified of this test question
	*/
	@Override
	public java.lang.String getUserNameModified() {
		return _testQuestion.getUserNameModified();
	}

	@Override
	public java.lang.String toString() {
		return _testQuestion.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _testQuestion.toXmlString();
	}

	@Override
	public java.lang.String[] getAvailableLanguageIds() {
		return _testQuestion.getAvailableLanguageIds();
	}

	/**
	* Returns the create date of this test question.
	*
	* @return the create date of this test question
	*/
	@Override
	public Date getCreateDate() {
		return _testQuestion.getCreateDate();
	}

	/**
	* Returns the modified date of this test question.
	*
	* @return the modified date of this test question
	*/
	@Override
	public Date getModifiedDate() {
		return _testQuestion.getModifiedDate();
	}

	/**
	* Returns a map of the locales and localized answer0 texts of this test question.
	*
	* @return the locales and localized answer0 texts of this test question
	*/
	@Override
	public Map<java.util.Locale, java.lang.String> getAnswer0TextMap() {
		return _testQuestion.getAnswer0TextMap();
	}

	/**
	* Returns a map of the locales and localized answer1 texts of this test question.
	*
	* @return the locales and localized answer1 texts of this test question
	*/
	@Override
	public Map<java.util.Locale, java.lang.String> getAnswer1TextMap() {
		return _testQuestion.getAnswer1TextMap();
	}

	/**
	* Returns a map of the locales and localized answer2 texts of this test question.
	*
	* @return the locales and localized answer2 texts of this test question
	*/
	@Override
	public Map<java.util.Locale, java.lang.String> getAnswer2TextMap() {
		return _testQuestion.getAnswer2TextMap();
	}

	/**
	* Returns a map of the locales and localized answer3 texts of this test question.
	*
	* @return the locales and localized answer3 texts of this test question
	*/
	@Override
	public Map<java.util.Locale, java.lang.String> getAnswer3TextMap() {
		return _testQuestion.getAnswer3TextMap();
	}

	/**
	* Returns a map of the locales and localized question texts of this test question.
	*
	* @return the locales and localized question texts of this test question
	*/
	@Override
	public Map<java.util.Locale, java.lang.String> getQuestionTextMap() {
		return _testQuestion.getQuestionTextMap();
	}

	/**
	* Returns the category ID of this test question.
	*
	* @return the category ID of this test question
	*/
	@Override
	public long getCategoryId() {
		return _testQuestion.getCategoryId();
	}

	/**
	* Returns the company ID of this test question.
	*
	* @return the company ID of this test question
	*/
	@Override
	public long getCompanyId() {
		return _testQuestion.getCompanyId();
	}

	/**
	* Returns the group ID of this test question.
	*
	* @return the group ID of this test question
	*/
	@Override
	public long getGroupId() {
		return _testQuestion.getGroupId();
	}

	/**
	* Returns the level ID of this test question.
	*
	* @return the level ID of this test question
	*/
	@Override
	public long getLevelId() {
		return _testQuestion.getLevelId();
	}

	/**
	* Returns the primary key of this test question.
	*
	* @return the primary key of this test question
	*/
	@Override
	public long getPrimaryKey() {
		return _testQuestion.getPrimaryKey();
	}

	/**
	* Returns the question ID of this test question.
	*
	* @return the question ID of this test question
	*/
	@Override
	public long getQuestionId() {
		return _testQuestion.getQuestionId();
	}

	/**
	* Returns the reporter ID of this test question.
	*
	* @return the reporter ID of this test question
	*/
	@Override
	public long getReporterId() {
		return _testQuestion.getReporterId();
	}

	/**
	* Returns the subcategory ID of this test question.
	*
	* @return the subcategory ID of this test question
	*/
	@Override
	public long getSubcategoryId() {
		return _testQuestion.getSubcategoryId();
	}

	/**
	* Returns the user ID creator of this test question.
	*
	* @return the user ID creator of this test question
	*/
	@Override
	public long getUserIdCreator() {
		return _testQuestion.getUserIdCreator();
	}

	/**
	* Returns the user ID modified of this test question.
	*
	* @return the user ID modified of this test question
	*/
	@Override
	public long getUserIdModified() {
		return _testQuestion.getUserIdModified();
	}

	@Override
	public void persist() {
		_testQuestion.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {
		_testQuestion.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
		java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {
		_testQuestion.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	* Sets the answer0 text of this test question.
	*
	* @param answer0Text the answer0 text of this test question
	*/
	@Override
	public void setAnswer0Text(java.lang.String answer0Text) {
		_testQuestion.setAnswer0Text(answer0Text);
	}

	/**
	* Sets the localized answer0 text of this test question in the language.
	*
	* @param answer0Text the localized answer0 text of this test question
	* @param locale the locale of the language
	*/
	@Override
	public void setAnswer0Text(java.lang.String answer0Text,
		java.util.Locale locale) {
		_testQuestion.setAnswer0Text(answer0Text, locale);
	}

	/**
	* Sets the localized answer0 text of this test question in the language, and sets the default locale.
	*
	* @param answer0Text the localized answer0 text of this test question
	* @param locale the locale of the language
	* @param defaultLocale the default locale
	*/
	@Override
	public void setAnswer0Text(java.lang.String answer0Text,
		java.util.Locale locale, java.util.Locale defaultLocale) {
		_testQuestion.setAnswer0Text(answer0Text, locale, defaultLocale);
	}

	@Override
	public void setAnswer0TextCurrentLanguageId(java.lang.String languageId) {
		_testQuestion.setAnswer0TextCurrentLanguageId(languageId);
	}

	/**
	* Sets the localized answer0 texts of this test question from the map of locales and localized answer0 texts.
	*
	* @param answer0TextMap the locales and localized answer0 texts of this test question
	*/
	@Override
	public void setAnswer0TextMap(
		Map<java.util.Locale, java.lang.String> answer0TextMap) {
		_testQuestion.setAnswer0TextMap(answer0TextMap);
	}

	/**
	* Sets the localized answer0 texts of this test question from the map of locales and localized answer0 texts, and sets the default locale.
	*
	* @param answer0TextMap the locales and localized answer0 texts of this test question
	* @param defaultLocale the default locale
	*/
	@Override
	public void setAnswer0TextMap(
		Map<java.util.Locale, java.lang.String> answer0TextMap,
		java.util.Locale defaultLocale) {
		_testQuestion.setAnswer0TextMap(answer0TextMap, defaultLocale);
	}

	/**
	* Sets the answer1 text of this test question.
	*
	* @param answer1Text the answer1 text of this test question
	*/
	@Override
	public void setAnswer1Text(java.lang.String answer1Text) {
		_testQuestion.setAnswer1Text(answer1Text);
	}

	/**
	* Sets the localized answer1 text of this test question in the language.
	*
	* @param answer1Text the localized answer1 text of this test question
	* @param locale the locale of the language
	*/
	@Override
	public void setAnswer1Text(java.lang.String answer1Text,
		java.util.Locale locale) {
		_testQuestion.setAnswer1Text(answer1Text, locale);
	}

	/**
	* Sets the localized answer1 text of this test question in the language, and sets the default locale.
	*
	* @param answer1Text the localized answer1 text of this test question
	* @param locale the locale of the language
	* @param defaultLocale the default locale
	*/
	@Override
	public void setAnswer1Text(java.lang.String answer1Text,
		java.util.Locale locale, java.util.Locale defaultLocale) {
		_testQuestion.setAnswer1Text(answer1Text, locale, defaultLocale);
	}

	@Override
	public void setAnswer1TextCurrentLanguageId(java.lang.String languageId) {
		_testQuestion.setAnswer1TextCurrentLanguageId(languageId);
	}

	/**
	* Sets the localized answer1 texts of this test question from the map of locales and localized answer1 texts.
	*
	* @param answer1TextMap the locales and localized answer1 texts of this test question
	*/
	@Override
	public void setAnswer1TextMap(
		Map<java.util.Locale, java.lang.String> answer1TextMap) {
		_testQuestion.setAnswer1TextMap(answer1TextMap);
	}

	/**
	* Sets the localized answer1 texts of this test question from the map of locales and localized answer1 texts, and sets the default locale.
	*
	* @param answer1TextMap the locales and localized answer1 texts of this test question
	* @param defaultLocale the default locale
	*/
	@Override
	public void setAnswer1TextMap(
		Map<java.util.Locale, java.lang.String> answer1TextMap,
		java.util.Locale defaultLocale) {
		_testQuestion.setAnswer1TextMap(answer1TextMap, defaultLocale);
	}

	/**
	* Sets the answer2 text of this test question.
	*
	* @param answer2Text the answer2 text of this test question
	*/
	@Override
	public void setAnswer2Text(java.lang.String answer2Text) {
		_testQuestion.setAnswer2Text(answer2Text);
	}

	/**
	* Sets the localized answer2 text of this test question in the language.
	*
	* @param answer2Text the localized answer2 text of this test question
	* @param locale the locale of the language
	*/
	@Override
	public void setAnswer2Text(java.lang.String answer2Text,
		java.util.Locale locale) {
		_testQuestion.setAnswer2Text(answer2Text, locale);
	}

	/**
	* Sets the localized answer2 text of this test question in the language, and sets the default locale.
	*
	* @param answer2Text the localized answer2 text of this test question
	* @param locale the locale of the language
	* @param defaultLocale the default locale
	*/
	@Override
	public void setAnswer2Text(java.lang.String answer2Text,
		java.util.Locale locale, java.util.Locale defaultLocale) {
		_testQuestion.setAnswer2Text(answer2Text, locale, defaultLocale);
	}

	@Override
	public void setAnswer2TextCurrentLanguageId(java.lang.String languageId) {
		_testQuestion.setAnswer2TextCurrentLanguageId(languageId);
	}

	/**
	* Sets the localized answer2 texts of this test question from the map of locales and localized answer2 texts.
	*
	* @param answer2TextMap the locales and localized answer2 texts of this test question
	*/
	@Override
	public void setAnswer2TextMap(
		Map<java.util.Locale, java.lang.String> answer2TextMap) {
		_testQuestion.setAnswer2TextMap(answer2TextMap);
	}

	/**
	* Sets the localized answer2 texts of this test question from the map of locales and localized answer2 texts, and sets the default locale.
	*
	* @param answer2TextMap the locales and localized answer2 texts of this test question
	* @param defaultLocale the default locale
	*/
	@Override
	public void setAnswer2TextMap(
		Map<java.util.Locale, java.lang.String> answer2TextMap,
		java.util.Locale defaultLocale) {
		_testQuestion.setAnswer2TextMap(answer2TextMap, defaultLocale);
	}

	/**
	* Sets the answer3 text of this test question.
	*
	* @param answer3Text the answer3 text of this test question
	*/
	@Override
	public void setAnswer3Text(java.lang.String answer3Text) {
		_testQuestion.setAnswer3Text(answer3Text);
	}

	/**
	* Sets the localized answer3 text of this test question in the language.
	*
	* @param answer3Text the localized answer3 text of this test question
	* @param locale the locale of the language
	*/
	@Override
	public void setAnswer3Text(java.lang.String answer3Text,
		java.util.Locale locale) {
		_testQuestion.setAnswer3Text(answer3Text, locale);
	}

	/**
	* Sets the localized answer3 text of this test question in the language, and sets the default locale.
	*
	* @param answer3Text the localized answer3 text of this test question
	* @param locale the locale of the language
	* @param defaultLocale the default locale
	*/
	@Override
	public void setAnswer3Text(java.lang.String answer3Text,
		java.util.Locale locale, java.util.Locale defaultLocale) {
		_testQuestion.setAnswer3Text(answer3Text, locale, defaultLocale);
	}

	@Override
	public void setAnswer3TextCurrentLanguageId(java.lang.String languageId) {
		_testQuestion.setAnswer3TextCurrentLanguageId(languageId);
	}

	/**
	* Sets the localized answer3 texts of this test question from the map of locales and localized answer3 texts.
	*
	* @param answer3TextMap the locales and localized answer3 texts of this test question
	*/
	@Override
	public void setAnswer3TextMap(
		Map<java.util.Locale, java.lang.String> answer3TextMap) {
		_testQuestion.setAnswer3TextMap(answer3TextMap);
	}

	/**
	* Sets the localized answer3 texts of this test question from the map of locales and localized answer3 texts, and sets the default locale.
	*
	* @param answer3TextMap the locales and localized answer3 texts of this test question
	* @param defaultLocale the default locale
	*/
	@Override
	public void setAnswer3TextMap(
		Map<java.util.Locale, java.lang.String> answer3TextMap,
		java.util.Locale defaultLocale) {
		_testQuestion.setAnswer3TextMap(answer3TextMap, defaultLocale);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_testQuestion.setCachedModel(cachedModel);
	}

	/**
	* Sets the category ID of this test question.
	*
	* @param categoryId the category ID of this test question
	*/
	@Override
	public void setCategoryId(long categoryId) {
		_testQuestion.setCategoryId(categoryId);
	}

	/**
	* Sets the company ID of this test question.
	*
	* @param companyId the company ID of this test question
	*/
	@Override
	public void setCompanyId(long companyId) {
		_testQuestion.setCompanyId(companyId);
	}

	/**
	* Sets the correct answers of this test question.
	*
	* @param correctAnswers the correct answers of this test question
	*/
	@Override
	public void setCorrectAnswers(java.lang.String correctAnswers) {
		_testQuestion.setCorrectAnswers(correctAnswers);
	}

	/**
	* Sets the create date of this test question.
	*
	* @param createDate the create date of this test question
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_testQuestion.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_testQuestion.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_testQuestion.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_testQuestion.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this test question.
	*
	* @param groupId the group ID of this test question
	*/
	@Override
	public void setGroupId(long groupId) {
		_testQuestion.setGroupId(groupId);
	}

	/**
	* Sets the level ID of this test question.
	*
	* @param levelId the level ID of this test question
	*/
	@Override
	public void setLevelId(long levelId) {
		_testQuestion.setLevelId(levelId);
	}

	/**
	* Sets the modified date of this test question.
	*
	* @param modifiedDate the modified date of this test question
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_testQuestion.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_testQuestion.setNew(n);
	}

	/**
	* Sets the primary key of this test question.
	*
	* @param primaryKey the primary key of this test question
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_testQuestion.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_testQuestion.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the question ID of this test question.
	*
	* @param questionId the question ID of this test question
	*/
	@Override
	public void setQuestionId(long questionId) {
		_testQuestion.setQuestionId(questionId);
	}

	/**
	* Sets the question text of this test question.
	*
	* @param questionText the question text of this test question
	*/
	@Override
	public void setQuestionText(java.lang.String questionText) {
		_testQuestion.setQuestionText(questionText);
	}

	/**
	* Sets the localized question text of this test question in the language.
	*
	* @param questionText the localized question text of this test question
	* @param locale the locale of the language
	*/
	@Override
	public void setQuestionText(java.lang.String questionText,
		java.util.Locale locale) {
		_testQuestion.setQuestionText(questionText, locale);
	}

	/**
	* Sets the localized question text of this test question in the language, and sets the default locale.
	*
	* @param questionText the localized question text of this test question
	* @param locale the locale of the language
	* @param defaultLocale the default locale
	*/
	@Override
	public void setQuestionText(java.lang.String questionText,
		java.util.Locale locale, java.util.Locale defaultLocale) {
		_testQuestion.setQuestionText(questionText, locale, defaultLocale);
	}

	@Override
	public void setQuestionTextCurrentLanguageId(java.lang.String languageId) {
		_testQuestion.setQuestionTextCurrentLanguageId(languageId);
	}

	/**
	* Sets the localized question texts of this test question from the map of locales and localized question texts.
	*
	* @param questionTextMap the locales and localized question texts of this test question
	*/
	@Override
	public void setQuestionTextMap(
		Map<java.util.Locale, java.lang.String> questionTextMap) {
		_testQuestion.setQuestionTextMap(questionTextMap);
	}

	/**
	* Sets the localized question texts of this test question from the map of locales and localized question texts, and sets the default locale.
	*
	* @param questionTextMap the locales and localized question texts of this test question
	* @param defaultLocale the default locale
	*/
	@Override
	public void setQuestionTextMap(
		Map<java.util.Locale, java.lang.String> questionTextMap,
		java.util.Locale defaultLocale) {
		_testQuestion.setQuestionTextMap(questionTextMap, defaultLocale);
	}

	/**
	* Sets the report content of this test question.
	*
	* @param reportContent the report content of this test question
	*/
	@Override
	public void setReportContent(java.lang.String reportContent) {
		_testQuestion.setReportContent(reportContent);
	}

	/**
	* Sets the reporter ID of this test question.
	*
	* @param reporterId the reporter ID of this test question
	*/
	@Override
	public void setReporterId(long reporterId) {
		_testQuestion.setReporterId(reporterId);
	}

	/**
	* Sets the status of this test question.
	*
	* @param status the status of this test question
	*/
	@Override
	public void setStatus(int status) {
		_testQuestion.setStatus(status);
	}

	/**
	* Sets the subcategory ID of this test question.
	*
	* @param subcategoryId the subcategory ID of this test question
	*/
	@Override
	public void setSubcategoryId(long subcategoryId) {
		_testQuestion.setSubcategoryId(subcategoryId);
	}

	/**
	* Sets the type of this test question.
	*
	* @param type the type of this test question
	*/
	@Override
	public void setType(int type) {
		_testQuestion.setType(type);
	}

	/**
	* Sets the user ID creator of this test question.
	*
	* @param userIdCreator the user ID creator of this test question
	*/
	@Override
	public void setUserIdCreator(long userIdCreator) {
		_testQuestion.setUserIdCreator(userIdCreator);
	}

	/**
	* Sets the user ID modified of this test question.
	*
	* @param userIdModified the user ID modified of this test question
	*/
	@Override
	public void setUserIdModified(long userIdModified) {
		_testQuestion.setUserIdModified(userIdModified);
	}

	/**
	* Sets the user name creator of this test question.
	*
	* @param userNameCreator the user name creator of this test question
	*/
	@Override
	public void setUserNameCreator(java.lang.String userNameCreator) {
		_testQuestion.setUserNameCreator(userNameCreator);
	}

	/**
	* Sets the user name modified of this test question.
	*
	* @param userNameModified the user name modified of this test question
	*/
	@Override
	public void setUserNameModified(java.lang.String userNameModified) {
		_testQuestion.setUserNameModified(userNameModified);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TestQuestionWrapper)) {
			return false;
		}

		TestQuestionWrapper testQuestionWrapper = (TestQuestionWrapper)obj;

		if (Objects.equals(_testQuestion, testQuestionWrapper._testQuestion)) {
			return true;
		}

		return false;
	}

	@Override
	public TestQuestion getWrappedModel() {
		return _testQuestion;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _testQuestion.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _testQuestion.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_testQuestion.resetOriginalValues();
	}

	private final TestQuestion _testQuestion;
}