/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.service.persistence.BadgeRulePK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class BadgeRuleSoap implements Serializable {
	public static BadgeRuleSoap toSoapModel(BadgeRule model) {
		BadgeRuleSoap soapModel = new BadgeRuleSoap();

		soapModel.setRuleId(model.getRuleId());
		soapModel.setBadgeId(model.getBadgeId());
		soapModel.setCompanyId(model.getCompanyId());

		return soapModel;
	}

	public static BadgeRuleSoap[] toSoapModels(BadgeRule[] models) {
		BadgeRuleSoap[] soapModels = new BadgeRuleSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static BadgeRuleSoap[][] toSoapModels(BadgeRule[][] models) {
		BadgeRuleSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new BadgeRuleSoap[models.length][models[0].length];
		}
		else {
			soapModels = new BadgeRuleSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static BadgeRuleSoap[] toSoapModels(List<BadgeRule> models) {
		List<BadgeRuleSoap> soapModels = new ArrayList<BadgeRuleSoap>(models.size());

		for (BadgeRule model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new BadgeRuleSoap[soapModels.size()]);
	}

	public BadgeRuleSoap() {
	}

	public BadgeRulePK getPrimaryKey() {
		return new BadgeRulePK(_ruleId, _badgeId);
	}

	public void setPrimaryKey(BadgeRulePK pk) {
		setRuleId(pk.ruleId);
		setBadgeId(pk.badgeId);
	}

	public long getRuleId() {
		return _ruleId;
	}

	public void setRuleId(long ruleId) {
		_ruleId = ruleId;
	}

	public long getBadgeId() {
		return _badgeId;
	}

	public void setBadgeId(long badgeId) {
		_badgeId = badgeId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	private long _ruleId;
	private long _badgeId;
	private long _companyId;
}