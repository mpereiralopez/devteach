/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.service.persistence.UserBadgePK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class UserBadgeSoap implements Serializable {
	public static UserBadgeSoap toSoapModel(UserBadge model) {
		UserBadgeSoap soapModel = new UserBadgeSoap();

		soapModel.setUserId(model.getUserId());
		soapModel.setBadgeId(model.getBadgeId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());

		return soapModel;
	}

	public static UserBadgeSoap[] toSoapModels(UserBadge[] models) {
		UserBadgeSoap[] soapModels = new UserBadgeSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UserBadgeSoap[][] toSoapModels(UserBadge[][] models) {
		UserBadgeSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UserBadgeSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UserBadgeSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UserBadgeSoap[] toSoapModels(List<UserBadge> models) {
		List<UserBadgeSoap> soapModels = new ArrayList<UserBadgeSoap>(models.size());

		for (UserBadge model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UserBadgeSoap[soapModels.size()]);
	}

	public UserBadgeSoap() {
	}

	public UserBadgePK getPrimaryKey() {
		return new UserBadgePK(_userId, _badgeId);
	}

	public void setPrimaryKey(UserBadgePK pk) {
		setUserId(pk.userId);
		setBadgeId(pk.badgeId);
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public long getBadgeId() {
		return _badgeId;
	}

	public void setBadgeId(long badgeId) {
		_badgeId = badgeId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	private long _userId;
	private long _badgeId;
	private long _companyId;
	private Date _createDate;
	private Date _modifiedDate;
}