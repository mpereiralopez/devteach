/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Rule}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Rule
 * @generated
 */
@ProviderType
public class RuleWrapper implements Rule, ModelWrapper<Rule> {
	public RuleWrapper(Rule rule) {
		_rule = rule;
	}

	@Override
	public Class<?> getModelClass() {
		return Rule.class;
	}

	@Override
	public String getModelClassName() {
		return Rule.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ruleId", getRuleId());
		attributes.put("ruleDesc", getRuleDesc());
		attributes.put("companyId", getCompanyId());
		attributes.put("userCreatorId", getUserCreatorId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ruleId = (Long)attributes.get("ruleId");

		if (ruleId != null) {
			setRuleId(ruleId);
		}

		String ruleDesc = (String)attributes.get("ruleDesc");

		if (ruleDesc != null) {
			setRuleDesc(ruleDesc);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userCreatorId = (Long)attributes.get("userCreatorId");

		if (userCreatorId != null) {
			setUserCreatorId(userCreatorId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}
	}

	@Override
	public Rule toEscapedModel() {
		return new RuleWrapper(_rule.toEscapedModel());
	}

	@Override
	public Rule toUnescapedModel() {
		return new RuleWrapper(_rule.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _rule.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _rule.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _rule.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _rule.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Rule> toCacheModel() {
		return _rule.toCacheModel();
	}

	@Override
	public int compareTo(Rule rule) {
		return _rule.compareTo(rule);
	}

	@Override
	public int hashCode() {
		return _rule.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _rule.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new RuleWrapper((Rule)_rule.clone());
	}

	/**
	* Returns the rule desc of this rule.
	*
	* @return the rule desc of this rule
	*/
	@Override
	public java.lang.String getRuleDesc() {
		return _rule.getRuleDesc();
	}

	/**
	* Returns the user name of this rule.
	*
	* @return the user name of this rule
	*/
	@Override
	public java.lang.String getUserName() {
		return _rule.getUserName();
	}

	@Override
	public java.lang.String toString() {
		return _rule.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _rule.toXmlString();
	}

	/**
	* Returns the create date of this rule.
	*
	* @return the create date of this rule
	*/
	@Override
	public Date getCreateDate() {
		return _rule.getCreateDate();
	}

	/**
	* Returns the modified date of this rule.
	*
	* @return the modified date of this rule
	*/
	@Override
	public Date getModifiedDate() {
		return _rule.getModifiedDate();
	}

	/**
	* Returns the company ID of this rule.
	*
	* @return the company ID of this rule
	*/
	@Override
	public long getCompanyId() {
		return _rule.getCompanyId();
	}

	/**
	* Returns the primary key of this rule.
	*
	* @return the primary key of this rule
	*/
	@Override
	public long getPrimaryKey() {
		return _rule.getPrimaryKey();
	}

	/**
	* Returns the rule ID of this rule.
	*
	* @return the rule ID of this rule
	*/
	@Override
	public long getRuleId() {
		return _rule.getRuleId();
	}

	/**
	* Returns the user creator ID of this rule.
	*
	* @return the user creator ID of this rule
	*/
	@Override
	public long getUserCreatorId() {
		return _rule.getUserCreatorId();
	}

	@Override
	public void persist() {
		_rule.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_rule.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this rule.
	*
	* @param companyId the company ID of this rule
	*/
	@Override
	public void setCompanyId(long companyId) {
		_rule.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this rule.
	*
	* @param createDate the create date of this rule
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_rule.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_rule.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_rule.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_rule.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the modified date of this rule.
	*
	* @param modifiedDate the modified date of this rule
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_rule.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_rule.setNew(n);
	}

	/**
	* Sets the primary key of this rule.
	*
	* @param primaryKey the primary key of this rule
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_rule.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_rule.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the rule desc of this rule.
	*
	* @param ruleDesc the rule desc of this rule
	*/
	@Override
	public void setRuleDesc(java.lang.String ruleDesc) {
		_rule.setRuleDesc(ruleDesc);
	}

	/**
	* Sets the rule ID of this rule.
	*
	* @param ruleId the rule ID of this rule
	*/
	@Override
	public void setRuleId(long ruleId) {
		_rule.setRuleId(ruleId);
	}

	/**
	* Sets the user creator ID of this rule.
	*
	* @param userCreatorId the user creator ID of this rule
	*/
	@Override
	public void setUserCreatorId(long userCreatorId) {
		_rule.setUserCreatorId(userCreatorId);
	}

	/**
	* Sets the user name of this rule.
	*
	* @param userName the user name of this rule
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_rule.setUserName(userName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof RuleWrapper)) {
			return false;
		}

		RuleWrapper ruleWrapper = (RuleWrapper)obj;

		if (Objects.equals(_rule, ruleWrapper._rule)) {
			return true;
		}

		return false;
	}

	@Override
	public Rule getWrappedModel() {
		return _rule;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _rule.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _rule.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_rule.resetOriginalValues();
	}

	private final Rule _rule;
}