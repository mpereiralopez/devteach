/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the UserTestQuestionResult service. Represents a row in the &quot;devShaker_UserTestQuestionResult&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see UserTestQuestionResultModel
 * @see com.devshaker.model.impl.UserTestQuestionResultImpl
 * @see com.devshaker.model.impl.UserTestQuestionResultModelImpl
 * @generated
 */
@ImplementationClassName("com.devshaker.model.impl.UserTestQuestionResultImpl")
@ProviderType
public interface UserTestQuestionResult extends UserTestQuestionResultModel,
	PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link com.devshaker.model.impl.UserTestQuestionResultImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<UserTestQuestionResult, Long> USER_TEST_QUESTION_RESULT_ID_ACCESSOR =
		new Accessor<UserTestQuestionResult, Long>() {
			@Override
			public Long get(UserTestQuestionResult userTestQuestionResult) {
				return userTestQuestionResult.getUserTestQuestionResultId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<UserTestQuestionResult> getTypeClass() {
				return UserTestQuestionResult.class;
			}
		};
}