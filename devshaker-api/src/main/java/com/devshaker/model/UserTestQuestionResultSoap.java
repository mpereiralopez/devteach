/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class UserTestQuestionResultSoap implements Serializable {
	public static UserTestQuestionResultSoap toSoapModel(
		UserTestQuestionResult model) {
		UserTestQuestionResultSoap soapModel = new UserTestQuestionResultSoap();

		soapModel.setUserTestQuestionResultId(model.getUserTestQuestionResultId());
		soapModel.setQuestionId(model.getQuestionId());
		soapModel.setUserId(model.getUserId());
		soapModel.setAnswersByUser(model.getAnswersByUser());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setIsCorrect(model.getIsCorrect());
		soapModel.setTokenData(model.getTokenData());

		return soapModel;
	}

	public static UserTestQuestionResultSoap[] toSoapModels(
		UserTestQuestionResult[] models) {
		UserTestQuestionResultSoap[] soapModels = new UserTestQuestionResultSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UserTestQuestionResultSoap[][] toSoapModels(
		UserTestQuestionResult[][] models) {
		UserTestQuestionResultSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UserTestQuestionResultSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UserTestQuestionResultSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UserTestQuestionResultSoap[] toSoapModels(
		List<UserTestQuestionResult> models) {
		List<UserTestQuestionResultSoap> soapModels = new ArrayList<UserTestQuestionResultSoap>(models.size());

		for (UserTestQuestionResult model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UserTestQuestionResultSoap[soapModels.size()]);
	}

	public UserTestQuestionResultSoap() {
	}

	public long getPrimaryKey() {
		return _userTestQuestionResultId;
	}

	public void setPrimaryKey(long pk) {
		setUserTestQuestionResultId(pk);
	}

	public long getUserTestQuestionResultId() {
		return _userTestQuestionResultId;
	}

	public void setUserTestQuestionResultId(long userTestQuestionResultId) {
		_userTestQuestionResultId = userTestQuestionResultId;
	}

	public long getQuestionId() {
		return _questionId;
	}

	public void setQuestionId(long questionId) {
		_questionId = questionId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getAnswersByUser() {
		return _answersByUser;
	}

	public void setAnswersByUser(String answersByUser) {
		_answersByUser = answersByUser;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public boolean getIsCorrect() {
		return _isCorrect;
	}

	public boolean isIsCorrect() {
		return _isCorrect;
	}

	public void setIsCorrect(boolean isCorrect) {
		_isCorrect = isCorrect;
	}

	public String getTokenData() {
		return _tokenData;
	}

	public void setTokenData(String tokenData) {
		_tokenData = tokenData;
	}

	private long _userTestQuestionResultId;
	private long _questionId;
	private long _userId;
	private String _answersByUser;
	private Date _createDate;
	private boolean _isCorrect;
	private String _tokenData;
}