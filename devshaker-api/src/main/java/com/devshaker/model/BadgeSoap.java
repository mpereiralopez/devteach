/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class BadgeSoap implements Serializable {
	public static BadgeSoap toSoapModel(Badge model) {
		BadgeSoap soapModel = new BadgeSoap();

		soapModel.setBadgeId(model.getBadgeId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserCreatorId(model.getUserCreatorId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setBadgeTitle(model.getBadgeTitle());
		soapModel.setBadgeDescription(model.getBadgeDescription());
		soapModel.setBadgeIcon(model.getBadgeIcon());
		soapModel.setObjPK(model.getObjPK());

		return soapModel;
	}

	public static BadgeSoap[] toSoapModels(Badge[] models) {
		BadgeSoap[] soapModels = new BadgeSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static BadgeSoap[][] toSoapModels(Badge[][] models) {
		BadgeSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new BadgeSoap[models.length][models[0].length];
		}
		else {
			soapModels = new BadgeSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static BadgeSoap[] toSoapModels(List<Badge> models) {
		List<BadgeSoap> soapModels = new ArrayList<BadgeSoap>(models.size());

		for (Badge model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new BadgeSoap[soapModels.size()]);
	}

	public BadgeSoap() {
	}

	public long getPrimaryKey() {
		return _badgeId;
	}

	public void setPrimaryKey(long pk) {
		setBadgeId(pk);
	}

	public long getBadgeId() {
		return _badgeId;
	}

	public void setBadgeId(long badgeId) {
		_badgeId = badgeId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserCreatorId() {
		return _userCreatorId;
	}

	public void setUserCreatorId(long userCreatorId) {
		_userCreatorId = userCreatorId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getBadgeTitle() {
		return _badgeTitle;
	}

	public void setBadgeTitle(String badgeTitle) {
		_badgeTitle = badgeTitle;
	}

	public String getBadgeDescription() {
		return _badgeDescription;
	}

	public void setBadgeDescription(String badgeDescription) {
		_badgeDescription = badgeDescription;
	}

	public long getBadgeIcon() {
		return _badgeIcon;
	}

	public void setBadgeIcon(long badgeIcon) {
		_badgeIcon = badgeIcon;
	}

	public long getObjPK() {
		return _objPK;
	}

	public void setObjPK(long objPK) {
		_objPK = objPK;
	}

	private long _badgeId;
	private long _groupId;
	private long _companyId;
	private long _userCreatorId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _badgeTitle;
	private String _badgeDescription;
	private long _badgeIcon;
	private long _objPK;
}