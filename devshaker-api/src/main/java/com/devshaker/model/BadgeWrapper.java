/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Badge}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Badge
 * @generated
 */
@ProviderType
public class BadgeWrapper implements Badge, ModelWrapper<Badge> {
	public BadgeWrapper(Badge badge) {
		_badge = badge;
	}

	@Override
	public Class<?> getModelClass() {
		return Badge.class;
	}

	@Override
	public String getModelClassName() {
		return Badge.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("badgeId", getBadgeId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userCreatorId", getUserCreatorId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("badgeTitle", getBadgeTitle());
		attributes.put("badgeDescription", getBadgeDescription());
		attributes.put("badgeIcon", getBadgeIcon());
		attributes.put("objPK", getObjPK());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long badgeId = (Long)attributes.get("badgeId");

		if (badgeId != null) {
			setBadgeId(badgeId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userCreatorId = (Long)attributes.get("userCreatorId");

		if (userCreatorId != null) {
			setUserCreatorId(userCreatorId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String badgeTitle = (String)attributes.get("badgeTitle");

		if (badgeTitle != null) {
			setBadgeTitle(badgeTitle);
		}

		String badgeDescription = (String)attributes.get("badgeDescription");

		if (badgeDescription != null) {
			setBadgeDescription(badgeDescription);
		}

		Long badgeIcon = (Long)attributes.get("badgeIcon");

		if (badgeIcon != null) {
			setBadgeIcon(badgeIcon);
		}

		Long objPK = (Long)attributes.get("objPK");

		if (objPK != null) {
			setObjPK(objPK);
		}
	}

	@Override
	public Badge toEscapedModel() {
		return new BadgeWrapper(_badge.toEscapedModel());
	}

	@Override
	public Badge toUnescapedModel() {
		return new BadgeWrapper(_badge.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _badge.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _badge.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _badge.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _badge.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Badge> toCacheModel() {
		return _badge.toCacheModel();
	}

	@Override
	public int compareTo(Badge badge) {
		return _badge.compareTo(badge);
	}

	@Override
	public int hashCode() {
		return _badge.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _badge.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new BadgeWrapper((Badge)_badge.clone());
	}

	/**
	* Returns the badge description of this badge.
	*
	* @return the badge description of this badge
	*/
	@Override
	public java.lang.String getBadgeDescription() {
		return _badge.getBadgeDescription();
	}

	/**
	* Returns the localized badge description of this badge in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @return the localized badge description of this badge
	*/
	@Override
	public java.lang.String getBadgeDescription(java.lang.String languageId) {
		return _badge.getBadgeDescription(languageId);
	}

	/**
	* Returns the localized badge description of this badge in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized badge description of this badge
	*/
	@Override
	public java.lang.String getBadgeDescription(java.lang.String languageId,
		boolean useDefault) {
		return _badge.getBadgeDescription(languageId, useDefault);
	}

	/**
	* Returns the localized badge description of this badge in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param locale the locale of the language
	* @return the localized badge description of this badge
	*/
	@Override
	public java.lang.String getBadgeDescription(java.util.Locale locale) {
		return _badge.getBadgeDescription(locale);
	}

	/**
	* Returns the localized badge description of this badge in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param locale the local of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized badge description of this badge. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	*/
	@Override
	public java.lang.String getBadgeDescription(java.util.Locale locale,
		boolean useDefault) {
		return _badge.getBadgeDescription(locale, useDefault);
	}

	@Override
	public java.lang.String getBadgeDescriptionCurrentLanguageId() {
		return _badge.getBadgeDescriptionCurrentLanguageId();
	}

	@Override
	public java.lang.String getBadgeDescriptionCurrentValue() {
		return _badge.getBadgeDescriptionCurrentValue();
	}

	/**
	* Returns the badge title of this badge.
	*
	* @return the badge title of this badge
	*/
	@Override
	public java.lang.String getBadgeTitle() {
		return _badge.getBadgeTitle();
	}

	/**
	* Returns the localized badge title of this badge in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @return the localized badge title of this badge
	*/
	@Override
	public java.lang.String getBadgeTitle(java.lang.String languageId) {
		return _badge.getBadgeTitle(languageId);
	}

	/**
	* Returns the localized badge title of this badge in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized badge title of this badge
	*/
	@Override
	public java.lang.String getBadgeTitle(java.lang.String languageId,
		boolean useDefault) {
		return _badge.getBadgeTitle(languageId, useDefault);
	}

	/**
	* Returns the localized badge title of this badge in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param locale the locale of the language
	* @return the localized badge title of this badge
	*/
	@Override
	public java.lang.String getBadgeTitle(java.util.Locale locale) {
		return _badge.getBadgeTitle(locale);
	}

	/**
	* Returns the localized badge title of this badge in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param locale the local of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized badge title of this badge. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	*/
	@Override
	public java.lang.String getBadgeTitle(java.util.Locale locale,
		boolean useDefault) {
		return _badge.getBadgeTitle(locale, useDefault);
	}

	@Override
	public java.lang.String getBadgeTitleCurrentLanguageId() {
		return _badge.getBadgeTitleCurrentLanguageId();
	}

	@Override
	public java.lang.String getBadgeTitleCurrentValue() {
		return _badge.getBadgeTitleCurrentValue();
	}

	@Override
	public java.lang.String getDefaultLanguageId() {
		return _badge.getDefaultLanguageId();
	}

	/**
	* Returns the user name of this badge.
	*
	* @return the user name of this badge
	*/
	@Override
	public java.lang.String getUserName() {
		return _badge.getUserName();
	}

	@Override
	public java.lang.String toString() {
		return _badge.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _badge.toXmlString();
	}

	@Override
	public java.lang.String[] getAvailableLanguageIds() {
		return _badge.getAvailableLanguageIds();
	}

	/**
	* Returns the create date of this badge.
	*
	* @return the create date of this badge
	*/
	@Override
	public Date getCreateDate() {
		return _badge.getCreateDate();
	}

	/**
	* Returns the modified date of this badge.
	*
	* @return the modified date of this badge
	*/
	@Override
	public Date getModifiedDate() {
		return _badge.getModifiedDate();
	}

	/**
	* Returns a map of the locales and localized badge descriptions of this badge.
	*
	* @return the locales and localized badge descriptions of this badge
	*/
	@Override
	public Map<java.util.Locale, java.lang.String> getBadgeDescriptionMap() {
		return _badge.getBadgeDescriptionMap();
	}

	/**
	* Returns a map of the locales and localized badge titles of this badge.
	*
	* @return the locales and localized badge titles of this badge
	*/
	@Override
	public Map<java.util.Locale, java.lang.String> getBadgeTitleMap() {
		return _badge.getBadgeTitleMap();
	}

	/**
	* Returns the badge icon of this badge.
	*
	* @return the badge icon of this badge
	*/
	@Override
	public long getBadgeIcon() {
		return _badge.getBadgeIcon();
	}

	/**
	* Returns the badge ID of this badge.
	*
	* @return the badge ID of this badge
	*/
	@Override
	public long getBadgeId() {
		return _badge.getBadgeId();
	}

	/**
	* Returns the company ID of this badge.
	*
	* @return the company ID of this badge
	*/
	@Override
	public long getCompanyId() {
		return _badge.getCompanyId();
	}

	/**
	* Returns the group ID of this badge.
	*
	* @return the group ID of this badge
	*/
	@Override
	public long getGroupId() {
		return _badge.getGroupId();
	}

	/**
	* Returns the obj pk of this badge.
	*
	* @return the obj pk of this badge
	*/
	@Override
	public long getObjPK() {
		return _badge.getObjPK();
	}

	/**
	* Returns the primary key of this badge.
	*
	* @return the primary key of this badge
	*/
	@Override
	public long getPrimaryKey() {
		return _badge.getPrimaryKey();
	}

	/**
	* Returns the user creator ID of this badge.
	*
	* @return the user creator ID of this badge
	*/
	@Override
	public long getUserCreatorId() {
		return _badge.getUserCreatorId();
	}

	@Override
	public void persist() {
		_badge.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {
		_badge.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
		java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {
		_badge.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	* Sets the badge description of this badge.
	*
	* @param badgeDescription the badge description of this badge
	*/
	@Override
	public void setBadgeDescription(java.lang.String badgeDescription) {
		_badge.setBadgeDescription(badgeDescription);
	}

	/**
	* Sets the localized badge description of this badge in the language.
	*
	* @param badgeDescription the localized badge description of this badge
	* @param locale the locale of the language
	*/
	@Override
	public void setBadgeDescription(java.lang.String badgeDescription,
		java.util.Locale locale) {
		_badge.setBadgeDescription(badgeDescription, locale);
	}

	/**
	* Sets the localized badge description of this badge in the language, and sets the default locale.
	*
	* @param badgeDescription the localized badge description of this badge
	* @param locale the locale of the language
	* @param defaultLocale the default locale
	*/
	@Override
	public void setBadgeDescription(java.lang.String badgeDescription,
		java.util.Locale locale, java.util.Locale defaultLocale) {
		_badge.setBadgeDescription(badgeDescription, locale, defaultLocale);
	}

	@Override
	public void setBadgeDescriptionCurrentLanguageId(
		java.lang.String languageId) {
		_badge.setBadgeDescriptionCurrentLanguageId(languageId);
	}

	/**
	* Sets the localized badge descriptions of this badge from the map of locales and localized badge descriptions.
	*
	* @param badgeDescriptionMap the locales and localized badge descriptions of this badge
	*/
	@Override
	public void setBadgeDescriptionMap(
		Map<java.util.Locale, java.lang.String> badgeDescriptionMap) {
		_badge.setBadgeDescriptionMap(badgeDescriptionMap);
	}

	/**
	* Sets the localized badge descriptions of this badge from the map of locales and localized badge descriptions, and sets the default locale.
	*
	* @param badgeDescriptionMap the locales and localized badge descriptions of this badge
	* @param defaultLocale the default locale
	*/
	@Override
	public void setBadgeDescriptionMap(
		Map<java.util.Locale, java.lang.String> badgeDescriptionMap,
		java.util.Locale defaultLocale) {
		_badge.setBadgeDescriptionMap(badgeDescriptionMap, defaultLocale);
	}

	/**
	* Sets the badge icon of this badge.
	*
	* @param badgeIcon the badge icon of this badge
	*/
	@Override
	public void setBadgeIcon(long badgeIcon) {
		_badge.setBadgeIcon(badgeIcon);
	}

	/**
	* Sets the badge ID of this badge.
	*
	* @param badgeId the badge ID of this badge
	*/
	@Override
	public void setBadgeId(long badgeId) {
		_badge.setBadgeId(badgeId);
	}

	/**
	* Sets the badge title of this badge.
	*
	* @param badgeTitle the badge title of this badge
	*/
	@Override
	public void setBadgeTitle(java.lang.String badgeTitle) {
		_badge.setBadgeTitle(badgeTitle);
	}

	/**
	* Sets the localized badge title of this badge in the language.
	*
	* @param badgeTitle the localized badge title of this badge
	* @param locale the locale of the language
	*/
	@Override
	public void setBadgeTitle(java.lang.String badgeTitle,
		java.util.Locale locale) {
		_badge.setBadgeTitle(badgeTitle, locale);
	}

	/**
	* Sets the localized badge title of this badge in the language, and sets the default locale.
	*
	* @param badgeTitle the localized badge title of this badge
	* @param locale the locale of the language
	* @param defaultLocale the default locale
	*/
	@Override
	public void setBadgeTitle(java.lang.String badgeTitle,
		java.util.Locale locale, java.util.Locale defaultLocale) {
		_badge.setBadgeTitle(badgeTitle, locale, defaultLocale);
	}

	@Override
	public void setBadgeTitleCurrentLanguageId(java.lang.String languageId) {
		_badge.setBadgeTitleCurrentLanguageId(languageId);
	}

	/**
	* Sets the localized badge titles of this badge from the map of locales and localized badge titles.
	*
	* @param badgeTitleMap the locales and localized badge titles of this badge
	*/
	@Override
	public void setBadgeTitleMap(
		Map<java.util.Locale, java.lang.String> badgeTitleMap) {
		_badge.setBadgeTitleMap(badgeTitleMap);
	}

	/**
	* Sets the localized badge titles of this badge from the map of locales and localized badge titles, and sets the default locale.
	*
	* @param badgeTitleMap the locales and localized badge titles of this badge
	* @param defaultLocale the default locale
	*/
	@Override
	public void setBadgeTitleMap(
		Map<java.util.Locale, java.lang.String> badgeTitleMap,
		java.util.Locale defaultLocale) {
		_badge.setBadgeTitleMap(badgeTitleMap, defaultLocale);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_badge.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this badge.
	*
	* @param companyId the company ID of this badge
	*/
	@Override
	public void setCompanyId(long companyId) {
		_badge.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this badge.
	*
	* @param createDate the create date of this badge
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_badge.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_badge.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_badge.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_badge.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this badge.
	*
	* @param groupId the group ID of this badge
	*/
	@Override
	public void setGroupId(long groupId) {
		_badge.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this badge.
	*
	* @param modifiedDate the modified date of this badge
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_badge.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_badge.setNew(n);
	}

	/**
	* Sets the obj pk of this badge.
	*
	* @param objPK the obj pk of this badge
	*/
	@Override
	public void setObjPK(long objPK) {
		_badge.setObjPK(objPK);
	}

	/**
	* Sets the primary key of this badge.
	*
	* @param primaryKey the primary key of this badge
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_badge.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_badge.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the user creator ID of this badge.
	*
	* @param userCreatorId the user creator ID of this badge
	*/
	@Override
	public void setUserCreatorId(long userCreatorId) {
		_badge.setUserCreatorId(userCreatorId);
	}

	/**
	* Sets the user name of this badge.
	*
	* @param userName the user name of this badge
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_badge.setUserName(userName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BadgeWrapper)) {
			return false;
		}

		BadgeWrapper badgeWrapper = (BadgeWrapper)obj;

		if (Objects.equals(_badge, badgeWrapper._badge)) {
			return true;
		}

		return false;
	}

	@Override
	public Badge getWrappedModel() {
		return _badge;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _badge.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _badge.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_badge.resetOriginalValues();
	}

	private final Badge _badge;
}