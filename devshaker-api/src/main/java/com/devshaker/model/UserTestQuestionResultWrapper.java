/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link UserTestQuestionResult}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserTestQuestionResult
 * @generated
 */
@ProviderType
public class UserTestQuestionResultWrapper implements UserTestQuestionResult,
	ModelWrapper<UserTestQuestionResult> {
	public UserTestQuestionResultWrapper(
		UserTestQuestionResult userTestQuestionResult) {
		_userTestQuestionResult = userTestQuestionResult;
	}

	@Override
	public Class<?> getModelClass() {
		return UserTestQuestionResult.class;
	}

	@Override
	public String getModelClassName() {
		return UserTestQuestionResult.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("userTestQuestionResultId", getUserTestQuestionResultId());
		attributes.put("questionId", getQuestionId());
		attributes.put("userId", getUserId());
		attributes.put("answersByUser", getAnswersByUser());
		attributes.put("createDate", getCreateDate());
		attributes.put("isCorrect", getIsCorrect());
		attributes.put("tokenData", getTokenData());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long userTestQuestionResultId = (Long)attributes.get(
				"userTestQuestionResultId");

		if (userTestQuestionResultId != null) {
			setUserTestQuestionResultId(userTestQuestionResultId);
		}

		Long questionId = (Long)attributes.get("questionId");

		if (questionId != null) {
			setQuestionId(questionId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String answersByUser = (String)attributes.get("answersByUser");

		if (answersByUser != null) {
			setAnswersByUser(answersByUser);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Boolean isCorrect = (Boolean)attributes.get("isCorrect");

		if (isCorrect != null) {
			setIsCorrect(isCorrect);
		}

		String tokenData = (String)attributes.get("tokenData");

		if (tokenData != null) {
			setTokenData(tokenData);
		}
	}

	@Override
	public UserTestQuestionResult toEscapedModel() {
		return new UserTestQuestionResultWrapper(_userTestQuestionResult.toEscapedModel());
	}

	@Override
	public UserTestQuestionResult toUnescapedModel() {
		return new UserTestQuestionResultWrapper(_userTestQuestionResult.toUnescapedModel());
	}

	/**
	* Returns the is correct of this user test question result.
	*
	* @return the is correct of this user test question result
	*/
	@Override
	public boolean getIsCorrect() {
		return _userTestQuestionResult.getIsCorrect();
	}

	@Override
	public boolean isCachedModel() {
		return _userTestQuestionResult.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _userTestQuestionResult.isEscapedModel();
	}

	/**
	* Returns <code>true</code> if this user test question result is is correct.
	*
	* @return <code>true</code> if this user test question result is is correct; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsCorrect() {
		return _userTestQuestionResult.isIsCorrect();
	}

	@Override
	public boolean isNew() {
		return _userTestQuestionResult.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _userTestQuestionResult.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<UserTestQuestionResult> toCacheModel() {
		return _userTestQuestionResult.toCacheModel();
	}

	@Override
	public int compareTo(UserTestQuestionResult userTestQuestionResult) {
		return _userTestQuestionResult.compareTo(userTestQuestionResult);
	}

	@Override
	public int hashCode() {
		return _userTestQuestionResult.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _userTestQuestionResult.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new UserTestQuestionResultWrapper((UserTestQuestionResult)_userTestQuestionResult.clone());
	}

	/**
	* Returns the answers by user of this user test question result.
	*
	* @return the answers by user of this user test question result
	*/
	@Override
	public java.lang.String getAnswersByUser() {
		return _userTestQuestionResult.getAnswersByUser();
	}

	/**
	* Returns the token data of this user test question result.
	*
	* @return the token data of this user test question result
	*/
	@Override
	public java.lang.String getTokenData() {
		return _userTestQuestionResult.getTokenData();
	}

	/**
	* Returns the user uuid of this user test question result.
	*
	* @return the user uuid of this user test question result
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _userTestQuestionResult.getUserUuid();
	}

	@Override
	public java.lang.String toString() {
		return _userTestQuestionResult.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _userTestQuestionResult.toXmlString();
	}

	/**
	* Returns the create date of this user test question result.
	*
	* @return the create date of this user test question result
	*/
	@Override
	public Date getCreateDate() {
		return _userTestQuestionResult.getCreateDate();
	}

	/**
	* Returns the primary key of this user test question result.
	*
	* @return the primary key of this user test question result
	*/
	@Override
	public long getPrimaryKey() {
		return _userTestQuestionResult.getPrimaryKey();
	}

	/**
	* Returns the question ID of this user test question result.
	*
	* @return the question ID of this user test question result
	*/
	@Override
	public long getQuestionId() {
		return _userTestQuestionResult.getQuestionId();
	}

	/**
	* Returns the user ID of this user test question result.
	*
	* @return the user ID of this user test question result
	*/
	@Override
	public long getUserId() {
		return _userTestQuestionResult.getUserId();
	}

	/**
	* Returns the user test question result ID of this user test question result.
	*
	* @return the user test question result ID of this user test question result
	*/
	@Override
	public long getUserTestQuestionResultId() {
		return _userTestQuestionResult.getUserTestQuestionResultId();
	}

	@Override
	public void persist() {
		_userTestQuestionResult.persist();
	}

	/**
	* Sets the answers by user of this user test question result.
	*
	* @param answersByUser the answers by user of this user test question result
	*/
	@Override
	public void setAnswersByUser(java.lang.String answersByUser) {
		_userTestQuestionResult.setAnswersByUser(answersByUser);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_userTestQuestionResult.setCachedModel(cachedModel);
	}

	/**
	* Sets the create date of this user test question result.
	*
	* @param createDate the create date of this user test question result
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_userTestQuestionResult.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_userTestQuestionResult.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_userTestQuestionResult.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_userTestQuestionResult.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets whether this user test question result is is correct.
	*
	* @param isCorrect the is correct of this user test question result
	*/
	@Override
	public void setIsCorrect(boolean isCorrect) {
		_userTestQuestionResult.setIsCorrect(isCorrect);
	}

	@Override
	public void setNew(boolean n) {
		_userTestQuestionResult.setNew(n);
	}

	/**
	* Sets the primary key of this user test question result.
	*
	* @param primaryKey the primary key of this user test question result
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_userTestQuestionResult.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_userTestQuestionResult.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the question ID of this user test question result.
	*
	* @param questionId the question ID of this user test question result
	*/
	@Override
	public void setQuestionId(long questionId) {
		_userTestQuestionResult.setQuestionId(questionId);
	}

	/**
	* Sets the token data of this user test question result.
	*
	* @param tokenData the token data of this user test question result
	*/
	@Override
	public void setTokenData(java.lang.String tokenData) {
		_userTestQuestionResult.setTokenData(tokenData);
	}

	/**
	* Sets the user ID of this user test question result.
	*
	* @param userId the user ID of this user test question result
	*/
	@Override
	public void setUserId(long userId) {
		_userTestQuestionResult.setUserId(userId);
	}

	/**
	* Sets the user test question result ID of this user test question result.
	*
	* @param userTestQuestionResultId the user test question result ID of this user test question result
	*/
	@Override
	public void setUserTestQuestionResultId(long userTestQuestionResultId) {
		_userTestQuestionResult.setUserTestQuestionResultId(userTestQuestionResultId);
	}

	/**
	* Sets the user uuid of this user test question result.
	*
	* @param userUuid the user uuid of this user test question result
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_userTestQuestionResult.setUserUuid(userUuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserTestQuestionResultWrapper)) {
			return false;
		}

		UserTestQuestionResultWrapper userTestQuestionResultWrapper = (UserTestQuestionResultWrapper)obj;

		if (Objects.equals(_userTestQuestionResult,
					userTestQuestionResultWrapper._userTestQuestionResult)) {
			return true;
		}

		return false;
	}

	@Override
	public UserTestQuestionResult getWrappedModel() {
		return _userTestQuestionResult;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _userTestQuestionResult.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _userTestQuestionResult.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_userTestQuestionResult.resetOriginalValues();
	}

	private final UserTestQuestionResult _userTestQuestionResult;
}