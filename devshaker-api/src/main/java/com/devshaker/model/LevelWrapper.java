/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Level}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Level
 * @generated
 */
@ProviderType
public class LevelWrapper implements Level, ModelWrapper<Level> {
	public LevelWrapper(Level level) {
		_level = level;
	}

	@Override
	public Class<?> getModelClass() {
		return Level.class;
	}

	@Override
	public String getModelClassName() {
		return Level.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("levelId", getLevelId());
		attributes.put("categoryId", getCategoryId());
		attributes.put("subcategoryId", getSubcategoryId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("levelName", getLevelName());
		attributes.put("levelDescription", getLevelDescription());
		attributes.put("levelIcon", getLevelIcon());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long levelId = (Long)attributes.get("levelId");

		if (levelId != null) {
			setLevelId(levelId);
		}

		Long categoryId = (Long)attributes.get("categoryId");

		if (categoryId != null) {
			setCategoryId(categoryId);
		}

		Long subcategoryId = (Long)attributes.get("subcategoryId");

		if (subcategoryId != null) {
			setSubcategoryId(subcategoryId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String levelName = (String)attributes.get("levelName");

		if (levelName != null) {
			setLevelName(levelName);
		}

		String levelDescription = (String)attributes.get("levelDescription");

		if (levelDescription != null) {
			setLevelDescription(levelDescription);
		}

		Long levelIcon = (Long)attributes.get("levelIcon");

		if (levelIcon != null) {
			setLevelIcon(levelIcon);
		}
	}

	@Override
	public Level toEscapedModel() {
		return new LevelWrapper(_level.toEscapedModel());
	}

	@Override
	public Level toUnescapedModel() {
		return new LevelWrapper(_level.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _level.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _level.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _level.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _level.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Level> toCacheModel() {
		return _level.toCacheModel();
	}

	@Override
	public int compareTo(Level level) {
		return _level.compareTo(level);
	}

	@Override
	public int hashCode() {
		return _level.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _level.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new LevelWrapper((Level)_level.clone());
	}

	@Override
	public java.lang.String getDefaultLanguageId() {
		return _level.getDefaultLanguageId();
	}

	/**
	* Returns the level description of this level.
	*
	* @return the level description of this level
	*/
	@Override
	public java.lang.String getLevelDescription() {
		return _level.getLevelDescription();
	}

	/**
	* Returns the localized level description of this level in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @return the localized level description of this level
	*/
	@Override
	public java.lang.String getLevelDescription(java.lang.String languageId) {
		return _level.getLevelDescription(languageId);
	}

	/**
	* Returns the localized level description of this level in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized level description of this level
	*/
	@Override
	public java.lang.String getLevelDescription(java.lang.String languageId,
		boolean useDefault) {
		return _level.getLevelDescription(languageId, useDefault);
	}

	/**
	* Returns the localized level description of this level in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param locale the locale of the language
	* @return the localized level description of this level
	*/
	@Override
	public java.lang.String getLevelDescription(java.util.Locale locale) {
		return _level.getLevelDescription(locale);
	}

	/**
	* Returns the localized level description of this level in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param locale the local of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized level description of this level. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	*/
	@Override
	public java.lang.String getLevelDescription(java.util.Locale locale,
		boolean useDefault) {
		return _level.getLevelDescription(locale, useDefault);
	}

	@Override
	public java.lang.String getLevelDescriptionCurrentLanguageId() {
		return _level.getLevelDescriptionCurrentLanguageId();
	}

	@Override
	public java.lang.String getLevelDescriptionCurrentValue() {
		return _level.getLevelDescriptionCurrentValue();
	}

	/**
	* Returns the level name of this level.
	*
	* @return the level name of this level
	*/
	@Override
	public java.lang.String getLevelName() {
		return _level.getLevelName();
	}

	/**
	* Returns the localized level name of this level in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @return the localized level name of this level
	*/
	@Override
	public java.lang.String getLevelName(java.lang.String languageId) {
		return _level.getLevelName(languageId);
	}

	/**
	* Returns the localized level name of this level in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param languageId the ID of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized level name of this level
	*/
	@Override
	public java.lang.String getLevelName(java.lang.String languageId,
		boolean useDefault) {
		return _level.getLevelName(languageId, useDefault);
	}

	/**
	* Returns the localized level name of this level in the language. Uses the default language if no localization exists for the requested language.
	*
	* @param locale the locale of the language
	* @return the localized level name of this level
	*/
	@Override
	public java.lang.String getLevelName(java.util.Locale locale) {
		return _level.getLevelName(locale);
	}

	/**
	* Returns the localized level name of this level in the language, optionally using the default language if no localization exists for the requested language.
	*
	* @param locale the local of the language
	* @param useDefault whether to use the default language if no localization exists for the requested language
	* @return the localized level name of this level. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	*/
	@Override
	public java.lang.String getLevelName(java.util.Locale locale,
		boolean useDefault) {
		return _level.getLevelName(locale, useDefault);
	}

	@Override
	public java.lang.String getLevelNameCurrentLanguageId() {
		return _level.getLevelNameCurrentLanguageId();
	}

	@Override
	public java.lang.String getLevelNameCurrentValue() {
		return _level.getLevelNameCurrentValue();
	}

	/**
	* Returns the user name of this level.
	*
	* @return the user name of this level
	*/
	@Override
	public java.lang.String getUserName() {
		return _level.getUserName();
	}

	/**
	* Returns the user uuid of this level.
	*
	* @return the user uuid of this level
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _level.getUserUuid();
	}

	@Override
	public java.lang.String toString() {
		return _level.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _level.toXmlString();
	}

	@Override
	public java.lang.String[] getAvailableLanguageIds() {
		return _level.getAvailableLanguageIds();
	}

	/**
	* Returns the create date of this level.
	*
	* @return the create date of this level
	*/
	@Override
	public Date getCreateDate() {
		return _level.getCreateDate();
	}

	/**
	* Returns the modified date of this level.
	*
	* @return the modified date of this level
	*/
	@Override
	public Date getModifiedDate() {
		return _level.getModifiedDate();
	}

	/**
	* Returns a map of the locales and localized level descriptions of this level.
	*
	* @return the locales and localized level descriptions of this level
	*/
	@Override
	public Map<java.util.Locale, java.lang.String> getLevelDescriptionMap() {
		return _level.getLevelDescriptionMap();
	}

	/**
	* Returns a map of the locales and localized level names of this level.
	*
	* @return the locales and localized level names of this level
	*/
	@Override
	public Map<java.util.Locale, java.lang.String> getLevelNameMap() {
		return _level.getLevelNameMap();
	}

	/**
	* Returns the category ID of this level.
	*
	* @return the category ID of this level
	*/
	@Override
	public long getCategoryId() {
		return _level.getCategoryId();
	}

	/**
	* Returns the company ID of this level.
	*
	* @return the company ID of this level
	*/
	@Override
	public long getCompanyId() {
		return _level.getCompanyId();
	}

	/**
	* Returns the group ID of this level.
	*
	* @return the group ID of this level
	*/
	@Override
	public long getGroupId() {
		return _level.getGroupId();
	}

	/**
	* Returns the level icon of this level.
	*
	* @return the level icon of this level
	*/
	@Override
	public long getLevelIcon() {
		return _level.getLevelIcon();
	}

	/**
	* Returns the level ID of this level.
	*
	* @return the level ID of this level
	*/
	@Override
	public long getLevelId() {
		return _level.getLevelId();
	}

	/**
	* Returns the primary key of this level.
	*
	* @return the primary key of this level
	*/
	@Override
	public long getPrimaryKey() {
		return _level.getPrimaryKey();
	}

	/**
	* Returns the subcategory ID of this level.
	*
	* @return the subcategory ID of this level
	*/
	@Override
	public long getSubcategoryId() {
		return _level.getSubcategoryId();
	}

	/**
	* Returns the user ID of this level.
	*
	* @return the user ID of this level
	*/
	@Override
	public long getUserId() {
		return _level.getUserId();
	}

	@Override
	public void persist() {
		_level.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {
		_level.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
		java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {
		_level.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_level.setCachedModel(cachedModel);
	}

	/**
	* Sets the category ID of this level.
	*
	* @param categoryId the category ID of this level
	*/
	@Override
	public void setCategoryId(long categoryId) {
		_level.setCategoryId(categoryId);
	}

	/**
	* Sets the company ID of this level.
	*
	* @param companyId the company ID of this level
	*/
	@Override
	public void setCompanyId(long companyId) {
		_level.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this level.
	*
	* @param createDate the create date of this level
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_level.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_level.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_level.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_level.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this level.
	*
	* @param groupId the group ID of this level
	*/
	@Override
	public void setGroupId(long groupId) {
		_level.setGroupId(groupId);
	}

	/**
	* Sets the level description of this level.
	*
	* @param levelDescription the level description of this level
	*/
	@Override
	public void setLevelDescription(java.lang.String levelDescription) {
		_level.setLevelDescription(levelDescription);
	}

	/**
	* Sets the localized level description of this level in the language.
	*
	* @param levelDescription the localized level description of this level
	* @param locale the locale of the language
	*/
	@Override
	public void setLevelDescription(java.lang.String levelDescription,
		java.util.Locale locale) {
		_level.setLevelDescription(levelDescription, locale);
	}

	/**
	* Sets the localized level description of this level in the language, and sets the default locale.
	*
	* @param levelDescription the localized level description of this level
	* @param locale the locale of the language
	* @param defaultLocale the default locale
	*/
	@Override
	public void setLevelDescription(java.lang.String levelDescription,
		java.util.Locale locale, java.util.Locale defaultLocale) {
		_level.setLevelDescription(levelDescription, locale, defaultLocale);
	}

	@Override
	public void setLevelDescriptionCurrentLanguageId(
		java.lang.String languageId) {
		_level.setLevelDescriptionCurrentLanguageId(languageId);
	}

	/**
	* Sets the localized level descriptions of this level from the map of locales and localized level descriptions.
	*
	* @param levelDescriptionMap the locales and localized level descriptions of this level
	*/
	@Override
	public void setLevelDescriptionMap(
		Map<java.util.Locale, java.lang.String> levelDescriptionMap) {
		_level.setLevelDescriptionMap(levelDescriptionMap);
	}

	/**
	* Sets the localized level descriptions of this level from the map of locales and localized level descriptions, and sets the default locale.
	*
	* @param levelDescriptionMap the locales and localized level descriptions of this level
	* @param defaultLocale the default locale
	*/
	@Override
	public void setLevelDescriptionMap(
		Map<java.util.Locale, java.lang.String> levelDescriptionMap,
		java.util.Locale defaultLocale) {
		_level.setLevelDescriptionMap(levelDescriptionMap, defaultLocale);
	}

	/**
	* Sets the level icon of this level.
	*
	* @param levelIcon the level icon of this level
	*/
	@Override
	public void setLevelIcon(long levelIcon) {
		_level.setLevelIcon(levelIcon);
	}

	/**
	* Sets the level ID of this level.
	*
	* @param levelId the level ID of this level
	*/
	@Override
	public void setLevelId(long levelId) {
		_level.setLevelId(levelId);
	}

	/**
	* Sets the level name of this level.
	*
	* @param levelName the level name of this level
	*/
	@Override
	public void setLevelName(java.lang.String levelName) {
		_level.setLevelName(levelName);
	}

	/**
	* Sets the localized level name of this level in the language.
	*
	* @param levelName the localized level name of this level
	* @param locale the locale of the language
	*/
	@Override
	public void setLevelName(java.lang.String levelName, java.util.Locale locale) {
		_level.setLevelName(levelName, locale);
	}

	/**
	* Sets the localized level name of this level in the language, and sets the default locale.
	*
	* @param levelName the localized level name of this level
	* @param locale the locale of the language
	* @param defaultLocale the default locale
	*/
	@Override
	public void setLevelName(java.lang.String levelName,
		java.util.Locale locale, java.util.Locale defaultLocale) {
		_level.setLevelName(levelName, locale, defaultLocale);
	}

	@Override
	public void setLevelNameCurrentLanguageId(java.lang.String languageId) {
		_level.setLevelNameCurrentLanguageId(languageId);
	}

	/**
	* Sets the localized level names of this level from the map of locales and localized level names.
	*
	* @param levelNameMap the locales and localized level names of this level
	*/
	@Override
	public void setLevelNameMap(
		Map<java.util.Locale, java.lang.String> levelNameMap) {
		_level.setLevelNameMap(levelNameMap);
	}

	/**
	* Sets the localized level names of this level from the map of locales and localized level names, and sets the default locale.
	*
	* @param levelNameMap the locales and localized level names of this level
	* @param defaultLocale the default locale
	*/
	@Override
	public void setLevelNameMap(
		Map<java.util.Locale, java.lang.String> levelNameMap,
		java.util.Locale defaultLocale) {
		_level.setLevelNameMap(levelNameMap, defaultLocale);
	}

	/**
	* Sets the modified date of this level.
	*
	* @param modifiedDate the modified date of this level
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_level.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_level.setNew(n);
	}

	/**
	* Sets the primary key of this level.
	*
	* @param primaryKey the primary key of this level
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_level.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_level.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the subcategory ID of this level.
	*
	* @param subcategoryId the subcategory ID of this level
	*/
	@Override
	public void setSubcategoryId(long subcategoryId) {
		_level.setSubcategoryId(subcategoryId);
	}

	/**
	* Sets the user ID of this level.
	*
	* @param userId the user ID of this level
	*/
	@Override
	public void setUserId(long userId) {
		_level.setUserId(userId);
	}

	/**
	* Sets the user name of this level.
	*
	* @param userName the user name of this level
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_level.setUserName(userName);
	}

	/**
	* Sets the user uuid of this level.
	*
	* @param userUuid the user uuid of this level
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_level.setUserUuid(userUuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LevelWrapper)) {
			return false;
		}

		LevelWrapper levelWrapper = (LevelWrapper)obj;

		if (Objects.equals(_level, levelWrapper._level)) {
			return true;
		}

		return false;
	}

	@Override
	public Level getWrappedModel() {
		return _level;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _level.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _level.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_level.resetOriginalValues();
	}

	private final Level _level;
}