/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class LevelSoap implements Serializable {
	public static LevelSoap toSoapModel(Level model) {
		LevelSoap soapModel = new LevelSoap();

		soapModel.setLevelId(model.getLevelId());
		soapModel.setCategoryId(model.getCategoryId());
		soapModel.setSubcategoryId(model.getSubcategoryId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setLevelName(model.getLevelName());
		soapModel.setLevelDescription(model.getLevelDescription());
		soapModel.setLevelIcon(model.getLevelIcon());

		return soapModel;
	}

	public static LevelSoap[] toSoapModels(Level[] models) {
		LevelSoap[] soapModels = new LevelSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LevelSoap[][] toSoapModels(Level[][] models) {
		LevelSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LevelSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LevelSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LevelSoap[] toSoapModels(List<Level> models) {
		List<LevelSoap> soapModels = new ArrayList<LevelSoap>(models.size());

		for (Level model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LevelSoap[soapModels.size()]);
	}

	public LevelSoap() {
	}

	public long getPrimaryKey() {
		return _levelId;
	}

	public void setPrimaryKey(long pk) {
		setLevelId(pk);
	}

	public long getLevelId() {
		return _levelId;
	}

	public void setLevelId(long levelId) {
		_levelId = levelId;
	}

	public long getCategoryId() {
		return _categoryId;
	}

	public void setCategoryId(long categoryId) {
		_categoryId = categoryId;
	}

	public long getSubcategoryId() {
		return _subcategoryId;
	}

	public void setSubcategoryId(long subcategoryId) {
		_subcategoryId = subcategoryId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getLevelName() {
		return _levelName;
	}

	public void setLevelName(String levelName) {
		_levelName = levelName;
	}

	public String getLevelDescription() {
		return _levelDescription;
	}

	public void setLevelDescription(String levelDescription) {
		_levelDescription = levelDescription;
	}

	public long getLevelIcon() {
		return _levelIcon;
	}

	public void setLevelIcon(long levelIcon) {
		_levelIcon = levelIcon;
	}

	private long _levelId;
	private long _categoryId;
	private long _subcategoryId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _levelName;
	private String _levelDescription;
	private long _levelIcon;
}