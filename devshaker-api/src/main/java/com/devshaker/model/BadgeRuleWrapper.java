/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link BadgeRule}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BadgeRule
 * @generated
 */
@ProviderType
public class BadgeRuleWrapper implements BadgeRule, ModelWrapper<BadgeRule> {
	public BadgeRuleWrapper(BadgeRule badgeRule) {
		_badgeRule = badgeRule;
	}

	@Override
	public Class<?> getModelClass() {
		return BadgeRule.class;
	}

	@Override
	public String getModelClassName() {
		return BadgeRule.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ruleId", getRuleId());
		attributes.put("badgeId", getBadgeId());
		attributes.put("companyId", getCompanyId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ruleId = (Long)attributes.get("ruleId");

		if (ruleId != null) {
			setRuleId(ruleId);
		}

		Long badgeId = (Long)attributes.get("badgeId");

		if (badgeId != null) {
			setBadgeId(badgeId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}
	}

	@Override
	public BadgeRule toEscapedModel() {
		return new BadgeRuleWrapper(_badgeRule.toEscapedModel());
	}

	@Override
	public BadgeRule toUnescapedModel() {
		return new BadgeRuleWrapper(_badgeRule.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _badgeRule.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _badgeRule.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _badgeRule.isNew();
	}

	/**
	* Returns the primary key of this badge rule.
	*
	* @return the primary key of this badge rule
	*/
	@Override
	public com.devshaker.service.persistence.BadgeRulePK getPrimaryKey() {
		return _badgeRule.getPrimaryKey();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _badgeRule.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<BadgeRule> toCacheModel() {
		return _badgeRule.toCacheModel();
	}

	@Override
	public int compareTo(BadgeRule badgeRule) {
		return _badgeRule.compareTo(badgeRule);
	}

	@Override
	public int hashCode() {
		return _badgeRule.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _badgeRule.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new BadgeRuleWrapper((BadgeRule)_badgeRule.clone());
	}

	@Override
	public java.lang.String toString() {
		return _badgeRule.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _badgeRule.toXmlString();
	}

	/**
	* Returns the badge ID of this badge rule.
	*
	* @return the badge ID of this badge rule
	*/
	@Override
	public long getBadgeId() {
		return _badgeRule.getBadgeId();
	}

	/**
	* Returns the company ID of this badge rule.
	*
	* @return the company ID of this badge rule
	*/
	@Override
	public long getCompanyId() {
		return _badgeRule.getCompanyId();
	}

	/**
	* Returns the rule ID of this badge rule.
	*
	* @return the rule ID of this badge rule
	*/
	@Override
	public long getRuleId() {
		return _badgeRule.getRuleId();
	}

	@Override
	public void persist() {
		_badgeRule.persist();
	}

	/**
	* Sets the badge ID of this badge rule.
	*
	* @param badgeId the badge ID of this badge rule
	*/
	@Override
	public void setBadgeId(long badgeId) {
		_badgeRule.setBadgeId(badgeId);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_badgeRule.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this badge rule.
	*
	* @param companyId the company ID of this badge rule
	*/
	@Override
	public void setCompanyId(long companyId) {
		_badgeRule.setCompanyId(companyId);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_badgeRule.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_badgeRule.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_badgeRule.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_badgeRule.setNew(n);
	}

	/**
	* Sets the primary key of this badge rule.
	*
	* @param primaryKey the primary key of this badge rule
	*/
	@Override
	public void setPrimaryKey(
		com.devshaker.service.persistence.BadgeRulePK primaryKey) {
		_badgeRule.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_badgeRule.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the rule ID of this badge rule.
	*
	* @param ruleId the rule ID of this badge rule
	*/
	@Override
	public void setRuleId(long ruleId) {
		_badgeRule.setRuleId(ruleId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BadgeRuleWrapper)) {
			return false;
		}

		BadgeRuleWrapper badgeRuleWrapper = (BadgeRuleWrapper)obj;

		if (Objects.equals(_badgeRule, badgeRuleWrapper._badgeRule)) {
			return true;
		}

		return false;
	}

	@Override
	public BadgeRule getWrappedModel() {
		return _badgeRule;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _badgeRule.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _badgeRule.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_badgeRule.resetOriginalValues();
	}

	private final BadgeRule _badgeRule;
}