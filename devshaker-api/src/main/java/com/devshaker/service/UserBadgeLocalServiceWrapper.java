/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link UserBadgeLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see UserBadgeLocalService
 * @generated
 */
@ProviderType
public class UserBadgeLocalServiceWrapper implements UserBadgeLocalService,
	ServiceWrapper<UserBadgeLocalService> {
	public UserBadgeLocalServiceWrapper(
		UserBadgeLocalService userBadgeLocalService) {
		_userBadgeLocalService = userBadgeLocalService;
	}

	/**
	* Adds the user badge to the database. Also notifies the appropriate model listeners.
	*
	* @param userBadge the user badge
	* @return the user badge that was added
	*/
	@Override
	public com.devshaker.model.UserBadge addUserBadge(
		com.devshaker.model.UserBadge userBadge) {
		return _userBadgeLocalService.addUserBadge(userBadge);
	}

	/**
	* Creates a new user badge with the primary key. Does not add the user badge to the database.
	*
	* @param userBadgePK the primary key for the new user badge
	* @return the new user badge
	*/
	@Override
	public com.devshaker.model.UserBadge createUserBadge(
		com.devshaker.service.persistence.UserBadgePK userBadgePK) {
		return _userBadgeLocalService.createUserBadge(userBadgePK);
	}

	/**
	* Deletes the user badge from the database. Also notifies the appropriate model listeners.
	*
	* @param userBadge the user badge
	* @return the user badge that was removed
	*/
	@Override
	public com.devshaker.model.UserBadge deleteUserBadge(
		com.devshaker.model.UserBadge userBadge) {
		return _userBadgeLocalService.deleteUserBadge(userBadge);
	}

	/**
	* Deletes the user badge with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userBadgePK the primary key of the user badge
	* @return the user badge that was removed
	* @throws PortalException if a user badge with the primary key could not be found
	*/
	@Override
	public com.devshaker.model.UserBadge deleteUserBadge(
		com.devshaker.service.persistence.UserBadgePK userBadgePK)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userBadgeLocalService.deleteUserBadge(userBadgePK);
	}

	@Override
	public com.devshaker.model.UserBadge fetchUserBadge(
		com.devshaker.service.persistence.UserBadgePK userBadgePK) {
		return _userBadgeLocalService.fetchUserBadge(userBadgePK);
	}

	/**
	* Returns the user badge with the primary key.
	*
	* @param userBadgePK the primary key of the user badge
	* @return the user badge
	* @throws PortalException if a user badge with the primary key could not be found
	*/
	@Override
	public com.devshaker.model.UserBadge getUserBadge(
		com.devshaker.service.persistence.UserBadgePK userBadgePK)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userBadgeLocalService.getUserBadge(userBadgePK);
	}

	/**
	* Updates the user badge in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param userBadge the user badge
	* @return the user badge that was updated
	*/
	@Override
	public com.devshaker.model.UserBadge updateUserBadge(
		com.devshaker.model.UserBadge userBadge) {
		return _userBadgeLocalService.updateUserBadge(userBadge);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _userBadgeLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _userBadgeLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _userBadgeLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userBadgeLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userBadgeLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of user badges.
	*
	* @return the number of user badges
	*/
	@Override
	public int getUserBadgesCount() {
		return _userBadgeLocalService.getUserBadgesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _userBadgeLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _userBadgeLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _userBadgeLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _userBadgeLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	@Override
	public java.util.List<com.devshaker.model.Badge> getBadgesObtainedByUserId(
		long userId) throws com.liferay.portal.kernel.exception.PortalException {
		return _userBadgeLocalService.getBadgesObtainedByUserId(userId);
	}

	/**
	* Returns a range of all the user badges.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @return the range of user badges
	*/
	@Override
	public java.util.List<com.devshaker.model.UserBadge> getUserBadges(
		int start, int end) {
		return _userBadgeLocalService.getUserBadges(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _userBadgeLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _userBadgeLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public UserBadgeLocalService getWrappedService() {
		return _userBadgeLocalService;
	}

	@Override
	public void setWrappedService(UserBadgeLocalService userBadgeLocalService) {
		_userBadgeLocalService = userBadgeLocalService;
	}

	private UserBadgeLocalService _userBadgeLocalService;
}