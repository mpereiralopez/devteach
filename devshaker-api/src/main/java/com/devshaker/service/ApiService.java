/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.access.control.AccessControlled;
import com.liferay.portal.kernel.service.BaseService;
import com.liferay.portal.kernel.spring.osgi.OSGiBeanProperties;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

import java.util.List;

/**
 * Provides the remote service interface for Api. Methods of this
 * service are expected to have security checks based on the propagated JAAS
 * credentials because this service can be accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see ApiServiceUtil
 * @see com.devshaker.service.base.ApiServiceBaseImpl
 * @see com.devshaker.service.impl.ApiServiceImpl
 * @generated
 */
@AccessControlled
@JSONWebService
@OSGiBeanProperties(property =  {
	"json.web.service.context.name=devshaker", "json.web.service.context.path=Api"}, service = ApiService.class)
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface ApiService extends BaseService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ApiServiceUtil} to access the api remote service. Add custom service methods to {@link com.devshaker.service.impl.ApiServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONObject getUserExtraDataInformation(long userId);

	@JSONWebService(method = "POST")
	public JSONObject reportQuestion(long questionId, java.lang.String reason);

	@JSONWebService(method = "POST")
	public JSONObject sendTestResultOfUser(
		java.lang.String jsonResponseStringify);

	public User updateUserInfo(long userId, java.lang.String screenName,
		java.lang.String firstName, java.lang.String emailAddress,
		java.lang.String phone, int year, int month, int day,
		java.lang.String country);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getApiVersion();

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<JSONObject> getCategoriesOfCompany();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<JSONObject> getLevelOfSubcategory(long subcategoryId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<JSONObject> getSubCategoriesOfCategory(long categoryId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<JSONObject> getTestQuestionOfLevel(long levelId);
}