/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link UserTestQuestionResultLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see UserTestQuestionResultLocalService
 * @generated
 */
@ProviderType
public class UserTestQuestionResultLocalServiceWrapper
	implements UserTestQuestionResultLocalService,
		ServiceWrapper<UserTestQuestionResultLocalService> {
	public UserTestQuestionResultLocalServiceWrapper(
		UserTestQuestionResultLocalService userTestQuestionResultLocalService) {
		_userTestQuestionResultLocalService = userTestQuestionResultLocalService;
	}

	@Override
	public com.devshaker.model.TestQuestion getLastTestQuestionOfUser(
		long userId) {
		return _userTestQuestionResultLocalService.getLastTestQuestionOfUser(userId);
	}

	/**
	* Adds the user test question result to the database. Also notifies the appropriate model listeners.
	*
	* @param userTestQuestionResult the user test question result
	* @return the user test question result that was added
	*/
	@Override
	public com.devshaker.model.UserTestQuestionResult addUserTestQuestionResult(
		com.devshaker.model.UserTestQuestionResult userTestQuestionResult) {
		return _userTestQuestionResultLocalService.addUserTestQuestionResult(userTestQuestionResult);
	}

	/**
	* Creates a new user test question result with the primary key. Does not add the user test question result to the database.
	*
	* @param userTestQuestionResultId the primary key for the new user test question result
	* @return the new user test question result
	*/
	@Override
	public com.devshaker.model.UserTestQuestionResult createUserTestQuestionResult(
		long userTestQuestionResultId) {
		return _userTestQuestionResultLocalService.createUserTestQuestionResult(userTestQuestionResultId);
	}

	/**
	* Deletes the user test question result from the database. Also notifies the appropriate model listeners.
	*
	* @param userTestQuestionResult the user test question result
	* @return the user test question result that was removed
	*/
	@Override
	public com.devshaker.model.UserTestQuestionResult deleteUserTestQuestionResult(
		com.devshaker.model.UserTestQuestionResult userTestQuestionResult) {
		return _userTestQuestionResultLocalService.deleteUserTestQuestionResult(userTestQuestionResult);
	}

	/**
	* Deletes the user test question result with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userTestQuestionResultId the primary key of the user test question result
	* @return the user test question result that was removed
	* @throws PortalException if a user test question result with the primary key could not be found
	*/
	@Override
	public com.devshaker.model.UserTestQuestionResult deleteUserTestQuestionResult(
		long userTestQuestionResultId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userTestQuestionResultLocalService.deleteUserTestQuestionResult(userTestQuestionResultId);
	}

	@Override
	public com.devshaker.model.UserTestQuestionResult fetchUserTestQuestionResult(
		long userTestQuestionResultId) {
		return _userTestQuestionResultLocalService.fetchUserTestQuestionResult(userTestQuestionResultId);
	}

	/**
	* Returns the user test question result with the primary key.
	*
	* @param userTestQuestionResultId the primary key of the user test question result
	* @return the user test question result
	* @throws PortalException if a user test question result with the primary key could not be found
	*/
	@Override
	public com.devshaker.model.UserTestQuestionResult getUserTestQuestionResult(
		long userTestQuestionResultId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userTestQuestionResultLocalService.getUserTestQuestionResult(userTestQuestionResultId);
	}

	/**
	* Updates the user test question result in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param userTestQuestionResult the user test question result
	* @return the user test question result that was updated
	*/
	@Override
	public com.devshaker.model.UserTestQuestionResult updateUserTestQuestionResult(
		com.devshaker.model.UserTestQuestionResult userTestQuestionResult) {
		return _userTestQuestionResultLocalService.updateUserTestQuestionResult(userTestQuestionResult);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _userTestQuestionResultLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _userTestQuestionResultLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _userTestQuestionResultLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userTestQuestionResultLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userTestQuestionResultLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public int getAccumulatedCorrectAnswersOfUserBetweenLevels(long userId,
		long subcategoryId, int levelBottomLimit, int levelTopLimit)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userTestQuestionResultLocalService.getAccumulatedCorrectAnswersOfUserBetweenLevels(userId,
			subcategoryId, levelBottomLimit, levelTopLimit);
	}

	@Override
	public int getQuestionTimesFailed(long questionId) {
		return _userTestQuestionResultLocalService.getQuestionTimesFailed(questionId);
	}

	@Override
	public int getQuestionTimesPassed(long questionId) {
		return _userTestQuestionResultLocalService.getQuestionTimesPassed(questionId);
	}

	/**
	* Returns the number of user test question results.
	*
	* @return the number of user test question results
	*/
	@Override
	public int getUserTestQuestionResultsCount() {
		return _userTestQuestionResultLocalService.getUserTestQuestionResultsCount();
	}

	@Override
	public int levelsInSameSession(long userId, java.lang.String tokenData) {
		return _userTestQuestionResultLocalService.levelsInSameSession(userId,
			tokenData);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _userTestQuestionResultLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _userTestQuestionResultLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _userTestQuestionResultLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _userTestQuestionResultLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	@Override
	public java.util.List<com.devshaker.model.UserTestQuestionResult> getTestQuestionResults() {
		return _userTestQuestionResultLocalService.getTestQuestionResults();
	}

	@Override
	public java.util.List<com.devshaker.model.UserTestQuestionResult> getTestQuestionResultsOfCategory(
		long categoryId) {
		return _userTestQuestionResultLocalService.getTestQuestionResultsOfCategory(categoryId);
	}

	@Override
	public java.util.List<com.devshaker.model.UserTestQuestionResult> getTestQuestionResultsOfLevel(
		long levelId) {
		return _userTestQuestionResultLocalService.getTestQuestionResultsOfLevel(levelId);
	}

	@Override
	public java.util.List<com.devshaker.model.UserTestQuestionResult> getTestQuestionResultsOfSubcategory(
		long subcategoryId) {
		return _userTestQuestionResultLocalService.getTestQuestionResultsOfSubcategory(subcategoryId);
	}

	/**
	* Returns a range of all the user test question results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @return the range of user test question results
	*/
	@Override
	public java.util.List<com.devshaker.model.UserTestQuestionResult> getUserTestQuestionResults(
		int start, int end) {
		return _userTestQuestionResultLocalService.getUserTestQuestionResults(start,
			end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _userTestQuestionResultLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _userTestQuestionResultLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public UserTestQuestionResultLocalService getWrappedService() {
		return _userTestQuestionResultLocalService;
	}

	@Override
	public void setWrappedService(
		UserTestQuestionResultLocalService userTestQuestionResultLocalService) {
		_userTestQuestionResultLocalService = userTestQuestionResultLocalService;
	}

	private UserTestQuestionResultLocalService _userTestQuestionResultLocalService;
}