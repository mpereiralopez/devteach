/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Subcategory. This utility wraps
 * {@link com.devshaker.service.impl.SubcategoryLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see SubcategoryLocalService
 * @see com.devshaker.service.base.SubcategoryLocalServiceBaseImpl
 * @see com.devshaker.service.impl.SubcategoryLocalServiceImpl
 * @generated
 */
@ProviderType
public class SubcategoryLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.devshaker.service.impl.SubcategoryLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the subcategory to the database. Also notifies the appropriate model listeners.
	*
	* @param subcategory the subcategory
	* @return the subcategory that was added
	*/
	public static com.devshaker.model.Subcategory addSubcategory(
		com.devshaker.model.Subcategory subcategory) {
		return getService().addSubcategory(subcategory);
	}

	/**
	* Creates a new subcategory with the primary key. Does not add the subcategory to the database.
	*
	* @param subcategoryId the primary key for the new subcategory
	* @return the new subcategory
	*/
	public static com.devshaker.model.Subcategory createSubcategory(
		long subcategoryId) {
		return getService().createSubcategory(subcategoryId);
	}

	/**
	* Deletes the subcategory from the database. Also notifies the appropriate model listeners.
	*
	* @param subcategory the subcategory
	* @return the subcategory that was removed
	*/
	public static com.devshaker.model.Subcategory deleteSubcategory(
		com.devshaker.model.Subcategory subcategory) {
		return getService().deleteSubcategory(subcategory);
	}

	/**
	* Deletes the subcategory with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param subcategoryId the primary key of the subcategory
	* @return the subcategory that was removed
	* @throws PortalException if a subcategory with the primary key could not be found
	*/
	public static com.devshaker.model.Subcategory deleteSubcategory(
		long subcategoryId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteSubcategory(subcategoryId);
	}

	public static com.devshaker.model.Subcategory fetchSubcategory(
		long subcategoryId) {
		return getService().fetchSubcategory(subcategoryId);
	}

	/**
	* Returns the subcategory matching the UUID and group.
	*
	* @param uuid the subcategory's UUID
	* @param groupId the primary key of the group
	* @return the matching subcategory, or <code>null</code> if a matching subcategory could not be found
	*/
	public static com.devshaker.model.Subcategory fetchSubcategoryByUuidAndGroupId(
		java.lang.String uuid, long groupId) {
		return getService().fetchSubcategoryByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns the subcategory with the primary key.
	*
	* @param subcategoryId the primary key of the subcategory
	* @return the subcategory
	* @throws PortalException if a subcategory with the primary key could not be found
	*/
	public static com.devshaker.model.Subcategory getSubcategory(
		long subcategoryId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getSubcategory(subcategoryId);
	}

	public static com.devshaker.model.Subcategory getSubcategoryByNameAndCategoryId(
		java.lang.String name, java.util.Locale locale, long categoryId) {
		return getService()
				   .getSubcategoryByNameAndCategoryId(name, locale, categoryId);
	}

	/**
	* Returns the subcategory matching the UUID and group.
	*
	* @param uuid the subcategory's UUID
	* @param groupId the primary key of the group
	* @return the matching subcategory
	* @throws PortalException if a matching subcategory could not be found
	*/
	public static com.devshaker.model.Subcategory getSubcategoryByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getSubcategoryByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Updates the subcategory in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param subcategory the subcategory
	* @return the subcategory that was updated
	*/
	public static com.devshaker.model.Subcategory updateSubcategory(
		com.devshaker.model.Subcategory subcategory) {
		return getService().updateSubcategory(subcategory);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of subcategories.
	*
	* @return the number of subcategories
	*/
	public static int getSubcategoriesCount() {
		return getService().getSubcategoriesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns a range of all the subcategories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @return the range of subcategories
	*/
	public static java.util.List<com.devshaker.model.Subcategory> getSubcategories(
		int start, int end) {
		return getService().getSubcategories(start, end);
	}

	/**
	* Returns all the subcategories matching the UUID and company.
	*
	* @param uuid the UUID of the subcategories
	* @param companyId the primary key of the company
	* @return the matching subcategories, or an empty list if no matches were found
	*/
	public static java.util.List<com.devshaker.model.Subcategory> getSubcategoriesByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return getService().getSubcategoriesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns a range of subcategories matching the UUID and company.
	*
	* @param uuid the UUID of the subcategories
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching subcategories, or an empty list if no matches were found
	*/
	public static java.util.List<com.devshaker.model.Subcategory> getSubcategoriesByUuidAndCompanyId(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<com.devshaker.model.Subcategory> orderByComparator) {
		return getService()
				   .getSubcategoriesByUuidAndCompanyId(uuid, companyId, start,
			end, orderByComparator);
	}

	public static java.util.List<com.devshaker.model.Subcategory> getSubcategoriesOfCategory(
		long categoryId) {
		return getService().getSubcategoriesOfCategory(categoryId);
	}

	public static java.util.List<com.devshaker.model.Subcategory> getSubcategoriesOfCompany(
		long companyId) {
		return getService().getSubcategoriesOfCompany(companyId);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static SubcategoryLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<SubcategoryLocalService, SubcategoryLocalService> _serviceTracker =
		ServiceTrackerFactory.open(SubcategoryLocalService.class);
}