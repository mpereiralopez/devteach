/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.UserBadge;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the user badge service. This utility wraps {@link com.devshaker.service.persistence.impl.UserBadgePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserBadgePersistence
 * @see com.devshaker.service.persistence.impl.UserBadgePersistenceImpl
 * @generated
 */
@ProviderType
public class UserBadgeUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(UserBadge userBadge) {
		getPersistence().clearCache(userBadge);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<UserBadge> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<UserBadge> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<UserBadge> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<UserBadge> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static UserBadge update(UserBadge userBadge) {
		return getPersistence().update(userBadge);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static UserBadge update(UserBadge userBadge,
		ServiceContext serviceContext) {
		return getPersistence().update(userBadge, serviceContext);
	}

	/**
	* Returns all the user badges where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching user badges
	*/
	public static List<UserBadge> findByUserId(long userId) {
		return getPersistence().findByUserId(userId);
	}

	/**
	* Returns a range of all the user badges where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @return the range of matching user badges
	*/
	public static List<UserBadge> findByUserId(long userId, int start, int end) {
		return getPersistence().findByUserId(userId, start, end);
	}

	/**
	* Returns an ordered range of all the user badges where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user badges
	*/
	public static List<UserBadge> findByUserId(long userId, int start, int end,
		OrderByComparator<UserBadge> orderByComparator) {
		return getPersistence()
				   .findByUserId(userId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the user badges where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user badges
	*/
	public static List<UserBadge> findByUserId(long userId, int start, int end,
		OrderByComparator<UserBadge> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUserId(userId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first user badge in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user badge
	* @throws NoSuchUserBadgeException if a matching user badge could not be found
	*/
	public static UserBadge findByUserId_First(long userId,
		OrderByComparator<UserBadge> orderByComparator)
		throws com.devshaker.exception.NoSuchUserBadgeException {
		return getPersistence().findByUserId_First(userId, orderByComparator);
	}

	/**
	* Returns the first user badge in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user badge, or <code>null</code> if a matching user badge could not be found
	*/
	public static UserBadge fetchByUserId_First(long userId,
		OrderByComparator<UserBadge> orderByComparator) {
		return getPersistence().fetchByUserId_First(userId, orderByComparator);
	}

	/**
	* Returns the last user badge in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user badge
	* @throws NoSuchUserBadgeException if a matching user badge could not be found
	*/
	public static UserBadge findByUserId_Last(long userId,
		OrderByComparator<UserBadge> orderByComparator)
		throws com.devshaker.exception.NoSuchUserBadgeException {
		return getPersistence().findByUserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the last user badge in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user badge, or <code>null</code> if a matching user badge could not be found
	*/
	public static UserBadge fetchByUserId_Last(long userId,
		OrderByComparator<UserBadge> orderByComparator) {
		return getPersistence().fetchByUserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the user badges before and after the current user badge in the ordered set where userId = &#63;.
	*
	* @param userBadgePK the primary key of the current user badge
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user badge
	* @throws NoSuchUserBadgeException if a user badge with the primary key could not be found
	*/
	public static UserBadge[] findByUserId_PrevAndNext(
		UserBadgePK userBadgePK, long userId,
		OrderByComparator<UserBadge> orderByComparator)
		throws com.devshaker.exception.NoSuchUserBadgeException {
		return getPersistence()
				   .findByUserId_PrevAndNext(userBadgePK, userId,
			orderByComparator);
	}

	/**
	* Removes all the user badges where userId = &#63; from the database.
	*
	* @param userId the user ID
	*/
	public static void removeByUserId(long userId) {
		getPersistence().removeByUserId(userId);
	}

	/**
	* Returns the number of user badges where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching user badges
	*/
	public static int countByUserId(long userId) {
		return getPersistence().countByUserId(userId);
	}

	/**
	* Returns all the user badges where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @return the matching user badges
	*/
	public static List<UserBadge> findByBadgeId(long badgeId) {
		return getPersistence().findByBadgeId(badgeId);
	}

	/**
	* Returns a range of all the user badges where badgeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param badgeId the badge ID
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @return the range of matching user badges
	*/
	public static List<UserBadge> findByBadgeId(long badgeId, int start, int end) {
		return getPersistence().findByBadgeId(badgeId, start, end);
	}

	/**
	* Returns an ordered range of all the user badges where badgeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param badgeId the badge ID
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user badges
	*/
	public static List<UserBadge> findByBadgeId(long badgeId, int start,
		int end, OrderByComparator<UserBadge> orderByComparator) {
		return getPersistence()
				   .findByBadgeId(badgeId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the user badges where badgeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param badgeId the badge ID
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user badges
	*/
	public static List<UserBadge> findByBadgeId(long badgeId, int start,
		int end, OrderByComparator<UserBadge> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByBadgeId(badgeId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first user badge in the ordered set where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user badge
	* @throws NoSuchUserBadgeException if a matching user badge could not be found
	*/
	public static UserBadge findByBadgeId_First(long badgeId,
		OrderByComparator<UserBadge> orderByComparator)
		throws com.devshaker.exception.NoSuchUserBadgeException {
		return getPersistence().findByBadgeId_First(badgeId, orderByComparator);
	}

	/**
	* Returns the first user badge in the ordered set where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user badge, or <code>null</code> if a matching user badge could not be found
	*/
	public static UserBadge fetchByBadgeId_First(long badgeId,
		OrderByComparator<UserBadge> orderByComparator) {
		return getPersistence().fetchByBadgeId_First(badgeId, orderByComparator);
	}

	/**
	* Returns the last user badge in the ordered set where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user badge
	* @throws NoSuchUserBadgeException if a matching user badge could not be found
	*/
	public static UserBadge findByBadgeId_Last(long badgeId,
		OrderByComparator<UserBadge> orderByComparator)
		throws com.devshaker.exception.NoSuchUserBadgeException {
		return getPersistence().findByBadgeId_Last(badgeId, orderByComparator);
	}

	/**
	* Returns the last user badge in the ordered set where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user badge, or <code>null</code> if a matching user badge could not be found
	*/
	public static UserBadge fetchByBadgeId_Last(long badgeId,
		OrderByComparator<UserBadge> orderByComparator) {
		return getPersistence().fetchByBadgeId_Last(badgeId, orderByComparator);
	}

	/**
	* Returns the user badges before and after the current user badge in the ordered set where badgeId = &#63;.
	*
	* @param userBadgePK the primary key of the current user badge
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user badge
	* @throws NoSuchUserBadgeException if a user badge with the primary key could not be found
	*/
	public static UserBadge[] findByBadgeId_PrevAndNext(
		UserBadgePK userBadgePK, long badgeId,
		OrderByComparator<UserBadge> orderByComparator)
		throws com.devshaker.exception.NoSuchUserBadgeException {
		return getPersistence()
				   .findByBadgeId_PrevAndNext(userBadgePK, badgeId,
			orderByComparator);
	}

	/**
	* Removes all the user badges where badgeId = &#63; from the database.
	*
	* @param badgeId the badge ID
	*/
	public static void removeByBadgeId(long badgeId) {
		getPersistence().removeByBadgeId(badgeId);
	}

	/**
	* Returns the number of user badges where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @return the number of matching user badges
	*/
	public static int countByBadgeId(long badgeId) {
		return getPersistence().countByBadgeId(badgeId);
	}

	/**
	* Caches the user badge in the entity cache if it is enabled.
	*
	* @param userBadge the user badge
	*/
	public static void cacheResult(UserBadge userBadge) {
		getPersistence().cacheResult(userBadge);
	}

	/**
	* Caches the user badges in the entity cache if it is enabled.
	*
	* @param userBadges the user badges
	*/
	public static void cacheResult(List<UserBadge> userBadges) {
		getPersistence().cacheResult(userBadges);
	}

	/**
	* Creates a new user badge with the primary key. Does not add the user badge to the database.
	*
	* @param userBadgePK the primary key for the new user badge
	* @return the new user badge
	*/
	public static UserBadge create(UserBadgePK userBadgePK) {
		return getPersistence().create(userBadgePK);
	}

	/**
	* Removes the user badge with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userBadgePK the primary key of the user badge
	* @return the user badge that was removed
	* @throws NoSuchUserBadgeException if a user badge with the primary key could not be found
	*/
	public static UserBadge remove(UserBadgePK userBadgePK)
		throws com.devshaker.exception.NoSuchUserBadgeException {
		return getPersistence().remove(userBadgePK);
	}

	public static UserBadge updateImpl(UserBadge userBadge) {
		return getPersistence().updateImpl(userBadge);
	}

	/**
	* Returns the user badge with the primary key or throws a {@link NoSuchUserBadgeException} if it could not be found.
	*
	* @param userBadgePK the primary key of the user badge
	* @return the user badge
	* @throws NoSuchUserBadgeException if a user badge with the primary key could not be found
	*/
	public static UserBadge findByPrimaryKey(UserBadgePK userBadgePK)
		throws com.devshaker.exception.NoSuchUserBadgeException {
		return getPersistence().findByPrimaryKey(userBadgePK);
	}

	/**
	* Returns the user badge with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userBadgePK the primary key of the user badge
	* @return the user badge, or <code>null</code> if a user badge with the primary key could not be found
	*/
	public static UserBadge fetchByPrimaryKey(UserBadgePK userBadgePK) {
		return getPersistence().fetchByPrimaryKey(userBadgePK);
	}

	public static java.util.Map<java.io.Serializable, UserBadge> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the user badges.
	*
	* @return the user badges
	*/
	public static List<UserBadge> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the user badges.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @return the range of user badges
	*/
	public static List<UserBadge> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the user badges.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user badges
	*/
	public static List<UserBadge> findAll(int start, int end,
		OrderByComparator<UserBadge> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the user badges.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of user badges
	*/
	public static List<UserBadge> findAll(int start, int end,
		OrderByComparator<UserBadge> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the user badges from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of user badges.
	*
	* @return the number of user badges
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static UserBadgePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<UserBadgePersistence, UserBadgePersistence> _serviceTracker =
		ServiceTrackerFactory.open(UserBadgePersistence.class);
}