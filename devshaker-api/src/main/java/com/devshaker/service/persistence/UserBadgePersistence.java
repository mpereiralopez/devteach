/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.exception.NoSuchUserBadgeException;

import com.devshaker.model.UserBadge;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the user badge service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.devshaker.service.persistence.impl.UserBadgePersistenceImpl
 * @see UserBadgeUtil
 * @generated
 */
@ProviderType
public interface UserBadgePersistence extends BasePersistence<UserBadge> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UserBadgeUtil} to access the user badge persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the user badges where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching user badges
	*/
	public java.util.List<UserBadge> findByUserId(long userId);

	/**
	* Returns a range of all the user badges where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @return the range of matching user badges
	*/
	public java.util.List<UserBadge> findByUserId(long userId, int start,
		int end);

	/**
	* Returns an ordered range of all the user badges where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user badges
	*/
	public java.util.List<UserBadge> findByUserId(long userId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserBadge> orderByComparator);

	/**
	* Returns an ordered range of all the user badges where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user badges
	*/
	public java.util.List<UserBadge> findByUserId(long userId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserBadge> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first user badge in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user badge
	* @throws NoSuchUserBadgeException if a matching user badge could not be found
	*/
	public UserBadge findByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<UserBadge> orderByComparator)
		throws NoSuchUserBadgeException;

	/**
	* Returns the first user badge in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user badge, or <code>null</code> if a matching user badge could not be found
	*/
	public UserBadge fetchByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<UserBadge> orderByComparator);

	/**
	* Returns the last user badge in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user badge
	* @throws NoSuchUserBadgeException if a matching user badge could not be found
	*/
	public UserBadge findByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<UserBadge> orderByComparator)
		throws NoSuchUserBadgeException;

	/**
	* Returns the last user badge in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user badge, or <code>null</code> if a matching user badge could not be found
	*/
	public UserBadge fetchByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<UserBadge> orderByComparator);

	/**
	* Returns the user badges before and after the current user badge in the ordered set where userId = &#63;.
	*
	* @param userBadgePK the primary key of the current user badge
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user badge
	* @throws NoSuchUserBadgeException if a user badge with the primary key could not be found
	*/
	public UserBadge[] findByUserId_PrevAndNext(UserBadgePK userBadgePK,
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator<UserBadge> orderByComparator)
		throws NoSuchUserBadgeException;

	/**
	* Removes all the user badges where userId = &#63; from the database.
	*
	* @param userId the user ID
	*/
	public void removeByUserId(long userId);

	/**
	* Returns the number of user badges where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching user badges
	*/
	public int countByUserId(long userId);

	/**
	* Returns all the user badges where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @return the matching user badges
	*/
	public java.util.List<UserBadge> findByBadgeId(long badgeId);

	/**
	* Returns a range of all the user badges where badgeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param badgeId the badge ID
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @return the range of matching user badges
	*/
	public java.util.List<UserBadge> findByBadgeId(long badgeId, int start,
		int end);

	/**
	* Returns an ordered range of all the user badges where badgeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param badgeId the badge ID
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user badges
	*/
	public java.util.List<UserBadge> findByBadgeId(long badgeId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserBadge> orderByComparator);

	/**
	* Returns an ordered range of all the user badges where badgeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param badgeId the badge ID
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user badges
	*/
	public java.util.List<UserBadge> findByBadgeId(long badgeId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserBadge> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first user badge in the ordered set where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user badge
	* @throws NoSuchUserBadgeException if a matching user badge could not be found
	*/
	public UserBadge findByBadgeId_First(long badgeId,
		com.liferay.portal.kernel.util.OrderByComparator<UserBadge> orderByComparator)
		throws NoSuchUserBadgeException;

	/**
	* Returns the first user badge in the ordered set where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user badge, or <code>null</code> if a matching user badge could not be found
	*/
	public UserBadge fetchByBadgeId_First(long badgeId,
		com.liferay.portal.kernel.util.OrderByComparator<UserBadge> orderByComparator);

	/**
	* Returns the last user badge in the ordered set where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user badge
	* @throws NoSuchUserBadgeException if a matching user badge could not be found
	*/
	public UserBadge findByBadgeId_Last(long badgeId,
		com.liferay.portal.kernel.util.OrderByComparator<UserBadge> orderByComparator)
		throws NoSuchUserBadgeException;

	/**
	* Returns the last user badge in the ordered set where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user badge, or <code>null</code> if a matching user badge could not be found
	*/
	public UserBadge fetchByBadgeId_Last(long badgeId,
		com.liferay.portal.kernel.util.OrderByComparator<UserBadge> orderByComparator);

	/**
	* Returns the user badges before and after the current user badge in the ordered set where badgeId = &#63;.
	*
	* @param userBadgePK the primary key of the current user badge
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user badge
	* @throws NoSuchUserBadgeException if a user badge with the primary key could not be found
	*/
	public UserBadge[] findByBadgeId_PrevAndNext(UserBadgePK userBadgePK,
		long badgeId,
		com.liferay.portal.kernel.util.OrderByComparator<UserBadge> orderByComparator)
		throws NoSuchUserBadgeException;

	/**
	* Removes all the user badges where badgeId = &#63; from the database.
	*
	* @param badgeId the badge ID
	*/
	public void removeByBadgeId(long badgeId);

	/**
	* Returns the number of user badges where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @return the number of matching user badges
	*/
	public int countByBadgeId(long badgeId);

	/**
	* Caches the user badge in the entity cache if it is enabled.
	*
	* @param userBadge the user badge
	*/
	public void cacheResult(UserBadge userBadge);

	/**
	* Caches the user badges in the entity cache if it is enabled.
	*
	* @param userBadges the user badges
	*/
	public void cacheResult(java.util.List<UserBadge> userBadges);

	/**
	* Creates a new user badge with the primary key. Does not add the user badge to the database.
	*
	* @param userBadgePK the primary key for the new user badge
	* @return the new user badge
	*/
	public UserBadge create(UserBadgePK userBadgePK);

	/**
	* Removes the user badge with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userBadgePK the primary key of the user badge
	* @return the user badge that was removed
	* @throws NoSuchUserBadgeException if a user badge with the primary key could not be found
	*/
	public UserBadge remove(UserBadgePK userBadgePK)
		throws NoSuchUserBadgeException;

	public UserBadge updateImpl(UserBadge userBadge);

	/**
	* Returns the user badge with the primary key or throws a {@link NoSuchUserBadgeException} if it could not be found.
	*
	* @param userBadgePK the primary key of the user badge
	* @return the user badge
	* @throws NoSuchUserBadgeException if a user badge with the primary key could not be found
	*/
	public UserBadge findByPrimaryKey(UserBadgePK userBadgePK)
		throws NoSuchUserBadgeException;

	/**
	* Returns the user badge with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userBadgePK the primary key of the user badge
	* @return the user badge, or <code>null</code> if a user badge with the primary key could not be found
	*/
	public UserBadge fetchByPrimaryKey(UserBadgePK userBadgePK);

	@Override
	public java.util.Map<java.io.Serializable, UserBadge> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the user badges.
	*
	* @return the user badges
	*/
	public java.util.List<UserBadge> findAll();

	/**
	* Returns a range of all the user badges.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @return the range of user badges
	*/
	public java.util.List<UserBadge> findAll(int start, int end);

	/**
	* Returns an ordered range of all the user badges.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user badges
	*/
	public java.util.List<UserBadge> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserBadge> orderByComparator);

	/**
	* Returns an ordered range of all the user badges.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of user badges
	*/
	public java.util.List<UserBadge> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserBadge> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the user badges from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of user badges.
	*
	* @return the number of user badges
	*/
	public int countAll();
}