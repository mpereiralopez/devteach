/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.TestQuestion;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the test question service. This utility wraps {@link com.devshaker.service.persistence.impl.TestQuestionPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TestQuestionPersistence
 * @see com.devshaker.service.persistence.impl.TestQuestionPersistenceImpl
 * @generated
 */
@ProviderType
public class TestQuestionUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(TestQuestion testQuestion) {
		getPersistence().clearCache(testQuestion);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<TestQuestion> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<TestQuestion> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<TestQuestion> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static TestQuestion update(TestQuestion testQuestion) {
		return getPersistence().update(testQuestion);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static TestQuestion update(TestQuestion testQuestion,
		ServiceContext serviceContext) {
		return getPersistence().update(testQuestion, serviceContext);
	}

	/**
	* Returns all the test questions where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching test questions
	*/
	public static List<TestQuestion> findByCompanyId(long companyId) {
		return getPersistence().findByCompanyId(companyId);
	}

	/**
	* Returns a range of all the test questions where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @return the range of matching test questions
	*/
	public static List<TestQuestion> findByCompanyId(long companyId, int start,
		int end) {
		return getPersistence().findByCompanyId(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the test questions where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching test questions
	*/
	public static List<TestQuestion> findByCompanyId(long companyId, int start,
		int end, OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence()
				   .findByCompanyId(companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the test questions where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching test questions
	*/
	public static List<TestQuestion> findByCompanyId(long companyId, int start,
		int end, OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByCompanyId(companyId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first test question in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public static TestQuestion findByCompanyId_First(long companyId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence()
				   .findByCompanyId_First(companyId, orderByComparator);
	}

	/**
	* Returns the first test question in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public static TestQuestion fetchByCompanyId_First(long companyId,
		OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyId_First(companyId, orderByComparator);
	}

	/**
	* Returns the last test question in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public static TestQuestion findByCompanyId_Last(long companyId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence()
				   .findByCompanyId_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last test question in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public static TestQuestion fetchByCompanyId_Last(long companyId,
		OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyId_Last(companyId, orderByComparator);
	}

	/**
	* Returns the test questions before and after the current test question in the ordered set where companyId = &#63;.
	*
	* @param questionId the primary key of the current test question
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next test question
	* @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	*/
	public static TestQuestion[] findByCompanyId_PrevAndNext(long questionId,
		long companyId, OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence()
				   .findByCompanyId_PrevAndNext(questionId, companyId,
			orderByComparator);
	}

	/**
	* Removes all the test questions where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	*/
	public static void removeByCompanyId(long companyId) {
		getPersistence().removeByCompanyId(companyId);
	}

	/**
	* Returns the number of test questions where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching test questions
	*/
	public static int countByCompanyId(long companyId) {
		return getPersistence().countByCompanyId(companyId);
	}

	/**
	* Returns all the test questions where userIdCreator = &#63;.
	*
	* @param userIdCreator the user ID creator
	* @return the matching test questions
	*/
	public static List<TestQuestion> findByUserIdCreator(long userIdCreator) {
		return getPersistence().findByUserIdCreator(userIdCreator);
	}

	/**
	* Returns a range of all the test questions where userIdCreator = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userIdCreator the user ID creator
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @return the range of matching test questions
	*/
	public static List<TestQuestion> findByUserIdCreator(long userIdCreator,
		int start, int end) {
		return getPersistence().findByUserIdCreator(userIdCreator, start, end);
	}

	/**
	* Returns an ordered range of all the test questions where userIdCreator = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userIdCreator the user ID creator
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching test questions
	*/
	public static List<TestQuestion> findByUserIdCreator(long userIdCreator,
		int start, int end, OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence()
				   .findByUserIdCreator(userIdCreator, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the test questions where userIdCreator = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userIdCreator the user ID creator
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching test questions
	*/
	public static List<TestQuestion> findByUserIdCreator(long userIdCreator,
		int start, int end, OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUserIdCreator(userIdCreator, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first test question in the ordered set where userIdCreator = &#63;.
	*
	* @param userIdCreator the user ID creator
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public static TestQuestion findByUserIdCreator_First(long userIdCreator,
		OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence()
				   .findByUserIdCreator_First(userIdCreator, orderByComparator);
	}

	/**
	* Returns the first test question in the ordered set where userIdCreator = &#63;.
	*
	* @param userIdCreator the user ID creator
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public static TestQuestion fetchByUserIdCreator_First(long userIdCreator,
		OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence()
				   .fetchByUserIdCreator_First(userIdCreator, orderByComparator);
	}

	/**
	* Returns the last test question in the ordered set where userIdCreator = &#63;.
	*
	* @param userIdCreator the user ID creator
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public static TestQuestion findByUserIdCreator_Last(long userIdCreator,
		OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence()
				   .findByUserIdCreator_Last(userIdCreator, orderByComparator);
	}

	/**
	* Returns the last test question in the ordered set where userIdCreator = &#63;.
	*
	* @param userIdCreator the user ID creator
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public static TestQuestion fetchByUserIdCreator_Last(long userIdCreator,
		OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence()
				   .fetchByUserIdCreator_Last(userIdCreator, orderByComparator);
	}

	/**
	* Returns the test questions before and after the current test question in the ordered set where userIdCreator = &#63;.
	*
	* @param questionId the primary key of the current test question
	* @param userIdCreator the user ID creator
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next test question
	* @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	*/
	public static TestQuestion[] findByUserIdCreator_PrevAndNext(
		long questionId, long userIdCreator,
		OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence()
				   .findByUserIdCreator_PrevAndNext(questionId, userIdCreator,
			orderByComparator);
	}

	/**
	* Removes all the test questions where userIdCreator = &#63; from the database.
	*
	* @param userIdCreator the user ID creator
	*/
	public static void removeByUserIdCreator(long userIdCreator) {
		getPersistence().removeByUserIdCreator(userIdCreator);
	}

	/**
	* Returns the number of test questions where userIdCreator = &#63;.
	*
	* @param userIdCreator the user ID creator
	* @return the number of matching test questions
	*/
	public static int countByUserIdCreator(long userIdCreator) {
		return getPersistence().countByUserIdCreator(userIdCreator);
	}

	/**
	* Returns all the test questions where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @return the matching test questions
	*/
	public static List<TestQuestion> findByCategoryId(long categoryId) {
		return getPersistence().findByCategoryId(categoryId);
	}

	/**
	* Returns a range of all the test questions where categoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoryId the category ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @return the range of matching test questions
	*/
	public static List<TestQuestion> findByCategoryId(long categoryId,
		int start, int end) {
		return getPersistence().findByCategoryId(categoryId, start, end);
	}

	/**
	* Returns an ordered range of all the test questions where categoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoryId the category ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching test questions
	*/
	public static List<TestQuestion> findByCategoryId(long categoryId,
		int start, int end, OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence()
				   .findByCategoryId(categoryId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the test questions where categoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoryId the category ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching test questions
	*/
	public static List<TestQuestion> findByCategoryId(long categoryId,
		int start, int end, OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByCategoryId(categoryId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first test question in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public static TestQuestion findByCategoryId_First(long categoryId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence()
				   .findByCategoryId_First(categoryId, orderByComparator);
	}

	/**
	* Returns the first test question in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public static TestQuestion fetchByCategoryId_First(long categoryId,
		OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence()
				   .fetchByCategoryId_First(categoryId, orderByComparator);
	}

	/**
	* Returns the last test question in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public static TestQuestion findByCategoryId_Last(long categoryId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence()
				   .findByCategoryId_Last(categoryId, orderByComparator);
	}

	/**
	* Returns the last test question in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public static TestQuestion fetchByCategoryId_Last(long categoryId,
		OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence()
				   .fetchByCategoryId_Last(categoryId, orderByComparator);
	}

	/**
	* Returns the test questions before and after the current test question in the ordered set where categoryId = &#63;.
	*
	* @param questionId the primary key of the current test question
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next test question
	* @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	*/
	public static TestQuestion[] findByCategoryId_PrevAndNext(long questionId,
		long categoryId, OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence()
				   .findByCategoryId_PrevAndNext(questionId, categoryId,
			orderByComparator);
	}

	/**
	* Removes all the test questions where categoryId = &#63; from the database.
	*
	* @param categoryId the category ID
	*/
	public static void removeByCategoryId(long categoryId) {
		getPersistence().removeByCategoryId(categoryId);
	}

	/**
	* Returns the number of test questions where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @return the number of matching test questions
	*/
	public static int countByCategoryId(long categoryId) {
		return getPersistence().countByCategoryId(categoryId);
	}

	/**
	* Returns all the test questions where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @return the matching test questions
	*/
	public static List<TestQuestion> findBySubcategoryId(long subcategoryId) {
		return getPersistence().findBySubcategoryId(subcategoryId);
	}

	/**
	* Returns a range of all the test questions where subcategoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param subcategoryId the subcategory ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @return the range of matching test questions
	*/
	public static List<TestQuestion> findBySubcategoryId(long subcategoryId,
		int start, int end) {
		return getPersistence().findBySubcategoryId(subcategoryId, start, end);
	}

	/**
	* Returns an ordered range of all the test questions where subcategoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param subcategoryId the subcategory ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching test questions
	*/
	public static List<TestQuestion> findBySubcategoryId(long subcategoryId,
		int start, int end, OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence()
				   .findBySubcategoryId(subcategoryId, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the test questions where subcategoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param subcategoryId the subcategory ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching test questions
	*/
	public static List<TestQuestion> findBySubcategoryId(long subcategoryId,
		int start, int end, OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findBySubcategoryId(subcategoryId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first test question in the ordered set where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public static TestQuestion findBySubcategoryId_First(long subcategoryId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence()
				   .findBySubcategoryId_First(subcategoryId, orderByComparator);
	}

	/**
	* Returns the first test question in the ordered set where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public static TestQuestion fetchBySubcategoryId_First(long subcategoryId,
		OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence()
				   .fetchBySubcategoryId_First(subcategoryId, orderByComparator);
	}

	/**
	* Returns the last test question in the ordered set where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public static TestQuestion findBySubcategoryId_Last(long subcategoryId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence()
				   .findBySubcategoryId_Last(subcategoryId, orderByComparator);
	}

	/**
	* Returns the last test question in the ordered set where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public static TestQuestion fetchBySubcategoryId_Last(long subcategoryId,
		OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence()
				   .fetchBySubcategoryId_Last(subcategoryId, orderByComparator);
	}

	/**
	* Returns the test questions before and after the current test question in the ordered set where subcategoryId = &#63;.
	*
	* @param questionId the primary key of the current test question
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next test question
	* @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	*/
	public static TestQuestion[] findBySubcategoryId_PrevAndNext(
		long questionId, long subcategoryId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence()
				   .findBySubcategoryId_PrevAndNext(questionId, subcategoryId,
			orderByComparator);
	}

	/**
	* Removes all the test questions where subcategoryId = &#63; from the database.
	*
	* @param subcategoryId the subcategory ID
	*/
	public static void removeBySubcategoryId(long subcategoryId) {
		getPersistence().removeBySubcategoryId(subcategoryId);
	}

	/**
	* Returns the number of test questions where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @return the number of matching test questions
	*/
	public static int countBySubcategoryId(long subcategoryId) {
		return getPersistence().countBySubcategoryId(subcategoryId);
	}

	/**
	* Returns all the test questions where levelId = &#63;.
	*
	* @param levelId the level ID
	* @return the matching test questions
	*/
	public static List<TestQuestion> findBylevelId(long levelId) {
		return getPersistence().findBylevelId(levelId);
	}

	/**
	* Returns a range of all the test questions where levelId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param levelId the level ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @return the range of matching test questions
	*/
	public static List<TestQuestion> findBylevelId(long levelId, int start,
		int end) {
		return getPersistence().findBylevelId(levelId, start, end);
	}

	/**
	* Returns an ordered range of all the test questions where levelId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param levelId the level ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching test questions
	*/
	public static List<TestQuestion> findBylevelId(long levelId, int start,
		int end, OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence()
				   .findBylevelId(levelId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the test questions where levelId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param levelId the level ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching test questions
	*/
	public static List<TestQuestion> findBylevelId(long levelId, int start,
		int end, OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findBylevelId(levelId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first test question in the ordered set where levelId = &#63;.
	*
	* @param levelId the level ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public static TestQuestion findBylevelId_First(long levelId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence().findBylevelId_First(levelId, orderByComparator);
	}

	/**
	* Returns the first test question in the ordered set where levelId = &#63;.
	*
	* @param levelId the level ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public static TestQuestion fetchBylevelId_First(long levelId,
		OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence().fetchBylevelId_First(levelId, orderByComparator);
	}

	/**
	* Returns the last test question in the ordered set where levelId = &#63;.
	*
	* @param levelId the level ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public static TestQuestion findBylevelId_Last(long levelId,
		OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence().findBylevelId_Last(levelId, orderByComparator);
	}

	/**
	* Returns the last test question in the ordered set where levelId = &#63;.
	*
	* @param levelId the level ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public static TestQuestion fetchBylevelId_Last(long levelId,
		OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence().fetchBylevelId_Last(levelId, orderByComparator);
	}

	/**
	* Returns the test questions before and after the current test question in the ordered set where levelId = &#63;.
	*
	* @param questionId the primary key of the current test question
	* @param levelId the level ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next test question
	* @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	*/
	public static TestQuestion[] findBylevelId_PrevAndNext(long questionId,
		long levelId, OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence()
				   .findBylevelId_PrevAndNext(questionId, levelId,
			orderByComparator);
	}

	/**
	* Removes all the test questions where levelId = &#63; from the database.
	*
	* @param levelId the level ID
	*/
	public static void removeBylevelId(long levelId) {
		getPersistence().removeBylevelId(levelId);
	}

	/**
	* Returns the number of test questions where levelId = &#63;.
	*
	* @param levelId the level ID
	* @return the number of matching test questions
	*/
	public static int countBylevelId(long levelId) {
		return getPersistence().countBylevelId(levelId);
	}

	/**
	* Returns all the test questions where status = &#63;.
	*
	* @param status the status
	* @return the matching test questions
	*/
	public static List<TestQuestion> findByStatus(int status) {
		return getPersistence().findByStatus(status);
	}

	/**
	* Returns a range of all the test questions where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @return the range of matching test questions
	*/
	public static List<TestQuestion> findByStatus(int status, int start, int end) {
		return getPersistence().findByStatus(status, start, end);
	}

	/**
	* Returns an ordered range of all the test questions where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching test questions
	*/
	public static List<TestQuestion> findByStatus(int status, int start,
		int end, OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence()
				   .findByStatus(status, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the test questions where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching test questions
	*/
	public static List<TestQuestion> findByStatus(int status, int start,
		int end, OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByStatus(status, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first test question in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public static TestQuestion findByStatus_First(int status,
		OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence().findByStatus_First(status, orderByComparator);
	}

	/**
	* Returns the first test question in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public static TestQuestion fetchByStatus_First(int status,
		OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence().fetchByStatus_First(status, orderByComparator);
	}

	/**
	* Returns the last test question in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public static TestQuestion findByStatus_Last(int status,
		OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence().findByStatus_Last(status, orderByComparator);
	}

	/**
	* Returns the last test question in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public static TestQuestion fetchByStatus_Last(int status,
		OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence().fetchByStatus_Last(status, orderByComparator);
	}

	/**
	* Returns the test questions before and after the current test question in the ordered set where status = &#63;.
	*
	* @param questionId the primary key of the current test question
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next test question
	* @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	*/
	public static TestQuestion[] findByStatus_PrevAndNext(long questionId,
		int status, OrderByComparator<TestQuestion> orderByComparator)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence()
				   .findByStatus_PrevAndNext(questionId, status,
			orderByComparator);
	}

	/**
	* Removes all the test questions where status = &#63; from the database.
	*
	* @param status the status
	*/
	public static void removeByStatus(int status) {
		getPersistence().removeByStatus(status);
	}

	/**
	* Returns the number of test questions where status = &#63;.
	*
	* @param status the status
	* @return the number of matching test questions
	*/
	public static int countByStatus(int status) {
		return getPersistence().countByStatus(status);
	}

	/**
	* Caches the test question in the entity cache if it is enabled.
	*
	* @param testQuestion the test question
	*/
	public static void cacheResult(TestQuestion testQuestion) {
		getPersistence().cacheResult(testQuestion);
	}

	/**
	* Caches the test questions in the entity cache if it is enabled.
	*
	* @param testQuestions the test questions
	*/
	public static void cacheResult(List<TestQuestion> testQuestions) {
		getPersistence().cacheResult(testQuestions);
	}

	/**
	* Creates a new test question with the primary key. Does not add the test question to the database.
	*
	* @param questionId the primary key for the new test question
	* @return the new test question
	*/
	public static TestQuestion create(long questionId) {
		return getPersistence().create(questionId);
	}

	/**
	* Removes the test question with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param questionId the primary key of the test question
	* @return the test question that was removed
	* @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	*/
	public static TestQuestion remove(long questionId)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence().remove(questionId);
	}

	public static TestQuestion updateImpl(TestQuestion testQuestion) {
		return getPersistence().updateImpl(testQuestion);
	}

	/**
	* Returns the test question with the primary key or throws a {@link NoSuchTestQuestionException} if it could not be found.
	*
	* @param questionId the primary key of the test question
	* @return the test question
	* @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	*/
	public static TestQuestion findByPrimaryKey(long questionId)
		throws com.devshaker.exception.NoSuchTestQuestionException {
		return getPersistence().findByPrimaryKey(questionId);
	}

	/**
	* Returns the test question with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param questionId the primary key of the test question
	* @return the test question, or <code>null</code> if a test question with the primary key could not be found
	*/
	public static TestQuestion fetchByPrimaryKey(long questionId) {
		return getPersistence().fetchByPrimaryKey(questionId);
	}

	public static java.util.Map<java.io.Serializable, TestQuestion> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the test questions.
	*
	* @return the test questions
	*/
	public static List<TestQuestion> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the test questions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @return the range of test questions
	*/
	public static List<TestQuestion> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the test questions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of test questions
	*/
	public static List<TestQuestion> findAll(int start, int end,
		OrderByComparator<TestQuestion> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the test questions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of test questions
	*/
	public static List<TestQuestion> findAll(int start, int end,
		OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the test questions from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of test questions.
	*
	* @return the number of test questions
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static TestQuestionPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<TestQuestionPersistence, TestQuestionPersistence> _serviceTracker =
		ServiceTrackerFactory.open(TestQuestionPersistence.class);
}