/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.exception.NoSuchLevelException;

import com.devshaker.model.Level;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the level service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.devshaker.service.persistence.impl.LevelPersistenceImpl
 * @see LevelUtil
 * @generated
 */
@ProviderType
public interface LevelPersistence extends BasePersistence<Level> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LevelUtil} to access the level persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the levels where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching levels
	*/
	public java.util.List<Level> findByCompanyId(long companyId);

	/**
	* Returns a range of all the levels where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @return the range of matching levels
	*/
	public java.util.List<Level> findByCompanyId(long companyId, int start,
		int end);

	/**
	* Returns an ordered range of all the levels where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching levels
	*/
	public java.util.List<Level> findByCompanyId(long companyId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator);

	/**
	* Returns an ordered range of all the levels where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching levels
	*/
	public java.util.List<Level> findByCompanyId(long companyId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first level in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching level
	* @throws NoSuchLevelException if a matching level could not be found
	*/
	public Level findByCompanyId_First(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator)
		throws NoSuchLevelException;

	/**
	* Returns the first level in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching level, or <code>null</code> if a matching level could not be found
	*/
	public Level fetchByCompanyId_First(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator);

	/**
	* Returns the last level in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching level
	* @throws NoSuchLevelException if a matching level could not be found
	*/
	public Level findByCompanyId_Last(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator)
		throws NoSuchLevelException;

	/**
	* Returns the last level in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching level, or <code>null</code> if a matching level could not be found
	*/
	public Level fetchByCompanyId_Last(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator);

	/**
	* Returns the levels before and after the current level in the ordered set where companyId = &#63;.
	*
	* @param levelId the primary key of the current level
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next level
	* @throws NoSuchLevelException if a level with the primary key could not be found
	*/
	public Level[] findByCompanyId_PrevAndNext(long levelId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator)
		throws NoSuchLevelException;

	/**
	* Removes all the levels where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	*/
	public void removeByCompanyId(long companyId);

	/**
	* Returns the number of levels where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching levels
	*/
	public int countByCompanyId(long companyId);

	/**
	* Returns all the levels where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching levels
	*/
	public java.util.List<Level> findByUserId(long userId);

	/**
	* Returns a range of all the levels where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @return the range of matching levels
	*/
	public java.util.List<Level> findByUserId(long userId, int start, int end);

	/**
	* Returns an ordered range of all the levels where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching levels
	*/
	public java.util.List<Level> findByUserId(long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator);

	/**
	* Returns an ordered range of all the levels where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching levels
	*/
	public java.util.List<Level> findByUserId(long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first level in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching level
	* @throws NoSuchLevelException if a matching level could not be found
	*/
	public Level findByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator)
		throws NoSuchLevelException;

	/**
	* Returns the first level in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching level, or <code>null</code> if a matching level could not be found
	*/
	public Level fetchByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator);

	/**
	* Returns the last level in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching level
	* @throws NoSuchLevelException if a matching level could not be found
	*/
	public Level findByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator)
		throws NoSuchLevelException;

	/**
	* Returns the last level in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching level, or <code>null</code> if a matching level could not be found
	*/
	public Level fetchByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator);

	/**
	* Returns the levels before and after the current level in the ordered set where userId = &#63;.
	*
	* @param levelId the primary key of the current level
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next level
	* @throws NoSuchLevelException if a level with the primary key could not be found
	*/
	public Level[] findByUserId_PrevAndNext(long levelId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator)
		throws NoSuchLevelException;

	/**
	* Removes all the levels where userId = &#63; from the database.
	*
	* @param userId the user ID
	*/
	public void removeByUserId(long userId);

	/**
	* Returns the number of levels where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching levels
	*/
	public int countByUserId(long userId);

	/**
	* Returns all the levels where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @return the matching levels
	*/
	public java.util.List<Level> findByCategoryId(long categoryId);

	/**
	* Returns a range of all the levels where categoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoryId the category ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @return the range of matching levels
	*/
	public java.util.List<Level> findByCategoryId(long categoryId, int start,
		int end);

	/**
	* Returns an ordered range of all the levels where categoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoryId the category ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching levels
	*/
	public java.util.List<Level> findByCategoryId(long categoryId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator);

	/**
	* Returns an ordered range of all the levels where categoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoryId the category ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching levels
	*/
	public java.util.List<Level> findByCategoryId(long categoryId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first level in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching level
	* @throws NoSuchLevelException if a matching level could not be found
	*/
	public Level findByCategoryId_First(long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator)
		throws NoSuchLevelException;

	/**
	* Returns the first level in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching level, or <code>null</code> if a matching level could not be found
	*/
	public Level fetchByCategoryId_First(long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator);

	/**
	* Returns the last level in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching level
	* @throws NoSuchLevelException if a matching level could not be found
	*/
	public Level findByCategoryId_Last(long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator)
		throws NoSuchLevelException;

	/**
	* Returns the last level in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching level, or <code>null</code> if a matching level could not be found
	*/
	public Level fetchByCategoryId_Last(long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator);

	/**
	* Returns the levels before and after the current level in the ordered set where categoryId = &#63;.
	*
	* @param levelId the primary key of the current level
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next level
	* @throws NoSuchLevelException if a level with the primary key could not be found
	*/
	public Level[] findByCategoryId_PrevAndNext(long levelId, long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator)
		throws NoSuchLevelException;

	/**
	* Removes all the levels where categoryId = &#63; from the database.
	*
	* @param categoryId the category ID
	*/
	public void removeByCategoryId(long categoryId);

	/**
	* Returns the number of levels where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @return the number of matching levels
	*/
	public int countByCategoryId(long categoryId);

	/**
	* Returns all the levels where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @return the matching levels
	*/
	public java.util.List<Level> findBySubcategoryId(long subcategoryId);

	/**
	* Returns a range of all the levels where subcategoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param subcategoryId the subcategory ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @return the range of matching levels
	*/
	public java.util.List<Level> findBySubcategoryId(long subcategoryId,
		int start, int end);

	/**
	* Returns an ordered range of all the levels where subcategoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param subcategoryId the subcategory ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching levels
	*/
	public java.util.List<Level> findBySubcategoryId(long subcategoryId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator);

	/**
	* Returns an ordered range of all the levels where subcategoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param subcategoryId the subcategory ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching levels
	*/
	public java.util.List<Level> findBySubcategoryId(long subcategoryId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first level in the ordered set where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching level
	* @throws NoSuchLevelException if a matching level could not be found
	*/
	public Level findBySubcategoryId_First(long subcategoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator)
		throws NoSuchLevelException;

	/**
	* Returns the first level in the ordered set where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching level, or <code>null</code> if a matching level could not be found
	*/
	public Level fetchBySubcategoryId_First(long subcategoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator);

	/**
	* Returns the last level in the ordered set where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching level
	* @throws NoSuchLevelException if a matching level could not be found
	*/
	public Level findBySubcategoryId_Last(long subcategoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator)
		throws NoSuchLevelException;

	/**
	* Returns the last level in the ordered set where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching level, or <code>null</code> if a matching level could not be found
	*/
	public Level fetchBySubcategoryId_Last(long subcategoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator);

	/**
	* Returns the levels before and after the current level in the ordered set where subcategoryId = &#63;.
	*
	* @param levelId the primary key of the current level
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next level
	* @throws NoSuchLevelException if a level with the primary key could not be found
	*/
	public Level[] findBySubcategoryId_PrevAndNext(long levelId,
		long subcategoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator)
		throws NoSuchLevelException;

	/**
	* Removes all the levels where subcategoryId = &#63; from the database.
	*
	* @param subcategoryId the subcategory ID
	*/
	public void removeBySubcategoryId(long subcategoryId);

	/**
	* Returns the number of levels where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @return the number of matching levels
	*/
	public int countBySubcategoryId(long subcategoryId);

	/**
	* Caches the level in the entity cache if it is enabled.
	*
	* @param level the level
	*/
	public void cacheResult(Level level);

	/**
	* Caches the levels in the entity cache if it is enabled.
	*
	* @param levels the levels
	*/
	public void cacheResult(java.util.List<Level> levels);

	/**
	* Creates a new level with the primary key. Does not add the level to the database.
	*
	* @param levelId the primary key for the new level
	* @return the new level
	*/
	public Level create(long levelId);

	/**
	* Removes the level with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param levelId the primary key of the level
	* @return the level that was removed
	* @throws NoSuchLevelException if a level with the primary key could not be found
	*/
	public Level remove(long levelId) throws NoSuchLevelException;

	public Level updateImpl(Level level);

	/**
	* Returns the level with the primary key or throws a {@link NoSuchLevelException} if it could not be found.
	*
	* @param levelId the primary key of the level
	* @return the level
	* @throws NoSuchLevelException if a level with the primary key could not be found
	*/
	public Level findByPrimaryKey(long levelId) throws NoSuchLevelException;

	/**
	* Returns the level with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param levelId the primary key of the level
	* @return the level, or <code>null</code> if a level with the primary key could not be found
	*/
	public Level fetchByPrimaryKey(long levelId);

	@Override
	public java.util.Map<java.io.Serializable, Level> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the levels.
	*
	* @return the levels
	*/
	public java.util.List<Level> findAll();

	/**
	* Returns a range of all the levels.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @return the range of levels
	*/
	public java.util.List<Level> findAll(int start, int end);

	/**
	* Returns an ordered range of all the levels.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of levels
	*/
	public java.util.List<Level> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator);

	/**
	* Returns an ordered range of all the levels.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of levels
	*/
	public java.util.List<Level> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Level> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the levels from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of levels.
	*
	* @return the number of levels
	*/
	public int countAll();
}