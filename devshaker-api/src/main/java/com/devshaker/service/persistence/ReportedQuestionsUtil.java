/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.ReportedQuestions;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the reported questions service. This utility wraps {@link com.devshaker.service.persistence.impl.ReportedQuestionsPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ReportedQuestionsPersistence
 * @see com.devshaker.service.persistence.impl.ReportedQuestionsPersistenceImpl
 * @generated
 */
@ProviderType
public class ReportedQuestionsUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ReportedQuestions reportedQuestions) {
		getPersistence().clearCache(reportedQuestions);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ReportedQuestions> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ReportedQuestions> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ReportedQuestions> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ReportedQuestions update(ReportedQuestions reportedQuestions) {
		return getPersistence().update(reportedQuestions);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ReportedQuestions update(
		ReportedQuestions reportedQuestions, ServiceContext serviceContext) {
		return getPersistence().update(reportedQuestions, serviceContext);
	}

	/**
	* Returns all the reported questionses where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching reported questionses
	*/
	public static List<ReportedQuestions> findByUserId(long userId) {
		return getPersistence().findByUserId(userId);
	}

	/**
	* Returns a range of all the reported questionses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @return the range of matching reported questionses
	*/
	public static List<ReportedQuestions> findByUserId(long userId, int start,
		int end) {
		return getPersistence().findByUserId(userId, start, end);
	}

	/**
	* Returns an ordered range of all the reported questionses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reported questionses
	*/
	public static List<ReportedQuestions> findByUserId(long userId, int start,
		int end, OrderByComparator<ReportedQuestions> orderByComparator) {
		return getPersistence()
				   .findByUserId(userId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the reported questionses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching reported questionses
	*/
	public static List<ReportedQuestions> findByUserId(long userId, int start,
		int end, OrderByComparator<ReportedQuestions> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUserId(userId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first reported questions in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reported questions
	* @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	*/
	public static ReportedQuestions findByUserId_First(long userId,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws com.devshaker.exception.NoSuchReportedQuestionsException {
		return getPersistence().findByUserId_First(userId, orderByComparator);
	}

	/**
	* Returns the first reported questions in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reported questions, or <code>null</code> if a matching reported questions could not be found
	*/
	public static ReportedQuestions fetchByUserId_First(long userId,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		return getPersistence().fetchByUserId_First(userId, orderByComparator);
	}

	/**
	* Returns the last reported questions in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reported questions
	* @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	*/
	public static ReportedQuestions findByUserId_Last(long userId,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws com.devshaker.exception.NoSuchReportedQuestionsException {
		return getPersistence().findByUserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the last reported questions in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reported questions, or <code>null</code> if a matching reported questions could not be found
	*/
	public static ReportedQuestions fetchByUserId_Last(long userId,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		return getPersistence().fetchByUserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the reported questionses before and after the current reported questions in the ordered set where userId = &#63;.
	*
	* @param reportId the primary key of the current reported questions
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next reported questions
	* @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	*/
	public static ReportedQuestions[] findByUserId_PrevAndNext(long reportId,
		long userId, OrderByComparator<ReportedQuestions> orderByComparator)
		throws com.devshaker.exception.NoSuchReportedQuestionsException {
		return getPersistence()
				   .findByUserId_PrevAndNext(reportId, userId, orderByComparator);
	}

	/**
	* Removes all the reported questionses where userId = &#63; from the database.
	*
	* @param userId the user ID
	*/
	public static void removeByUserId(long userId) {
		getPersistence().removeByUserId(userId);
	}

	/**
	* Returns the number of reported questionses where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching reported questionses
	*/
	public static int countByUserId(long userId) {
		return getPersistence().countByUserId(userId);
	}

	/**
	* Returns all the reported questionses where questionId = &#63;.
	*
	* @param questionId the question ID
	* @return the matching reported questionses
	*/
	public static List<ReportedQuestions> findByquestionId(long questionId) {
		return getPersistence().findByquestionId(questionId);
	}

	/**
	* Returns a range of all the reported questionses where questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @return the range of matching reported questionses
	*/
	public static List<ReportedQuestions> findByquestionId(long questionId,
		int start, int end) {
		return getPersistence().findByquestionId(questionId, start, end);
	}

	/**
	* Returns an ordered range of all the reported questionses where questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reported questionses
	*/
	public static List<ReportedQuestions> findByquestionId(long questionId,
		int start, int end,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		return getPersistence()
				   .findByquestionId(questionId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the reported questionses where questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching reported questionses
	*/
	public static List<ReportedQuestions> findByquestionId(long questionId,
		int start, int end,
		OrderByComparator<ReportedQuestions> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByquestionId(questionId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first reported questions in the ordered set where questionId = &#63;.
	*
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reported questions
	* @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	*/
	public static ReportedQuestions findByquestionId_First(long questionId,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws com.devshaker.exception.NoSuchReportedQuestionsException {
		return getPersistence()
				   .findByquestionId_First(questionId, orderByComparator);
	}

	/**
	* Returns the first reported questions in the ordered set where questionId = &#63;.
	*
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reported questions, or <code>null</code> if a matching reported questions could not be found
	*/
	public static ReportedQuestions fetchByquestionId_First(long questionId,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		return getPersistence()
				   .fetchByquestionId_First(questionId, orderByComparator);
	}

	/**
	* Returns the last reported questions in the ordered set where questionId = &#63;.
	*
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reported questions
	* @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	*/
	public static ReportedQuestions findByquestionId_Last(long questionId,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws com.devshaker.exception.NoSuchReportedQuestionsException {
		return getPersistence()
				   .findByquestionId_Last(questionId, orderByComparator);
	}

	/**
	* Returns the last reported questions in the ordered set where questionId = &#63;.
	*
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reported questions, or <code>null</code> if a matching reported questions could not be found
	*/
	public static ReportedQuestions fetchByquestionId_Last(long questionId,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		return getPersistence()
				   .fetchByquestionId_Last(questionId, orderByComparator);
	}

	/**
	* Returns the reported questionses before and after the current reported questions in the ordered set where questionId = &#63;.
	*
	* @param reportId the primary key of the current reported questions
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next reported questions
	* @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	*/
	public static ReportedQuestions[] findByquestionId_PrevAndNext(
		long reportId, long questionId,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws com.devshaker.exception.NoSuchReportedQuestionsException {
		return getPersistence()
				   .findByquestionId_PrevAndNext(reportId, questionId,
			orderByComparator);
	}

	/**
	* Removes all the reported questionses where questionId = &#63; from the database.
	*
	* @param questionId the question ID
	*/
	public static void removeByquestionId(long questionId) {
		getPersistence().removeByquestionId(questionId);
	}

	/**
	* Returns the number of reported questionses where questionId = &#63;.
	*
	* @param questionId the question ID
	* @return the number of matching reported questionses
	*/
	public static int countByquestionId(long questionId) {
		return getPersistence().countByquestionId(questionId);
	}

	/**
	* Returns all the reported questionses where reportStatus = &#63;.
	*
	* @param reportStatus the report status
	* @return the matching reported questionses
	*/
	public static List<ReportedQuestions> findByreportStatus(
		java.lang.String reportStatus) {
		return getPersistence().findByreportStatus(reportStatus);
	}

	/**
	* Returns a range of all the reported questionses where reportStatus = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reportStatus the report status
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @return the range of matching reported questionses
	*/
	public static List<ReportedQuestions> findByreportStatus(
		java.lang.String reportStatus, int start, int end) {
		return getPersistence().findByreportStatus(reportStatus, start, end);
	}

	/**
	* Returns an ordered range of all the reported questionses where reportStatus = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reportStatus the report status
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reported questionses
	*/
	public static List<ReportedQuestions> findByreportStatus(
		java.lang.String reportStatus, int start, int end,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		return getPersistence()
				   .findByreportStatus(reportStatus, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the reported questionses where reportStatus = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reportStatus the report status
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching reported questionses
	*/
	public static List<ReportedQuestions> findByreportStatus(
		java.lang.String reportStatus, int start, int end,
		OrderByComparator<ReportedQuestions> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByreportStatus(reportStatus, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first reported questions in the ordered set where reportStatus = &#63;.
	*
	* @param reportStatus the report status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reported questions
	* @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	*/
	public static ReportedQuestions findByreportStatus_First(
		java.lang.String reportStatus,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws com.devshaker.exception.NoSuchReportedQuestionsException {
		return getPersistence()
				   .findByreportStatus_First(reportStatus, orderByComparator);
	}

	/**
	* Returns the first reported questions in the ordered set where reportStatus = &#63;.
	*
	* @param reportStatus the report status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reported questions, or <code>null</code> if a matching reported questions could not be found
	*/
	public static ReportedQuestions fetchByreportStatus_First(
		java.lang.String reportStatus,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		return getPersistence()
				   .fetchByreportStatus_First(reportStatus, orderByComparator);
	}

	/**
	* Returns the last reported questions in the ordered set where reportStatus = &#63;.
	*
	* @param reportStatus the report status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reported questions
	* @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	*/
	public static ReportedQuestions findByreportStatus_Last(
		java.lang.String reportStatus,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws com.devshaker.exception.NoSuchReportedQuestionsException {
		return getPersistence()
				   .findByreportStatus_Last(reportStatus, orderByComparator);
	}

	/**
	* Returns the last reported questions in the ordered set where reportStatus = &#63;.
	*
	* @param reportStatus the report status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reported questions, or <code>null</code> if a matching reported questions could not be found
	*/
	public static ReportedQuestions fetchByreportStatus_Last(
		java.lang.String reportStatus,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		return getPersistence()
				   .fetchByreportStatus_Last(reportStatus, orderByComparator);
	}

	/**
	* Returns the reported questionses before and after the current reported questions in the ordered set where reportStatus = &#63;.
	*
	* @param reportId the primary key of the current reported questions
	* @param reportStatus the report status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next reported questions
	* @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	*/
	public static ReportedQuestions[] findByreportStatus_PrevAndNext(
		long reportId, java.lang.String reportStatus,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws com.devshaker.exception.NoSuchReportedQuestionsException {
		return getPersistence()
				   .findByreportStatus_PrevAndNext(reportId, reportStatus,
			orderByComparator);
	}

	/**
	* Removes all the reported questionses where reportStatus = &#63; from the database.
	*
	* @param reportStatus the report status
	*/
	public static void removeByreportStatus(java.lang.String reportStatus) {
		getPersistence().removeByreportStatus(reportStatus);
	}

	/**
	* Returns the number of reported questionses where reportStatus = &#63;.
	*
	* @param reportStatus the report status
	* @return the number of matching reported questionses
	*/
	public static int countByreportStatus(java.lang.String reportStatus) {
		return getPersistence().countByreportStatus(reportStatus);
	}

	/**
	* Returns all the reported questionses where userId = &#63; and questionId = &#63;.
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @return the matching reported questionses
	*/
	public static List<ReportedQuestions> findByUserAndQuestion(long userId,
		long questionId) {
		return getPersistence().findByUserAndQuestion(userId, questionId);
	}

	/**
	* Returns a range of all the reported questionses where userId = &#63; and questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @return the range of matching reported questionses
	*/
	public static List<ReportedQuestions> findByUserAndQuestion(long userId,
		long questionId, int start, int end) {
		return getPersistence()
				   .findByUserAndQuestion(userId, questionId, start, end);
	}

	/**
	* Returns an ordered range of all the reported questionses where userId = &#63; and questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reported questionses
	*/
	public static List<ReportedQuestions> findByUserAndQuestion(long userId,
		long questionId, int start, int end,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		return getPersistence()
				   .findByUserAndQuestion(userId, questionId, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the reported questionses where userId = &#63; and questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching reported questionses
	*/
	public static List<ReportedQuestions> findByUserAndQuestion(long userId,
		long questionId, int start, int end,
		OrderByComparator<ReportedQuestions> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUserAndQuestion(userId, questionId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first reported questions in the ordered set where userId = &#63; and questionId = &#63;.
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reported questions
	* @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	*/
	public static ReportedQuestions findByUserAndQuestion_First(long userId,
		long questionId, OrderByComparator<ReportedQuestions> orderByComparator)
		throws com.devshaker.exception.NoSuchReportedQuestionsException {
		return getPersistence()
				   .findByUserAndQuestion_First(userId, questionId,
			orderByComparator);
	}

	/**
	* Returns the first reported questions in the ordered set where userId = &#63; and questionId = &#63;.
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reported questions, or <code>null</code> if a matching reported questions could not be found
	*/
	public static ReportedQuestions fetchByUserAndQuestion_First(long userId,
		long questionId, OrderByComparator<ReportedQuestions> orderByComparator) {
		return getPersistence()
				   .fetchByUserAndQuestion_First(userId, questionId,
			orderByComparator);
	}

	/**
	* Returns the last reported questions in the ordered set where userId = &#63; and questionId = &#63;.
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reported questions
	* @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	*/
	public static ReportedQuestions findByUserAndQuestion_Last(long userId,
		long questionId, OrderByComparator<ReportedQuestions> orderByComparator)
		throws com.devshaker.exception.NoSuchReportedQuestionsException {
		return getPersistence()
				   .findByUserAndQuestion_Last(userId, questionId,
			orderByComparator);
	}

	/**
	* Returns the last reported questions in the ordered set where userId = &#63; and questionId = &#63;.
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reported questions, or <code>null</code> if a matching reported questions could not be found
	*/
	public static ReportedQuestions fetchByUserAndQuestion_Last(long userId,
		long questionId, OrderByComparator<ReportedQuestions> orderByComparator) {
		return getPersistence()
				   .fetchByUserAndQuestion_Last(userId, questionId,
			orderByComparator);
	}

	/**
	* Returns the reported questionses before and after the current reported questions in the ordered set where userId = &#63; and questionId = &#63;.
	*
	* @param reportId the primary key of the current reported questions
	* @param userId the user ID
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next reported questions
	* @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	*/
	public static ReportedQuestions[] findByUserAndQuestion_PrevAndNext(
		long reportId, long userId, long questionId,
		OrderByComparator<ReportedQuestions> orderByComparator)
		throws com.devshaker.exception.NoSuchReportedQuestionsException {
		return getPersistence()
				   .findByUserAndQuestion_PrevAndNext(reportId, userId,
			questionId, orderByComparator);
	}

	/**
	* Removes all the reported questionses where userId = &#63; and questionId = &#63; from the database.
	*
	* @param userId the user ID
	* @param questionId the question ID
	*/
	public static void removeByUserAndQuestion(long userId, long questionId) {
		getPersistence().removeByUserAndQuestion(userId, questionId);
	}

	/**
	* Returns the number of reported questionses where userId = &#63; and questionId = &#63;.
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @return the number of matching reported questionses
	*/
	public static int countByUserAndQuestion(long userId, long questionId) {
		return getPersistence().countByUserAndQuestion(userId, questionId);
	}

	/**
	* Caches the reported questions in the entity cache if it is enabled.
	*
	* @param reportedQuestions the reported questions
	*/
	public static void cacheResult(ReportedQuestions reportedQuestions) {
		getPersistence().cacheResult(reportedQuestions);
	}

	/**
	* Caches the reported questionses in the entity cache if it is enabled.
	*
	* @param reportedQuestionses the reported questionses
	*/
	public static void cacheResult(List<ReportedQuestions> reportedQuestionses) {
		getPersistence().cacheResult(reportedQuestionses);
	}

	/**
	* Creates a new reported questions with the primary key. Does not add the reported questions to the database.
	*
	* @param reportId the primary key for the new reported questions
	* @return the new reported questions
	*/
	public static ReportedQuestions create(long reportId) {
		return getPersistence().create(reportId);
	}

	/**
	* Removes the reported questions with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param reportId the primary key of the reported questions
	* @return the reported questions that was removed
	* @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	*/
	public static ReportedQuestions remove(long reportId)
		throws com.devshaker.exception.NoSuchReportedQuestionsException {
		return getPersistence().remove(reportId);
	}

	public static ReportedQuestions updateImpl(
		ReportedQuestions reportedQuestions) {
		return getPersistence().updateImpl(reportedQuestions);
	}

	/**
	* Returns the reported questions with the primary key or throws a {@link NoSuchReportedQuestionsException} if it could not be found.
	*
	* @param reportId the primary key of the reported questions
	* @return the reported questions
	* @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	*/
	public static ReportedQuestions findByPrimaryKey(long reportId)
		throws com.devshaker.exception.NoSuchReportedQuestionsException {
		return getPersistence().findByPrimaryKey(reportId);
	}

	/**
	* Returns the reported questions with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param reportId the primary key of the reported questions
	* @return the reported questions, or <code>null</code> if a reported questions with the primary key could not be found
	*/
	public static ReportedQuestions fetchByPrimaryKey(long reportId) {
		return getPersistence().fetchByPrimaryKey(reportId);
	}

	public static java.util.Map<java.io.Serializable, ReportedQuestions> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the reported questionses.
	*
	* @return the reported questionses
	*/
	public static List<ReportedQuestions> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the reported questionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @return the range of reported questionses
	*/
	public static List<ReportedQuestions> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the reported questionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of reported questionses
	*/
	public static List<ReportedQuestions> findAll(int start, int end,
		OrderByComparator<ReportedQuestions> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the reported questionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of reported questionses
	*/
	public static List<ReportedQuestions> findAll(int start, int end,
		OrderByComparator<ReportedQuestions> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the reported questionses from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of reported questionses.
	*
	* @return the number of reported questionses
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ReportedQuestionsPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ReportedQuestionsPersistence, ReportedQuestionsPersistence> _serviceTracker =
		ServiceTrackerFactory.open(ReportedQuestionsPersistence.class);
}