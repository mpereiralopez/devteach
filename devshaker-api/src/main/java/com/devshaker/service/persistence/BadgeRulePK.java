/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class BadgeRulePK implements Comparable<BadgeRulePK>, Serializable {
	public long ruleId;
	public long badgeId;

	public BadgeRulePK() {
	}

	public BadgeRulePK(long ruleId, long badgeId) {
		this.ruleId = ruleId;
		this.badgeId = badgeId;
	}

	public long getRuleId() {
		return ruleId;
	}

	public void setRuleId(long ruleId) {
		this.ruleId = ruleId;
	}

	public long getBadgeId() {
		return badgeId;
	}

	public void setBadgeId(long badgeId) {
		this.badgeId = badgeId;
	}

	@Override
	public int compareTo(BadgeRulePK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (ruleId < pk.ruleId) {
			value = -1;
		}
		else if (ruleId > pk.ruleId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (badgeId < pk.badgeId) {
			value = -1;
		}
		else if (badgeId > pk.badgeId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BadgeRulePK)) {
			return false;
		}

		BadgeRulePK pk = (BadgeRulePK)obj;

		if ((ruleId == pk.ruleId) && (badgeId == pk.badgeId)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		hashCode = HashUtil.hash(hashCode, ruleId);
		hashCode = HashUtil.hash(hashCode, badgeId);

		return hashCode;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("ruleId");
		sb.append(StringPool.EQUAL);
		sb.append(ruleId);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("badgeId");
		sb.append(StringPool.EQUAL);
		sb.append(badgeId);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}