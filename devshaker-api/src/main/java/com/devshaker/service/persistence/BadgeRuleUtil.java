/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.BadgeRule;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the badge rule service. This utility wraps {@link com.devshaker.service.persistence.impl.BadgeRulePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BadgeRulePersistence
 * @see com.devshaker.service.persistence.impl.BadgeRulePersistenceImpl
 * @generated
 */
@ProviderType
public class BadgeRuleUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(BadgeRule badgeRule) {
		getPersistence().clearCache(badgeRule);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<BadgeRule> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<BadgeRule> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<BadgeRule> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<BadgeRule> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static BadgeRule update(BadgeRule badgeRule) {
		return getPersistence().update(badgeRule);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static BadgeRule update(BadgeRule badgeRule,
		ServiceContext serviceContext) {
		return getPersistence().update(badgeRule, serviceContext);
	}

	/**
	* Returns all the badge rules where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @return the matching badge rules
	*/
	public static List<BadgeRule> findByBadgeId(long badgeId) {
		return getPersistence().findByBadgeId(badgeId);
	}

	/**
	* Returns a range of all the badge rules where badgeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param badgeId the badge ID
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @return the range of matching badge rules
	*/
	public static List<BadgeRule> findByBadgeId(long badgeId, int start, int end) {
		return getPersistence().findByBadgeId(badgeId, start, end);
	}

	/**
	* Returns an ordered range of all the badge rules where badgeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param badgeId the badge ID
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching badge rules
	*/
	public static List<BadgeRule> findByBadgeId(long badgeId, int start,
		int end, OrderByComparator<BadgeRule> orderByComparator) {
		return getPersistence()
				   .findByBadgeId(badgeId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the badge rules where badgeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param badgeId the badge ID
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching badge rules
	*/
	public static List<BadgeRule> findByBadgeId(long badgeId, int start,
		int end, OrderByComparator<BadgeRule> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByBadgeId(badgeId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first badge rule in the ordered set where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching badge rule
	* @throws NoSuchBadgeRuleException if a matching badge rule could not be found
	*/
	public static BadgeRule findByBadgeId_First(long badgeId,
		OrderByComparator<BadgeRule> orderByComparator)
		throws com.devshaker.exception.NoSuchBadgeRuleException {
		return getPersistence().findByBadgeId_First(badgeId, orderByComparator);
	}

	/**
	* Returns the first badge rule in the ordered set where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching badge rule, or <code>null</code> if a matching badge rule could not be found
	*/
	public static BadgeRule fetchByBadgeId_First(long badgeId,
		OrderByComparator<BadgeRule> orderByComparator) {
		return getPersistence().fetchByBadgeId_First(badgeId, orderByComparator);
	}

	/**
	* Returns the last badge rule in the ordered set where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching badge rule
	* @throws NoSuchBadgeRuleException if a matching badge rule could not be found
	*/
	public static BadgeRule findByBadgeId_Last(long badgeId,
		OrderByComparator<BadgeRule> orderByComparator)
		throws com.devshaker.exception.NoSuchBadgeRuleException {
		return getPersistence().findByBadgeId_Last(badgeId, orderByComparator);
	}

	/**
	* Returns the last badge rule in the ordered set where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching badge rule, or <code>null</code> if a matching badge rule could not be found
	*/
	public static BadgeRule fetchByBadgeId_Last(long badgeId,
		OrderByComparator<BadgeRule> orderByComparator) {
		return getPersistence().fetchByBadgeId_Last(badgeId, orderByComparator);
	}

	/**
	* Returns the badge rules before and after the current badge rule in the ordered set where badgeId = &#63;.
	*
	* @param badgeRulePK the primary key of the current badge rule
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next badge rule
	* @throws NoSuchBadgeRuleException if a badge rule with the primary key could not be found
	*/
	public static BadgeRule[] findByBadgeId_PrevAndNext(
		BadgeRulePK badgeRulePK, long badgeId,
		OrderByComparator<BadgeRule> orderByComparator)
		throws com.devshaker.exception.NoSuchBadgeRuleException {
		return getPersistence()
				   .findByBadgeId_PrevAndNext(badgeRulePK, badgeId,
			orderByComparator);
	}

	/**
	* Removes all the badge rules where badgeId = &#63; from the database.
	*
	* @param badgeId the badge ID
	*/
	public static void removeByBadgeId(long badgeId) {
		getPersistence().removeByBadgeId(badgeId);
	}

	/**
	* Returns the number of badge rules where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @return the number of matching badge rules
	*/
	public static int countByBadgeId(long badgeId) {
		return getPersistence().countByBadgeId(badgeId);
	}

	/**
	* Returns all the badge rules where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching badge rules
	*/
	public static List<BadgeRule> findByCompanyId(long companyId) {
		return getPersistence().findByCompanyId(companyId);
	}

	/**
	* Returns a range of all the badge rules where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @return the range of matching badge rules
	*/
	public static List<BadgeRule> findByCompanyId(long companyId, int start,
		int end) {
		return getPersistence().findByCompanyId(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the badge rules where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching badge rules
	*/
	public static List<BadgeRule> findByCompanyId(long companyId, int start,
		int end, OrderByComparator<BadgeRule> orderByComparator) {
		return getPersistence()
				   .findByCompanyId(companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the badge rules where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching badge rules
	*/
	public static List<BadgeRule> findByCompanyId(long companyId, int start,
		int end, OrderByComparator<BadgeRule> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByCompanyId(companyId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first badge rule in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching badge rule
	* @throws NoSuchBadgeRuleException if a matching badge rule could not be found
	*/
	public static BadgeRule findByCompanyId_First(long companyId,
		OrderByComparator<BadgeRule> orderByComparator)
		throws com.devshaker.exception.NoSuchBadgeRuleException {
		return getPersistence()
				   .findByCompanyId_First(companyId, orderByComparator);
	}

	/**
	* Returns the first badge rule in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching badge rule, or <code>null</code> if a matching badge rule could not be found
	*/
	public static BadgeRule fetchByCompanyId_First(long companyId,
		OrderByComparator<BadgeRule> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyId_First(companyId, orderByComparator);
	}

	/**
	* Returns the last badge rule in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching badge rule
	* @throws NoSuchBadgeRuleException if a matching badge rule could not be found
	*/
	public static BadgeRule findByCompanyId_Last(long companyId,
		OrderByComparator<BadgeRule> orderByComparator)
		throws com.devshaker.exception.NoSuchBadgeRuleException {
		return getPersistence()
				   .findByCompanyId_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last badge rule in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching badge rule, or <code>null</code> if a matching badge rule could not be found
	*/
	public static BadgeRule fetchByCompanyId_Last(long companyId,
		OrderByComparator<BadgeRule> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyId_Last(companyId, orderByComparator);
	}

	/**
	* Returns the badge rules before and after the current badge rule in the ordered set where companyId = &#63;.
	*
	* @param badgeRulePK the primary key of the current badge rule
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next badge rule
	* @throws NoSuchBadgeRuleException if a badge rule with the primary key could not be found
	*/
	public static BadgeRule[] findByCompanyId_PrevAndNext(
		BadgeRulePK badgeRulePK, long companyId,
		OrderByComparator<BadgeRule> orderByComparator)
		throws com.devshaker.exception.NoSuchBadgeRuleException {
		return getPersistence()
				   .findByCompanyId_PrevAndNext(badgeRulePK, companyId,
			orderByComparator);
	}

	/**
	* Removes all the badge rules where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	*/
	public static void removeByCompanyId(long companyId) {
		getPersistence().removeByCompanyId(companyId);
	}

	/**
	* Returns the number of badge rules where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching badge rules
	*/
	public static int countByCompanyId(long companyId) {
		return getPersistence().countByCompanyId(companyId);
	}

	/**
	* Caches the badge rule in the entity cache if it is enabled.
	*
	* @param badgeRule the badge rule
	*/
	public static void cacheResult(BadgeRule badgeRule) {
		getPersistence().cacheResult(badgeRule);
	}

	/**
	* Caches the badge rules in the entity cache if it is enabled.
	*
	* @param badgeRules the badge rules
	*/
	public static void cacheResult(List<BadgeRule> badgeRules) {
		getPersistence().cacheResult(badgeRules);
	}

	/**
	* Creates a new badge rule with the primary key. Does not add the badge rule to the database.
	*
	* @param badgeRulePK the primary key for the new badge rule
	* @return the new badge rule
	*/
	public static BadgeRule create(BadgeRulePK badgeRulePK) {
		return getPersistence().create(badgeRulePK);
	}

	/**
	* Removes the badge rule with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param badgeRulePK the primary key of the badge rule
	* @return the badge rule that was removed
	* @throws NoSuchBadgeRuleException if a badge rule with the primary key could not be found
	*/
	public static BadgeRule remove(BadgeRulePK badgeRulePK)
		throws com.devshaker.exception.NoSuchBadgeRuleException {
		return getPersistence().remove(badgeRulePK);
	}

	public static BadgeRule updateImpl(BadgeRule badgeRule) {
		return getPersistence().updateImpl(badgeRule);
	}

	/**
	* Returns the badge rule with the primary key or throws a {@link NoSuchBadgeRuleException} if it could not be found.
	*
	* @param badgeRulePK the primary key of the badge rule
	* @return the badge rule
	* @throws NoSuchBadgeRuleException if a badge rule with the primary key could not be found
	*/
	public static BadgeRule findByPrimaryKey(BadgeRulePK badgeRulePK)
		throws com.devshaker.exception.NoSuchBadgeRuleException {
		return getPersistence().findByPrimaryKey(badgeRulePK);
	}

	/**
	* Returns the badge rule with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param badgeRulePK the primary key of the badge rule
	* @return the badge rule, or <code>null</code> if a badge rule with the primary key could not be found
	*/
	public static BadgeRule fetchByPrimaryKey(BadgeRulePK badgeRulePK) {
		return getPersistence().fetchByPrimaryKey(badgeRulePK);
	}

	public static java.util.Map<java.io.Serializable, BadgeRule> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the badge rules.
	*
	* @return the badge rules
	*/
	public static List<BadgeRule> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the badge rules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @return the range of badge rules
	*/
	public static List<BadgeRule> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the badge rules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of badge rules
	*/
	public static List<BadgeRule> findAll(int start, int end,
		OrderByComparator<BadgeRule> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the badge rules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of badge rules
	*/
	public static List<BadgeRule> findAll(int start, int end,
		OrderByComparator<BadgeRule> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the badge rules from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of badge rules.
	*
	* @return the number of badge rules
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static BadgeRulePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<BadgeRulePersistence, BadgeRulePersistence> _serviceTracker =
		ServiceTrackerFactory.open(BadgeRulePersistence.class);
}