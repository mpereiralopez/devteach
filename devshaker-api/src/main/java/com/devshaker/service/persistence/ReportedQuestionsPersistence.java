/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.exception.NoSuchReportedQuestionsException;

import com.devshaker.model.ReportedQuestions;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the reported questions service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.devshaker.service.persistence.impl.ReportedQuestionsPersistenceImpl
 * @see ReportedQuestionsUtil
 * @generated
 */
@ProviderType
public interface ReportedQuestionsPersistence extends BasePersistence<ReportedQuestions> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ReportedQuestionsUtil} to access the reported questions persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the reported questionses where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching reported questionses
	*/
	public java.util.List<ReportedQuestions> findByUserId(long userId);

	/**
	* Returns a range of all the reported questionses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @return the range of matching reported questionses
	*/
	public java.util.List<ReportedQuestions> findByUserId(long userId,
		int start, int end);

	/**
	* Returns an ordered range of all the reported questionses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reported questionses
	*/
	public java.util.List<ReportedQuestions> findByUserId(long userId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator);

	/**
	* Returns an ordered range of all the reported questionses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching reported questionses
	*/
	public java.util.List<ReportedQuestions> findByUserId(long userId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first reported questions in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reported questions
	* @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	*/
	public ReportedQuestions findByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException;

	/**
	* Returns the first reported questions in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reported questions, or <code>null</code> if a matching reported questions could not be found
	*/
	public ReportedQuestions fetchByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator);

	/**
	* Returns the last reported questions in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reported questions
	* @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	*/
	public ReportedQuestions findByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException;

	/**
	* Returns the last reported questions in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reported questions, or <code>null</code> if a matching reported questions could not be found
	*/
	public ReportedQuestions fetchByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator);

	/**
	* Returns the reported questionses before and after the current reported questions in the ordered set where userId = &#63;.
	*
	* @param reportId the primary key of the current reported questions
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next reported questions
	* @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	*/
	public ReportedQuestions[] findByUserId_PrevAndNext(long reportId,
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException;

	/**
	* Removes all the reported questionses where userId = &#63; from the database.
	*
	* @param userId the user ID
	*/
	public void removeByUserId(long userId);

	/**
	* Returns the number of reported questionses where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching reported questionses
	*/
	public int countByUserId(long userId);

	/**
	* Returns all the reported questionses where questionId = &#63;.
	*
	* @param questionId the question ID
	* @return the matching reported questionses
	*/
	public java.util.List<ReportedQuestions> findByquestionId(long questionId);

	/**
	* Returns a range of all the reported questionses where questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @return the range of matching reported questionses
	*/
	public java.util.List<ReportedQuestions> findByquestionId(long questionId,
		int start, int end);

	/**
	* Returns an ordered range of all the reported questionses where questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reported questionses
	*/
	public java.util.List<ReportedQuestions> findByquestionId(long questionId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator);

	/**
	* Returns an ordered range of all the reported questionses where questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching reported questionses
	*/
	public java.util.List<ReportedQuestions> findByquestionId(long questionId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first reported questions in the ordered set where questionId = &#63;.
	*
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reported questions
	* @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	*/
	public ReportedQuestions findByquestionId_First(long questionId,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException;

	/**
	* Returns the first reported questions in the ordered set where questionId = &#63;.
	*
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reported questions, or <code>null</code> if a matching reported questions could not be found
	*/
	public ReportedQuestions fetchByquestionId_First(long questionId,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator);

	/**
	* Returns the last reported questions in the ordered set where questionId = &#63;.
	*
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reported questions
	* @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	*/
	public ReportedQuestions findByquestionId_Last(long questionId,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException;

	/**
	* Returns the last reported questions in the ordered set where questionId = &#63;.
	*
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reported questions, or <code>null</code> if a matching reported questions could not be found
	*/
	public ReportedQuestions fetchByquestionId_Last(long questionId,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator);

	/**
	* Returns the reported questionses before and after the current reported questions in the ordered set where questionId = &#63;.
	*
	* @param reportId the primary key of the current reported questions
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next reported questions
	* @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	*/
	public ReportedQuestions[] findByquestionId_PrevAndNext(long reportId,
		long questionId,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException;

	/**
	* Removes all the reported questionses where questionId = &#63; from the database.
	*
	* @param questionId the question ID
	*/
	public void removeByquestionId(long questionId);

	/**
	* Returns the number of reported questionses where questionId = &#63;.
	*
	* @param questionId the question ID
	* @return the number of matching reported questionses
	*/
	public int countByquestionId(long questionId);

	/**
	* Returns all the reported questionses where reportStatus = &#63;.
	*
	* @param reportStatus the report status
	* @return the matching reported questionses
	*/
	public java.util.List<ReportedQuestions> findByreportStatus(
		java.lang.String reportStatus);

	/**
	* Returns a range of all the reported questionses where reportStatus = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reportStatus the report status
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @return the range of matching reported questionses
	*/
	public java.util.List<ReportedQuestions> findByreportStatus(
		java.lang.String reportStatus, int start, int end);

	/**
	* Returns an ordered range of all the reported questionses where reportStatus = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reportStatus the report status
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reported questionses
	*/
	public java.util.List<ReportedQuestions> findByreportStatus(
		java.lang.String reportStatus, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator);

	/**
	* Returns an ordered range of all the reported questionses where reportStatus = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reportStatus the report status
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching reported questionses
	*/
	public java.util.List<ReportedQuestions> findByreportStatus(
		java.lang.String reportStatus, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first reported questions in the ordered set where reportStatus = &#63;.
	*
	* @param reportStatus the report status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reported questions
	* @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	*/
	public ReportedQuestions findByreportStatus_First(
		java.lang.String reportStatus,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException;

	/**
	* Returns the first reported questions in the ordered set where reportStatus = &#63;.
	*
	* @param reportStatus the report status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reported questions, or <code>null</code> if a matching reported questions could not be found
	*/
	public ReportedQuestions fetchByreportStatus_First(
		java.lang.String reportStatus,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator);

	/**
	* Returns the last reported questions in the ordered set where reportStatus = &#63;.
	*
	* @param reportStatus the report status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reported questions
	* @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	*/
	public ReportedQuestions findByreportStatus_Last(
		java.lang.String reportStatus,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException;

	/**
	* Returns the last reported questions in the ordered set where reportStatus = &#63;.
	*
	* @param reportStatus the report status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reported questions, or <code>null</code> if a matching reported questions could not be found
	*/
	public ReportedQuestions fetchByreportStatus_Last(
		java.lang.String reportStatus,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator);

	/**
	* Returns the reported questionses before and after the current reported questions in the ordered set where reportStatus = &#63;.
	*
	* @param reportId the primary key of the current reported questions
	* @param reportStatus the report status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next reported questions
	* @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	*/
	public ReportedQuestions[] findByreportStatus_PrevAndNext(long reportId,
		java.lang.String reportStatus,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException;

	/**
	* Removes all the reported questionses where reportStatus = &#63; from the database.
	*
	* @param reportStatus the report status
	*/
	public void removeByreportStatus(java.lang.String reportStatus);

	/**
	* Returns the number of reported questionses where reportStatus = &#63;.
	*
	* @param reportStatus the report status
	* @return the number of matching reported questionses
	*/
	public int countByreportStatus(java.lang.String reportStatus);

	/**
	* Returns all the reported questionses where userId = &#63; and questionId = &#63;.
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @return the matching reported questionses
	*/
	public java.util.List<ReportedQuestions> findByUserAndQuestion(
		long userId, long questionId);

	/**
	* Returns a range of all the reported questionses where userId = &#63; and questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @return the range of matching reported questionses
	*/
	public java.util.List<ReportedQuestions> findByUserAndQuestion(
		long userId, long questionId, int start, int end);

	/**
	* Returns an ordered range of all the reported questionses where userId = &#63; and questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reported questionses
	*/
	public java.util.List<ReportedQuestions> findByUserAndQuestion(
		long userId, long questionId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator);

	/**
	* Returns an ordered range of all the reported questionses where userId = &#63; and questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching reported questionses
	*/
	public java.util.List<ReportedQuestions> findByUserAndQuestion(
		long userId, long questionId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first reported questions in the ordered set where userId = &#63; and questionId = &#63;.
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reported questions
	* @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	*/
	public ReportedQuestions findByUserAndQuestion_First(long userId,
		long questionId,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException;

	/**
	* Returns the first reported questions in the ordered set where userId = &#63; and questionId = &#63;.
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reported questions, or <code>null</code> if a matching reported questions could not be found
	*/
	public ReportedQuestions fetchByUserAndQuestion_First(long userId,
		long questionId,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator);

	/**
	* Returns the last reported questions in the ordered set where userId = &#63; and questionId = &#63;.
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reported questions
	* @throws NoSuchReportedQuestionsException if a matching reported questions could not be found
	*/
	public ReportedQuestions findByUserAndQuestion_Last(long userId,
		long questionId,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException;

	/**
	* Returns the last reported questions in the ordered set where userId = &#63; and questionId = &#63;.
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reported questions, or <code>null</code> if a matching reported questions could not be found
	*/
	public ReportedQuestions fetchByUserAndQuestion_Last(long userId,
		long questionId,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator);

	/**
	* Returns the reported questionses before and after the current reported questions in the ordered set where userId = &#63; and questionId = &#63;.
	*
	* @param reportId the primary key of the current reported questions
	* @param userId the user ID
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next reported questions
	* @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	*/
	public ReportedQuestions[] findByUserAndQuestion_PrevAndNext(
		long reportId, long userId, long questionId,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator)
		throws NoSuchReportedQuestionsException;

	/**
	* Removes all the reported questionses where userId = &#63; and questionId = &#63; from the database.
	*
	* @param userId the user ID
	* @param questionId the question ID
	*/
	public void removeByUserAndQuestion(long userId, long questionId);

	/**
	* Returns the number of reported questionses where userId = &#63; and questionId = &#63;.
	*
	* @param userId the user ID
	* @param questionId the question ID
	* @return the number of matching reported questionses
	*/
	public int countByUserAndQuestion(long userId, long questionId);

	/**
	* Caches the reported questions in the entity cache if it is enabled.
	*
	* @param reportedQuestions the reported questions
	*/
	public void cacheResult(ReportedQuestions reportedQuestions);

	/**
	* Caches the reported questionses in the entity cache if it is enabled.
	*
	* @param reportedQuestionses the reported questionses
	*/
	public void cacheResult(
		java.util.List<ReportedQuestions> reportedQuestionses);

	/**
	* Creates a new reported questions with the primary key. Does not add the reported questions to the database.
	*
	* @param reportId the primary key for the new reported questions
	* @return the new reported questions
	*/
	public ReportedQuestions create(long reportId);

	/**
	* Removes the reported questions with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param reportId the primary key of the reported questions
	* @return the reported questions that was removed
	* @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	*/
	public ReportedQuestions remove(long reportId)
		throws NoSuchReportedQuestionsException;

	public ReportedQuestions updateImpl(ReportedQuestions reportedQuestions);

	/**
	* Returns the reported questions with the primary key or throws a {@link NoSuchReportedQuestionsException} if it could not be found.
	*
	* @param reportId the primary key of the reported questions
	* @return the reported questions
	* @throws NoSuchReportedQuestionsException if a reported questions with the primary key could not be found
	*/
	public ReportedQuestions findByPrimaryKey(long reportId)
		throws NoSuchReportedQuestionsException;

	/**
	* Returns the reported questions with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param reportId the primary key of the reported questions
	* @return the reported questions, or <code>null</code> if a reported questions with the primary key could not be found
	*/
	public ReportedQuestions fetchByPrimaryKey(long reportId);

	@Override
	public java.util.Map<java.io.Serializable, ReportedQuestions> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the reported questionses.
	*
	* @return the reported questionses
	*/
	public java.util.List<ReportedQuestions> findAll();

	/**
	* Returns a range of all the reported questionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @return the range of reported questionses
	*/
	public java.util.List<ReportedQuestions> findAll(int start, int end);

	/**
	* Returns an ordered range of all the reported questionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of reported questionses
	*/
	public java.util.List<ReportedQuestions> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator);

	/**
	* Returns an ordered range of all the reported questionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of reported questionses
	*/
	public java.util.List<ReportedQuestions> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ReportedQuestions> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the reported questionses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of reported questionses.
	*
	* @return the number of reported questionses
	*/
	public int countAll();
}