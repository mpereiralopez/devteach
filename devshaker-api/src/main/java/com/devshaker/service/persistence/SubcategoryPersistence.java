/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.exception.NoSuchSubcategoryException;

import com.devshaker.model.Subcategory;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the subcategory service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.devshaker.service.persistence.impl.SubcategoryPersistenceImpl
 * @see SubcategoryUtil
 * @generated
 */
@ProviderType
public interface SubcategoryPersistence extends BasePersistence<Subcategory> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SubcategoryUtil} to access the subcategory persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the subcategories where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching subcategories
	*/
	public java.util.List<Subcategory> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the subcategories where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @return the range of matching subcategories
	*/
	public java.util.List<Subcategory> findByUuid(java.lang.String uuid,
		int start, int end);

	/**
	* Returns an ordered range of all the subcategories where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching subcategories
	*/
	public java.util.List<Subcategory> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator);

	/**
	* Returns an ordered range of all the subcategories where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching subcategories
	*/
	public java.util.List<Subcategory> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first subcategory in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subcategory
	* @throws NoSuchSubcategoryException if a matching subcategory could not be found
	*/
	public Subcategory findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator)
		throws NoSuchSubcategoryException;

	/**
	* Returns the first subcategory in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subcategory, or <code>null</code> if a matching subcategory could not be found
	*/
	public Subcategory fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator);

	/**
	* Returns the last subcategory in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subcategory
	* @throws NoSuchSubcategoryException if a matching subcategory could not be found
	*/
	public Subcategory findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator)
		throws NoSuchSubcategoryException;

	/**
	* Returns the last subcategory in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subcategory, or <code>null</code> if a matching subcategory could not be found
	*/
	public Subcategory fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator);

	/**
	* Returns the subcategories before and after the current subcategory in the ordered set where uuid = &#63;.
	*
	* @param subcategoryId the primary key of the current subcategory
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next subcategory
	* @throws NoSuchSubcategoryException if a subcategory with the primary key could not be found
	*/
	public Subcategory[] findByUuid_PrevAndNext(long subcategoryId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator)
		throws NoSuchSubcategoryException;

	/**
	* Removes all the subcategories where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of subcategories where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching subcategories
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns the subcategory where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchSubcategoryException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching subcategory
	* @throws NoSuchSubcategoryException if a matching subcategory could not be found
	*/
	public Subcategory findByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchSubcategoryException;

	/**
	* Returns the subcategory where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching subcategory, or <code>null</code> if a matching subcategory could not be found
	*/
	public Subcategory fetchByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns the subcategory where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching subcategory, or <code>null</code> if a matching subcategory could not be found
	*/
	public Subcategory fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the subcategory where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the subcategory that was removed
	*/
	public Subcategory removeByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchSubcategoryException;

	/**
	* Returns the number of subcategories where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching subcategories
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns all the subcategories where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching subcategories
	*/
	public java.util.List<Subcategory> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the subcategories where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @return the range of matching subcategories
	*/
	public java.util.List<Subcategory> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the subcategories where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching subcategories
	*/
	public java.util.List<Subcategory> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator);

	/**
	* Returns an ordered range of all the subcategories where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching subcategories
	*/
	public java.util.List<Subcategory> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first subcategory in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subcategory
	* @throws NoSuchSubcategoryException if a matching subcategory could not be found
	*/
	public Subcategory findByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator)
		throws NoSuchSubcategoryException;

	/**
	* Returns the first subcategory in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subcategory, or <code>null</code> if a matching subcategory could not be found
	*/
	public Subcategory fetchByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator);

	/**
	* Returns the last subcategory in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subcategory
	* @throws NoSuchSubcategoryException if a matching subcategory could not be found
	*/
	public Subcategory findByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator)
		throws NoSuchSubcategoryException;

	/**
	* Returns the last subcategory in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subcategory, or <code>null</code> if a matching subcategory could not be found
	*/
	public Subcategory fetchByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator);

	/**
	* Returns the subcategories before and after the current subcategory in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param subcategoryId the primary key of the current subcategory
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next subcategory
	* @throws NoSuchSubcategoryException if a subcategory with the primary key could not be found
	*/
	public Subcategory[] findByUuid_C_PrevAndNext(long subcategoryId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator)
		throws NoSuchSubcategoryException;

	/**
	* Removes all the subcategories where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of subcategories where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching subcategories
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns all the subcategories where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching subcategories
	*/
	public java.util.List<Subcategory> findByCompanyId(long companyId);

	/**
	* Returns a range of all the subcategories where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @return the range of matching subcategories
	*/
	public java.util.List<Subcategory> findByCompanyId(long companyId,
		int start, int end);

	/**
	* Returns an ordered range of all the subcategories where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching subcategories
	*/
	public java.util.List<Subcategory> findByCompanyId(long companyId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator);

	/**
	* Returns an ordered range of all the subcategories where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching subcategories
	*/
	public java.util.List<Subcategory> findByCompanyId(long companyId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first subcategory in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subcategory
	* @throws NoSuchSubcategoryException if a matching subcategory could not be found
	*/
	public Subcategory findByCompanyId_First(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator)
		throws NoSuchSubcategoryException;

	/**
	* Returns the first subcategory in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subcategory, or <code>null</code> if a matching subcategory could not be found
	*/
	public Subcategory fetchByCompanyId_First(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator);

	/**
	* Returns the last subcategory in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subcategory
	* @throws NoSuchSubcategoryException if a matching subcategory could not be found
	*/
	public Subcategory findByCompanyId_Last(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator)
		throws NoSuchSubcategoryException;

	/**
	* Returns the last subcategory in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subcategory, or <code>null</code> if a matching subcategory could not be found
	*/
	public Subcategory fetchByCompanyId_Last(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator);

	/**
	* Returns the subcategories before and after the current subcategory in the ordered set where companyId = &#63;.
	*
	* @param subcategoryId the primary key of the current subcategory
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next subcategory
	* @throws NoSuchSubcategoryException if a subcategory with the primary key could not be found
	*/
	public Subcategory[] findByCompanyId_PrevAndNext(long subcategoryId,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator)
		throws NoSuchSubcategoryException;

	/**
	* Removes all the subcategories where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	*/
	public void removeByCompanyId(long companyId);

	/**
	* Returns the number of subcategories where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching subcategories
	*/
	public int countByCompanyId(long companyId);

	/**
	* Returns all the subcategories where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching subcategories
	*/
	public java.util.List<Subcategory> findByUserId(long userId);

	/**
	* Returns a range of all the subcategories where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @return the range of matching subcategories
	*/
	public java.util.List<Subcategory> findByUserId(long userId, int start,
		int end);

	/**
	* Returns an ordered range of all the subcategories where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching subcategories
	*/
	public java.util.List<Subcategory> findByUserId(long userId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator);

	/**
	* Returns an ordered range of all the subcategories where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching subcategories
	*/
	public java.util.List<Subcategory> findByUserId(long userId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first subcategory in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subcategory
	* @throws NoSuchSubcategoryException if a matching subcategory could not be found
	*/
	public Subcategory findByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator)
		throws NoSuchSubcategoryException;

	/**
	* Returns the first subcategory in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subcategory, or <code>null</code> if a matching subcategory could not be found
	*/
	public Subcategory fetchByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator);

	/**
	* Returns the last subcategory in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subcategory
	* @throws NoSuchSubcategoryException if a matching subcategory could not be found
	*/
	public Subcategory findByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator)
		throws NoSuchSubcategoryException;

	/**
	* Returns the last subcategory in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subcategory, or <code>null</code> if a matching subcategory could not be found
	*/
	public Subcategory fetchByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator);

	/**
	* Returns the subcategories before and after the current subcategory in the ordered set where userId = &#63;.
	*
	* @param subcategoryId the primary key of the current subcategory
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next subcategory
	* @throws NoSuchSubcategoryException if a subcategory with the primary key could not be found
	*/
	public Subcategory[] findByUserId_PrevAndNext(long subcategoryId,
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator)
		throws NoSuchSubcategoryException;

	/**
	* Removes all the subcategories where userId = &#63; from the database.
	*
	* @param userId the user ID
	*/
	public void removeByUserId(long userId);

	/**
	* Returns the number of subcategories where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching subcategories
	*/
	public int countByUserId(long userId);

	/**
	* Returns all the subcategories where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @return the matching subcategories
	*/
	public java.util.List<Subcategory> findByCategoryId(long categoryId);

	/**
	* Returns a range of all the subcategories where categoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoryId the category ID
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @return the range of matching subcategories
	*/
	public java.util.List<Subcategory> findByCategoryId(long categoryId,
		int start, int end);

	/**
	* Returns an ordered range of all the subcategories where categoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoryId the category ID
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching subcategories
	*/
	public java.util.List<Subcategory> findByCategoryId(long categoryId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator);

	/**
	* Returns an ordered range of all the subcategories where categoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoryId the category ID
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching subcategories
	*/
	public java.util.List<Subcategory> findByCategoryId(long categoryId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first subcategory in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subcategory
	* @throws NoSuchSubcategoryException if a matching subcategory could not be found
	*/
	public Subcategory findByCategoryId_First(long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator)
		throws NoSuchSubcategoryException;

	/**
	* Returns the first subcategory in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subcategory, or <code>null</code> if a matching subcategory could not be found
	*/
	public Subcategory fetchByCategoryId_First(long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator);

	/**
	* Returns the last subcategory in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subcategory
	* @throws NoSuchSubcategoryException if a matching subcategory could not be found
	*/
	public Subcategory findByCategoryId_Last(long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator)
		throws NoSuchSubcategoryException;

	/**
	* Returns the last subcategory in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subcategory, or <code>null</code> if a matching subcategory could not be found
	*/
	public Subcategory fetchByCategoryId_Last(long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator);

	/**
	* Returns the subcategories before and after the current subcategory in the ordered set where categoryId = &#63;.
	*
	* @param subcategoryId the primary key of the current subcategory
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next subcategory
	* @throws NoSuchSubcategoryException if a subcategory with the primary key could not be found
	*/
	public Subcategory[] findByCategoryId_PrevAndNext(long subcategoryId,
		long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator)
		throws NoSuchSubcategoryException;

	/**
	* Removes all the subcategories where categoryId = &#63; from the database.
	*
	* @param categoryId the category ID
	*/
	public void removeByCategoryId(long categoryId);

	/**
	* Returns the number of subcategories where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @return the number of matching subcategories
	*/
	public int countByCategoryId(long categoryId);

	/**
	* Caches the subcategory in the entity cache if it is enabled.
	*
	* @param subcategory the subcategory
	*/
	public void cacheResult(Subcategory subcategory);

	/**
	* Caches the subcategories in the entity cache if it is enabled.
	*
	* @param subcategories the subcategories
	*/
	public void cacheResult(java.util.List<Subcategory> subcategories);

	/**
	* Creates a new subcategory with the primary key. Does not add the subcategory to the database.
	*
	* @param subcategoryId the primary key for the new subcategory
	* @return the new subcategory
	*/
	public Subcategory create(long subcategoryId);

	/**
	* Removes the subcategory with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param subcategoryId the primary key of the subcategory
	* @return the subcategory that was removed
	* @throws NoSuchSubcategoryException if a subcategory with the primary key could not be found
	*/
	public Subcategory remove(long subcategoryId)
		throws NoSuchSubcategoryException;

	public Subcategory updateImpl(Subcategory subcategory);

	/**
	* Returns the subcategory with the primary key or throws a {@link NoSuchSubcategoryException} if it could not be found.
	*
	* @param subcategoryId the primary key of the subcategory
	* @return the subcategory
	* @throws NoSuchSubcategoryException if a subcategory with the primary key could not be found
	*/
	public Subcategory findByPrimaryKey(long subcategoryId)
		throws NoSuchSubcategoryException;

	/**
	* Returns the subcategory with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param subcategoryId the primary key of the subcategory
	* @return the subcategory, or <code>null</code> if a subcategory with the primary key could not be found
	*/
	public Subcategory fetchByPrimaryKey(long subcategoryId);

	@Override
	public java.util.Map<java.io.Serializable, Subcategory> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the subcategories.
	*
	* @return the subcategories
	*/
	public java.util.List<Subcategory> findAll();

	/**
	* Returns a range of all the subcategories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @return the range of subcategories
	*/
	public java.util.List<Subcategory> findAll(int start, int end);

	/**
	* Returns an ordered range of all the subcategories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of subcategories
	*/
	public java.util.List<Subcategory> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator);

	/**
	* Returns an ordered range of all the subcategories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of subcategories
	*/
	public java.util.List<Subcategory> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Subcategory> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the subcategories from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of subcategories.
	*
	* @return the number of subcategories
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}