/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.UserTestQuestionResult;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the user test question result service. This utility wraps {@link com.devshaker.service.persistence.impl.UserTestQuestionResultPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserTestQuestionResultPersistence
 * @see com.devshaker.service.persistence.impl.UserTestQuestionResultPersistenceImpl
 * @generated
 */
@ProviderType
public class UserTestQuestionResultUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(UserTestQuestionResult userTestQuestionResult) {
		getPersistence().clearCache(userTestQuestionResult);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<UserTestQuestionResult> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<UserTestQuestionResult> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<UserTestQuestionResult> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static UserTestQuestionResult update(
		UserTestQuestionResult userTestQuestionResult) {
		return getPersistence().update(userTestQuestionResult);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static UserTestQuestionResult update(
		UserTestQuestionResult userTestQuestionResult,
		ServiceContext serviceContext) {
		return getPersistence().update(userTestQuestionResult, serviceContext);
	}

	/**
	* Returns all the user test question results where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching user test question results
	*/
	public static List<UserTestQuestionResult> findByUserId(long userId) {
		return getPersistence().findByUserId(userId);
	}

	/**
	* Returns a range of all the user test question results where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @return the range of matching user test question results
	*/
	public static List<UserTestQuestionResult> findByUserId(long userId,
		int start, int end) {
		return getPersistence().findByUserId(userId, start, end);
	}

	/**
	* Returns an ordered range of all the user test question results where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user test question results
	*/
	public static List<UserTestQuestionResult> findByUserId(long userId,
		int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence()
				   .findByUserId(userId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the user test question results where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user test question results
	*/
	public static List<UserTestQuestionResult> findByUserId(long userId,
		int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUserId(userId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first user test question result in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult findByUserId_First(long userId,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence().findByUserId_First(userId, orderByComparator);
	}

	/**
	* Returns the first user test question result in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult fetchByUserId_First(long userId,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence().fetchByUserId_First(userId, orderByComparator);
	}

	/**
	* Returns the last user test question result in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult findByUserId_Last(long userId,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence().findByUserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the last user test question result in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult fetchByUserId_Last(long userId,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence().fetchByUserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the user test question results before and after the current user test question result in the ordered set where userId = &#63;.
	*
	* @param userTestQuestionResultId the primary key of the current user test question result
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user test question result
	* @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	*/
	public static UserTestQuestionResult[] findByUserId_PrevAndNext(
		long userTestQuestionResultId, long userId,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence()
				   .findByUserId_PrevAndNext(userTestQuestionResultId, userId,
			orderByComparator);
	}

	/**
	* Removes all the user test question results where userId = &#63; from the database.
	*
	* @param userId the user ID
	*/
	public static void removeByUserId(long userId) {
		getPersistence().removeByUserId(userId);
	}

	/**
	* Returns the number of user test question results where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching user test question results
	*/
	public static int countByUserId(long userId) {
		return getPersistence().countByUserId(userId);
	}

	/**
	* Returns all the user test question results where userId = &#63; and isCorrect = &#63;.
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @return the matching user test question results
	*/
	public static List<UserTestQuestionResult> findByUserIdAndStatus(
		long userId, boolean isCorrect) {
		return getPersistence().findByUserIdAndStatus(userId, isCorrect);
	}

	/**
	* Returns a range of all the user test question results where userId = &#63; and isCorrect = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @return the range of matching user test question results
	*/
	public static List<UserTestQuestionResult> findByUserIdAndStatus(
		long userId, boolean isCorrect, int start, int end) {
		return getPersistence()
				   .findByUserIdAndStatus(userId, isCorrect, start, end);
	}

	/**
	* Returns an ordered range of all the user test question results where userId = &#63; and isCorrect = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user test question results
	*/
	public static List<UserTestQuestionResult> findByUserIdAndStatus(
		long userId, boolean isCorrect, int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence()
				   .findByUserIdAndStatus(userId, isCorrect, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the user test question results where userId = &#63; and isCorrect = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user test question results
	*/
	public static List<UserTestQuestionResult> findByUserIdAndStatus(
		long userId, boolean isCorrect, int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUserIdAndStatus(userId, isCorrect, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first user test question result in the ordered set where userId = &#63; and isCorrect = &#63;.
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult findByUserIdAndStatus_First(
		long userId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence()
				   .findByUserIdAndStatus_First(userId, isCorrect,
			orderByComparator);
	}

	/**
	* Returns the first user test question result in the ordered set where userId = &#63; and isCorrect = &#63;.
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult fetchByUserIdAndStatus_First(
		long userId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence()
				   .fetchByUserIdAndStatus_First(userId, isCorrect,
			orderByComparator);
	}

	/**
	* Returns the last user test question result in the ordered set where userId = &#63; and isCorrect = &#63;.
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult findByUserIdAndStatus_Last(
		long userId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence()
				   .findByUserIdAndStatus_Last(userId, isCorrect,
			orderByComparator);
	}

	/**
	* Returns the last user test question result in the ordered set where userId = &#63; and isCorrect = &#63;.
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult fetchByUserIdAndStatus_Last(
		long userId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence()
				   .fetchByUserIdAndStatus_Last(userId, isCorrect,
			orderByComparator);
	}

	/**
	* Returns the user test question results before and after the current user test question result in the ordered set where userId = &#63; and isCorrect = &#63;.
	*
	* @param userTestQuestionResultId the primary key of the current user test question result
	* @param userId the user ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user test question result
	* @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	*/
	public static UserTestQuestionResult[] findByUserIdAndStatus_PrevAndNext(
		long userTestQuestionResultId, long userId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence()
				   .findByUserIdAndStatus_PrevAndNext(userTestQuestionResultId,
			userId, isCorrect, orderByComparator);
	}

	/**
	* Removes all the user test question results where userId = &#63; and isCorrect = &#63; from the database.
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	*/
	public static void removeByUserIdAndStatus(long userId, boolean isCorrect) {
		getPersistence().removeByUserIdAndStatus(userId, isCorrect);
	}

	/**
	* Returns the number of user test question results where userId = &#63; and isCorrect = &#63;.
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @return the number of matching user test question results
	*/
	public static int countByUserIdAndStatus(long userId, boolean isCorrect) {
		return getPersistence().countByUserIdAndStatus(userId, isCorrect);
	}

	/**
	* Returns all the user test question results where questionId = &#63;.
	*
	* @param questionId the question ID
	* @return the matching user test question results
	*/
	public static List<UserTestQuestionResult> findByQuestionId(long questionId) {
		return getPersistence().findByQuestionId(questionId);
	}

	/**
	* Returns a range of all the user test question results where questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @return the range of matching user test question results
	*/
	public static List<UserTestQuestionResult> findByQuestionId(
		long questionId, int start, int end) {
		return getPersistence().findByQuestionId(questionId, start, end);
	}

	/**
	* Returns an ordered range of all the user test question results where questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user test question results
	*/
	public static List<UserTestQuestionResult> findByQuestionId(
		long questionId, int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence()
				   .findByQuestionId(questionId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the user test question results where questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user test question results
	*/
	public static List<UserTestQuestionResult> findByQuestionId(
		long questionId, int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByQuestionId(questionId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first user test question result in the ordered set where questionId = &#63;.
	*
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult findByQuestionId_First(
		long questionId,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence()
				   .findByQuestionId_First(questionId, orderByComparator);
	}

	/**
	* Returns the first user test question result in the ordered set where questionId = &#63;.
	*
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult fetchByQuestionId_First(
		long questionId,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence()
				   .fetchByQuestionId_First(questionId, orderByComparator);
	}

	/**
	* Returns the last user test question result in the ordered set where questionId = &#63;.
	*
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult findByQuestionId_Last(
		long questionId,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence()
				   .findByQuestionId_Last(questionId, orderByComparator);
	}

	/**
	* Returns the last user test question result in the ordered set where questionId = &#63;.
	*
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult fetchByQuestionId_Last(
		long questionId,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence()
				   .fetchByQuestionId_Last(questionId, orderByComparator);
	}

	/**
	* Returns the user test question results before and after the current user test question result in the ordered set where questionId = &#63;.
	*
	* @param userTestQuestionResultId the primary key of the current user test question result
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user test question result
	* @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	*/
	public static UserTestQuestionResult[] findByQuestionId_PrevAndNext(
		long userTestQuestionResultId, long questionId,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence()
				   .findByQuestionId_PrevAndNext(userTestQuestionResultId,
			questionId, orderByComparator);
	}

	/**
	* Removes all the user test question results where questionId = &#63; from the database.
	*
	* @param questionId the question ID
	*/
	public static void removeByQuestionId(long questionId) {
		getPersistence().removeByQuestionId(questionId);
	}

	/**
	* Returns the number of user test question results where questionId = &#63;.
	*
	* @param questionId the question ID
	* @return the number of matching user test question results
	*/
	public static int countByQuestionId(long questionId) {
		return getPersistence().countByQuestionId(questionId);
	}

	/**
	* Returns all the user test question results where questionId = &#63; and isCorrect = &#63;.
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @return the matching user test question results
	*/
	public static List<UserTestQuestionResult> findByQuestionIdAndStatus(
		long questionId, boolean isCorrect) {
		return getPersistence().findByQuestionIdAndStatus(questionId, isCorrect);
	}

	/**
	* Returns a range of all the user test question results where questionId = &#63; and isCorrect = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @return the range of matching user test question results
	*/
	public static List<UserTestQuestionResult> findByQuestionIdAndStatus(
		long questionId, boolean isCorrect, int start, int end) {
		return getPersistence()
				   .findByQuestionIdAndStatus(questionId, isCorrect, start, end);
	}

	/**
	* Returns an ordered range of all the user test question results where questionId = &#63; and isCorrect = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user test question results
	*/
	public static List<UserTestQuestionResult> findByQuestionIdAndStatus(
		long questionId, boolean isCorrect, int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence()
				   .findByQuestionIdAndStatus(questionId, isCorrect, start,
			end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the user test question results where questionId = &#63; and isCorrect = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user test question results
	*/
	public static List<UserTestQuestionResult> findByQuestionIdAndStatus(
		long questionId, boolean isCorrect, int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByQuestionIdAndStatus(questionId, isCorrect, start,
			end, orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first user test question result in the ordered set where questionId = &#63; and isCorrect = &#63;.
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult findByQuestionIdAndStatus_First(
		long questionId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence()
				   .findByQuestionIdAndStatus_First(questionId, isCorrect,
			orderByComparator);
	}

	/**
	* Returns the first user test question result in the ordered set where questionId = &#63; and isCorrect = &#63;.
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult fetchByQuestionIdAndStatus_First(
		long questionId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence()
				   .fetchByQuestionIdAndStatus_First(questionId, isCorrect,
			orderByComparator);
	}

	/**
	* Returns the last user test question result in the ordered set where questionId = &#63; and isCorrect = &#63;.
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult findByQuestionIdAndStatus_Last(
		long questionId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence()
				   .findByQuestionIdAndStatus_Last(questionId, isCorrect,
			orderByComparator);
	}

	/**
	* Returns the last user test question result in the ordered set where questionId = &#63; and isCorrect = &#63;.
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult fetchByQuestionIdAndStatus_Last(
		long questionId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence()
				   .fetchByQuestionIdAndStatus_Last(questionId, isCorrect,
			orderByComparator);
	}

	/**
	* Returns the user test question results before and after the current user test question result in the ordered set where questionId = &#63; and isCorrect = &#63;.
	*
	* @param userTestQuestionResultId the primary key of the current user test question result
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user test question result
	* @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	*/
	public static UserTestQuestionResult[] findByQuestionIdAndStatus_PrevAndNext(
		long userTestQuestionResultId, long questionId, boolean isCorrect,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence()
				   .findByQuestionIdAndStatus_PrevAndNext(userTestQuestionResultId,
			questionId, isCorrect, orderByComparator);
	}

	/**
	* Removes all the user test question results where questionId = &#63; and isCorrect = &#63; from the database.
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	*/
	public static void removeByQuestionIdAndStatus(long questionId,
		boolean isCorrect) {
		getPersistence().removeByQuestionIdAndStatus(questionId, isCorrect);
	}

	/**
	* Returns the number of user test question results where questionId = &#63; and isCorrect = &#63;.
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @return the number of matching user test question results
	*/
	public static int countByQuestionIdAndStatus(long questionId,
		boolean isCorrect) {
		return getPersistence().countByQuestionIdAndStatus(questionId, isCorrect);
	}

	/**
	* Returns all the user test question results where userId = &#63; and tokenData = &#63;.
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @return the matching user test question results
	*/
	public static List<UserTestQuestionResult> findByUserIdAndTokenDate(
		long userId, java.lang.String tokenData) {
		return getPersistence().findByUserIdAndTokenDate(userId, tokenData);
	}

	/**
	* Returns a range of all the user test question results where userId = &#63; and tokenData = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @return the range of matching user test question results
	*/
	public static List<UserTestQuestionResult> findByUserIdAndTokenDate(
		long userId, java.lang.String tokenData, int start, int end) {
		return getPersistence()
				   .findByUserIdAndTokenDate(userId, tokenData, start, end);
	}

	/**
	* Returns an ordered range of all the user test question results where userId = &#63; and tokenData = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user test question results
	*/
	public static List<UserTestQuestionResult> findByUserIdAndTokenDate(
		long userId, java.lang.String tokenData, int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence()
				   .findByUserIdAndTokenDate(userId, tokenData, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the user test question results where userId = &#63; and tokenData = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user test question results
	*/
	public static List<UserTestQuestionResult> findByUserIdAndTokenDate(
		long userId, java.lang.String tokenData, int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUserIdAndTokenDate(userId, tokenData, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first user test question result in the ordered set where userId = &#63; and tokenData = &#63;.
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult findByUserIdAndTokenDate_First(
		long userId, java.lang.String tokenData,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence()
				   .findByUserIdAndTokenDate_First(userId, tokenData,
			orderByComparator);
	}

	/**
	* Returns the first user test question result in the ordered set where userId = &#63; and tokenData = &#63;.
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult fetchByUserIdAndTokenDate_First(
		long userId, java.lang.String tokenData,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence()
				   .fetchByUserIdAndTokenDate_First(userId, tokenData,
			orderByComparator);
	}

	/**
	* Returns the last user test question result in the ordered set where userId = &#63; and tokenData = &#63;.
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult findByUserIdAndTokenDate_Last(
		long userId, java.lang.String tokenData,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence()
				   .findByUserIdAndTokenDate_Last(userId, tokenData,
			orderByComparator);
	}

	/**
	* Returns the last user test question result in the ordered set where userId = &#63; and tokenData = &#63;.
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public static UserTestQuestionResult fetchByUserIdAndTokenDate_Last(
		long userId, java.lang.String tokenData,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence()
				   .fetchByUserIdAndTokenDate_Last(userId, tokenData,
			orderByComparator);
	}

	/**
	* Returns the user test question results before and after the current user test question result in the ordered set where userId = &#63; and tokenData = &#63;.
	*
	* @param userTestQuestionResultId the primary key of the current user test question result
	* @param userId the user ID
	* @param tokenData the token data
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user test question result
	* @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	*/
	public static UserTestQuestionResult[] findByUserIdAndTokenDate_PrevAndNext(
		long userTestQuestionResultId, long userId, java.lang.String tokenData,
		OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence()
				   .findByUserIdAndTokenDate_PrevAndNext(userTestQuestionResultId,
			userId, tokenData, orderByComparator);
	}

	/**
	* Removes all the user test question results where userId = &#63; and tokenData = &#63; from the database.
	*
	* @param userId the user ID
	* @param tokenData the token data
	*/
	public static void removeByUserIdAndTokenDate(long userId,
		java.lang.String tokenData) {
		getPersistence().removeByUserIdAndTokenDate(userId, tokenData);
	}

	/**
	* Returns the number of user test question results where userId = &#63; and tokenData = &#63;.
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @return the number of matching user test question results
	*/
	public static int countByUserIdAndTokenDate(long userId,
		java.lang.String tokenData) {
		return getPersistence().countByUserIdAndTokenDate(userId, tokenData);
	}

	/**
	* Caches the user test question result in the entity cache if it is enabled.
	*
	* @param userTestQuestionResult the user test question result
	*/
	public static void cacheResult(
		UserTestQuestionResult userTestQuestionResult) {
		getPersistence().cacheResult(userTestQuestionResult);
	}

	/**
	* Caches the user test question results in the entity cache if it is enabled.
	*
	* @param userTestQuestionResults the user test question results
	*/
	public static void cacheResult(
		List<UserTestQuestionResult> userTestQuestionResults) {
		getPersistence().cacheResult(userTestQuestionResults);
	}

	/**
	* Creates a new user test question result with the primary key. Does not add the user test question result to the database.
	*
	* @param userTestQuestionResultId the primary key for the new user test question result
	* @return the new user test question result
	*/
	public static UserTestQuestionResult create(long userTestQuestionResultId) {
		return getPersistence().create(userTestQuestionResultId);
	}

	/**
	* Removes the user test question result with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userTestQuestionResultId the primary key of the user test question result
	* @return the user test question result that was removed
	* @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	*/
	public static UserTestQuestionResult remove(long userTestQuestionResultId)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence().remove(userTestQuestionResultId);
	}

	public static UserTestQuestionResult updateImpl(
		UserTestQuestionResult userTestQuestionResult) {
		return getPersistence().updateImpl(userTestQuestionResult);
	}

	/**
	* Returns the user test question result with the primary key or throws a {@link NoSuchUserTestQuestionResultException} if it could not be found.
	*
	* @param userTestQuestionResultId the primary key of the user test question result
	* @return the user test question result
	* @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	*/
	public static UserTestQuestionResult findByPrimaryKey(
		long userTestQuestionResultId)
		throws com.devshaker.exception.NoSuchUserTestQuestionResultException {
		return getPersistence().findByPrimaryKey(userTestQuestionResultId);
	}

	/**
	* Returns the user test question result with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userTestQuestionResultId the primary key of the user test question result
	* @return the user test question result, or <code>null</code> if a user test question result with the primary key could not be found
	*/
	public static UserTestQuestionResult fetchByPrimaryKey(
		long userTestQuestionResultId) {
		return getPersistence().fetchByPrimaryKey(userTestQuestionResultId);
	}

	public static java.util.Map<java.io.Serializable, UserTestQuestionResult> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the user test question results.
	*
	* @return the user test question results
	*/
	public static List<UserTestQuestionResult> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the user test question results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @return the range of user test question results
	*/
	public static List<UserTestQuestionResult> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the user test question results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user test question results
	*/
	public static List<UserTestQuestionResult> findAll(int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the user test question results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of user test question results
	*/
	public static List<UserTestQuestionResult> findAll(int start, int end,
		OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the user test question results from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of user test question results.
	*
	* @return the number of user test question results
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static UserTestQuestionResultPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<UserTestQuestionResultPersistence, UserTestQuestionResultPersistence> _serviceTracker =
		ServiceTrackerFactory.open(UserTestQuestionResultPersistence.class);
}