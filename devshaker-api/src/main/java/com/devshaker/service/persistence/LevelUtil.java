/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.Level;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the level service. This utility wraps {@link com.devshaker.service.persistence.impl.LevelPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LevelPersistence
 * @see com.devshaker.service.persistence.impl.LevelPersistenceImpl
 * @generated
 */
@ProviderType
public class LevelUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Level level) {
		getPersistence().clearCache(level);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Level> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Level> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Level> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator<Level> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Level update(Level level) {
		return getPersistence().update(level);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Level update(Level level, ServiceContext serviceContext) {
		return getPersistence().update(level, serviceContext);
	}

	/**
	* Returns all the levels where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching levels
	*/
	public static List<Level> findByCompanyId(long companyId) {
		return getPersistence().findByCompanyId(companyId);
	}

	/**
	* Returns a range of all the levels where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @return the range of matching levels
	*/
	public static List<Level> findByCompanyId(long companyId, int start, int end) {
		return getPersistence().findByCompanyId(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the levels where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching levels
	*/
	public static List<Level> findByCompanyId(long companyId, int start,
		int end, OrderByComparator<Level> orderByComparator) {
		return getPersistence()
				   .findByCompanyId(companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the levels where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching levels
	*/
	public static List<Level> findByCompanyId(long companyId, int start,
		int end, OrderByComparator<Level> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByCompanyId(companyId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first level in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching level
	* @throws NoSuchLevelException if a matching level could not be found
	*/
	public static Level findByCompanyId_First(long companyId,
		OrderByComparator<Level> orderByComparator)
		throws com.devshaker.exception.NoSuchLevelException {
		return getPersistence()
				   .findByCompanyId_First(companyId, orderByComparator);
	}

	/**
	* Returns the first level in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching level, or <code>null</code> if a matching level could not be found
	*/
	public static Level fetchByCompanyId_First(long companyId,
		OrderByComparator<Level> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyId_First(companyId, orderByComparator);
	}

	/**
	* Returns the last level in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching level
	* @throws NoSuchLevelException if a matching level could not be found
	*/
	public static Level findByCompanyId_Last(long companyId,
		OrderByComparator<Level> orderByComparator)
		throws com.devshaker.exception.NoSuchLevelException {
		return getPersistence()
				   .findByCompanyId_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last level in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching level, or <code>null</code> if a matching level could not be found
	*/
	public static Level fetchByCompanyId_Last(long companyId,
		OrderByComparator<Level> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyId_Last(companyId, orderByComparator);
	}

	/**
	* Returns the levels before and after the current level in the ordered set where companyId = &#63;.
	*
	* @param levelId the primary key of the current level
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next level
	* @throws NoSuchLevelException if a level with the primary key could not be found
	*/
	public static Level[] findByCompanyId_PrevAndNext(long levelId,
		long companyId, OrderByComparator<Level> orderByComparator)
		throws com.devshaker.exception.NoSuchLevelException {
		return getPersistence()
				   .findByCompanyId_PrevAndNext(levelId, companyId,
			orderByComparator);
	}

	/**
	* Removes all the levels where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	*/
	public static void removeByCompanyId(long companyId) {
		getPersistence().removeByCompanyId(companyId);
	}

	/**
	* Returns the number of levels where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching levels
	*/
	public static int countByCompanyId(long companyId) {
		return getPersistence().countByCompanyId(companyId);
	}

	/**
	* Returns all the levels where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching levels
	*/
	public static List<Level> findByUserId(long userId) {
		return getPersistence().findByUserId(userId);
	}

	/**
	* Returns a range of all the levels where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @return the range of matching levels
	*/
	public static List<Level> findByUserId(long userId, int start, int end) {
		return getPersistence().findByUserId(userId, start, end);
	}

	/**
	* Returns an ordered range of all the levels where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching levels
	*/
	public static List<Level> findByUserId(long userId, int start, int end,
		OrderByComparator<Level> orderByComparator) {
		return getPersistence()
				   .findByUserId(userId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the levels where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching levels
	*/
	public static List<Level> findByUserId(long userId, int start, int end,
		OrderByComparator<Level> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByUserId(userId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first level in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching level
	* @throws NoSuchLevelException if a matching level could not be found
	*/
	public static Level findByUserId_First(long userId,
		OrderByComparator<Level> orderByComparator)
		throws com.devshaker.exception.NoSuchLevelException {
		return getPersistence().findByUserId_First(userId, orderByComparator);
	}

	/**
	* Returns the first level in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching level, or <code>null</code> if a matching level could not be found
	*/
	public static Level fetchByUserId_First(long userId,
		OrderByComparator<Level> orderByComparator) {
		return getPersistence().fetchByUserId_First(userId, orderByComparator);
	}

	/**
	* Returns the last level in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching level
	* @throws NoSuchLevelException if a matching level could not be found
	*/
	public static Level findByUserId_Last(long userId,
		OrderByComparator<Level> orderByComparator)
		throws com.devshaker.exception.NoSuchLevelException {
		return getPersistence().findByUserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the last level in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching level, or <code>null</code> if a matching level could not be found
	*/
	public static Level fetchByUserId_Last(long userId,
		OrderByComparator<Level> orderByComparator) {
		return getPersistence().fetchByUserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the levels before and after the current level in the ordered set where userId = &#63;.
	*
	* @param levelId the primary key of the current level
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next level
	* @throws NoSuchLevelException if a level with the primary key could not be found
	*/
	public static Level[] findByUserId_PrevAndNext(long levelId, long userId,
		OrderByComparator<Level> orderByComparator)
		throws com.devshaker.exception.NoSuchLevelException {
		return getPersistence()
				   .findByUserId_PrevAndNext(levelId, userId, orderByComparator);
	}

	/**
	* Removes all the levels where userId = &#63; from the database.
	*
	* @param userId the user ID
	*/
	public static void removeByUserId(long userId) {
		getPersistence().removeByUserId(userId);
	}

	/**
	* Returns the number of levels where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching levels
	*/
	public static int countByUserId(long userId) {
		return getPersistence().countByUserId(userId);
	}

	/**
	* Returns all the levels where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @return the matching levels
	*/
	public static List<Level> findByCategoryId(long categoryId) {
		return getPersistence().findByCategoryId(categoryId);
	}

	/**
	* Returns a range of all the levels where categoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoryId the category ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @return the range of matching levels
	*/
	public static List<Level> findByCategoryId(long categoryId, int start,
		int end) {
		return getPersistence().findByCategoryId(categoryId, start, end);
	}

	/**
	* Returns an ordered range of all the levels where categoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoryId the category ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching levels
	*/
	public static List<Level> findByCategoryId(long categoryId, int start,
		int end, OrderByComparator<Level> orderByComparator) {
		return getPersistence()
				   .findByCategoryId(categoryId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the levels where categoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoryId the category ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching levels
	*/
	public static List<Level> findByCategoryId(long categoryId, int start,
		int end, OrderByComparator<Level> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByCategoryId(categoryId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first level in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching level
	* @throws NoSuchLevelException if a matching level could not be found
	*/
	public static Level findByCategoryId_First(long categoryId,
		OrderByComparator<Level> orderByComparator)
		throws com.devshaker.exception.NoSuchLevelException {
		return getPersistence()
				   .findByCategoryId_First(categoryId, orderByComparator);
	}

	/**
	* Returns the first level in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching level, or <code>null</code> if a matching level could not be found
	*/
	public static Level fetchByCategoryId_First(long categoryId,
		OrderByComparator<Level> orderByComparator) {
		return getPersistence()
				   .fetchByCategoryId_First(categoryId, orderByComparator);
	}

	/**
	* Returns the last level in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching level
	* @throws NoSuchLevelException if a matching level could not be found
	*/
	public static Level findByCategoryId_Last(long categoryId,
		OrderByComparator<Level> orderByComparator)
		throws com.devshaker.exception.NoSuchLevelException {
		return getPersistence()
				   .findByCategoryId_Last(categoryId, orderByComparator);
	}

	/**
	* Returns the last level in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching level, or <code>null</code> if a matching level could not be found
	*/
	public static Level fetchByCategoryId_Last(long categoryId,
		OrderByComparator<Level> orderByComparator) {
		return getPersistence()
				   .fetchByCategoryId_Last(categoryId, orderByComparator);
	}

	/**
	* Returns the levels before and after the current level in the ordered set where categoryId = &#63;.
	*
	* @param levelId the primary key of the current level
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next level
	* @throws NoSuchLevelException if a level with the primary key could not be found
	*/
	public static Level[] findByCategoryId_PrevAndNext(long levelId,
		long categoryId, OrderByComparator<Level> orderByComparator)
		throws com.devshaker.exception.NoSuchLevelException {
		return getPersistence()
				   .findByCategoryId_PrevAndNext(levelId, categoryId,
			orderByComparator);
	}

	/**
	* Removes all the levels where categoryId = &#63; from the database.
	*
	* @param categoryId the category ID
	*/
	public static void removeByCategoryId(long categoryId) {
		getPersistence().removeByCategoryId(categoryId);
	}

	/**
	* Returns the number of levels where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @return the number of matching levels
	*/
	public static int countByCategoryId(long categoryId) {
		return getPersistence().countByCategoryId(categoryId);
	}

	/**
	* Returns all the levels where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @return the matching levels
	*/
	public static List<Level> findBySubcategoryId(long subcategoryId) {
		return getPersistence().findBySubcategoryId(subcategoryId);
	}

	/**
	* Returns a range of all the levels where subcategoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param subcategoryId the subcategory ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @return the range of matching levels
	*/
	public static List<Level> findBySubcategoryId(long subcategoryId,
		int start, int end) {
		return getPersistence().findBySubcategoryId(subcategoryId, start, end);
	}

	/**
	* Returns an ordered range of all the levels where subcategoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param subcategoryId the subcategory ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching levels
	*/
	public static List<Level> findBySubcategoryId(long subcategoryId,
		int start, int end, OrderByComparator<Level> orderByComparator) {
		return getPersistence()
				   .findBySubcategoryId(subcategoryId, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the levels where subcategoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param subcategoryId the subcategory ID
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching levels
	*/
	public static List<Level> findBySubcategoryId(long subcategoryId,
		int start, int end, OrderByComparator<Level> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findBySubcategoryId(subcategoryId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first level in the ordered set where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching level
	* @throws NoSuchLevelException if a matching level could not be found
	*/
	public static Level findBySubcategoryId_First(long subcategoryId,
		OrderByComparator<Level> orderByComparator)
		throws com.devshaker.exception.NoSuchLevelException {
		return getPersistence()
				   .findBySubcategoryId_First(subcategoryId, orderByComparator);
	}

	/**
	* Returns the first level in the ordered set where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching level, or <code>null</code> if a matching level could not be found
	*/
	public static Level fetchBySubcategoryId_First(long subcategoryId,
		OrderByComparator<Level> orderByComparator) {
		return getPersistence()
				   .fetchBySubcategoryId_First(subcategoryId, orderByComparator);
	}

	/**
	* Returns the last level in the ordered set where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching level
	* @throws NoSuchLevelException if a matching level could not be found
	*/
	public static Level findBySubcategoryId_Last(long subcategoryId,
		OrderByComparator<Level> orderByComparator)
		throws com.devshaker.exception.NoSuchLevelException {
		return getPersistence()
				   .findBySubcategoryId_Last(subcategoryId, orderByComparator);
	}

	/**
	* Returns the last level in the ordered set where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching level, or <code>null</code> if a matching level could not be found
	*/
	public static Level fetchBySubcategoryId_Last(long subcategoryId,
		OrderByComparator<Level> orderByComparator) {
		return getPersistence()
				   .fetchBySubcategoryId_Last(subcategoryId, orderByComparator);
	}

	/**
	* Returns the levels before and after the current level in the ordered set where subcategoryId = &#63;.
	*
	* @param levelId the primary key of the current level
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next level
	* @throws NoSuchLevelException if a level with the primary key could not be found
	*/
	public static Level[] findBySubcategoryId_PrevAndNext(long levelId,
		long subcategoryId, OrderByComparator<Level> orderByComparator)
		throws com.devshaker.exception.NoSuchLevelException {
		return getPersistence()
				   .findBySubcategoryId_PrevAndNext(levelId, subcategoryId,
			orderByComparator);
	}

	/**
	* Removes all the levels where subcategoryId = &#63; from the database.
	*
	* @param subcategoryId the subcategory ID
	*/
	public static void removeBySubcategoryId(long subcategoryId) {
		getPersistence().removeBySubcategoryId(subcategoryId);
	}

	/**
	* Returns the number of levels where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @return the number of matching levels
	*/
	public static int countBySubcategoryId(long subcategoryId) {
		return getPersistence().countBySubcategoryId(subcategoryId);
	}

	/**
	* Caches the level in the entity cache if it is enabled.
	*
	* @param level the level
	*/
	public static void cacheResult(Level level) {
		getPersistence().cacheResult(level);
	}

	/**
	* Caches the levels in the entity cache if it is enabled.
	*
	* @param levels the levels
	*/
	public static void cacheResult(List<Level> levels) {
		getPersistence().cacheResult(levels);
	}

	/**
	* Creates a new level with the primary key. Does not add the level to the database.
	*
	* @param levelId the primary key for the new level
	* @return the new level
	*/
	public static Level create(long levelId) {
		return getPersistence().create(levelId);
	}

	/**
	* Removes the level with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param levelId the primary key of the level
	* @return the level that was removed
	* @throws NoSuchLevelException if a level with the primary key could not be found
	*/
	public static Level remove(long levelId)
		throws com.devshaker.exception.NoSuchLevelException {
		return getPersistence().remove(levelId);
	}

	public static Level updateImpl(Level level) {
		return getPersistence().updateImpl(level);
	}

	/**
	* Returns the level with the primary key or throws a {@link NoSuchLevelException} if it could not be found.
	*
	* @param levelId the primary key of the level
	* @return the level
	* @throws NoSuchLevelException if a level with the primary key could not be found
	*/
	public static Level findByPrimaryKey(long levelId)
		throws com.devshaker.exception.NoSuchLevelException {
		return getPersistence().findByPrimaryKey(levelId);
	}

	/**
	* Returns the level with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param levelId the primary key of the level
	* @return the level, or <code>null</code> if a level with the primary key could not be found
	*/
	public static Level fetchByPrimaryKey(long levelId) {
		return getPersistence().fetchByPrimaryKey(levelId);
	}

	public static java.util.Map<java.io.Serializable, Level> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the levels.
	*
	* @return the levels
	*/
	public static List<Level> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the levels.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @return the range of levels
	*/
	public static List<Level> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the levels.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of levels
	*/
	public static List<Level> findAll(int start, int end,
		OrderByComparator<Level> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the levels.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LevelModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of levels
	* @param end the upper bound of the range of levels (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of levels
	*/
	public static List<Level> findAll(int start, int end,
		OrderByComparator<Level> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the levels from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of levels.
	*
	* @return the number of levels
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static LevelPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<LevelPersistence, LevelPersistence> _serviceTracker =
		ServiceTrackerFactory.open(LevelPersistence.class);
}