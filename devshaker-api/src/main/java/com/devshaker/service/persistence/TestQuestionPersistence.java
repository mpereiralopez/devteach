/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.exception.NoSuchTestQuestionException;

import com.devshaker.model.TestQuestion;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the test question service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.devshaker.service.persistence.impl.TestQuestionPersistenceImpl
 * @see TestQuestionUtil
 * @generated
 */
@ProviderType
public interface TestQuestionPersistence extends BasePersistence<TestQuestion> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TestQuestionUtil} to access the test question persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the test questions where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching test questions
	*/
	public java.util.List<TestQuestion> findByCompanyId(long companyId);

	/**
	* Returns a range of all the test questions where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @return the range of matching test questions
	*/
	public java.util.List<TestQuestion> findByCompanyId(long companyId,
		int start, int end);

	/**
	* Returns an ordered range of all the test questions where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching test questions
	*/
	public java.util.List<TestQuestion> findByCompanyId(long companyId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns an ordered range of all the test questions where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching test questions
	*/
	public java.util.List<TestQuestion> findByCompanyId(long companyId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first test question in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public TestQuestion findByCompanyId_First(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Returns the first test question in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public TestQuestion fetchByCompanyId_First(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns the last test question in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public TestQuestion findByCompanyId_Last(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Returns the last test question in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public TestQuestion fetchByCompanyId_Last(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns the test questions before and after the current test question in the ordered set where companyId = &#63;.
	*
	* @param questionId the primary key of the current test question
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next test question
	* @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	*/
	public TestQuestion[] findByCompanyId_PrevAndNext(long questionId,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Removes all the test questions where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	*/
	public void removeByCompanyId(long companyId);

	/**
	* Returns the number of test questions where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching test questions
	*/
	public int countByCompanyId(long companyId);

	/**
	* Returns all the test questions where userIdCreator = &#63;.
	*
	* @param userIdCreator the user ID creator
	* @return the matching test questions
	*/
	public java.util.List<TestQuestion> findByUserIdCreator(long userIdCreator);

	/**
	* Returns a range of all the test questions where userIdCreator = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userIdCreator the user ID creator
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @return the range of matching test questions
	*/
	public java.util.List<TestQuestion> findByUserIdCreator(
		long userIdCreator, int start, int end);

	/**
	* Returns an ordered range of all the test questions where userIdCreator = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userIdCreator the user ID creator
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching test questions
	*/
	public java.util.List<TestQuestion> findByUserIdCreator(
		long userIdCreator, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns an ordered range of all the test questions where userIdCreator = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userIdCreator the user ID creator
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching test questions
	*/
	public java.util.List<TestQuestion> findByUserIdCreator(
		long userIdCreator, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first test question in the ordered set where userIdCreator = &#63;.
	*
	* @param userIdCreator the user ID creator
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public TestQuestion findByUserIdCreator_First(long userIdCreator,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Returns the first test question in the ordered set where userIdCreator = &#63;.
	*
	* @param userIdCreator the user ID creator
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public TestQuestion fetchByUserIdCreator_First(long userIdCreator,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns the last test question in the ordered set where userIdCreator = &#63;.
	*
	* @param userIdCreator the user ID creator
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public TestQuestion findByUserIdCreator_Last(long userIdCreator,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Returns the last test question in the ordered set where userIdCreator = &#63;.
	*
	* @param userIdCreator the user ID creator
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public TestQuestion fetchByUserIdCreator_Last(long userIdCreator,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns the test questions before and after the current test question in the ordered set where userIdCreator = &#63;.
	*
	* @param questionId the primary key of the current test question
	* @param userIdCreator the user ID creator
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next test question
	* @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	*/
	public TestQuestion[] findByUserIdCreator_PrevAndNext(long questionId,
		long userIdCreator,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Removes all the test questions where userIdCreator = &#63; from the database.
	*
	* @param userIdCreator the user ID creator
	*/
	public void removeByUserIdCreator(long userIdCreator);

	/**
	* Returns the number of test questions where userIdCreator = &#63;.
	*
	* @param userIdCreator the user ID creator
	* @return the number of matching test questions
	*/
	public int countByUserIdCreator(long userIdCreator);

	/**
	* Returns all the test questions where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @return the matching test questions
	*/
	public java.util.List<TestQuestion> findByCategoryId(long categoryId);

	/**
	* Returns a range of all the test questions where categoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoryId the category ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @return the range of matching test questions
	*/
	public java.util.List<TestQuestion> findByCategoryId(long categoryId,
		int start, int end);

	/**
	* Returns an ordered range of all the test questions where categoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoryId the category ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching test questions
	*/
	public java.util.List<TestQuestion> findByCategoryId(long categoryId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns an ordered range of all the test questions where categoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoryId the category ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching test questions
	*/
	public java.util.List<TestQuestion> findByCategoryId(long categoryId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first test question in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public TestQuestion findByCategoryId_First(long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Returns the first test question in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public TestQuestion fetchByCategoryId_First(long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns the last test question in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public TestQuestion findByCategoryId_Last(long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Returns the last test question in the ordered set where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public TestQuestion fetchByCategoryId_Last(long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns the test questions before and after the current test question in the ordered set where categoryId = &#63;.
	*
	* @param questionId the primary key of the current test question
	* @param categoryId the category ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next test question
	* @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	*/
	public TestQuestion[] findByCategoryId_PrevAndNext(long questionId,
		long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Removes all the test questions where categoryId = &#63; from the database.
	*
	* @param categoryId the category ID
	*/
	public void removeByCategoryId(long categoryId);

	/**
	* Returns the number of test questions where categoryId = &#63;.
	*
	* @param categoryId the category ID
	* @return the number of matching test questions
	*/
	public int countByCategoryId(long categoryId);

	/**
	* Returns all the test questions where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @return the matching test questions
	*/
	public java.util.List<TestQuestion> findBySubcategoryId(long subcategoryId);

	/**
	* Returns a range of all the test questions where subcategoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param subcategoryId the subcategory ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @return the range of matching test questions
	*/
	public java.util.List<TestQuestion> findBySubcategoryId(
		long subcategoryId, int start, int end);

	/**
	* Returns an ordered range of all the test questions where subcategoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param subcategoryId the subcategory ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching test questions
	*/
	public java.util.List<TestQuestion> findBySubcategoryId(
		long subcategoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns an ordered range of all the test questions where subcategoryId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param subcategoryId the subcategory ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching test questions
	*/
	public java.util.List<TestQuestion> findBySubcategoryId(
		long subcategoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first test question in the ordered set where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public TestQuestion findBySubcategoryId_First(long subcategoryId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Returns the first test question in the ordered set where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public TestQuestion fetchBySubcategoryId_First(long subcategoryId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns the last test question in the ordered set where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public TestQuestion findBySubcategoryId_Last(long subcategoryId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Returns the last test question in the ordered set where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public TestQuestion fetchBySubcategoryId_Last(long subcategoryId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns the test questions before and after the current test question in the ordered set where subcategoryId = &#63;.
	*
	* @param questionId the primary key of the current test question
	* @param subcategoryId the subcategory ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next test question
	* @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	*/
	public TestQuestion[] findBySubcategoryId_PrevAndNext(long questionId,
		long subcategoryId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Removes all the test questions where subcategoryId = &#63; from the database.
	*
	* @param subcategoryId the subcategory ID
	*/
	public void removeBySubcategoryId(long subcategoryId);

	/**
	* Returns the number of test questions where subcategoryId = &#63;.
	*
	* @param subcategoryId the subcategory ID
	* @return the number of matching test questions
	*/
	public int countBySubcategoryId(long subcategoryId);

	/**
	* Returns all the test questions where levelId = &#63;.
	*
	* @param levelId the level ID
	* @return the matching test questions
	*/
	public java.util.List<TestQuestion> findBylevelId(long levelId);

	/**
	* Returns a range of all the test questions where levelId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param levelId the level ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @return the range of matching test questions
	*/
	public java.util.List<TestQuestion> findBylevelId(long levelId, int start,
		int end);

	/**
	* Returns an ordered range of all the test questions where levelId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param levelId the level ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching test questions
	*/
	public java.util.List<TestQuestion> findBylevelId(long levelId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns an ordered range of all the test questions where levelId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param levelId the level ID
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching test questions
	*/
	public java.util.List<TestQuestion> findBylevelId(long levelId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first test question in the ordered set where levelId = &#63;.
	*
	* @param levelId the level ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public TestQuestion findBylevelId_First(long levelId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Returns the first test question in the ordered set where levelId = &#63;.
	*
	* @param levelId the level ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public TestQuestion fetchBylevelId_First(long levelId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns the last test question in the ordered set where levelId = &#63;.
	*
	* @param levelId the level ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public TestQuestion findBylevelId_Last(long levelId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Returns the last test question in the ordered set where levelId = &#63;.
	*
	* @param levelId the level ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public TestQuestion fetchBylevelId_Last(long levelId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns the test questions before and after the current test question in the ordered set where levelId = &#63;.
	*
	* @param questionId the primary key of the current test question
	* @param levelId the level ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next test question
	* @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	*/
	public TestQuestion[] findBylevelId_PrevAndNext(long questionId,
		long levelId,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Removes all the test questions where levelId = &#63; from the database.
	*
	* @param levelId the level ID
	*/
	public void removeBylevelId(long levelId);

	/**
	* Returns the number of test questions where levelId = &#63;.
	*
	* @param levelId the level ID
	* @return the number of matching test questions
	*/
	public int countBylevelId(long levelId);

	/**
	* Returns all the test questions where status = &#63;.
	*
	* @param status the status
	* @return the matching test questions
	*/
	public java.util.List<TestQuestion> findByStatus(int status);

	/**
	* Returns a range of all the test questions where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @return the range of matching test questions
	*/
	public java.util.List<TestQuestion> findByStatus(int status, int start,
		int end);

	/**
	* Returns an ordered range of all the test questions where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching test questions
	*/
	public java.util.List<TestQuestion> findByStatus(int status, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns an ordered range of all the test questions where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching test questions
	*/
	public java.util.List<TestQuestion> findByStatus(int status, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first test question in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public TestQuestion findByStatus_First(int status,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Returns the first test question in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public TestQuestion fetchByStatus_First(int status,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns the last test question in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question
	* @throws NoSuchTestQuestionException if a matching test question could not be found
	*/
	public TestQuestion findByStatus_Last(int status,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Returns the last test question in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching test question, or <code>null</code> if a matching test question could not be found
	*/
	public TestQuestion fetchByStatus_Last(int status,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns the test questions before and after the current test question in the ordered set where status = &#63;.
	*
	* @param questionId the primary key of the current test question
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next test question
	* @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	*/
	public TestQuestion[] findByStatus_PrevAndNext(long questionId, int status,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator)
		throws NoSuchTestQuestionException;

	/**
	* Removes all the test questions where status = &#63; from the database.
	*
	* @param status the status
	*/
	public void removeByStatus(int status);

	/**
	* Returns the number of test questions where status = &#63;.
	*
	* @param status the status
	* @return the number of matching test questions
	*/
	public int countByStatus(int status);

	/**
	* Caches the test question in the entity cache if it is enabled.
	*
	* @param testQuestion the test question
	*/
	public void cacheResult(TestQuestion testQuestion);

	/**
	* Caches the test questions in the entity cache if it is enabled.
	*
	* @param testQuestions the test questions
	*/
	public void cacheResult(java.util.List<TestQuestion> testQuestions);

	/**
	* Creates a new test question with the primary key. Does not add the test question to the database.
	*
	* @param questionId the primary key for the new test question
	* @return the new test question
	*/
	public TestQuestion create(long questionId);

	/**
	* Removes the test question with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param questionId the primary key of the test question
	* @return the test question that was removed
	* @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	*/
	public TestQuestion remove(long questionId)
		throws NoSuchTestQuestionException;

	public TestQuestion updateImpl(TestQuestion testQuestion);

	/**
	* Returns the test question with the primary key or throws a {@link NoSuchTestQuestionException} if it could not be found.
	*
	* @param questionId the primary key of the test question
	* @return the test question
	* @throws NoSuchTestQuestionException if a test question with the primary key could not be found
	*/
	public TestQuestion findByPrimaryKey(long questionId)
		throws NoSuchTestQuestionException;

	/**
	* Returns the test question with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param questionId the primary key of the test question
	* @return the test question, or <code>null</code> if a test question with the primary key could not be found
	*/
	public TestQuestion fetchByPrimaryKey(long questionId);

	@Override
	public java.util.Map<java.io.Serializable, TestQuestion> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the test questions.
	*
	* @return the test questions
	*/
	public java.util.List<TestQuestion> findAll();

	/**
	* Returns a range of all the test questions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @return the range of test questions
	*/
	public java.util.List<TestQuestion> findAll(int start, int end);

	/**
	* Returns an ordered range of all the test questions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of test questions
	*/
	public java.util.List<TestQuestion> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator);

	/**
	* Returns an ordered range of all the test questions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of test questions
	*/
	public java.util.List<TestQuestion> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TestQuestion> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the test questions from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of test questions.
	*
	* @return the number of test questions
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}