/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.exception.NoSuchBadgeRuleException;

import com.devshaker.model.BadgeRule;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the badge rule service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.devshaker.service.persistence.impl.BadgeRulePersistenceImpl
 * @see BadgeRuleUtil
 * @generated
 */
@ProviderType
public interface BadgeRulePersistence extends BasePersistence<BadgeRule> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BadgeRuleUtil} to access the badge rule persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the badge rules where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @return the matching badge rules
	*/
	public java.util.List<BadgeRule> findByBadgeId(long badgeId);

	/**
	* Returns a range of all the badge rules where badgeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param badgeId the badge ID
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @return the range of matching badge rules
	*/
	public java.util.List<BadgeRule> findByBadgeId(long badgeId, int start,
		int end);

	/**
	* Returns an ordered range of all the badge rules where badgeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param badgeId the badge ID
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching badge rules
	*/
	public java.util.List<BadgeRule> findByBadgeId(long badgeId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<BadgeRule> orderByComparator);

	/**
	* Returns an ordered range of all the badge rules where badgeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param badgeId the badge ID
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching badge rules
	*/
	public java.util.List<BadgeRule> findByBadgeId(long badgeId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<BadgeRule> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first badge rule in the ordered set where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching badge rule
	* @throws NoSuchBadgeRuleException if a matching badge rule could not be found
	*/
	public BadgeRule findByBadgeId_First(long badgeId,
		com.liferay.portal.kernel.util.OrderByComparator<BadgeRule> orderByComparator)
		throws NoSuchBadgeRuleException;

	/**
	* Returns the first badge rule in the ordered set where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching badge rule, or <code>null</code> if a matching badge rule could not be found
	*/
	public BadgeRule fetchByBadgeId_First(long badgeId,
		com.liferay.portal.kernel.util.OrderByComparator<BadgeRule> orderByComparator);

	/**
	* Returns the last badge rule in the ordered set where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching badge rule
	* @throws NoSuchBadgeRuleException if a matching badge rule could not be found
	*/
	public BadgeRule findByBadgeId_Last(long badgeId,
		com.liferay.portal.kernel.util.OrderByComparator<BadgeRule> orderByComparator)
		throws NoSuchBadgeRuleException;

	/**
	* Returns the last badge rule in the ordered set where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching badge rule, or <code>null</code> if a matching badge rule could not be found
	*/
	public BadgeRule fetchByBadgeId_Last(long badgeId,
		com.liferay.portal.kernel.util.OrderByComparator<BadgeRule> orderByComparator);

	/**
	* Returns the badge rules before and after the current badge rule in the ordered set where badgeId = &#63;.
	*
	* @param badgeRulePK the primary key of the current badge rule
	* @param badgeId the badge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next badge rule
	* @throws NoSuchBadgeRuleException if a badge rule with the primary key could not be found
	*/
	public BadgeRule[] findByBadgeId_PrevAndNext(BadgeRulePK badgeRulePK,
		long badgeId,
		com.liferay.portal.kernel.util.OrderByComparator<BadgeRule> orderByComparator)
		throws NoSuchBadgeRuleException;

	/**
	* Removes all the badge rules where badgeId = &#63; from the database.
	*
	* @param badgeId the badge ID
	*/
	public void removeByBadgeId(long badgeId);

	/**
	* Returns the number of badge rules where badgeId = &#63;.
	*
	* @param badgeId the badge ID
	* @return the number of matching badge rules
	*/
	public int countByBadgeId(long badgeId);

	/**
	* Returns all the badge rules where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching badge rules
	*/
	public java.util.List<BadgeRule> findByCompanyId(long companyId);

	/**
	* Returns a range of all the badge rules where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @return the range of matching badge rules
	*/
	public java.util.List<BadgeRule> findByCompanyId(long companyId, int start,
		int end);

	/**
	* Returns an ordered range of all the badge rules where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching badge rules
	*/
	public java.util.List<BadgeRule> findByCompanyId(long companyId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<BadgeRule> orderByComparator);

	/**
	* Returns an ordered range of all the badge rules where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching badge rules
	*/
	public java.util.List<BadgeRule> findByCompanyId(long companyId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<BadgeRule> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first badge rule in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching badge rule
	* @throws NoSuchBadgeRuleException if a matching badge rule could not be found
	*/
	public BadgeRule findByCompanyId_First(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<BadgeRule> orderByComparator)
		throws NoSuchBadgeRuleException;

	/**
	* Returns the first badge rule in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching badge rule, or <code>null</code> if a matching badge rule could not be found
	*/
	public BadgeRule fetchByCompanyId_First(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<BadgeRule> orderByComparator);

	/**
	* Returns the last badge rule in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching badge rule
	* @throws NoSuchBadgeRuleException if a matching badge rule could not be found
	*/
	public BadgeRule findByCompanyId_Last(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<BadgeRule> orderByComparator)
		throws NoSuchBadgeRuleException;

	/**
	* Returns the last badge rule in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching badge rule, or <code>null</code> if a matching badge rule could not be found
	*/
	public BadgeRule fetchByCompanyId_Last(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<BadgeRule> orderByComparator);

	/**
	* Returns the badge rules before and after the current badge rule in the ordered set where companyId = &#63;.
	*
	* @param badgeRulePK the primary key of the current badge rule
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next badge rule
	* @throws NoSuchBadgeRuleException if a badge rule with the primary key could not be found
	*/
	public BadgeRule[] findByCompanyId_PrevAndNext(BadgeRulePK badgeRulePK,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<BadgeRule> orderByComparator)
		throws NoSuchBadgeRuleException;

	/**
	* Removes all the badge rules where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	*/
	public void removeByCompanyId(long companyId);

	/**
	* Returns the number of badge rules where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching badge rules
	*/
	public int countByCompanyId(long companyId);

	/**
	* Caches the badge rule in the entity cache if it is enabled.
	*
	* @param badgeRule the badge rule
	*/
	public void cacheResult(BadgeRule badgeRule);

	/**
	* Caches the badge rules in the entity cache if it is enabled.
	*
	* @param badgeRules the badge rules
	*/
	public void cacheResult(java.util.List<BadgeRule> badgeRules);

	/**
	* Creates a new badge rule with the primary key. Does not add the badge rule to the database.
	*
	* @param badgeRulePK the primary key for the new badge rule
	* @return the new badge rule
	*/
	public BadgeRule create(BadgeRulePK badgeRulePK);

	/**
	* Removes the badge rule with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param badgeRulePK the primary key of the badge rule
	* @return the badge rule that was removed
	* @throws NoSuchBadgeRuleException if a badge rule with the primary key could not be found
	*/
	public BadgeRule remove(BadgeRulePK badgeRulePK)
		throws NoSuchBadgeRuleException;

	public BadgeRule updateImpl(BadgeRule badgeRule);

	/**
	* Returns the badge rule with the primary key or throws a {@link NoSuchBadgeRuleException} if it could not be found.
	*
	* @param badgeRulePK the primary key of the badge rule
	* @return the badge rule
	* @throws NoSuchBadgeRuleException if a badge rule with the primary key could not be found
	*/
	public BadgeRule findByPrimaryKey(BadgeRulePK badgeRulePK)
		throws NoSuchBadgeRuleException;

	/**
	* Returns the badge rule with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param badgeRulePK the primary key of the badge rule
	* @return the badge rule, or <code>null</code> if a badge rule with the primary key could not be found
	*/
	public BadgeRule fetchByPrimaryKey(BadgeRulePK badgeRulePK);

	@Override
	public java.util.Map<java.io.Serializable, BadgeRule> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the badge rules.
	*
	* @return the badge rules
	*/
	public java.util.List<BadgeRule> findAll();

	/**
	* Returns a range of all the badge rules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @return the range of badge rules
	*/
	public java.util.List<BadgeRule> findAll(int start, int end);

	/**
	* Returns an ordered range of all the badge rules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of badge rules
	*/
	public java.util.List<BadgeRule> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<BadgeRule> orderByComparator);

	/**
	* Returns an ordered range of all the badge rules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of badge rules
	*/
	public java.util.List<BadgeRule> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<BadgeRule> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the badge rules from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of badge rules.
	*
	* @return the number of badge rules
	*/
	public int countAll();
}