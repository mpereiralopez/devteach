/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.exception.NoSuchUserTestQuestionResultException;

import com.devshaker.model.UserTestQuestionResult;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the user test question result service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.devshaker.service.persistence.impl.UserTestQuestionResultPersistenceImpl
 * @see UserTestQuestionResultUtil
 * @generated
 */
@ProviderType
public interface UserTestQuestionResultPersistence extends BasePersistence<UserTestQuestionResult> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UserTestQuestionResultUtil} to access the user test question result persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the user test question results where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByUserId(long userId);

	/**
	* Returns a range of all the user test question results where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @return the range of matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByUserId(long userId,
		int start, int end);

	/**
	* Returns an ordered range of all the user test question results where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByUserId(long userId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator);

	/**
	* Returns an ordered range of all the user test question results where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByUserId(long userId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first user test question result in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public UserTestQuestionResult findByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException;

	/**
	* Returns the first user test question result in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public UserTestQuestionResult fetchByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator);

	/**
	* Returns the last user test question result in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public UserTestQuestionResult findByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException;

	/**
	* Returns the last user test question result in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public UserTestQuestionResult fetchByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator);

	/**
	* Returns the user test question results before and after the current user test question result in the ordered set where userId = &#63;.
	*
	* @param userTestQuestionResultId the primary key of the current user test question result
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user test question result
	* @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	*/
	public UserTestQuestionResult[] findByUserId_PrevAndNext(
		long userTestQuestionResultId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException;

	/**
	* Removes all the user test question results where userId = &#63; from the database.
	*
	* @param userId the user ID
	*/
	public void removeByUserId(long userId);

	/**
	* Returns the number of user test question results where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching user test question results
	*/
	public int countByUserId(long userId);

	/**
	* Returns all the user test question results where userId = &#63; and isCorrect = &#63;.
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @return the matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByUserIdAndStatus(
		long userId, boolean isCorrect);

	/**
	* Returns a range of all the user test question results where userId = &#63; and isCorrect = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @return the range of matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByUserIdAndStatus(
		long userId, boolean isCorrect, int start, int end);

	/**
	* Returns an ordered range of all the user test question results where userId = &#63; and isCorrect = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByUserIdAndStatus(
		long userId, boolean isCorrect, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator);

	/**
	* Returns an ordered range of all the user test question results where userId = &#63; and isCorrect = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByUserIdAndStatus(
		long userId, boolean isCorrect, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first user test question result in the ordered set where userId = &#63; and isCorrect = &#63;.
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public UserTestQuestionResult findByUserIdAndStatus_First(long userId,
		boolean isCorrect,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException;

	/**
	* Returns the first user test question result in the ordered set where userId = &#63; and isCorrect = &#63;.
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public UserTestQuestionResult fetchByUserIdAndStatus_First(long userId,
		boolean isCorrect,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator);

	/**
	* Returns the last user test question result in the ordered set where userId = &#63; and isCorrect = &#63;.
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public UserTestQuestionResult findByUserIdAndStatus_Last(long userId,
		boolean isCorrect,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException;

	/**
	* Returns the last user test question result in the ordered set where userId = &#63; and isCorrect = &#63;.
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public UserTestQuestionResult fetchByUserIdAndStatus_Last(long userId,
		boolean isCorrect,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator);

	/**
	* Returns the user test question results before and after the current user test question result in the ordered set where userId = &#63; and isCorrect = &#63;.
	*
	* @param userTestQuestionResultId the primary key of the current user test question result
	* @param userId the user ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user test question result
	* @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	*/
	public UserTestQuestionResult[] findByUserIdAndStatus_PrevAndNext(
		long userTestQuestionResultId, long userId, boolean isCorrect,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException;

	/**
	* Removes all the user test question results where userId = &#63; and isCorrect = &#63; from the database.
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	*/
	public void removeByUserIdAndStatus(long userId, boolean isCorrect);

	/**
	* Returns the number of user test question results where userId = &#63; and isCorrect = &#63;.
	*
	* @param userId the user ID
	* @param isCorrect the is correct
	* @return the number of matching user test question results
	*/
	public int countByUserIdAndStatus(long userId, boolean isCorrect);

	/**
	* Returns all the user test question results where questionId = &#63;.
	*
	* @param questionId the question ID
	* @return the matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByQuestionId(
		long questionId);

	/**
	* Returns a range of all the user test question results where questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @return the range of matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByQuestionId(
		long questionId, int start, int end);

	/**
	* Returns an ordered range of all the user test question results where questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByQuestionId(
		long questionId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator);

	/**
	* Returns an ordered range of all the user test question results where questionId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByQuestionId(
		long questionId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first user test question result in the ordered set where questionId = &#63;.
	*
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public UserTestQuestionResult findByQuestionId_First(long questionId,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException;

	/**
	* Returns the first user test question result in the ordered set where questionId = &#63;.
	*
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public UserTestQuestionResult fetchByQuestionId_First(long questionId,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator);

	/**
	* Returns the last user test question result in the ordered set where questionId = &#63;.
	*
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public UserTestQuestionResult findByQuestionId_Last(long questionId,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException;

	/**
	* Returns the last user test question result in the ordered set where questionId = &#63;.
	*
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public UserTestQuestionResult fetchByQuestionId_Last(long questionId,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator);

	/**
	* Returns the user test question results before and after the current user test question result in the ordered set where questionId = &#63;.
	*
	* @param userTestQuestionResultId the primary key of the current user test question result
	* @param questionId the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user test question result
	* @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	*/
	public UserTestQuestionResult[] findByQuestionId_PrevAndNext(
		long userTestQuestionResultId, long questionId,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException;

	/**
	* Removes all the user test question results where questionId = &#63; from the database.
	*
	* @param questionId the question ID
	*/
	public void removeByQuestionId(long questionId);

	/**
	* Returns the number of user test question results where questionId = &#63;.
	*
	* @param questionId the question ID
	* @return the number of matching user test question results
	*/
	public int countByQuestionId(long questionId);

	/**
	* Returns all the user test question results where questionId = &#63; and isCorrect = &#63;.
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @return the matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByQuestionIdAndStatus(
		long questionId, boolean isCorrect);

	/**
	* Returns a range of all the user test question results where questionId = &#63; and isCorrect = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @return the range of matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByQuestionIdAndStatus(
		long questionId, boolean isCorrect, int start, int end);

	/**
	* Returns an ordered range of all the user test question results where questionId = &#63; and isCorrect = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByQuestionIdAndStatus(
		long questionId, boolean isCorrect, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator);

	/**
	* Returns an ordered range of all the user test question results where questionId = &#63; and isCorrect = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByQuestionIdAndStatus(
		long questionId, boolean isCorrect, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first user test question result in the ordered set where questionId = &#63; and isCorrect = &#63;.
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public UserTestQuestionResult findByQuestionIdAndStatus_First(
		long questionId, boolean isCorrect,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException;

	/**
	* Returns the first user test question result in the ordered set where questionId = &#63; and isCorrect = &#63;.
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public UserTestQuestionResult fetchByQuestionIdAndStatus_First(
		long questionId, boolean isCorrect,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator);

	/**
	* Returns the last user test question result in the ordered set where questionId = &#63; and isCorrect = &#63;.
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public UserTestQuestionResult findByQuestionIdAndStatus_Last(
		long questionId, boolean isCorrect,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException;

	/**
	* Returns the last user test question result in the ordered set where questionId = &#63; and isCorrect = &#63;.
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public UserTestQuestionResult fetchByQuestionIdAndStatus_Last(
		long questionId, boolean isCorrect,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator);

	/**
	* Returns the user test question results before and after the current user test question result in the ordered set where questionId = &#63; and isCorrect = &#63;.
	*
	* @param userTestQuestionResultId the primary key of the current user test question result
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user test question result
	* @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	*/
	public UserTestQuestionResult[] findByQuestionIdAndStatus_PrevAndNext(
		long userTestQuestionResultId, long questionId, boolean isCorrect,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException;

	/**
	* Removes all the user test question results where questionId = &#63; and isCorrect = &#63; from the database.
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	*/
	public void removeByQuestionIdAndStatus(long questionId, boolean isCorrect);

	/**
	* Returns the number of user test question results where questionId = &#63; and isCorrect = &#63;.
	*
	* @param questionId the question ID
	* @param isCorrect the is correct
	* @return the number of matching user test question results
	*/
	public int countByQuestionIdAndStatus(long questionId, boolean isCorrect);

	/**
	* Returns all the user test question results where userId = &#63; and tokenData = &#63;.
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @return the matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByUserIdAndTokenDate(
		long userId, java.lang.String tokenData);

	/**
	* Returns a range of all the user test question results where userId = &#63; and tokenData = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @return the range of matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByUserIdAndTokenDate(
		long userId, java.lang.String tokenData, int start, int end);

	/**
	* Returns an ordered range of all the user test question results where userId = &#63; and tokenData = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByUserIdAndTokenDate(
		long userId, java.lang.String tokenData, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator);

	/**
	* Returns an ordered range of all the user test question results where userId = &#63; and tokenData = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user test question results
	*/
	public java.util.List<UserTestQuestionResult> findByUserIdAndTokenDate(
		long userId, java.lang.String tokenData, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first user test question result in the ordered set where userId = &#63; and tokenData = &#63;.
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public UserTestQuestionResult findByUserIdAndTokenDate_First(long userId,
		java.lang.String tokenData,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException;

	/**
	* Returns the first user test question result in the ordered set where userId = &#63; and tokenData = &#63;.
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public UserTestQuestionResult fetchByUserIdAndTokenDate_First(long userId,
		java.lang.String tokenData,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator);

	/**
	* Returns the last user test question result in the ordered set where userId = &#63; and tokenData = &#63;.
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result
	* @throws NoSuchUserTestQuestionResultException if a matching user test question result could not be found
	*/
	public UserTestQuestionResult findByUserIdAndTokenDate_Last(long userId,
		java.lang.String tokenData,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException;

	/**
	* Returns the last user test question result in the ordered set where userId = &#63; and tokenData = &#63;.
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user test question result, or <code>null</code> if a matching user test question result could not be found
	*/
	public UserTestQuestionResult fetchByUserIdAndTokenDate_Last(long userId,
		java.lang.String tokenData,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator);

	/**
	* Returns the user test question results before and after the current user test question result in the ordered set where userId = &#63; and tokenData = &#63;.
	*
	* @param userTestQuestionResultId the primary key of the current user test question result
	* @param userId the user ID
	* @param tokenData the token data
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user test question result
	* @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	*/
	public UserTestQuestionResult[] findByUserIdAndTokenDate_PrevAndNext(
		long userTestQuestionResultId, long userId, java.lang.String tokenData,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator)
		throws NoSuchUserTestQuestionResultException;

	/**
	* Removes all the user test question results where userId = &#63; and tokenData = &#63; from the database.
	*
	* @param userId the user ID
	* @param tokenData the token data
	*/
	public void removeByUserIdAndTokenDate(long userId,
		java.lang.String tokenData);

	/**
	* Returns the number of user test question results where userId = &#63; and tokenData = &#63;.
	*
	* @param userId the user ID
	* @param tokenData the token data
	* @return the number of matching user test question results
	*/
	public int countByUserIdAndTokenDate(long userId, java.lang.String tokenData);

	/**
	* Caches the user test question result in the entity cache if it is enabled.
	*
	* @param userTestQuestionResult the user test question result
	*/
	public void cacheResult(UserTestQuestionResult userTestQuestionResult);

	/**
	* Caches the user test question results in the entity cache if it is enabled.
	*
	* @param userTestQuestionResults the user test question results
	*/
	public void cacheResult(
		java.util.List<UserTestQuestionResult> userTestQuestionResults);

	/**
	* Creates a new user test question result with the primary key. Does not add the user test question result to the database.
	*
	* @param userTestQuestionResultId the primary key for the new user test question result
	* @return the new user test question result
	*/
	public UserTestQuestionResult create(long userTestQuestionResultId);

	/**
	* Removes the user test question result with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userTestQuestionResultId the primary key of the user test question result
	* @return the user test question result that was removed
	* @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	*/
	public UserTestQuestionResult remove(long userTestQuestionResultId)
		throws NoSuchUserTestQuestionResultException;

	public UserTestQuestionResult updateImpl(
		UserTestQuestionResult userTestQuestionResult);

	/**
	* Returns the user test question result with the primary key or throws a {@link NoSuchUserTestQuestionResultException} if it could not be found.
	*
	* @param userTestQuestionResultId the primary key of the user test question result
	* @return the user test question result
	* @throws NoSuchUserTestQuestionResultException if a user test question result with the primary key could not be found
	*/
	public UserTestQuestionResult findByPrimaryKey(
		long userTestQuestionResultId)
		throws NoSuchUserTestQuestionResultException;

	/**
	* Returns the user test question result with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userTestQuestionResultId the primary key of the user test question result
	* @return the user test question result, or <code>null</code> if a user test question result with the primary key could not be found
	*/
	public UserTestQuestionResult fetchByPrimaryKey(
		long userTestQuestionResultId);

	@Override
	public java.util.Map<java.io.Serializable, UserTestQuestionResult> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the user test question results.
	*
	* @return the user test question results
	*/
	public java.util.List<UserTestQuestionResult> findAll();

	/**
	* Returns a range of all the user test question results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @return the range of user test question results
	*/
	public java.util.List<UserTestQuestionResult> findAll(int start, int end);

	/**
	* Returns an ordered range of all the user test question results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user test question results
	*/
	public java.util.List<UserTestQuestionResult> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator);

	/**
	* Returns an ordered range of all the user test question results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of user test question results
	*/
	public java.util.List<UserTestQuestionResult> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserTestQuestionResult> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the user test question results from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of user test question results.
	*
	* @return the number of user test question results
	*/
	public int countAll();
}