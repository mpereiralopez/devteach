/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SubcategoryLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see SubcategoryLocalService
 * @generated
 */
@ProviderType
public class SubcategoryLocalServiceWrapper implements SubcategoryLocalService,
	ServiceWrapper<SubcategoryLocalService> {
	public SubcategoryLocalServiceWrapper(
		SubcategoryLocalService subcategoryLocalService) {
		_subcategoryLocalService = subcategoryLocalService;
	}

	/**
	* Adds the subcategory to the database. Also notifies the appropriate model listeners.
	*
	* @param subcategory the subcategory
	* @return the subcategory that was added
	*/
	@Override
	public com.devshaker.model.Subcategory addSubcategory(
		com.devshaker.model.Subcategory subcategory) {
		return _subcategoryLocalService.addSubcategory(subcategory);
	}

	/**
	* Creates a new subcategory with the primary key. Does not add the subcategory to the database.
	*
	* @param subcategoryId the primary key for the new subcategory
	* @return the new subcategory
	*/
	@Override
	public com.devshaker.model.Subcategory createSubcategory(long subcategoryId) {
		return _subcategoryLocalService.createSubcategory(subcategoryId);
	}

	/**
	* Deletes the subcategory from the database. Also notifies the appropriate model listeners.
	*
	* @param subcategory the subcategory
	* @return the subcategory that was removed
	*/
	@Override
	public com.devshaker.model.Subcategory deleteSubcategory(
		com.devshaker.model.Subcategory subcategory) {
		return _subcategoryLocalService.deleteSubcategory(subcategory);
	}

	/**
	* Deletes the subcategory with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param subcategoryId the primary key of the subcategory
	* @return the subcategory that was removed
	* @throws PortalException if a subcategory with the primary key could not be found
	*/
	@Override
	public com.devshaker.model.Subcategory deleteSubcategory(long subcategoryId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _subcategoryLocalService.deleteSubcategory(subcategoryId);
	}

	@Override
	public com.devshaker.model.Subcategory fetchSubcategory(long subcategoryId) {
		return _subcategoryLocalService.fetchSubcategory(subcategoryId);
	}

	/**
	* Returns the subcategory matching the UUID and group.
	*
	* @param uuid the subcategory's UUID
	* @param groupId the primary key of the group
	* @return the matching subcategory, or <code>null</code> if a matching subcategory could not be found
	*/
	@Override
	public com.devshaker.model.Subcategory fetchSubcategoryByUuidAndGroupId(
		java.lang.String uuid, long groupId) {
		return _subcategoryLocalService.fetchSubcategoryByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Returns the subcategory with the primary key.
	*
	* @param subcategoryId the primary key of the subcategory
	* @return the subcategory
	* @throws PortalException if a subcategory with the primary key could not be found
	*/
	@Override
	public com.devshaker.model.Subcategory getSubcategory(long subcategoryId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _subcategoryLocalService.getSubcategory(subcategoryId);
	}

	@Override
	public com.devshaker.model.Subcategory getSubcategoryByNameAndCategoryId(
		java.lang.String name, java.util.Locale locale, long categoryId) {
		return _subcategoryLocalService.getSubcategoryByNameAndCategoryId(name,
			locale, categoryId);
	}

	/**
	* Returns the subcategory matching the UUID and group.
	*
	* @param uuid the subcategory's UUID
	* @param groupId the primary key of the group
	* @return the matching subcategory
	* @throws PortalException if a matching subcategory could not be found
	*/
	@Override
	public com.devshaker.model.Subcategory getSubcategoryByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _subcategoryLocalService.getSubcategoryByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Updates the subcategory in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param subcategory the subcategory
	* @return the subcategory that was updated
	*/
	@Override
	public com.devshaker.model.Subcategory updateSubcategory(
		com.devshaker.model.Subcategory subcategory) {
		return _subcategoryLocalService.updateSubcategory(subcategory);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _subcategoryLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _subcategoryLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _subcategoryLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _subcategoryLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _subcategoryLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _subcategoryLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of subcategories.
	*
	* @return the number of subcategories
	*/
	@Override
	public int getSubcategoriesCount() {
		return _subcategoryLocalService.getSubcategoriesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _subcategoryLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _subcategoryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _subcategoryLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _subcategoryLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the subcategories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.SubcategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @return the range of subcategories
	*/
	@Override
	public java.util.List<com.devshaker.model.Subcategory> getSubcategories(
		int start, int end) {
		return _subcategoryLocalService.getSubcategories(start, end);
	}

	/**
	* Returns all the subcategories matching the UUID and company.
	*
	* @param uuid the UUID of the subcategories
	* @param companyId the primary key of the company
	* @return the matching subcategories, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<com.devshaker.model.Subcategory> getSubcategoriesByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return _subcategoryLocalService.getSubcategoriesByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns a range of subcategories matching the UUID and company.
	*
	* @param uuid the UUID of the subcategories
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of subcategories
	* @param end the upper bound of the range of subcategories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching subcategories, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<com.devshaker.model.Subcategory> getSubcategoriesByUuidAndCompanyId(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<com.devshaker.model.Subcategory> orderByComparator) {
		return _subcategoryLocalService.getSubcategoriesByUuidAndCompanyId(uuid,
			companyId, start, end, orderByComparator);
	}

	@Override
	public java.util.List<com.devshaker.model.Subcategory> getSubcategoriesOfCategory(
		long categoryId) {
		return _subcategoryLocalService.getSubcategoriesOfCategory(categoryId);
	}

	@Override
	public java.util.List<com.devshaker.model.Subcategory> getSubcategoriesOfCompany(
		long companyId) {
		return _subcategoryLocalService.getSubcategoriesOfCompany(companyId);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _subcategoryLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _subcategoryLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public SubcategoryLocalService getWrappedService() {
		return _subcategoryLocalService;
	}

	@Override
	public void setWrappedService(
		SubcategoryLocalService subcategoryLocalService) {
		_subcategoryLocalService = subcategoryLocalService;
	}

	private SubcategoryLocalService _subcategoryLocalService;
}