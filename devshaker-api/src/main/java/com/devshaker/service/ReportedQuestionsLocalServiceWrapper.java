/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ReportedQuestionsLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ReportedQuestionsLocalService
 * @generated
 */
@ProviderType
public class ReportedQuestionsLocalServiceWrapper
	implements ReportedQuestionsLocalService,
		ServiceWrapper<ReportedQuestionsLocalService> {
	public ReportedQuestionsLocalServiceWrapper(
		ReportedQuestionsLocalService reportedQuestionsLocalService) {
		_reportedQuestionsLocalService = reportedQuestionsLocalService;
	}

	/**
	* Adds the reported questions to the database. Also notifies the appropriate model listeners.
	*
	* @param reportedQuestions the reported questions
	* @return the reported questions that was added
	*/
	@Override
	public com.devshaker.model.ReportedQuestions addReportedQuestions(
		com.devshaker.model.ReportedQuestions reportedQuestions) {
		return _reportedQuestionsLocalService.addReportedQuestions(reportedQuestions);
	}

	/**
	* Creates a new reported questions with the primary key. Does not add the reported questions to the database.
	*
	* @param reportId the primary key for the new reported questions
	* @return the new reported questions
	*/
	@Override
	public com.devshaker.model.ReportedQuestions createReportedQuestions(
		long reportId) {
		return _reportedQuestionsLocalService.createReportedQuestions(reportId);
	}

	/**
	* Deletes the reported questions from the database. Also notifies the appropriate model listeners.
	*
	* @param reportedQuestions the reported questions
	* @return the reported questions that was removed
	*/
	@Override
	public com.devshaker.model.ReportedQuestions deleteReportedQuestions(
		com.devshaker.model.ReportedQuestions reportedQuestions) {
		return _reportedQuestionsLocalService.deleteReportedQuestions(reportedQuestions);
	}

	/**
	* Deletes the reported questions with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param reportId the primary key of the reported questions
	* @return the reported questions that was removed
	* @throws PortalException if a reported questions with the primary key could not be found
	*/
	@Override
	public com.devshaker.model.ReportedQuestions deleteReportedQuestions(
		long reportId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _reportedQuestionsLocalService.deleteReportedQuestions(reportId);
	}

	@Override
	public com.devshaker.model.ReportedQuestions fetchReportedQuestions(
		long reportId) {
		return _reportedQuestionsLocalService.fetchReportedQuestions(reportId);
	}

	/**
	* Returns the reported questions with the primary key.
	*
	* @param reportId the primary key of the reported questions
	* @return the reported questions
	* @throws PortalException if a reported questions with the primary key could not be found
	*/
	@Override
	public com.devshaker.model.ReportedQuestions getReportedQuestions(
		long reportId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _reportedQuestionsLocalService.getReportedQuestions(reportId);
	}

	/**
	* Updates the reported questions in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param reportedQuestions the reported questions
	* @return the reported questions that was updated
	*/
	@Override
	public com.devshaker.model.ReportedQuestions updateReportedQuestions(
		com.devshaker.model.ReportedQuestions reportedQuestions) {
		return _reportedQuestionsLocalService.updateReportedQuestions(reportedQuestions);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _reportedQuestionsLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _reportedQuestionsLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _reportedQuestionsLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _reportedQuestionsLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _reportedQuestionsLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of reported questionses.
	*
	* @return the number of reported questionses
	*/
	@Override
	public int getReportedQuestionsesCount() {
		return _reportedQuestionsLocalService.getReportedQuestionsesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _reportedQuestionsLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _reportedQuestionsLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _reportedQuestionsLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _reportedQuestionsLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns a range of all the reported questionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.ReportedQuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reported questionses
	* @param end the upper bound of the range of reported questionses (not inclusive)
	* @return the range of reported questionses
	*/
	@Override
	public java.util.List<com.devshaker.model.ReportedQuestions> getReportedQuestionses(
		int start, int end) {
		return _reportedQuestionsLocalService.getReportedQuestionses(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _reportedQuestionsLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _reportedQuestionsLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public ReportedQuestionsLocalService getWrappedService() {
		return _reportedQuestionsLocalService;
	}

	@Override
	public void setWrappedService(
		ReportedQuestionsLocalService reportedQuestionsLocalService) {
		_reportedQuestionsLocalService = reportedQuestionsLocalService;
	}

	private ReportedQuestionsLocalService _reportedQuestionsLocalService;
}