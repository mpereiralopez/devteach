/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ApiService}.
 *
 * @author Brian Wing Shun Chan
 * @see ApiService
 * @generated
 */
@ProviderType
public class ApiServiceWrapper implements ApiService,
	ServiceWrapper<ApiService> {
	public ApiServiceWrapper(ApiService apiService) {
		_apiService = apiService;
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getUserExtraDataInformation(
		long userId) {
		return _apiService.getUserExtraDataInformation(userId);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject reportQuestion(
		long questionId, java.lang.String reason) {
		return _apiService.reportQuestion(questionId, reason);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject sendTestResultOfUser(
		java.lang.String jsonResponseStringify) {
		return _apiService.sendTestResultOfUser(jsonResponseStringify);
	}

	@Override
	public com.liferay.portal.kernel.model.User updateUserInfo(long userId,
		java.lang.String screenName, java.lang.String firstName,
		java.lang.String emailAddress, java.lang.String phone, int year,
		int month, int day, java.lang.String country) {
		return _apiService.updateUserInfo(userId, screenName, firstName,
			emailAddress, phone, year, month, day, country);
	}

	@Override
	public java.lang.String getApiVersion() {
		return _apiService.getApiVersion();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _apiService.getOSGiServiceIdentifier();
	}

	@Override
	public java.util.List<com.liferay.portal.kernel.json.JSONObject> getCategoriesOfCompany() {
		return _apiService.getCategoriesOfCompany();
	}

	@Override
	public java.util.List<com.liferay.portal.kernel.json.JSONObject> getLevelOfSubcategory(
		long subcategoryId) {
		return _apiService.getLevelOfSubcategory(subcategoryId);
	}

	@Override
	public java.util.List<com.liferay.portal.kernel.json.JSONObject> getSubCategoriesOfCategory(
		long categoryId) {
		return _apiService.getSubCategoriesOfCategory(categoryId);
	}

	@Override
	public java.util.List<com.liferay.portal.kernel.json.JSONObject> getTestQuestionOfLevel(
		long levelId) {
		return _apiService.getTestQuestionOfLevel(levelId);
	}

	@Override
	public ApiService getWrappedService() {
		return _apiService;
	}

	@Override
	public void setWrappedService(ApiService apiService) {
		_apiService = apiService;
	}

	private ApiService _apiService;
}