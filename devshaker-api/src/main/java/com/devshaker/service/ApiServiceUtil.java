/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the remote service utility for Api. This utility wraps
 * {@link com.devshaker.service.impl.ApiServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see ApiService
 * @see com.devshaker.service.base.ApiServiceBaseImpl
 * @see com.devshaker.service.impl.ApiServiceImpl
 * @generated
 */
@ProviderType
public class ApiServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.devshaker.service.impl.ApiServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.liferay.portal.kernel.json.JSONObject getUserExtraDataInformation(
		long userId) {
		return getService().getUserExtraDataInformation(userId);
	}

	public static com.liferay.portal.kernel.json.JSONObject reportQuestion(
		long questionId, java.lang.String reason) {
		return getService().reportQuestion(questionId, reason);
	}

	public static com.liferay.portal.kernel.json.JSONObject sendTestResultOfUser(
		java.lang.String jsonResponseStringify) {
		return getService().sendTestResultOfUser(jsonResponseStringify);
	}

	public static com.liferay.portal.kernel.model.User updateUserInfo(
		long userId, java.lang.String screenName, java.lang.String firstName,
		java.lang.String emailAddress, java.lang.String phone, int year,
		int month, int day, java.lang.String country) {
		return getService()
				   .updateUserInfo(userId, screenName, firstName, emailAddress,
			phone, year, month, day, country);
	}

	public static java.lang.String getApiVersion() {
		return getService().getApiVersion();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static java.util.List<com.liferay.portal.kernel.json.JSONObject> getCategoriesOfCompany() {
		return getService().getCategoriesOfCompany();
	}

	public static java.util.List<com.liferay.portal.kernel.json.JSONObject> getLevelOfSubcategory(
		long subcategoryId) {
		return getService().getLevelOfSubcategory(subcategoryId);
	}

	public static java.util.List<com.liferay.portal.kernel.json.JSONObject> getSubCategoriesOfCategory(
		long categoryId) {
		return getService().getSubCategoriesOfCategory(categoryId);
	}

	public static java.util.List<com.liferay.portal.kernel.json.JSONObject> getTestQuestionOfLevel(
		long levelId) {
		return getService().getTestQuestionOfLevel(levelId);
	}

	public static ApiService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ApiService, ApiService> _serviceTracker = ServiceTrackerFactory.open(ApiService.class);
}