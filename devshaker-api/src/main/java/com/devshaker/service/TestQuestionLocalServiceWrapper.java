/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link TestQuestionLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see TestQuestionLocalService
 * @generated
 */
@ProviderType
public class TestQuestionLocalServiceWrapper implements TestQuestionLocalService,
	ServiceWrapper<TestQuestionLocalService> {
	public TestQuestionLocalServiceWrapper(
		TestQuestionLocalService testQuestionLocalService) {
		_testQuestionLocalService = testQuestionLocalService;
	}

	/**
	* Adds the test question to the database. Also notifies the appropriate model listeners.
	*
	* @param testQuestion the test question
	* @return the test question that was added
	*/
	@Override
	public com.devshaker.model.TestQuestion addTestQuestion(
		com.devshaker.model.TestQuestion testQuestion) {
		return _testQuestionLocalService.addTestQuestion(testQuestion);
	}

	/**
	* Creates a new test question with the primary key. Does not add the test question to the database.
	*
	* @param questionId the primary key for the new test question
	* @return the new test question
	*/
	@Override
	public com.devshaker.model.TestQuestion createTestQuestion(long questionId) {
		return _testQuestionLocalService.createTestQuestion(questionId);
	}

	/**
	* Deletes the test question from the database. Also notifies the appropriate model listeners.
	*
	* @param testQuestion the test question
	* @return the test question that was removed
	*/
	@Override
	public com.devshaker.model.TestQuestion deleteTestQuestion(
		com.devshaker.model.TestQuestion testQuestion) {
		return _testQuestionLocalService.deleteTestQuestion(testQuestion);
	}

	/**
	* Deletes the test question with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param questionId the primary key of the test question
	* @return the test question that was removed
	* @throws PortalException if a test question with the primary key could not be found
	*/
	@Override
	public com.devshaker.model.TestQuestion deleteTestQuestion(long questionId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _testQuestionLocalService.deleteTestQuestion(questionId);
	}

	@Override
	public com.devshaker.model.TestQuestion fetchTestQuestion(long questionId) {
		return _testQuestionLocalService.fetchTestQuestion(questionId);
	}

	/**
	* Returns the test question with the primary key.
	*
	* @param questionId the primary key of the test question
	* @return the test question
	* @throws PortalException if a test question with the primary key could not be found
	*/
	@Override
	public com.devshaker.model.TestQuestion getTestQuestion(long questionId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _testQuestionLocalService.getTestQuestion(questionId);
	}

	/**
	* Updates the test question in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param testQuestion the test question
	* @return the test question that was updated
	*/
	@Override
	public com.devshaker.model.TestQuestion updateTestQuestion(
		com.devshaker.model.TestQuestion testQuestion) {
		return _testQuestionLocalService.updateTestQuestion(testQuestion);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _testQuestionLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _testQuestionLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _testQuestionLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _testQuestionLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _testQuestionLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of test questions.
	*
	* @return the number of test questions
	*/
	@Override
	public int getTestQuestionsCount() {
		return _testQuestionLocalService.getTestQuestionsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _testQuestionLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _testQuestionLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _testQuestionLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _testQuestionLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the test questions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.TestQuestionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of test questions
	* @param end the upper bound of the range of test questions (not inclusive)
	* @return the range of test questions
	*/
	@Override
	public java.util.List<com.devshaker.model.TestQuestion> getTestQuestions(
		int start, int end) {
		return _testQuestionLocalService.getTestQuestions(start, end);
	}

	@Override
	public java.util.List<com.devshaker.model.TestQuestion> getTestQuestionsByCategoryId(
		long categoryId) {
		return _testQuestionLocalService.getTestQuestionsByCategoryId(categoryId);
	}

	@Override
	public java.util.List<com.devshaker.model.TestQuestion> getTestQuestionsByCompanyId(
		long companyId) {
		return _testQuestionLocalService.getTestQuestionsByCompanyId(companyId);
	}

	@Override
	public java.util.List<com.devshaker.model.TestQuestion> getTestQuestionsBySubcategoryId(
		long subcategoryId) {
		return _testQuestionLocalService.getTestQuestionsBySubcategoryId(subcategoryId);
	}

	@Override
	public java.util.List<com.devshaker.model.TestQuestion> getTestQuestionsBylevelId(
		long levelId) {
		return _testQuestionLocalService.getTestQuestionsBylevelId(levelId);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _testQuestionLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _testQuestionLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public TestQuestionLocalService getWrappedService() {
		return _testQuestionLocalService;
	}

	@Override
	public void setWrappedService(
		TestQuestionLocalService testQuestionLocalService) {
		_testQuestionLocalService = testQuestionLocalService;
	}

	private TestQuestionLocalService _testQuestionLocalService;
}