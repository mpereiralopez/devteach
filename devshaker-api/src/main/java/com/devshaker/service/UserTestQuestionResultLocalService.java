/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service;

import aQute.bnd.annotation.ProviderType;

import com.devshaker.model.TestQuestion;
import com.devshaker.model.UserTestQuestionResult;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service interface for UserTestQuestionResult. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see UserTestQuestionResultLocalServiceUtil
 * @see com.devshaker.service.base.UserTestQuestionResultLocalServiceBaseImpl
 * @see com.devshaker.service.impl.UserTestQuestionResultLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface UserTestQuestionResultLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UserTestQuestionResultLocalServiceUtil} to access the user test question result local service. Add custom service methods to {@link com.devshaker.service.impl.UserTestQuestionResultLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public TestQuestion getLastTestQuestionOfUser(long userId);

	/**
	* Adds the user test question result to the database. Also notifies the appropriate model listeners.
	*
	* @param userTestQuestionResult the user test question result
	* @return the user test question result that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public UserTestQuestionResult addUserTestQuestionResult(
		UserTestQuestionResult userTestQuestionResult);

	/**
	* Creates a new user test question result with the primary key. Does not add the user test question result to the database.
	*
	* @param userTestQuestionResultId the primary key for the new user test question result
	* @return the new user test question result
	*/
	public UserTestQuestionResult createUserTestQuestionResult(
		long userTestQuestionResultId);

	/**
	* Deletes the user test question result from the database. Also notifies the appropriate model listeners.
	*
	* @param userTestQuestionResult the user test question result
	* @return the user test question result that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public UserTestQuestionResult deleteUserTestQuestionResult(
		UserTestQuestionResult userTestQuestionResult);

	/**
	* Deletes the user test question result with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userTestQuestionResultId the primary key of the user test question result
	* @return the user test question result that was removed
	* @throws PortalException if a user test question result with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public UserTestQuestionResult deleteUserTestQuestionResult(
		long userTestQuestionResultId) throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public UserTestQuestionResult fetchUserTestQuestionResult(
		long userTestQuestionResultId);

	/**
	* Returns the user test question result with the primary key.
	*
	* @param userTestQuestionResultId the primary key of the user test question result
	* @return the user test question result
	* @throws PortalException if a user test question result with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public UserTestQuestionResult getUserTestQuestionResult(
		long userTestQuestionResultId) throws PortalException;

	/**
	* Updates the user test question result in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param userTestQuestionResult the user test question result
	* @return the user test question result that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public UserTestQuestionResult updateUserTestQuestionResult(
		UserTestQuestionResult userTestQuestionResult);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	public DynamicQuery dynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getAccumulatedCorrectAnswersOfUserBetweenLevels(long userId,
		long subcategoryId, int levelBottomLimit, int levelTopLimit)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getQuestionTimesFailed(long questionId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getQuestionTimesPassed(long questionId);

	/**
	* Returns the number of user test question results.
	*
	* @return the number of user test question results
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getUserTestQuestionResultsCount();

	public int levelsInSameSession(long userId, java.lang.String tokenData);

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<UserTestQuestionResult> getTestQuestionResults();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<UserTestQuestionResult> getTestQuestionResultsOfCategory(
		long categoryId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<UserTestQuestionResult> getTestQuestionResultsOfLevel(
		long levelId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<UserTestQuestionResult> getTestQuestionResultsOfSubcategory(
		long subcategoryId);

	/**
	* Returns a range of all the user test question results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @return the range of user test question results
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<UserTestQuestionResult> getUserTestQuestionResults(int start,
		int end);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);
}