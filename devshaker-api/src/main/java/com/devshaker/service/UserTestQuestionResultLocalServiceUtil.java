/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for UserTestQuestionResult. This utility wraps
 * {@link com.devshaker.service.impl.UserTestQuestionResultLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see UserTestQuestionResultLocalService
 * @see com.devshaker.service.base.UserTestQuestionResultLocalServiceBaseImpl
 * @see com.devshaker.service.impl.UserTestQuestionResultLocalServiceImpl
 * @generated
 */
@ProviderType
public class UserTestQuestionResultLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.devshaker.service.impl.UserTestQuestionResultLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.devshaker.model.TestQuestion getLastTestQuestionOfUser(
		long userId) {
		return getService().getLastTestQuestionOfUser(userId);
	}

	/**
	* Adds the user test question result to the database. Also notifies the appropriate model listeners.
	*
	* @param userTestQuestionResult the user test question result
	* @return the user test question result that was added
	*/
	public static com.devshaker.model.UserTestQuestionResult addUserTestQuestionResult(
		com.devshaker.model.UserTestQuestionResult userTestQuestionResult) {
		return getService().addUserTestQuestionResult(userTestQuestionResult);
	}

	/**
	* Creates a new user test question result with the primary key. Does not add the user test question result to the database.
	*
	* @param userTestQuestionResultId the primary key for the new user test question result
	* @return the new user test question result
	*/
	public static com.devshaker.model.UserTestQuestionResult createUserTestQuestionResult(
		long userTestQuestionResultId) {
		return getService()
				   .createUserTestQuestionResult(userTestQuestionResultId);
	}

	/**
	* Deletes the user test question result from the database. Also notifies the appropriate model listeners.
	*
	* @param userTestQuestionResult the user test question result
	* @return the user test question result that was removed
	*/
	public static com.devshaker.model.UserTestQuestionResult deleteUserTestQuestionResult(
		com.devshaker.model.UserTestQuestionResult userTestQuestionResult) {
		return getService().deleteUserTestQuestionResult(userTestQuestionResult);
	}

	/**
	* Deletes the user test question result with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userTestQuestionResultId the primary key of the user test question result
	* @return the user test question result that was removed
	* @throws PortalException if a user test question result with the primary key could not be found
	*/
	public static com.devshaker.model.UserTestQuestionResult deleteUserTestQuestionResult(
		long userTestQuestionResultId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .deleteUserTestQuestionResult(userTestQuestionResultId);
	}

	public static com.devshaker.model.UserTestQuestionResult fetchUserTestQuestionResult(
		long userTestQuestionResultId) {
		return getService().fetchUserTestQuestionResult(userTestQuestionResultId);
	}

	/**
	* Returns the user test question result with the primary key.
	*
	* @param userTestQuestionResultId the primary key of the user test question result
	* @return the user test question result
	* @throws PortalException if a user test question result with the primary key could not be found
	*/
	public static com.devshaker.model.UserTestQuestionResult getUserTestQuestionResult(
		long userTestQuestionResultId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getUserTestQuestionResult(userTestQuestionResultId);
	}

	/**
	* Updates the user test question result in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param userTestQuestionResult the user test question result
	* @return the user test question result that was updated
	*/
	public static com.devshaker.model.UserTestQuestionResult updateUserTestQuestionResult(
		com.devshaker.model.UserTestQuestionResult userTestQuestionResult) {
		return getService().updateUserTestQuestionResult(userTestQuestionResult);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	public static int getAccumulatedCorrectAnswersOfUserBetweenLevels(
		long userId, long subcategoryId, int levelBottomLimit, int levelTopLimit)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .getAccumulatedCorrectAnswersOfUserBetweenLevels(userId,
			subcategoryId, levelBottomLimit, levelTopLimit);
	}

	public static int getQuestionTimesFailed(long questionId) {
		return getService().getQuestionTimesFailed(questionId);
	}

	public static int getQuestionTimesPassed(long questionId) {
		return getService().getQuestionTimesPassed(questionId);
	}

	/**
	* Returns the number of user test question results.
	*
	* @return the number of user test question results
	*/
	public static int getUserTestQuestionResultsCount() {
		return getService().getUserTestQuestionResultsCount();
	}

	public static int levelsInSameSession(long userId,
		java.lang.String tokenData) {
		return getService().levelsInSameSession(userId, tokenData);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	public static java.util.List<com.devshaker.model.UserTestQuestionResult> getTestQuestionResults() {
		return getService().getTestQuestionResults();
	}

	public static java.util.List<com.devshaker.model.UserTestQuestionResult> getTestQuestionResultsOfCategory(
		long categoryId) {
		return getService().getTestQuestionResultsOfCategory(categoryId);
	}

	public static java.util.List<com.devshaker.model.UserTestQuestionResult> getTestQuestionResultsOfLevel(
		long levelId) {
		return getService().getTestQuestionResultsOfLevel(levelId);
	}

	public static java.util.List<com.devshaker.model.UserTestQuestionResult> getTestQuestionResultsOfSubcategory(
		long subcategoryId) {
		return getService().getTestQuestionResultsOfSubcategory(subcategoryId);
	}

	/**
	* Returns a range of all the user test question results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserTestQuestionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user test question results
	* @param end the upper bound of the range of user test question results (not inclusive)
	* @return the range of user test question results
	*/
	public static java.util.List<com.devshaker.model.UserTestQuestionResult> getUserTestQuestionResults(
		int start, int end) {
		return getService().getUserTestQuestionResults(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static UserTestQuestionResultLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<UserTestQuestionResultLocalService, UserTestQuestionResultLocalService> _serviceTracker =
		ServiceTrackerFactory.open(UserTestQuestionResultLocalService.class);
}