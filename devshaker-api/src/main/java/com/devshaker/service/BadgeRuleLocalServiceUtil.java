/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for BadgeRule. This utility wraps
 * {@link com.devshaker.service.impl.BadgeRuleLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see BadgeRuleLocalService
 * @see com.devshaker.service.base.BadgeRuleLocalServiceBaseImpl
 * @see com.devshaker.service.impl.BadgeRuleLocalServiceImpl
 * @generated
 */
@ProviderType
public class BadgeRuleLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.devshaker.service.impl.BadgeRuleLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the badge rule to the database. Also notifies the appropriate model listeners.
	*
	* @param badgeRule the badge rule
	* @return the badge rule that was added
	*/
	public static com.devshaker.model.BadgeRule addBadgeRule(
		com.devshaker.model.BadgeRule badgeRule) {
		return getService().addBadgeRule(badgeRule);
	}

	/**
	* Creates a new badge rule with the primary key. Does not add the badge rule to the database.
	*
	* @param badgeRulePK the primary key for the new badge rule
	* @return the new badge rule
	*/
	public static com.devshaker.model.BadgeRule createBadgeRule(
		com.devshaker.service.persistence.BadgeRulePK badgeRulePK) {
		return getService().createBadgeRule(badgeRulePK);
	}

	/**
	* Deletes the badge rule from the database. Also notifies the appropriate model listeners.
	*
	* @param badgeRule the badge rule
	* @return the badge rule that was removed
	*/
	public static com.devshaker.model.BadgeRule deleteBadgeRule(
		com.devshaker.model.BadgeRule badgeRule) {
		return getService().deleteBadgeRule(badgeRule);
	}

	/**
	* Deletes the badge rule with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param badgeRulePK the primary key of the badge rule
	* @return the badge rule that was removed
	* @throws PortalException if a badge rule with the primary key could not be found
	*/
	public static com.devshaker.model.BadgeRule deleteBadgeRule(
		com.devshaker.service.persistence.BadgeRulePK badgeRulePK)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteBadgeRule(badgeRulePK);
	}

	public static com.devshaker.model.BadgeRule fetchBadgeRule(
		com.devshaker.service.persistence.BadgeRulePK badgeRulePK) {
		return getService().fetchBadgeRule(badgeRulePK);
	}

	/**
	* Returns the badge rule with the primary key.
	*
	* @param badgeRulePK the primary key of the badge rule
	* @return the badge rule
	* @throws PortalException if a badge rule with the primary key could not be found
	*/
	public static com.devshaker.model.BadgeRule getBadgeRule(
		com.devshaker.service.persistence.BadgeRulePK badgeRulePK)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getBadgeRule(badgeRulePK);
	}

	public static com.devshaker.model.BadgeRule getBadgeRuleByBadgeIdAndRuleId(
		long ruleId, long badgeId) {
		return getService().getBadgeRuleByBadgeIdAndRuleId(ruleId, badgeId);
	}

	/**
	* Updates the badge rule in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param badgeRule the badge rule
	* @return the badge rule that was updated
	*/
	public static com.devshaker.model.BadgeRule updateBadgeRule(
		com.devshaker.model.BadgeRule badgeRule) {
		return getService().updateBadgeRule(badgeRule);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of badge rules.
	*
	* @return the number of badge rules
	*/
	public static int getBadgeRulesCount() {
		return getService().getBadgeRulesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	public static java.util.List<com.devshaker.model.BadgeRule> getBadgeRuleByCompanyId(
		long companyId) {
		return getService().getBadgeRuleByCompanyId(companyId);
	}

	/**
	* Returns a range of all the badge rules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.BadgeRuleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of badge rules
	* @param end the upper bound of the range of badge rules (not inclusive)
	* @return the range of badge rules
	*/
	public static java.util.List<com.devshaker.model.BadgeRule> getBadgeRules(
		int start, int end) {
		return getService().getBadgeRules(start, end);
	}

	public static java.util.List<com.devshaker.model.Rule> getRulesOfBadge(
		long badgeId) {
		return getService().getRulesOfBadge(badgeId);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static BadgeRuleLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<BadgeRuleLocalService, BadgeRuleLocalService> _serviceTracker =
		ServiceTrackerFactory.open(BadgeRuleLocalService.class);
}