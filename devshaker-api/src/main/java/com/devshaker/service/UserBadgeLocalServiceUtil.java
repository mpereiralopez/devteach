/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for UserBadge. This utility wraps
 * {@link com.devshaker.service.impl.UserBadgeLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see UserBadgeLocalService
 * @see com.devshaker.service.base.UserBadgeLocalServiceBaseImpl
 * @see com.devshaker.service.impl.UserBadgeLocalServiceImpl
 * @generated
 */
@ProviderType
public class UserBadgeLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.devshaker.service.impl.UserBadgeLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the user badge to the database. Also notifies the appropriate model listeners.
	*
	* @param userBadge the user badge
	* @return the user badge that was added
	*/
	public static com.devshaker.model.UserBadge addUserBadge(
		com.devshaker.model.UserBadge userBadge) {
		return getService().addUserBadge(userBadge);
	}

	/**
	* Creates a new user badge with the primary key. Does not add the user badge to the database.
	*
	* @param userBadgePK the primary key for the new user badge
	* @return the new user badge
	*/
	public static com.devshaker.model.UserBadge createUserBadge(
		com.devshaker.service.persistence.UserBadgePK userBadgePK) {
		return getService().createUserBadge(userBadgePK);
	}

	/**
	* Deletes the user badge from the database. Also notifies the appropriate model listeners.
	*
	* @param userBadge the user badge
	* @return the user badge that was removed
	*/
	public static com.devshaker.model.UserBadge deleteUserBadge(
		com.devshaker.model.UserBadge userBadge) {
		return getService().deleteUserBadge(userBadge);
	}

	/**
	* Deletes the user badge with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userBadgePK the primary key of the user badge
	* @return the user badge that was removed
	* @throws PortalException if a user badge with the primary key could not be found
	*/
	public static com.devshaker.model.UserBadge deleteUserBadge(
		com.devshaker.service.persistence.UserBadgePK userBadgePK)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteUserBadge(userBadgePK);
	}

	public static com.devshaker.model.UserBadge fetchUserBadge(
		com.devshaker.service.persistence.UserBadgePK userBadgePK) {
		return getService().fetchUserBadge(userBadgePK);
	}

	/**
	* Returns the user badge with the primary key.
	*
	* @param userBadgePK the primary key of the user badge
	* @return the user badge
	* @throws PortalException if a user badge with the primary key could not be found
	*/
	public static com.devshaker.model.UserBadge getUserBadge(
		com.devshaker.service.persistence.UserBadgePK userBadgePK)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getUserBadge(userBadgePK);
	}

	/**
	* Updates the user badge in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param userBadge the user badge
	* @return the user badge that was updated
	*/
	public static com.devshaker.model.UserBadge updateUserBadge(
		com.devshaker.model.UserBadge userBadge) {
		return getService().updateUserBadge(userBadge);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of user badges.
	*
	* @return the number of user badges
	*/
	public static int getUserBadgesCount() {
		return getService().getUserBadgesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	public static java.util.List<com.devshaker.model.Badge> getBadgesObtainedByUserId(
		long userId) throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getBadgesObtainedByUserId(userId);
	}

	/**
	* Returns a range of all the user badges.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.devshaker.model.impl.UserBadgeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user badges
	* @param end the upper bound of the range of user badges (not inclusive)
	* @return the range of user badges
	*/
	public static java.util.List<com.devshaker.model.UserBadge> getUserBadges(
		int start, int end) {
		return getService().getUserBadges(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static UserBadgeLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<UserBadgeLocalService, UserBadgeLocalService> _serviceTracker =
		ServiceTrackerFactory.open(UserBadgeLocalService.class);
}