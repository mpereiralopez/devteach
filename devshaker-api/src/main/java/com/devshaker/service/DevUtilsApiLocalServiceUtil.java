/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for DevUtilsApi. This utility wraps
 * {@link com.devshaker.service.impl.DevUtilsApiLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see DevUtilsApiLocalService
 * @see com.devshaker.service.base.DevUtilsApiLocalServiceBaseImpl
 * @see com.devshaker.service.impl.DevUtilsApiLocalServiceImpl
 * @generated
 */
@ProviderType
public class DevUtilsApiLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.devshaker.service.impl.DevUtilsApiLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static boolean validateFileSize(java.io.File file) {
		return getService().validateFileSize(file);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static java.lang.String[] getConditionsArray() {
		return getService().getConditionsArray();
	}

	public static java.lang.String[] getMetricArray() {
		return getService().getMetricArray();
	}

	public static java.lang.String[] getPredicateArray() {
		return getService().getPredicateArray();
	}

	public static java.lang.String[] getQuantificatorArray() {
		return getService().getQuantificatorArray();
	}

	public static long createIGFolders(javax.portlet.PortletRequest request,
		long userId, long repositoryId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().createIGFolders(request, userId, repositoryId);
	}

	public static DevUtilsApiLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<DevUtilsApiLocalService, DevUtilsApiLocalService> _serviceTracker =
		ServiceTrackerFactory.open(DevUtilsApiLocalService.class);
}