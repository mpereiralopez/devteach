/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.devshaker.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DevUtilsApiLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see DevUtilsApiLocalService
 * @generated
 */
@ProviderType
public class DevUtilsApiLocalServiceWrapper implements DevUtilsApiLocalService,
	ServiceWrapper<DevUtilsApiLocalService> {
	public DevUtilsApiLocalServiceWrapper(
		DevUtilsApiLocalService devUtilsApiLocalService) {
		_devUtilsApiLocalService = devUtilsApiLocalService;
	}

	@Override
	public boolean validateFileSize(java.io.File file) {
		return _devUtilsApiLocalService.validateFileSize(file);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _devUtilsApiLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public java.lang.String[] getConditionsArray() {
		return _devUtilsApiLocalService.getConditionsArray();
	}

	@Override
	public java.lang.String[] getMetricArray() {
		return _devUtilsApiLocalService.getMetricArray();
	}

	@Override
	public java.lang.String[] getPredicateArray() {
		return _devUtilsApiLocalService.getPredicateArray();
	}

	@Override
	public java.lang.String[] getQuantificatorArray() {
		return _devUtilsApiLocalService.getQuantificatorArray();
	}

	@Override
	public long createIGFolders(javax.portlet.PortletRequest request,
		long userId, long repositoryId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _devUtilsApiLocalService.createIGFolders(request, userId,
			repositoryId);
	}

	@Override
	public DevUtilsApiLocalService getWrappedService() {
		return _devUtilsApiLocalService;
	}

	@Override
	public void setWrappedService(
		DevUtilsApiLocalService devUtilsApiLocalService) {
		_devUtilsApiLocalService = devUtilsApiLocalService;
	}

	private DevUtilsApiLocalService _devUtilsApiLocalService;
}