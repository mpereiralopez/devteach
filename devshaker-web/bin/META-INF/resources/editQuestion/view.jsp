<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.devshaker.model.TestQuestion"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.devshaker.model.Level"%>
<%@page import="com.liferay.document.library.kernel.service.DLAppLocalServiceUtil"%>
<%@page import="com.liferay.document.library.kernel.util.DLUtil"%>
<%@page import="com.liferay.portal.kernel.repository.model.FileEntry"%>
<%@page import="com.devshaker.model.Subcategory"%>
<%@page import="com.devshaker.model.Category"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.devshaker.service.LevelLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="com.devshaker.service.SubcategoryLocalServiceUtil"%>
<%@page import="com.devshaker.service.CategoryLocalServiceUtil"%>
<%@ include file="../init.jsp"%>
<%@ include file="/editQuestion/searchQuestion.jsp" %>

<%  
List<TestQuestion> testquestionlist = (List<TestQuestion>) request.getAttribute("testQuestionList");
PortletURL iteratorURL = renderResponse.createRenderURL();
iteratorURL.setParameter("jspPage", "/editQuestion/view.jsp");
if(testquestionlist!=null){
	session.setAttribute("testQuestionList",testquestionlist); 
}
%>
<portlet:renderURL var="newQuestionURL">
	<portlet:param name="jspPage" value="/editQuestion/editQuestion.jsp"></portlet:param>
	<portlet:param name="redirect" value="<%=PortalUtil.getCurrentURL(request)%>"></portlet:param>
</portlet:renderURL>

	<portlet:renderURL var="viewImportQuestions">   
		<portlet:param name="jspPage" value="/editQuestion/popup/import.jsp" />
		<portlet:param name="redirect" value="<%=PortalUtil.getCurrentURL(request)%>"></portlet:param>
    </portlet:renderURL>


<%SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");%>



<aui:button-row>
				<aui:button type="button" value="create" href="<%=newQuestionURL%>" ></aui:button>
				<aui:button type="button" value="import"  href="<%=viewImportQuestions%>"></aui:button>
</aui:button-row>


<liferay-ui:search-container delta="10" iteratorURL="<%=iteratorURL%>"
	emptyResultsMessage="no-questions-were-found">
	<liferay-ui:search-container-results>
	<% 	
	List<TestQuestion> tempResults = (List<TestQuestion>) session.getAttribute("testQuestionList");

	if(tempResults!=null){
			searchContainer.setTotal(tempResults.size());
			pageContext.setAttribute("total", searchContainer.getTotal());
			pageContext.setAttribute("results",  ListUtil.subList(tempResults, searchContainer.getStart(), searchContainer.getEnd()));
			iteratorURL.setParameter("cur",searchContainer.getCurParam());
	}
			%>
</liferay-ui:search-container-results>	
	<liferay-ui:search-container-row
		className="com.devshaker.model.TestQuestion" keyProperty="questionId"
		modelVar="question">
		
		<%
		Category cat = CategoryLocalServiceUtil.getCategory(question.getCategoryId());		
		FileEntry image_ = null;
		String thumbnailSRC = new String();
		String catValue = new String();
		if(cat.getIcon()!=0){
			image_=DLAppLocalServiceUtil.getFileEntry(cat.getIcon());
			thumbnailSRC =DLUtil.getThumbnailSrc(image_, image_.getFileVersion(), themeDisplay);
			catValue = "<div class='user-icon-square user-icon-xl' style='margin:auto'> <img alt='thumbnail' class='img-responsive img-rounded' src='"+thumbnailSRC+"'></div>"+cat.getName(themeDisplay.getLocale());
		}else{
			catValue = cat.getName(themeDisplay.getLocale());
		}
		
		Subcategory subcat = SubcategoryLocalServiceUtil.getSubcategory(question.getSubcategoryId());
		String sucatValue = new String();
		if(subcat.getSubcategoryIcon()!=0){
			image_=DLAppLocalServiceUtil.getFileEntry(subcat.getSubcategoryIcon());
			thumbnailSRC =DLUtil.getThumbnailSrc(image_, image_.getFileVersion(), themeDisplay);
			sucatValue = "<div class='user-icon-square user-icon-xl' style='margin:auto'> <img class='img-responsive img-rounded'  src='"+thumbnailSRC+"'> </div> "+subcat.getSubcategoryName(themeDisplay.getLocale());
		}else{
			sucatValue = subcat.getSubcategoryName(themeDisplay.getLocale());
		}
		
		Level level = LevelLocalServiceUtil.getLevel(question.getLevelId());
		String levelValue = new String();
		if(level.getLevelIcon()!=0){
			image_=DLAppLocalServiceUtil.getFileEntry(level.getLevelIcon());
			thumbnailSRC =DLUtil.getThumbnailSrc(image_, image_.getFileVersion(), themeDisplay);
			levelValue = "<div class='user-icon-square user-icon-xl' style='margin:auto'> <img class='img-responsive img-rounded'  src='"+thumbnailSRC+"'> </div> "+level.getLevelName(themeDisplay.getLocale());
		}else{
			levelValue = level.getLevelName(themeDisplay.getLocale());
		}
		  
		%>
		
		<liferay-ui:search-container-column-text name="category" align="center"
			value="<%=catValue%>" />
		<liferay-ui:search-container-column-text name="subcategory" align="center"
			value="<%= sucatValue%>" />

		<liferay-ui:search-container-column-text name="level" align="center"
			value="<%= levelValue%>" />

		<liferay-ui:search-container-column-text name="user"
			value="<%= question.getUserNameCreator()%>" />


		<%
			String textToShow = HtmlUtil.escape(question.getQuestionText(themeDisplay.getLocale()));
			if(textToShow.length()>50)
				textToShow = textToShow.substring(0, 47);
				textToShow = textToShow+"...";
		 %>
		<liferay-ui:search-container-column-text name="question"
			value="<%= textToShow%>" />

		<liferay-ui:search-container-column-text name="create-date"
			value="<%= dateFormat.format( question.getCreateDate())%>" />

		<liferay-ui:search-container-column-text name="modified-date"
			value="<%=dateFormat.format( question.getModifiedDate())%>" />

		<%
					String qStatus = new String();
					if(question.getStatus()==0)qStatus="new";
					if(question.getStatus()==1)qStatus="modified";

				%>



		<liferay-ui:search-container-column-text name="status">
			<liferay-ui:message key="<%= qStatus%>" />
		</liferay-ui:search-container-column-text>


		<portlet:actionURL name="deleteQuestion" var="deleteQuestionURL">
			<portlet:param name="jspPage" value="/editQuestion/editQuestion.jsp"></portlet:param>
			<portlet:param name="redirect"
		value="<%=PortalUtil.getCurrentURL(request)%>"></portlet:param>
			<portlet:param name="questionId" value="<%= Long.toString(question.getQuestionId()) %>" />
		</portlet:actionURL>

		<portlet:renderURL var="editQuestionURL">
			<portlet:param name="jspPage" value="/editQuestion/editQuestion.jsp"></portlet:param>
			<portlet:param name="questionId" value="<%=Long.toString(question.getQuestionId()) %>" />
			<portlet:param name="catIdSelected" value="<%=Long.toString(question.getCategoryId()) %>" />
			<portlet:param name="subcatIdSelected" value="<%=Long.toString(question.getSubcategoryId()) %>" />
			<portlet:param name="levelIdSelected" value="<%=Long.toString(question.getLevelId()) %>" />
			<portlet:param name="redirect" value="<%=PortalUtil.getCurrentURL(request)%>"></portlet:param>
			
		</portlet:renderURL>
		<liferay-ui:search-container-column-text name="actions">
			<liferay-ui:icon-delete trash="true" image="edit" message="edit"
				url='${editQuestionURL}' />
			<liferay-ui:icon-delete message="delete" image="delete"
				url="${deleteQuestionURL}" />


		</liferay-ui:search-container-column-text>

	</liferay-ui:search-container-row>



	<liferay-ui:search-iterator />

</liferay-ui:search-container>