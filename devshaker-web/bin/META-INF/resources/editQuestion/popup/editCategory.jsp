<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.document.library.kernel.util.DLUtil"%>
<%@page import="com.liferay.document.library.kernel.service.DLAppLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.repository.model.FileEntry"%>
<%@page import="com.liferay.portal.kernel.util.LocalizationUtil"%>
<%@page import="com.devshaker.model.Category"%>
<%@page import="com.devshaker.service.CategoryLocalServiceUtil"%>
<%@ include file="../../init.jsp"%>

<%
	long catId = Long.parseLong(request.getParameter("categoryId"));
	Category cat = null;
	if (catId != -1) {
		cat = CategoryLocalServiceUtil.getCategory(catId);
	}

%>

<style>
.ico_course{
max-width: 200px
}

</style>


		<portlet:renderURL var="backToQuestionURL">
			<portlet:param name="jspPage" value="/editQuestion/editQuestion.jsp"></portlet:param>
			<portlet:param name="catIdSelected" value="<%=request.getParameter("categoryId")%>"></portlet:param>
			<portlet:param name="redirect" value="<%=request.getParameter("redirect") %>"/>
		</portlet:renderURL>
    
        <portlet:actionURL name="editCategory" var="editCategoryURL">
			<portlet:param name="id" value="<%=request.getParameter("categoryId") %>" />
			<portlet:param name="jspPage" value="/editQuestion/editQuestion.jsp"></portlet:param>
			<portlet:param name="redirect" value="<%=request.getParameter("redirect") %>"/>
		</portlet:actionURL>

		


		<aui:form action="${editCategoryURL}" method="post"
			name="editCategory" role="form">
			
			<div class="form-group">
			
				<label for="tilte"><liferay-ui:message key="title" /></label>
				<liferay-ui:input-localized  name="tilte" formName="tilte" 
					xml="<%= (cat!=null)? cat.getName():"" %>" cssClass="form-control">
				</liferay-ui:input-localized>
			</div>

			<div class="form-group">
				<label for="description"><liferay-ui:message key="description" /></label>

				<liferay-ui:input-localized 
				  toolbarSet="liferayArticle"	formName="description" name="description" xml="<%= (cat!=null)?cat.getDescription():"" %>" type="editor" cssClass="form-control">
				</liferay-ui:input-localized>

			</div>
			
			<div class="form-group">				
			<aui:input cssClass="" name="fileName" label="image" id="fileName" type="file" value="" >
				<aui:validator name="acceptFiles">'jpg, jpeg, png, gif, svg'</aui:validator>	
			</aui:input>	
			
			<%	if(cat != null && cat.getIcon() != 0) {
			FileEntry image_=DLAppLocalServiceUtil.getFileEntry(cat.getIcon());
			String previewUrl =DLUtil.getPreviewURL(image_, image_.getFileVersion(), themeDisplay, StringPool.BLANK); %>
			<div id="container_ico_course" class="container_ico_course">
				
					<img id="<portlet:namespace/>icon_course" alt="" class="ico_course" src="<%= previewUrl %>"/>

			</div>
			
		<%} %>
			
			</div>
			
			<aui:button-row>
				<aui:button type="submit" cssClass="btn btn-primary"></aui:button>
				<aui:button name="cancel"  type="button" value="cancel" cssClass="btn btn-danger" onClick="${backToQuestionURL}"></aui:button>
			</aui:button-row>
		</aui:form>

    
