<%@page import="com.devshaker.service.TestQuestionLocalServiceUtil"%>
<%@page import="com.devshaker.model.TestQuestion"%>
<%@page import="com.devshaker.model.Category"%>
<%@page import="java.util.List"%>
<%@page import="javax.portlet.PortletSession"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../init.jsp"%>
<!-- ERROR MESSAGE AREA -->
<liferay-ui:error key="question-text-required" message="question-text-required" />
<liferay-ui:error key="ans1-required" message="ans1-required" />
<liferay-ui:error key="ans2-required" message="ans2-required" />
<liferay-ui:error key="ans3-required" message="ans3-required" />
<liferay-ui:error key="ans0-required" message="ans0-required" />

<liferay-ui:error key="at-least-one-correct-required" message="at-least-one-correct-required" />
<!--  -->


 
<portlet:resourceURL id="reloadCategory" var="reloadCategoryURL">
		<portlet:param name="jspPage" value="/editQuestion/fragments/categoryFragment.jsp"></portlet:param>
</portlet:resourceURL>

<portlet:resourceURL id="reloadSubcategory" var="reloadSubcategoryURL">
		<portlet:param name="jspPage" value="/editQuestion/fragments/subcategoryFragment.jsp"></portlet:param>
</portlet:resourceURL>

<portlet:resourceURL id="levelReload" var="levelReloadURL">
		<portlet:param name="jspPage" value="/editQuestion/fragments/levelFragment.jsp"></portlet:param>
</portlet:resourceURL>


<%
long questionId = ParamUtil.get(request, "questionId", -1);
TestQuestion question = null;
boolean isCorrect0=false,isCorrect1=false,isCorrect2=false,isCorrect3=false;
if(questionId!=-1){
	question = TestQuestionLocalServiceUtil.getTestQuestion(questionId);
	String [] correctAnswers = question.getCorrectAnswers().split("#");
	for(int i=0; i<correctAnswers.length;i++){
		if(correctAnswers[i].equals("0"))isCorrect0=true;
		if(correctAnswers[i].equals("1"))isCorrect1=true;
		if(correctAnswers[i].equals("2"))isCorrect2=true;
		if(correctAnswers[i].equals("3"))isCorrect3=true;

	}
}


%>
 
<script type="text/javascript">

$(document).ready(function() {
	
	
	if(<%=questionId%> == -1){
		$( "#questionFields" ).hide();
		$( "#asnwerFields" ).hide();
		
	}
});

function onCategorySelectChange(){
	var catSelectedId = $( "#<portlet:namespace />category" ).val();
	var subcatSelectedId = $( "#<portlet:namespace />subcategory" ).val();
	//Primero cambio el menu de las acciones
	jQuery.ajax({
        type: "POST",
        url: "<%=reloadCategoryURL%>",
        cache: false,
        data :{
        	<portlet:namespace/>catIdSelected:catSelectedId
        	
        },
        async: true,
       	dataType: "html",
        success: function(data) {
        	console.log(data);
        	$("#<portlet:namespace />categoryContainer").html('');
        	$("#<portlet:namespace />categoryContainer").html(data);
        }
	 }); 
	
	
	jQuery.ajax({
        type: "POST",
        url: "<%=reloadSubcategoryURL%>",
        cache: false,
        data :{
        	<portlet:namespace/>catIdSelected:catSelectedId,
        },
        async: true,
       	dataType: "html",
        success: function(data) {
        	console.log(data);
        	$("#<portlet:namespace />subcategoryContainer").html('');
        	$("#<portlet:namespace />subcategoryContainer").html(data);
        }
	 }); 
	
	jQuery.ajax({
        type: "POST",
        url: "<%=levelReloadURL%>",
        cache: false,
        data :{
        	<portlet:namespace/>catIdSelected:catSelectedId,
        },
        async: true,
       	dataType: "html",
        success: function(data) {
        	console.log(data);
        	$("#<portlet:namespace />levelContainer").html('');
        	$("#<portlet:namespace />levelContainer").html(data);
        }
	 }); 
	
}

function onSubCategorySelectChange (){
	var catIdSelected = $( "#<portlet:namespace />category" ).val();
	var subcatIdSelected = $( "#<portlet:namespace />subcategory" ).val();
	jQuery.ajax({
        type: "POST",
        url: "<%=reloadSubcategoryURL%>",
        cache: false,
        data :{
        	<portlet:namespace/>catIdSelected:catIdSelected,
        	<portlet:namespace/>subcatIdSelected:subcatIdSelected
        },
        async: true,
       	dataType: "html",
        success: function(data) {
        	console.log(data);
        	$("#<portlet:namespace />subcategoryContainer").html('');
        	$("#<portlet:namespace />subcategoryContainer").html(data);
        }
 	});
	
	
	jQuery.ajax({
        type: "POST",
        url: "<%=levelReloadURL%>",
        cache: false,
        data :{
        	<portlet:namespace/>catIdSelected:catIdSelected,
        	<portlet:namespace/>subcatIdSelected:subcatIdSelected
        	},
        async: true,
       	dataType: "html",
        success: function(data) {
        	$("#<portlet:namespace />levelContainer").html('');
        	$("#<portlet:namespace />levelContainer").html(data);	
        }
 	}); 
	
	
	
	
	
	
}


function onLevelSelectChange(){
	var catIdSelected = $( "#<portlet:namespace />category" ).val();
	var subcatIdSelected = $( "#<portlet:namespace />subcategory" ).val();
	var levelIdSelected  = $( "#<portlet:namespace />level" ).val();
	jQuery.ajax({
        type: "POST",
        url: "<%=levelReloadURL%>",
        cache: false,
        data :{
        	<portlet:namespace/>catIdSelected:catIdSelected,
        	<portlet:namespace/>subcatIdSelected:subcatIdSelected,
        	<portlet:namespace/>levelIdSelected:levelIdSelected
        },
        async: true,
       	dataType: "html",
        success: function(data) {
        	$("#<portlet:namespace />levelContainer").html('');
        	$("#<portlet:namespace />levelContainer").html(data);
        	if(levelIdSelected!=-1){
        		$( "#questionFields" ).show();
        		$( "#asnwerFields" ).show();
        	}else{
        		$( "#questionFields" ).hide();
        		$( "#asnwerFields" ).hide();
        	}
        }
 	});  
}

</script>

<portlet:actionURL name="editQuestion" var="editQuestionURL" />

<aui:form action="<%= editQuestionURL %>" method="post" id="editQuestion" name="editQuestion">


        <aui:input name="questionId" type="hidden" value="<%=questionId %>"></aui:input>


		<div class="form-group row" id="<portlet:namespace />categoryContainer">
				<%@ include file="fragments/categoryFragment.jsp"%>
		</div>
		
		<div class="form-group row" id="<portlet:namespace />subcategoryContainer">
				<%@ include file="fragments/subcategoryFragment.jsp"%>
		</div>
		
		
		<div class="form-group row" id="<portlet:namespace />levelContainer">
				<%@ include file="fragments/levelFragment.jsp"%>
		</div>
		
		<fieldset id="questionFields">
			  <legend><liferay-ui:message key="testQuestion" /></legend>
			
		<div class="form-group" id="questionTextInput">
				<liferay-ui:input-localized formName="testQuestion" toolbarSet="liferayArticle"
				maxLength="5000" name="testQuestion" 
				xml="<%=(question!=null)?question.getQuestionText(themeDisplay.getLocale()):"" %>" 
				type="editor" cssClass="form-control">
				</liferay-ui:input-localized >
		</div>
		</fieldset>
		
		<fieldset id="asnwerFields">
		
					<legend><liferay-ui:message key="textAnswers" /></legend>
		
		<div class="form-group">

		<div class="row">
			<div class="col-md-8">
				<liferay-ui:input-localized formName="testAnswers0" maxLength="5000" toolbarSet="liferayArticle"
					name="testAnswers0" xml="<%=(question!=null)?question.getAnswer0Text(themeDisplay.getLocale()):"" %>" type="editor" cssClass="form-control">
				</liferay-ui:input-localized>
			</div>
			<div class="col-md-4">
				<div class="checkbox">
  					<aui:input type="checkbox" label="isCorrectCheckbox" name="is_correct0" checked="<%=isCorrect0 %>"></aui:input>
				</div>
			</div>
				
		</div>



		<div class="row">
			<div class="col-md-8">
				<liferay-ui:input-localized formName="testAnswers1" maxLength="5000" toolbarSet="liferayArticle"
					name="testAnswers1" xml="<%=(question!=null)?question.getAnswer1Text(themeDisplay.getLocale()):"" %>" type="editor" cssClass="form-control">
				</liferay-ui:input-localized>
			</div>
			<div class="col-md-4">
			<aui:input type="checkbox" label="isCorrectCheckbox" name="is_correct1" checked="<%=isCorrect1 %>"></aui:input>
				
			</div>

		</div>
		
		
		
		
		<div class="row">
			<div class="col-md-8">
				<liferay-ui:input-localized formName="testAnswers2" maxLength="5000" toolbarSet="liferayArticle"
					name="testAnswers2" xml="<%=(question!=null)?question.getAnswer2Text(themeDisplay.getLocale()):"" %>" type="editor" cssClass="form-control">
				</liferay-ui:input-localized>
			</div>
			<div class="col-md-4">
				<aui:input type="checkbox" label="isCorrectCheckbox" name="is_correct2" checked="<%=isCorrect2 %>"></aui:input>
			</div>

		</div>
		
		<div class="row">
			<div class="col-md-8">
				<liferay-ui:input-localized formName="testAnswers3" maxLength="5000" toolbarSet="liferayArticle"
					name="testAnswers3" xml="<%=(question!=null)?question.getAnswer3Text(themeDisplay.getLocale()):"" %>" type="editor" cssClass="form-control">
				</liferay-ui:input-localized>
			</div>
			<div class="col-md-4">
				<aui:input type="checkbox" label="isCorrectCheckbox" name="is_correct3" checked="<%=isCorrect3 %>"></aui:input>
			</div>

		</div>
		
		</fieldset>
		
		<portlet:renderURL portletMode="view" var="viewURL" />
	
		
	<aui:button-row>
		<aui:button type="submit" cssClass="btn btn-primary"></aui:button>
		<aui:button name="cancel"  type="button" value="cancel" cssClass="btn btn-danger" onClick="<%= viewURL %>"></aui:button>
	</aui:button-row>
</aui:form>





