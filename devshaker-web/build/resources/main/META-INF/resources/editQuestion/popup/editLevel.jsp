<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.devshaker.service.LevelLocalServiceUtil"%>
<%@page import="com.devshaker.model.Level"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.document.library.kernel.util.DLUtil"%>
<%@page import="com.liferay.document.library.kernel.service.DLAppLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.repository.model.FileEntry"%>
<%@page import="com.liferay.portal.kernel.util.LocalizationUtil"%>
<%@page import="com.devshaker.model.Category"%>
<%@page import="com.devshaker.service.CategoryLocalServiceUtil"%>
<%@ include file="../../init.jsp"%>

<%
long catId =ParamUtil.getLong(request, "catId",-1);
long subcatId = ParamUtil.getLong(request, "subcatId",-1);
long levelId = ParamUtil.getLong(request, "levelId",-1);
Level level = null;
if (levelId != -1) {
	level = LevelLocalServiceUtil.getLevel(levelId);
}

%>

<style>
.ico_course{
max-width: 200px
}

</style>


		<portlet:renderURL var="backToQuestionURL">
			<portlet:param name="jspPage" value="/editQuestion/editQuestion.jsp"></portlet:param>
			<portlet:param name="catId" value="<%=request.getParameter("catId")%>"></portlet:param>
			<portlet:param name="subcatId" value="<%=request.getParameter("subcatId")%>"></portlet:param>
			<portlet:param name="levelId" value="<%=request.getParameter("levelId")%>"></portlet:param>
			<portlet:param name="redirect" value="<%=request.getParameter("redirect") %>"/>
		</portlet:renderURL>
    
        <portlet:actionURL name="editLevel" var="editLevelURL">
        	<portlet:param name="catId" value="<%=request.getParameter("catId") %>" />
			<portlet:param name="subcatId" value="<%=request.getParameter("subcatId")%>"></portlet:param>
			<portlet:param name="levelId" value="<%=request.getParameter("levelId") %>" />
			<portlet:param name="jspPage" value="/editQuestion/editQuestion.jsp"></portlet:param>
		</portlet:actionURL>

		


		<aui:form action="${editLevelURL}" method="post"
			name="editLevel" role="form">
			
			<div class="form-group">
			
				<label for="tilte"><liferay-ui:message key="title" /></label>
				<liferay-ui:input-localized  name="tilte" formName="tilte" 
					xml="<%= (level!=null)? level.getLevelName():"" %>" cssClass="form-control">
				</liferay-ui:input-localized>
			</div>

			<div class="form-group">
				<label for="description"><liferay-ui:message key="description" /></label>

				<liferay-ui:input-localized 
					toolbarSet="liferayArticle" formName="description" name="description" xml="<%= (level!=null)?level.getLevelDescription():"" %>" type="editor" cssClass="form-control">
				</liferay-ui:input-localized>

			</div>
			
			<div class="form-group">				
			<aui:input name="fileName" label="image" id="fileName" type="file" value="" >
				<aui:validator name="acceptFiles">'jpg, jpeg, png, gif, svg'</aui:validator>	
			</aui:input>	
			
			<%	if(level != null && level.getLevelIcon() != 0) {
			FileEntry image_=DLAppLocalServiceUtil.getFileEntry(level.getLevelIcon());
			String previewUrl =DLUtil.getPreviewURL(image_, image_.getFileVersion(), themeDisplay, StringPool.BLANK); %>
			<div id="container_ico_course" class="container_ico_course">
				
					<img id="<portlet:namespace/>icon_course" alt="" class="ico_course" src="<%= previewUrl %>"/>

			</div>
			
		<%} %>
			
			</div>
			
			<aui:button-row>
				<aui:button type="submit" cssClass="btn btn-primary"></aui:button>
				<aui:button name="cancel"  type="button" value="cancel" cssClass="btn btn-danger" onClick="${backToQuestionURL}"></aui:button>
			</aui:button-row>
		</aui:form>

    
