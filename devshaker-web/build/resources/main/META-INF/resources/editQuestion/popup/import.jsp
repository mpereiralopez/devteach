<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="javax.portlet.PortletPreferences"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ include file="/init.jsp" %>


<portlet:renderURL var="backToSearchQuestionURL">
			<portlet:param name="jspPage" value="/editQuestion/view.jsp"></portlet:param>
</portlet:renderURL>

<portlet:actionURL var="importQuestionsURL" name="importQuestions">
	<portlet:param name="jspPage" value="/editQuestion/popup/import.jsp"></portlet:param>
	<portlet:param name="redirect"
		value="<%=PortalUtil.getCurrentURL(renderRequest)%>"></portlet:param>
</portlet:actionURL>


<aui:form name="fmM"   method="POST" action="<%=importQuestionsURL %>">
	<aui:fieldset>
		<aui:field-wrapper label="editorquestions.importQuestions.csv" helpMessage="editorquestions.importQuestions.csv.help" >
		    	<aui:input inlineLabel="left" inlineField="true" name="fileName" label="" id="fileName" type="file" value="" >
		    		<aui:validator name="acceptFiles">'csv'</aui:validator>
		    	</aui:input>
		</aui:field-wrapper>
	</aui:fieldset> 
	<aui:button-row>
		<aui:button type="submit" cssClass="btn btn-primary" id="btnSubmit"></aui:button>
		<aui:button name="close" value="close"  type="button" onClick="<%=backToSearchQuestionURL%>"> </aui:button>
	</aui:button-row>
</aui:form>

		<liferay-ui:error key="bad.format.file" message="bad.format.file" />
		<liferay-ui:error key="error-importing-questions" message="error-importing-questions" />
				<liferay-ui:error key="too.much.questions" message="too.much.questions" />
		
		
		
		<%List<String> errorsInCSV = (List<String>) request.getAttribute("errorsInCSV");%>
		<% if(errorsInCSV!=null && !errorsInCSV.isEmpty()) {
			for (String errorInCSV : errorsInCSV) {
		%>
		<div class="portlet-msg-error"><%=errorInCSV %> </div>
		<% }} %>	