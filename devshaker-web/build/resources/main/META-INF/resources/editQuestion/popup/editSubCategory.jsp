<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.devshaker.service.SubcategoryLocalServiceUtil"%>
<%@page import="com.devshaker.model.Subcategory"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.document.library.kernel.util.DLUtil"%>
<%@page import="com.liferay.document.library.kernel.service.DLAppLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.repository.model.FileEntry"%>
<%@page import="com.liferay.portal.kernel.util.LocalizationUtil"%>
<%@page import="com.devshaker.model.Category"%>
<%@page import="com.devshaker.service.CategoryLocalServiceUtil"%>
<%@ include file="../../init.jsp"%>

<%

	long catId = ParamUtil.getLong(request,"categoryId",-1);
	long subCatId = ParamUtil.getLong(request,"subcategoryId",-1);
	Subcategory subCat = null;
	if (catId != -1) {
		if(subCatId!=-1){
			subCat = SubcategoryLocalServiceUtil.getSubcategory(subCatId);	
		}
	}

%>

<style>
.ico_course{
max-width: 200px
}

</style>


		<portlet:renderURL var="backToQuestionURL">
			<portlet:param name="jspPage" value="/editQuestion/editQuestion.jsp"></portlet:param>
			<portlet:param name="catIdSelected" value="<%=Long.toString(catId)%>"></portlet:param>
			<portlet:param name="subcatIdSelected" value="<%=Long.toString(subCatId)%>"></portlet:param>
			<portlet:param name="redirect" value="<%=request.getParameter("redirect") %>"/>
		</portlet:renderURL>
    
        <portlet:actionURL name="editSubcategory" var="editSubcategoryURL">
			<portlet:param name="catId" value="<%=request.getParameter("categoryId") %>" />
			<portlet:param name="subCatId" value="<%=request.getParameter("subcategoryId")%>"></portlet:param>
			<portlet:param name="jspPage" value="/editQuestion/editQuestion.jsp"></portlet:param>
		</portlet:actionURL>

		<h2>EDIT A SUBCATEGORY</h2>


		<aui:form action="${editSubcategoryURL}" method="post"
			name="editSubcategoryURL" role="form">
			
			<div class="form-group">
			
				<label for="tilte"><liferay-ui:message key="title" /></label>
				<liferay-ui:input-localized  name="tilte" formName="tilte" 
					xml="<%= (subCat!=null)? subCat.getSubcategoryName():"" %>" cssClass="form-control">
				</liferay-ui:input-localized>
			</div>

			<div class="form-group">
				<label for="description"><liferay-ui:message key="description" /></label>

				<liferay-ui:input-localized 
					toolbarSet="liferayArticle" formName="description" name="description" xml="<%= (subCat!=null)?subCat.getSubcategoryDescription():"" %>" type="editor" cssClass="form-control">
				</liferay-ui:input-localized>

			</div>
			
			<div class="form-group">				
			<aui:input name="fileName" label="image" id="fileName" type="file" value="" onChange="refreshPreview(this);">
				<aui:validator name="acceptFiles">'jpg, jpeg, png, gif, svg'</aui:validator>	
			</aui:input>
			
			
			
			<%	if(subCat != null && subCat.getSubcategoryIcon() != 0) {
			FileEntry image_=DLAppLocalServiceUtil.getFileEntry(subCat.getSubcategoryIcon());
			String previewUrl =DLUtil.getPreviewURL(image_, image_.getFileVersion(), themeDisplay, StringPool.BLANK); %>
			<div id="container_ico_course" class="container_ico_course">
				
					<img id="<portlet:namespace/>icon_course" alt="" class="ico_course" src="<%= previewUrl %>"/>

			</div>
			
		<%} %>
			
			</div>
			
			<aui:button-row>
				<aui:button type="submit" cssClass="btn btn-primary"></aui:button>
				<aui:button name="cancel"  type="button" value="cancel" cssClass="btn btn-danger" onClick="${backToQuestionURL}"></aui:button>
			</aui:button-row>
		</aui:form>