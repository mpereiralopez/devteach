<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="com.devshaker.model.Category"%>
<%@page import="com.devshaker.service.CategoryLocalServiceUtil"%>
<%@page import="java.util.List"%>
<%@page import="com.devshaker.service.TestQuestionLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../init.jsp"%>

<%
long catId = ParamUtil.get(request, "catIdSelected", -1);
pageContext.setAttribute("catId", catId);
List<Category> categoryList = 	CategoryLocalServiceUtil.getCategoriesOfCompany(themeDisplay.getCompanyId());
String redirect = ParamUtil.get(request,"redirect","");
%>


<div id="<portlet:namespace />categorySelectContainer" class="col-md-8">

					<c:choose>
	<c:when test="<%=categoryList.size()==0%>">
		<aui:select name="category" id="category" cssClass="form-control"
			disabled="true">
			<aui:option>No Categories created</aui:option>
		</aui:select>

	</c:when>
	<c:otherwise>
		<aui:select label="category" name="category" required="true"
			onChange="onCategorySelectChange();">
			<c:choose>
				<c:when test="<%=catId ==-1 %>">
					<aui:option value="-1"> Select a category </aui:option>
					<c:forEach items="<%=categoryList %>" var="categorySaved">
						<aui:option value="${categorySaved.getCategoryId()}"> ${categorySaved.getName(themeDisplay.getLocale())} </aui:option>
					</c:forEach>

				</c:when>

				<c:otherwise>
					<c:forEach items="<%=categoryList %>" var="categorySaved">
						<c:choose>
							<c:when test="${catId eq categorySaved.getCategoryId()}">
								<aui:option selected="true" value="${categorySaved.getCategoryId()}"> ${categorySaved.getName(themeDisplay.getLocale())} </aui:option>
							</c:when>
							<c:otherwise>
								<aui:option value="${categorySaved.getCategoryId()}"> ${categorySaved.getName(themeDisplay.getLocale())} </aui:option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</aui:select>

	</c:otherwise>
</c:choose>
					

				</div>
				<div class="col-md-4" id="<portlet:namespace />categoryActionsContainer">
				
<c:choose>
	<c:when test="<%=categoryList.size()==0 || catId == -1 %>">
		<portlet:renderURL var="createCategoryURL">
			<portlet:param name="categoryId" value="-1" />
			<portlet:param name="jspPage" value="/editQuestion/popup/editCategory.jsp" />
			<portlet:param name="redirect" value="<%= PortalUtil.getCurrentURL(request) %>" />
		</portlet:renderURL>


		<liferay-ui:icon-menu id="categoryActions" message="actions"
			showArrow="true" direction="right">
			<liferay-ui:icon-delete trash="true" image="add" message="add"
				url="${createCategoryURL}" />
		</liferay-ui:icon-menu>

	</c:when>


	<c:otherwise>
		<portlet:renderURL var="createCategoryURL">
			<portlet:param name="categoryId" value="-1" />
			<portlet:param name="jspPage"
				value="/editQuestion/popup/editCategory.jsp" />
			<portlet:param name="redirect"
				value="<%= PortalUtil.getCurrentURL(request) %>" />
		</portlet:renderURL>

		<c:if test="<%= catId != -1 %>">

			<portlet:renderURL var="editCategoryURL">
				<portlet:param name="categoryId" value="${catId}" />
				<portlet:param name="jspPage" value="/editQuestion/popup/editCategory.jsp" />
				<portlet:param name="redirect" value="<%= PortalUtil.getCurrentURL(request) %>" />
			</portlet:renderURL>
			
			<portlet:actionURL name="deleteCategory" var="deleteCategoryURL">
				<portlet:param name="categoryId" value="${catId}" />
				<portlet:param name="jspPage" value="/editQuestion/editQuestion.jsp"></portlet:param>
			</portlet:actionURL>

		</c:if>


		<liferay-ui:icon-menu id="categoryActions" message="actions" showArrow="true" direction="right">
			<liferay-ui:icon-delete trash="true" image="add" message="add" url="${createCategoryURL}" />
			<c:if test="<%= catId!=-1 %>">
				<liferay-ui:icon-delete trash="true" image="edit" message="edit" url='${editCategoryURL}' />
				<liferay-ui:icon-delete message="delete" image="delete" url="${deleteCategoryURL}" />
			</c:if>


		</liferay-ui:icon-menu>

	</c:otherwise>
</c:choose>
				
				</div>


