<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="java.util.LinkedList"%>
<%@page import="com.devshaker.service.SubcategoryLocalServiceUtil"%>
<%@page import="com.devshaker.model.Subcategory"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../init.jsp"%>

<%
long catIdSelectedForSubcat = ParamUtil.get(request, "catIdSelected", -1);
long subcatIdSelected = ParamUtil.get(request, "subcatIdSelected", -1);

List <Subcategory> subcategoryList = new LinkedList<Subcategory>();
if(catIdSelectedForSubcat!=-1){
	subcategoryList = SubcategoryLocalServiceUtil.getSubcategoriesOfCategory(catIdSelectedForSubcat);
}
if(subcatIdSelected != -1)pageContext.setAttribute("subcatId", subcatIdSelected); 
%>


<div id="<portlet:namespace />subcategorySelectContainer" class="col-md-8">
					<c:choose>
	<c:when test="<%=catIdSelectedForSubcat==-1 %>">
		<aui:select required="true" label="subcategory[message-board]" name="subcategory" id="subcategory" cssClass="form-control" disabled="true">
			<aui:option>Please, select a category</aui:option>
		</aui:select>

	</c:when>
	<c:otherwise>
		<aui:select required="true" label="subcategory[message-board]" name="subcategory" cssClass="form-control"
			onChange="onSubCategorySelectChange();">
					<aui:option value="-1"> Select a subcategory </aui:option>
					<c:forEach items="<%=subcategoryList %>" var="subcategorySaved">
						<c:choose>
							<c:when test="${subcatId eq subcategorySaved.getSubcategoryId()}">
								<aui:option selected="true" value="${subcategorySaved.getSubcategoryId()}"> ${subcategorySaved.getSubcategoryName(themeDisplay.getLocale())} </aui:option>
							</c:when>
							<c:otherwise>
								<aui:option value="${subcategorySaved.getSubcategoryId()}"> ${subcategorySaved.getSubcategoryName(themeDisplay.getLocale())} </aui:option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
		</aui:select>

	</c:otherwise>
</c:choose>

				</div>
				<div class="col-md-4" id="<portlet:namespace />subcategoryActionsContainer">
					
<c:choose>
	<c:when test="<%=subcategoryList.size()==0 || subcatIdSelected == -1%>">
		<portlet:renderURL var="createSubcategoryURL">
			<portlet:param name="categoryId" value="<%=Long.toString(catIdSelectedForSubcat)%>" />
			<portlet:param name="subcategoryId" value="-1" />
			<portlet:param name="jspPage" value="/editQuestion/popup/editSubCategory.jsp" />
			<portlet:param name="redirect" value="<%= PortalUtil.getCurrentURL(request) %>" />
		</portlet:renderURL>


		<liferay-ui:icon-menu disabled="<%=catIdSelectedForSubcat==-1 %>" id="categoryActions" message="actions" showArrow="true" direction="right">
			<liferay-ui:icon-delete trash="true" image="add" message="add" url="${createSubcategoryURL}" />
			
			<%if(catIdSelectedForSubcat==-1) {%>
				<liferay-ui:icon-delete trash="true" image="edit" message="edit" url='' />
			<%} %>
			
			
		</liferay-ui:icon-menu>

	</c:when>


	<c:otherwise>
		<portlet:renderURL var="createSubcategoryURL">
			<portlet:param name="categoryId" value="<%=Long.toString(catIdSelectedForSubcat)%>" />
			<portlet:param name="subcategoryId" value="-1" />
			<portlet:param name="jspPage" value="/editQuestion/popup/editSubCategory.jsp" />
			<portlet:param name="redirect" value="<%= PortalUtil.getCurrentURL(request) %>" />
		</portlet:renderURL>

		<c:if test="<%=subcatIdSelected != -1 %>">
			<portlet:renderURL var="editSubcategoryURL">
				<portlet:param name="categoryId" value="<%=Long.toString(catIdSelectedForSubcat)%>" />
				<portlet:param name="subcategoryId" value="<%=Long.toString(subcatIdSelected )%>" />
				<portlet:param name="jspPage" value="/editQuestion/popup/editSubCategory.jsp" />
				<portlet:param name="redirect" value="<%= PortalUtil.getCurrentURL(request) %>" />
			</portlet:renderURL>
			
			<portlet:actionURL name="deleteSubcategory" var="deleteSubcategoryURL">
				<portlet:param name="categoryId" value="<%=Long.toString(catIdSelectedForSubcat)%>" />
				<portlet:param name="subcategoryId" value="<%=Long.toString(subcatIdSelected )%>" />
				<portlet:param name="jspPage" value="/editQuestion/editQuestion.jsp"></portlet:param>
			</portlet:actionURL>

		</c:if>

		<liferay-ui:icon-menu id="subcategoryActions" message="actions" showArrow="true" direction="right">
			<liferay-ui:icon-delete trash="true" image="add" message="add" url="${createSubcategoryURL}" />
			<c:if test="<%=catIdSelectedForSubcat!=-1 %>">
				<liferay-ui:icon-delete trash="true" image="edit" message="edit" url='${editSubcategoryURL}' />
				<liferay-ui:icon-delete message="delete" image="delete" url="${deleteSubcategoryURL}" />
			</c:if>


		</liferay-ui:icon-menu>

	</c:otherwise>
</c:choose>
				</div>





