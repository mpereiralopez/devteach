<%@page import="java.util.LinkedList"%>
<%@page import="com.devshaker.model.Level"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="com.devshaker.service.LevelLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../init.jsp"%>

<%

	long catIdSelectedForLevel = ParamUtil.get(request, "catIdSelected", -1);
	long subcatIdSelectedForLevel = ParamUtil.get(request, "subcatIdSelected", -1);
	long levelId = ParamUtil.get(request, "levelIdSelected", -1);
	List<Level> levelList = new LinkedList<Level>();
	if (catIdSelectedForLevel != -1)
		pageContext.setAttribute("catIdSelected", catIdSelectedForLevel);
	if (subcatIdSelectedForLevel != -1){
		pageContext.setAttribute("subcatIdSelected", subcatIdSelectedForLevel);
		levelList = LevelLocalServiceUtil.getLevelsOfSubcategory(subcatIdSelectedForLevel);
		pageContext.setAttribute("levelList", levelList);
	}
		
	if (levelId != -1){
		pageContext.setAttribute("levelId", levelId);
	}
		
%>

<div id="<portlet:namespace />levelSelectContainer" class="col-md-8">
	<c:choose>
		<c:when test="<%=catIdSelectedForLevel == -1 %>">
			<aui:select required="true" name="level" id="level"
				cssClass="form-control" disabled="true">
				<aui:option>Please, select a category</aui:option>
			</aui:select>

		</c:when>
		<c:when test="<%=subcatIdSelectedForLevel == -1 %>">
			<aui:select required="true" name="level" id="level"
				cssClass="form-control" disabled="true">
				<aui:option>Please, select a subcategory</aui:option>
			</aui:select>

		</c:when>
		<c:otherwise>
			<aui:select required="true" label="level" name="level"
				cssClass="form-control" onChange="onLevelSelectChange();">
				<c:choose>
					<c:when test="<%=levelId==-1 %>">
						<aui:option value="-1"> Select a level </aui:option>
						<c:forEach items="${levelList}" var="levelSaved">
							<aui:option value="${levelSaved.getLevelId()}"> ${levelSaved.getLevelName(themeDisplay.getLocale())} </aui:option>
						</c:forEach>

					</c:when>

					<c:otherwise>
						<c:forEach items="${levelList}" var="levelSaved">
							<c:choose>
								<c:when test="${levelId eq levelSaved.getLevelId()}">
									<aui:option selected="true" value="${levelSaved.getLevelId()}"> ${levelSaved.getLevelName(themeDisplay.getLocale())} </aui:option>
								</c:when>
								<c:otherwise>
									<aui:option value="${levelSaved.getLevelId()}"> ${levelSaved.getLevelName(themeDisplay.getLocale())} </aui:option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</aui:select>

		</c:otherwise>
	</c:choose>
</div>
<div class="col-md-4" id="<portlet:namespace />levelActionsContainer">
	<portlet:renderURL var="createLevelURL">
		<portlet:param name="catId" value="${catIdSelected}" />
		<portlet:param name="subcatId" value="${subcatIdSelected}" />
		<portlet:param name="levelId" value="-1" />
		<portlet:param name="jspPage"
			value="/editQuestion/popup/editLevel.jsp" />
		<portlet:param name="redirect"
			value="<%=PortalUtil.getCurrentURL(request)%>" />
	</portlet:renderURL>

	<c:choose>
		<c:when
			test="<%=levelList.size() == 0 %>">
			<liferay-ui:icon-menu disabled="<%=subcatIdSelectedForLevel==-1 %>" id="levelActions"
				message="actions" showArrow="true" direction="right">
				<liferay-ui:icon-delete trash="true" image="add" message="add"
					url="${createLevelURL}" />

				<%
					if (subcatIdSelectedForLevel==-1) {
				%>
				<liferay-ui:icon-delete trash="true" image="edit" message="edit"
					url='' />
				<%
					}
				%>


			</liferay-ui:icon-menu>

		</c:when>


		<c:otherwise>


			<c:if
				test="<%=levelId != -1 %>">
				
				<portlet:renderURL var="editLevelURL">
					<portlet:param name="catId" value="${catIdSelected}" />
					<portlet:param name="subcatId" value="${subcatIdSelected}" />
					<portlet:param name="levelId" value="${levelId}" />
					<portlet:param name="jspPage"
						value="/editQuestion/popup/editLevel.jsp" />
					<portlet:param name="redirect"
						value="<%=PortalUtil.getCurrentURL(request)%>" />
				</portlet:renderURL>

				<portlet:actionURL name="deleteLevel" var="deleteLevelURL">
					<portlet:param name="catId" value="${catIdSelected}" />
					<portlet:param name="subcatId" value="${subcatIdSelected}" />
					<portlet:param name="levelId" value="${levelId}" />
									<portlet:param name="jspPage" value="/editQuestion/editQuestion.jsp"></portlet:param>

				</portlet:actionURL>

			</c:if>


			<liferay-ui:icon-menu id="levelActions" message="actions"
				showArrow="true" direction="right">
				<liferay-ui:icon-delete trash="true" image="add" message="add"
					url="${createLevelURL}" />
				<c:if test="${catIdSelected != -1}">
					<liferay-ui:icon-delete trash="true" image="edit" message="edit"
						url='${editLevelURL}' />
					<liferay-ui:icon-delete message="delete" image="delete"
						url="${deleteLevelURL}" />
				</c:if>


			</liferay-ui:icon-menu>

		</c:otherwise>
	</c:choose>
</div>


