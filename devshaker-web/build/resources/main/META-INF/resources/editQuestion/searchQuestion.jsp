<%@page import="com.devshaker.model.Subcategory"%>
<%@page import="javax.portlet.PortletSession"%>
<%@page import="com.devshaker.model.Category"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.dao.search.DisplayTerms"%>
<%@page import="com.liferay.portal.kernel.dao.search.SearchContainer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../init.jsp"%>


			<portlet:resourceURL id="selectedCategoryFillSubcategorySearch" var="selectedCategoryFillSubcategoryURL">
				<portlet:param name="jspPage" value="/editQuestion/subcategorySelectSearch.jsp"></portlet:param>
			</portlet:resourceURL>
			<portlet:resourceURL id="selectedSubCategoryFillSubcategorySearch" var="selectedSubCategoryFillSubcategoryURL">
				<portlet:param name="jspPage" value="/editQuestion/levelSelectSearch.jsp"></portlet:param>
			</portlet:resourceURL>

<script type="text/javascript">
function onCategorySelectChange(){
	var catSelectedId = $( "#<portlet:namespace />category" ).val();
jQuery.ajax({
    type: "POST",
    url: "<%=selectedCategoryFillSubcategoryURL%>",
    cache: false,
    data :{
    	<portlet:namespace/>catIdSelected:catSelectedId,
    },
    async: true,
   	dataType: "html",
    success: function(data) {
    	$("#<portlet:namespace />subcategorySelectContainer").html('');
    	$("#<portlet:namespace />subcategorySelectContainer").html(data);
    }
});


jQuery.ajax({
    type: "POST",
    url: "<%=selectedSubCategoryFillSubcategoryURL%>",
    cache: false,
    data :{
    	<portlet:namespace/>catIdSelected:catSelectedId,
    },
    async: true,
   	dataType: "html",
    success: function(data) {
    	$("#<portlet:namespace />levelSelectContainer").html('');
    	$("#<portlet:namespace />levelSelectContainer").html(data);
    }
});


}



function onSubCategorySelectSearchChange(){
	var subcatSelectedId = $( "#<portlet:namespace />subcategory" ).val();
	jQuery.ajax({
	    type: "POST",
	    url: "<%=selectedSubCategoryFillSubcategoryURL%>",
	    cache: false,
	    data :{
	    	<portlet:namespace/>subcatIdSelected:subcatSelectedId,
	    },
	    async: true,
	   	dataType: "html",
	    success: function(data) {
	    	$("#<portlet:namespace />levelSelectContainer").html('');
	    	$("#<portlet:namespace />levelSelectContainer").html(data);
	    }
	});
}
</script>

		<liferay-portlet:actionURL name="searchQuestions" var="searchQuestionsURL">
			<liferay-portlet:param name="jspPage" value="/editQuestion/view.jsp" />
		</liferay-portlet:actionURL>

		<aui:form name="searchQuestions" action="<%=searchQuestionsURL%>" method="post">
			<aui:fieldset>

				<aui:select label="category" name="category" onChange="onCategorySelectChange();">
				<aui:option value="-1"><liferay-ui:message key="all"/> </aui:option>
					<c:forEach items="${categoryList}" var="categorySaved">
						<aui:option value="${categorySaved.getCategoryId()}"> ${categorySaved.getName(themeDisplay.getLocale())} </aui:option>
					</c:forEach>
				</aui:select>
			</aui:fieldset>
			
			<aui:fieldset>
		

				<div id="<portlet:namespace />subcategorySelectContainer">
					<%@ include file="subcategorySelectSearch.jsp"%>
				</div>
			
				
				<div id="<portlet:namespace />levelSelectContainer">
					<%@ include file="levelSelectSearch.jsp"%>
				</div>
				
				
				
			</aui:fieldset>

				<aui:button-row>
					<aui:button name="searchQuestions" value="search" type="submit" />	
				</aui:button-row>
		</aui:form>


