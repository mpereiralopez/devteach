<%@page import="java.util.LinkedList"%>
<%@page import="java.util.List"%>
<%@page import="com.devshaker.service.SubcategoryLocalServiceUtil"%>
<%@page import="com.devshaker.model.Subcategory"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../init.jsp"%>

<%
long catIdSelectedSearch = ParamUtil.get(request, "catIdSelected", -1);
List <Subcategory> subcategorylist = new LinkedList<Subcategory>();
if(catIdSelectedSearch!=-1){
	subcategorylist = SubcategoryLocalServiceUtil.getSubcategoriesOfCategory(catIdSelectedSearch);
}
%>

	<aui:select  label="subcategory[message-board]" name="subcategory" cssClass="form-control" disabled="<%=(catIdSelectedSearch==-1) ?true:false %>"
			onChange="onSubCategorySelectSearchChange();">
				<aui:option value="-1">Select a category to filter results</aui:option>
					<c:forEach items="<%=subcategorylist %>" var="subcategorySaved">
						<aui:option value="${subcategorySaved.getSubcategoryId()}"> ${subcategorySaved.getSubcategoryName(themeDisplay.getLocale())} </aui:option>
					</c:forEach>
	</aui:select>