package com.devshaker.portlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletSession;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;

import com.devshaker.model.Category;
import com.devshaker.model.Level;
import com.devshaker.model.Subcategory;
import com.devshaker.model.TestQuestion;
import com.devshaker.service.CategoryLocalServiceUtil;
import com.devshaker.service.DevUtilsApiLocalServiceUtil;
import com.devshaker.service.LevelLocalServiceUtil;
import com.devshaker.service.SubcategoryLocalServiceUtil;
import com.devshaker.service.TestQuestionLocalServiceUtil;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.portal.kernel.exception.NestableException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.CSVUtil;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import au.com.bytecode.opencsv.CSVReader;

@Component(immediate = true, property = { "com.liferay.portlet.display-category=Devshaker",
		"com.liferay.portlet.instanceable=false", "javax.portlet.display-name=Edit Question Portlet",
		"javax.portlet.security-role-ref=power-user,user", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/editQuestion/view.jsp",
		"javax.portlet.resource-bundle=content.Language" }, service = Portlet.class)
public class EditQuestionPortlet extends MVCPortlet {

	Log log = LogFactoryUtil.getLog(EditQuestionPortlet.class);

	private final static String SERVE_RESOURCE_ID_CATEGORY_SELECTED = "selectedCategory";
	private final static String SERVE_RESOURCE_ID_CATEGORY_ACTION = "selectedCategoryActions";

	private final static String SERVE_RESOURCE_ID_SUBCATEGORY_SELECTED = "selectedCategoryFillSubcategory";
	private final static String SERVE_RESOURCE_ID_SUBCATEGORY_ACTION = "selectedCategoryFillSubcategoryActions";

	private final static String SERVE_RESOURCE_ID_LEVEL_ACTION = "selectedSUBCategoryFillSubcategory";

	
	private static final int STATUS_NEW = 0;
	private static final int STATUS_MODIFIED = 1;
	private static final int STATUS_DENUNCIED = -2;
	private static final int STATUS_ACTIVE = 2;
	private static final int STATUS_PENDING = -1;

	private static final int TYPE_MULTI = 0;
	private static final int TYPE_DRAG = 1;

	private static final String[] CABECERAS = { "CATEGORY_EN", "SUBCATEGORY_EN", "LEVEL_EN", "CATEGORY_ES",
			"SUBCATEGORY_ES", "LEVEL_ES", "CATEGORY_FR", "SUBCATEGORY_FR", "LEVEL_FR", "TEXTQUESTION_EN", "ANSWER1_EN",
			"ANSWER2_EN", "ANSWER3_EN", "ANSWER4_EN", "TEXTQUESTION_ES", "ANSWER1_ES", "ANSWER2_ES", "ANSWER3_ES",
			"ANSWER4_ES", "TEXTQUESTION_FR", "ANSWER1_FR", "ANSWER2_FR", "ANSWER3_FR", "ANSWER4_FR", "RIGHTANSWERS" };

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		// TODO Auto-generated method stub
		super.render(renderRequest, renderResponse);

	}

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		log.info("doViewCalled");

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		User user = themeDisplay.getUser();

		if (renderRequest.getUserPrincipal() == null)
			renderRequest.setAttribute(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY, Boolean.FALSE);

		List<Category> categoryList = CategoryLocalServiceUtil.getCategoriesOfCompany(user.getCompanyId());
		renderRequest.setAttribute("categoryList", categoryList);

		renderRequest.getPortletSession().setAttribute("categoryList", categoryList, PortletSession.APPLICATION_SCOPE);
		super.doView(renderRequest, renderResponse);
	}

	@ProcessAction(name = "searchQuestions")
	public void searchQuestions(ActionRequest actionRequest, ActionResponse actionResponse)
			throws PortalException, IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		long catId = ParamUtil.getLong(actionRequest, "category", -1);
		long subcatId = ParamUtil.getLong(actionRequest, "subcategory", -1);
		long levelId = ParamUtil.getLong(actionRequest, "level", -1);

		List<TestQuestion> testQuestionList = new LinkedList<TestQuestion>();
		if (catId == -1) {
			testQuestionList = TestQuestionLocalServiceUtil.getTestQuestionsByCompanyId(themeDisplay.getCompanyId());
		} else {
			if (subcatId == -1) {
				testQuestionList = TestQuestionLocalServiceUtil.getTestQuestionsByCategoryId(catId);
			} else {
				if (levelId == -1) {
					testQuestionList = TestQuestionLocalServiceUtil.getTestQuestionsBySubcategoryId(subcatId);
				} else {
					testQuestionList = TestQuestionLocalServiceUtil.getTestQuestionsBylevelId(levelId);

				}
			}
		}

		actionRequest.setAttribute("testQuestionList", testQuestionList);
		actionResponse.setRenderParameter("subcatIdSelected", Long.toString(subcatId));
		actionResponse.setRenderParameter("catIdSelected", Long.toString(catId));
		actionResponse.setRenderParameter("levelIdSelected", Long.toString(levelId));

	}

	@ProcessAction(name = "editCategory")
	public void editCategory(ActionRequest actionRequest, ActionResponse actionResponse)
			throws PortalException, IOException {
		log.info("Llamando al editCategory");
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		Map<Locale, String> titleMap = LocalizationUtil.getLocalizationMap(actionRequest, "tilte");
		Map<Locale, String> descriptionMap = LocalizationUtil.getLocalizationMap(actionRequest, "description");
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		String fileName = uploadRequest.getFileName("fileName");
		long catId = Long.parseLong(actionRequest.getParameter("id"));
		Category category = null;
		if (catId == -1) {
			category = CategoryLocalServiceUtil
					.createCategory(CounterLocalServiceUtil.increment(Category.class.getName()));
			category.setCreateDate(Calendar.getInstance().getTime());
			category.setCompanyId(themeDisplay.getCompanyId());
		} else {
			category = CategoryLocalServiceUtil.getCategory(catId);
			category.setModifiedDate(Calendar.getInstance().getTime());

		}
		category.setGroupId(themeDisplay.getScopeGroupId());
		category.setNameMap(titleMap);
		category.setDescriptionMap(descriptionMap);
		category.setUserId(themeDisplay.getUserId());
		if (Validator.isNotNull(fileName)) {
			File file = uploadRequest.getFile("fileName");
			if (DevUtilsApiLocalServiceUtil.validateFileSize(file)) {
				String contentType = uploadRequest.getContentType("fileName");
				long repositoryId = DLFolderConstants.getDataRepositoryId(category.getGroupId(),
						DLFolderConstants.DEFAULT_PARENT_FOLDER_ID);
				long igFolderId = DevUtilsApiLocalServiceUtil.createIGFolders(actionRequest, themeDisplay.getUserId(), repositoryId);
				// Subimos el Archivo en la Document Library
				ServiceContext serviceContextImg = new ServiceContext();
				serviceContextImg.setScopeGroupId(category.getGroupId());
				// Damos permisos al archivo para usuarios de comunidad.
				serviceContextImg.setAddGroupPermissions(true);
				serviceContextImg.setAddGuestPermissions(true);
				FileEntry image = DLAppLocalServiceUtil.addFileEntry(themeDisplay.getUserId(), repositoryId, igFolderId,
						fileName, contentType, fileName, StringPool.BLANK, StringPool.BLANK, file, serviceContextImg);
				category.setIcon(image.getFileEntryId());
			} else {
				category.setIcon(0);
			}
		}

		CategoryLocalServiceUtil.updateCategory(category);
		List<Category> categoryList = CategoryLocalServiceUtil.getCategoriesOfCompany(themeDisplay.getCompanyId());
		actionRequest.getPortletSession().setAttribute("categoryList", categoryList, PortletSession.APPLICATION_SCOPE);
		String redirect = actionRequest.getParameter("redirect");
		actionResponse.sendRedirect(redirect);
	}

	@ProcessAction(name = "deleteQuestion")
	public void deleteQuestion(ActionRequest actionRequest, ActionResponse actionResponse)
			throws PortalException, IOException {
		log.info("deleteQuestion");
		long questionId = Long.parseLong(actionRequest.getParameter("questionId"));
		TestQuestionLocalServiceUtil.deleteTestQuestion(questionId);
		String redirect = actionRequest.getParameter("redirect");
		actionResponse.sendRedirect(redirect);
	}

	@ProcessAction(name = "deleteCategory")
	public void deleteCategory(ActionRequest actionRequest, ActionResponse actionResponse)
			throws PortalException, IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		log.info("deleteCategory");
		long categoryId = Long.parseLong(actionRequest.getParameter("categoryId"));
		CategoryLocalServiceUtil.deleteCategory(categoryId);
		List<Category> categoryList = CategoryLocalServiceUtil.getCategoriesOfCompany(themeDisplay.getCompanyId());
		actionRequest.getPortletSession().setAttribute("categoryList", categoryList, PortletSession.APPLICATION_SCOPE);
		/*String redirect = actionRequest.getParameter("redirect");
		actionResponse.sendRedirect(redirect);*/
		
	}

	@ProcessAction(name = "editSubcategory")
	public void editSubcategory(ActionRequest actionRequest, ActionResponse actionResponse)
			throws PortalException, IOException {
		log.info("Llamando al editSubcategory");
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		Map<Locale, String> titleMap = LocalizationUtil.getLocalizationMap(actionRequest, "tilte");
		Map<Locale, String> descriptionMap = LocalizationUtil.getLocalizationMap(actionRequest, "description");
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		String fileName = uploadRequest.getFileName("fileName");
		long catId = ParamUtil.getLong(actionRequest, "catId", -1);
		long subCatId = ParamUtil.getLong(actionRequest, "subCatId", -1);
		;
		Subcategory subcategoty = null;
		if (subCatId == -1) {

			subcategoty = SubcategoryLocalServiceUtil
					.createSubcategory(CounterLocalServiceUtil.increment(Subcategory.class.getName()));
			subcategoty.setCreateDate(Calendar.getInstance().getTime());
			subcategoty.setCompanyId(themeDisplay.getCompanyId());
		} else {
			subcategoty = SubcategoryLocalServiceUtil.getSubcategory(subCatId);
			subcategoty.setModifiedDate(Calendar.getInstance().getTime());

		}
		subcategoty.setCategoryId(catId);
		subcategoty.setGroupId(themeDisplay.getScopeGroupId());
		subcategoty.setSubcategoryNameMap(titleMap);
		subcategoty.setSubcategoryDescriptionMap(descriptionMap);
		subcategoty.setUserId(themeDisplay.getUserId());
		if (Validator.isNotNull(fileName)) {
			File file = uploadRequest.getFile("fileName");
			if (DevUtilsApiLocalServiceUtil.validateFileSize(file)) {
				String contentType = uploadRequest.getContentType("fileName");
				long repositoryId = DLFolderConstants.getDataRepositoryId(subcategoty.getGroupId(),
						DLFolderConstants.DEFAULT_PARENT_FOLDER_ID);
				long igFolderId = DevUtilsApiLocalServiceUtil.createIGFolders(actionRequest, themeDisplay.getUserId(), repositoryId);
				// Subimos el Archivo en la Document Library
				ServiceContext serviceContextImg = new ServiceContext();
				serviceContextImg.setScopeGroupId(subcategoty.getGroupId());
				// Damos permisos al archivo para usuarios de comunidad.
				serviceContextImg.setAddGroupPermissions(true);
				serviceContextImg.setAddGuestPermissions(true);
				FileEntry image = DLAppLocalServiceUtil.addFileEntry(themeDisplay.getUserId(), repositoryId, igFolderId,
						fileName, contentType, fileName, StringPool.BLANK, StringPool.BLANK, file, serviceContextImg);
				subcategoty.setSubcategoryIcon(image.getFileEntryId());
			} else {
				subcategoty.setSubcategoryIcon(0);
			}
		}

		SubcategoryLocalServiceUtil.updateSubcategory(subcategoty);
		List<Category> categoryList = CategoryLocalServiceUtil.getCategoriesOfCompany(themeDisplay.getCompanyId());
		actionRequest.getPortletSession().setAttribute("categoryList", categoryList, PortletSession.APPLICATION_SCOPE);

		List<Subcategory> subcategoryList = SubcategoryLocalServiceUtil.getSubcategoriesOfCategory(catId);
		actionRequest.getPortletSession().setAttribute("subcategoryList", subcategoryList,
				PortletSession.APPLICATION_SCOPE);

		// String jspPage = actionRequest.getParameter("jspPage");
		// actionResponse.setRenderParameter("jspPage", jspPage);
	}

	@ProcessAction(name = "deleteSubcategory")
	public void deleteSubcategory(ActionRequest actionRequest, ActionResponse actionResponse)
			throws PortalException, IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		log.info("deleteSubcategory");
		long categoryId = ParamUtil.getLong(actionRequest, "categoryId", -1);
		long subcategoryId = ParamUtil.getLong(actionRequest, "subcategoryId", -1);
		SubcategoryLocalServiceUtil.deleteSubcategory(subcategoryId);
		List<Subcategory> subcategoryList = SubcategoryLocalServiceUtil.getSubcategoriesOfCategory(categoryId);
		actionRequest.getPortletSession().setAttribute("subcategoryList", subcategoryList,
				PortletSession.APPLICATION_SCOPE);
		// String redirect = actionRequest.getParameter("redirect");
		// actionResponse.sendRedirect(redirect);
	}

	@ProcessAction(name = "editLevel")
	public void editLevel(ActionRequest actionRequest, ActionResponse actionResponse)
			throws PortalException, IOException {
		log.info("Llamando al editLevel");
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		Map<Locale, String> titleMap = LocalizationUtil.getLocalizationMap(actionRequest, "tilte");
		Map<Locale, String> descriptionMap = LocalizationUtil.getLocalizationMap(actionRequest, "description");
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		String fileName = uploadRequest.getFileName("fileName");
		long catId = ParamUtil.getLong(actionRequest, "catId", -1);
		long subCatId = ParamUtil.getLong(actionRequest, "subcatId", -1);
		long levelId = ParamUtil.getLong(actionRequest, "levelId", -1);
		Level level = null;
		if (levelId == -1) {
			level = LevelLocalServiceUtil.createLevel(CounterLocalServiceUtil.increment(Level.class.getName()));
			level.setCreateDate(Calendar.getInstance().getTime());
			level.setCompanyId(themeDisplay.getCompanyId());
		} else {
			level = LevelLocalServiceUtil.getLevel(levelId);
			level.setModifiedDate(Calendar.getInstance().getTime());

		}
		level.setCategoryId(catId);
		level.setSubcategoryId(subCatId);
		level.setGroupId(themeDisplay.getScopeGroupId());
		level.setLevelNameMap(titleMap);
		level.setLevelDescriptionMap(descriptionMap);
		level.setUserId(themeDisplay.getUserId());
		if (Validator.isNotNull(fileName)) {
			File file = uploadRequest.getFile("fileName");
			if (DevUtilsApiLocalServiceUtil.validateFileSize(file)) {
				String contentType = uploadRequest.getContentType("fileName");
				long repositoryId = DLFolderConstants.getDataRepositoryId(level.getGroupId(),
						DLFolderConstants.DEFAULT_PARENT_FOLDER_ID);
				long igFolderId = DevUtilsApiLocalServiceUtil.createIGFolders(actionRequest, themeDisplay.getUserId(), repositoryId);
				// Subimos el Archivo en la Document Library
				ServiceContext serviceContextImg = new ServiceContext();
				serviceContextImg.setScopeGroupId(level.getGroupId());
				// Damos permisos al archivo para usuarios de comunidad.
				serviceContextImg.setAddGroupPermissions(true);
				serviceContextImg.setAddGuestPermissions(true);
				FileEntry image = DLAppLocalServiceUtil.addFileEntry(themeDisplay.getUserId(), repositoryId, igFolderId,
						fileName, contentType, fileName, StringPool.BLANK, StringPool.BLANK, file, serviceContextImg);
				level.setLevelIcon(image.getFileEntryId());
			} else {
				level.setLevelIcon(0);
			}
		}

		LevelLocalServiceUtil.updateLevel(level);

	}

	@ProcessAction(name = "deleteLevel")
	public void deleteLevel(ActionRequest actionRequest, ActionResponse actionResponse)
			throws PortalException, IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		log.info("deleteSubcategory");
		long subcategoryId = Long.parseLong(actionRequest.getParameter("subcatId"));
		long levelId = Long.parseLong(actionRequest.getParameter("levelId"));
		LevelLocalServiceUtil.deleteLevel(levelId);
		List<Level> levelList = LevelLocalServiceUtil.getLevelsOfSubcategory(subcategoryId);
		actionRequest.getPortletSession().setAttribute("levelList", levelList, PortletSession.APPLICATION_SCOPE);
		/*String redirect = actionRequest.getParameter("redirect");
		actionResponse.sendRedirect(redirect);*/
	}
	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		// TODO Auto-generated method stub

		String pageAddress = resourceRequest.getParameter("jspPage");
		resourceResponse.setContentType("text/html");
		PortletRequestDispatcher dispatcher = getPortletContext().getRequestDispatcher(pageAddress);
		dispatcher.include(resourceRequest, resourceResponse);

		// super.serveResource(resourceRequest, resourceResponse);
	}

	@ProcessAction(name = "editQuestion")
	public void editQuestion(ActionRequest actionRequest, ActionResponse actionResponse)
			throws PortalException, IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String redirect = ParamUtil.getString(actionRequest, "redirect");

		Set<String> paramNames = actionRequest.getParameterMap().keySet();
		Iterator<String> it = paramNames.iterator();

		while (it.hasNext()) {
			String key = it.next();
		}

		long categoryId = Long.parseLong(actionRequest.getParameter("category"));
		long subcategoryId = Long.parseLong(actionRequest.getParameter("subcategory"));
		long levelId = Long.parseLong(actionRequest.getParameter("level"));
		long questionId = Long.parseLong(actionRequest.getParameter("questionId"));

		/** VALIDATION AREA **/
		// If there is no questionText in any language --> ERROR
		boolean noQuestionText = true, noAns1Text = true, noAns2Text = true, noAns3Text = true, noAns0Text = true;
		Enumeration<String> parNames = actionRequest.getParameterNames();
		while (parNames.hasMoreElements()) {
			String paramName = parNames.nextElement();
			if (paramName.startsWith("testQuestion_") && paramName.length() > 13
					&& ParamUtil.getString(actionRequest, paramName, StringPool.BLANK).length() > 0) {
				noQuestionText = false;
			}

			if (paramName.startsWith("testAnswers0_") && paramName.length() > 13
					&& ParamUtil.getString(actionRequest, paramName, StringPool.BLANK).length() > 0) {
				noAns0Text = false;
			}

			if (paramName.startsWith("testAnswers1_") && paramName.length() > 13
					&& ParamUtil.getString(actionRequest, paramName, StringPool.BLANK).length() > 0) {
				noAns1Text = false;
			}

			if (paramName.startsWith("testAnswers2_") && paramName.length() > 13
					&& ParamUtil.getString(actionRequest, paramName, StringPool.BLANK).length() > 0) {
				noAns2Text = false;
			}

			if (paramName.startsWith("testAnswers3_") && paramName.length() > 6
					&& ParamUtil.getString(actionRequest, paramName, StringPool.BLANK).length() > 0) {
				noAns3Text = false;
			}

		}

		if (noQuestionText || noAns1Text || noAns2Text || noAns3Text || noAns0Text) {
			if (noQuestionText)
				SessionErrors.add(actionRequest, "question-text-required");
			if (noAns0Text)
				SessionErrors.add(actionRequest, "ans0-required");
			if (noAns1Text)
				SessionErrors.add(actionRequest, "ans1-required");
			if (noAns2Text)
				SessionErrors.add(actionRequest, "ans2-required");
			if (noAns3Text)
				SessionErrors.add(actionRequest, "ans3-required");
			actionResponse.setRenderParameter("questionId", String.valueOf(questionId));
			actionResponse.setRenderParameter("redirect", redirect);
			actionResponse.setRenderParameter("jspPage", "/editQuestion/editQuestion.jsp");
			return;
		}

		// if there is no one at least marked is an error

		boolean isCorrect1 = Boolean.parseBoolean(actionRequest.getParameter("is_correct1"));
		boolean isCorrect2 = Boolean.parseBoolean(actionRequest.getParameter("is_correct2"));
		boolean isCorrect3 = Boolean.parseBoolean(actionRequest.getParameter("is_correct3"));
		boolean isCorrect0 = Boolean.parseBoolean(actionRequest.getParameter("is_correct0"));

		if (!isCorrect1 && !isCorrect2 && !isCorrect3 && !isCorrect0) {
			SessionErrors.add(actionRequest, "at-least-one-correct-required");
			actionResponse.setRenderParameter("questionId", String.valueOf(questionId));
			actionResponse.setRenderParameter("redirect", redirect);
			actionResponse.setRenderParameter("jspPage", "/editQuestion/editQuestion.jsp");
			return;
		}

		/*********************/

		TestQuestion question = null;

		if (questionId == -1) {
			question = TestQuestionLocalServiceUtil
					.createTestQuestion(CounterLocalServiceUtil.increment(TestQuestion.class.getName()));
			question.setCreateDate(Calendar.getInstance().getTime());
			question.setUserIdCreator(themeDisplay.getUserId());
			question.setUserNameCreator(themeDisplay.getUser().getFullName());
			question.setGroupId(themeDisplay.getScopeGroupId());
			question.setCompanyId(themeDisplay.getCompanyId());
			question.setStatus(STATUS_NEW);
			question.setType(TYPE_MULTI);

		} else {
			question = TestQuestionLocalServiceUtil.getTestQuestion(questionId);
			question.setModifiedDate(Calendar.getInstance().getTime());
			question.setUserIdModified(themeDisplay.getUserId());
			question.setUserNameModified(themeDisplay.getUser().getFullName());
			question.setStatus(STATUS_MODIFIED);
		}

		Map<Locale, String> questionTextMap = LocalizationUtil.getLocalizationMap(actionRequest, "testQuestion");
		Map<Locale, String> ans0Map = LocalizationUtil.getLocalizationMap(actionRequest, "testAnswers0");
		Map<Locale, String> ans1Map = LocalizationUtil.getLocalizationMap(actionRequest, "testAnswers1");
		Map<Locale, String> ans2Map = LocalizationUtil.getLocalizationMap(actionRequest, "testAnswers2");
		Map<Locale, String> ans3Map = LocalizationUtil.getLocalizationMap(actionRequest, "testAnswers3");

		String correctAnswers = new String();

		if (isCorrect0)
			correctAnswers += "0#";
		if (isCorrect1)
			correctAnswers += "1#";
		if (isCorrect2)
			correctAnswers += "2#";
		if (isCorrect3)
			correctAnswers += "3#";

		question.setCorrectAnswers(correctAnswers);

		question.setCategoryId(categoryId);
		question.setSubcategoryId(subcategoryId);
		question.setLevelId(levelId);

		question.setQuestionTextMap(questionTextMap);
		question.setAnswer0TextMap(ans0Map);
		question.setAnswer1TextMap(ans1Map);
		question.setAnswer2TextMap(ans2Map);
		question.setAnswer3TextMap(ans3Map);

		TestQuestionLocalServiceUtil.updateTestQuestion(question);

		// actionResponse.sendRedirect(redirect);
	}

	


	
	@ProcessAction(name = "importQuestions")
	public void importQuestions(ActionRequest actionRequest, ActionResponse actionResponse)
			throws PortalException, IOException {
		System.out.println("importQuestions called");
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		ThemeDisplay themeDisplay = (ThemeDisplay) uploadRequest.getAttribute(WebKeys.THEME_DISPLAY);
		InputStream csvFileToRead = uploadRequest.getFileAsStream("fileName");
		List<String>errors = new LinkedList<String>();
		CSVReader reader = new CSVReader(new InputStreamReader(csvFileToRead, StringPool.UTF8), CharPool.SEMICOLON);
		HttpServletRequest servletRequest = PortalUtil.getHttpServletRequest(actionRequest);

		int line = 0;
		
		
		List<String[]> listaTotal =  reader.readAll();
		
		System.out.println("LINEAS PARA LEER "+listaTotal.size());
		if(listaTotal.size()>350){
			SessionErrors.add(actionRequest, "too.much.questions");
		}else{
			
		
		
		for ( String [] nextLine : listaTotal ) {
			// nextLine[] is an array of values from the line
			if (line == 0) {
				// CHECK IF HEADERS ARE SET
				if (!areArrayEquals(nextLine, CABECERAS)) {
					System.out.println(line+" bad.format.file" );
					SessionErrors.add(actionRequest, "bad.format.file");
					break;
				}
			} else {
				// CATEGORIES
				String CATEGORY_EN = nextLine[0];
				String SUBCATEGORY_EN = nextLine[1];
				String LEVEL_EN = nextLine[2];
				String CATEGORY_ES = nextLine[3];
				String SUBCATEGORY_ES = nextLine[4];
				String LEVEL_ES = nextLine[5];
				String CATEGORY_FR = nextLine[6];
				String SUBCATEGORY_FR = nextLine[7];
				String LEVEL_FR = nextLine[8];
				String TEXTQUESTION_EN = nextLine[9];
				String ANSWER1_EN = nextLine[10];
				String ANSWER2_EN = nextLine[11];
				String ANSWER3_EN = nextLine[12];
				String ANSWER4_EN = nextLine[13];
				String TEXTQUESTION_ES = nextLine[14];
				String ANSWER1_ES = nextLine[15];
				String ANSWER2_ES = nextLine[16];
				String ANSWER3_ES = nextLine[17];
				String ANSWER4_ES = nextLine[18];
				String TEXTQUESTION_FR = nextLine[19];
				String ANSWER1_FR = nextLine[20];
				String ANSWER2_FR = nextLine[21];
				String ANSWER3_FR = nextLine[22];
				String ANSWER4_FR = nextLine[23];
				String RIGHTANSWERS = nextLine[24];

				if (Validator.isNotNull(CATEGORY_EN) || Validator.isNotNull(CATEGORY_ES)
						|| Validator.isNotNull(CATEGORY_FR)) {

					Category cat = CategoryLocalServiceUtil.getCategoryByName(CATEGORY_EN, LocaleUtil.UK, themeDisplay.getCompanyId());

					if (cat == null)
						cat = CategoryLocalServiceUtil.getCategoryByName(CATEGORY_ES, LocaleUtil.SPAIN,
								themeDisplay.getCompanyId());
					if (cat == null)
						cat = CategoryLocalServiceUtil.getCategoryByName(CATEGORY_FR, LocaleUtil.FRANCE,
								themeDisplay.getCompanyId());

					if (cat == null) {
						cat = CategoryLocalServiceUtil
								.createCategory(CounterLocalServiceUtil.increment(Category.class.getName(), 1));
						cat.setName(CATEGORY_EN, LocaleUtil.UK);
						cat.setName(CATEGORY_ES, LocaleUtil.SPAIN);
						cat.setName(CATEGORY_FR, LocaleUtil.FRANCE);
						cat.setDescription(CATEGORY_EN, LocaleUtil.UK);
						cat.setDescription(CATEGORY_ES, LocaleUtil.SPAIN);
						cat.setDescription(CATEGORY_FR, LocaleUtil.FRANCE);
						cat.setCompanyId(themeDisplay.getCompanyId());
						cat.setCreateDate(Calendar.getInstance().getTime());
						cat.setGroupId(themeDisplay.getScopeGroupId());
						cat.setNew(true);
						cat.setUserId(themeDisplay.getUserId());
						cat.setUserName(themeDisplay.getUser().getFullName());

						cat = CategoryLocalServiceUtil.updateCategory(cat);

					}

					// SUBCATEGORIES
					if (Validator.isNotNull(SUBCATEGORY_EN) || Validator.isNotNull(SUBCATEGORY_ES)
							|| Validator.isNotNull(SUBCATEGORY_FR)) {
						Subcategory subcat = SubcategoryLocalServiceUtil.getSubcategoryByNameAndCategoryId(SUBCATEGORY_EN,
								LocaleUtil.UK, cat.getCategoryId());
						if (subcat == null)
							subcat = SubcategoryLocalServiceUtil.getSubcategoryByNameAndCategoryId(SUBCATEGORY_ES, LocaleUtil.SPAIN,
									cat.getCategoryId());
						if (subcat == null)
							subcat = SubcategoryLocalServiceUtil.getSubcategoryByNameAndCategoryId(SUBCATEGORY_FR, LocaleUtil.FRANCE,
									cat.getCategoryId());

						if (subcat == null) {
							subcat = SubcategoryLocalServiceUtil.createSubcategory(
									CounterLocalServiceUtil.increment(Subcategory.class.getName(), 1));
							subcat.setSubcategoryName(SUBCATEGORY_EN, LocaleUtil.UK);
							subcat.setSubcategoryName(SUBCATEGORY_ES, LocaleUtil.SPAIN);
							subcat.setSubcategoryName(SUBCATEGORY_FR, LocaleUtil.FRANCE);
							subcat.setSubcategoryDescription(SUBCATEGORY_EN, LocaleUtil.UK);
							subcat.setSubcategoryDescription(SUBCATEGORY_ES, LocaleUtil.SPAIN);
							subcat.setSubcategoryDescription(SUBCATEGORY_FR, LocaleUtil.FRANCE);
							subcat.setCompanyId(themeDisplay.getCompanyId());
							subcat.setCreateDate(Calendar.getInstance().getTime());
							subcat.setGroupId(themeDisplay.getScopeGroupId());
							subcat.setNew(true);
							subcat.setUserId(themeDisplay.getUserId());
							subcat.setUserName(themeDisplay.getUser().getFullName());
							subcat.setCategoryId(cat.getCategoryId());

							subcat = SubcategoryLocalServiceUtil.updateSubcategory(subcat);

						}

						// LEVELS
						if (Validator.isNotNull(LEVEL_EN) || Validator.isNotNull(LEVEL_ES)
								|| Validator.isNotNull(LEVEL_FR)) {
							Level level = LevelLocalServiceUtil.getLevelByNameAndSubcategoryId(LEVEL_EN, LocaleUtil.UK,
									subcat.getSubcategoryId());
							if (level == null)
								level = LevelLocalServiceUtil.getLevelByNameAndSubcategoryId(LEVEL_ES, LocaleUtil.SPAIN,
										subcat.getSubcategoryId());
							if (level == null)
								level = LevelLocalServiceUtil.getLevelByNameAndSubcategoryId(LEVEL_FR, LocaleUtil.FRANCE,
										subcat.getSubcategoryId());

							if (level == null) {
								level = LevelLocalServiceUtil
										.createLevel(CounterLocalServiceUtil.increment(Level.class.getName(), 1));
								level.setLevelName(LEVEL_EN, LocaleUtil.UK);
								level.setLevelName(LEVEL_ES, LocaleUtil.SPAIN);
								level.setLevelName(LEVEL_FR, LocaleUtil.FRANCE);
								level.setLevelDescription(LEVEL_EN, LocaleUtil.UK);
								level.setLevelDescription(LEVEL_ES, LocaleUtil.SPAIN);
								level.setLevelDescription(LEVEL_FR, LocaleUtil.FRANCE);
								level.setCompanyId(themeDisplay.getCompanyId());
								level.setCreateDate(Calendar.getInstance().getTime());
								level.setGroupId(themeDisplay.getScopeGroupId());
								level.setNew(true);
								level.setUserId(themeDisplay.getUserId());
								level.setUserName(themeDisplay.getUser().getFullName());
								level.setCategoryId(cat.getCategoryId());
								level.setSubcategoryId(subcat.getSubcategoryId());
								level = LevelLocalServiceUtil.updateLevel(level);

							}

							// TEST QUESTION
							if (Validator.isNotNull(TEXTQUESTION_EN) || Validator.isNotNull(TEXTQUESTION_ES)
									|| Validator.isNotNull(TEXTQUESTION_FR)) {
								if (Validator.isNotNull(RIGHTANSWERS)) {

									Map<Locale, String> languageTestMaps = new HashMap<>();
									Map<Locale, String> languageAnswers0 = new HashMap<>();
									Map<Locale, String> languageAnswers1 = new HashMap<>();
									Map<Locale, String> languageAnswers2 = new HashMap<>();
									Map<Locale, String> languageAnswers3 = new HashMap<>();

									boolean couldAddQuesiton = false;

									// if english text questions --> english
									// answers required
									if (Validator.isNotNull(TEXTQUESTION_EN)) {
										if (Validator.isNotNull(ANSWER1_EN) && Validator.isNotNull(ANSWER2_EN)
												&& Validator.isNotNull(ANSWER3_EN) && Validator.isNotNull(ANSWER4_EN)) {
											languageTestMaps.put(LocaleUtil.UK, TEXTQUESTION_EN);
											languageAnswers0.put(LocaleUtil.UK, ANSWER1_EN);
											languageAnswers1.put(LocaleUtil.UK, ANSWER2_EN);
											languageAnswers2.put(LocaleUtil.UK, ANSWER3_EN);
											languageAnswers3.put(LocaleUtil.UK, ANSWER4_EN);
											couldAddQuesiton = true;

										} else {
											errors.add(LanguageUtil.format(servletRequest,"no.answersenglish",line));
										}

									}

									// if spanish text questions --> spanish
									// answers required
									if (Validator.isNotNull(TEXTQUESTION_ES)) {
										if (Validator.isNotNull(ANSWER1_ES) && Validator.isNotNull(ANSWER2_ES)
												&& Validator.isNotNull(ANSWER3_ES) && Validator.isNotNull(ANSWER4_ES)) {
											languageTestMaps.put(LocaleUtil.SPAIN, TEXTQUESTION_ES);
											languageAnswers0.put(LocaleUtil.SPAIN, ANSWER1_ES);
											languageAnswers1.put(LocaleUtil.SPAIN, ANSWER2_ES);
											languageAnswers2.put(LocaleUtil.SPAIN, ANSWER3_ES);
											languageAnswers3.put(LocaleUtil.SPAIN, ANSWER4_ES);
											couldAddQuesiton = true;

										} else {
											System.out.println(line+" no.answersspanish" );
											errors.add(LanguageUtil.format(servletRequest,"no.answersspanish",line));
										}

									}

									// if french text questions --> french
									// answers required
									if (Validator.isNotNull(TEXTQUESTION_FR)) {
										if (Validator.isNotNull(ANSWER1_FR) && Validator.isNotNull(ANSWER2_FR)
												&& Validator.isNotNull(ANSWER3_FR) && Validator.isNotNull(ANSWER4_FR)) {
											languageTestMaps.put(LocaleUtil.FRANCE, TEXTQUESTION_FR);
											languageAnswers0.put(LocaleUtil.FRANCE, ANSWER1_FR);
											languageAnswers1.put(LocaleUtil.FRANCE, ANSWER2_FR);
											languageAnswers2.put(LocaleUtil.FRANCE, ANSWER3_FR);
											languageAnswers3.put(LocaleUtil.FRANCE, ANSWER4_FR);
											couldAddQuesiton = true;

										} else {
											System.out.println(line+" no.answersfrench" );
											errors.add(LanguageUtil.format(servletRequest,"no.answersfrench",line));
										}

									}

									if (couldAddQuesiton) {
										TestQuestion question = TestQuestionLocalServiceUtil.createTestQuestion(
												CounterLocalServiceUtil.increment(TestQuestion.class.getName(), 1));
										question.setQuestionText(languageTestMaps.get(LocaleUtil.UK), LocaleUtil.UK);
										question.setQuestionText(languageTestMaps.get(LocaleUtil.SPAIN), LocaleUtil.SPAIN);
										question.setQuestionText(languageTestMaps.get(LocaleUtil.FRANCE), LocaleUtil.FRANCE);

										question.setAnswer0Text(languageAnswers0.get(LocaleUtil.UK), LocaleUtil.UK);
										question.setAnswer0Text(languageAnswers0.get(LocaleUtil.SPAIN), LocaleUtil.SPAIN);
										question.setAnswer0Text(languageAnswers0.get(LocaleUtil.FRANCE), LocaleUtil.FRANCE);
										
										question.setAnswer1Text(languageAnswers1.get(LocaleUtil.UK), LocaleUtil.UK);
										question.setAnswer1Text(languageAnswers1.get(LocaleUtil.SPAIN), LocaleUtil.SPAIN);
										question.setAnswer1Text(languageAnswers1.get(LocaleUtil.FRANCE), LocaleUtil.FRANCE);
										
										question.setAnswer2Text(languageAnswers2.get(LocaleUtil.UK), LocaleUtil.UK);
										question.setAnswer2Text(languageAnswers2.get(LocaleUtil.SPAIN), LocaleUtil.SPAIN);
										question.setAnswer2Text(languageAnswers2.get(LocaleUtil.FRANCE), LocaleUtil.FRANCE);
									
										question.setAnswer3Text(languageAnswers3.get(LocaleUtil.UK), LocaleUtil.UK);
										question.setAnswer3Text(languageAnswers3.get(LocaleUtil.SPAIN), LocaleUtil.SPAIN);
										question.setAnswer3Text(languageAnswers3.get(LocaleUtil.FRANCE), LocaleUtil.FRANCE);


										question.setCorrectAnswers(RIGHTANSWERS);
										question.setCompanyId(themeDisplay.getCompanyId());
										question.setCreateDate(Calendar.getInstance().getTime());
										question.setGroupId(themeDisplay.getScopeGroupId());
										question.setNew(true);
										question.setUserIdCreator(themeDisplay.getUserId());
										question.setUserNameCreator(themeDisplay.getUser().getFullName());
										question.setCategoryId(cat.getCategoryId());
										question.setSubcategoryId(subcat.getSubcategoryId());
										question.setLevelId(level.getLevelId());
										question.setStatus(STATUS_NEW);
										TestQuestionLocalServiceUtil.updateTestQuestion(question);

									}

								} else {
									System.out.println(line+" no.rightanswers" );
									errors.add(LanguageUtil.format(servletRequest,"no.rightanswers",line));
								}

							} else {
								System.out.println(line+" no.testquestions" );
								errors.add(LanguageUtil.format(servletRequest,"no.testquestions",line));

							}

						} else {
							System.out.println(line+" no.level" );
							errors.add(LanguageUtil.format(servletRequest,"no.level",line));
						}

					} else {
						System.out.println(line+" no.subcategory" );
						errors.add(LanguageUtil.format(servletRequest,"no.subcategory",line));
					}

				} else {
					System.out.println(line+" no.category" );
					errors.add(LanguageUtil.format(servletRequest,"no.category",line));

				}
				
			}
			System.out.println("PROCESSED LINE "+line);

			line++;

			// System.out.println(nextLine[1] + "etc...");
		}
		}
		reader.close();
		csvFileToRead.close();
		if(errors.size()>0)SessionErrors.add(actionRequest, "error-importing-questions");
		actionRequest.setAttribute("errorsInCSV", errors);
		System.out.println("FINISHED WITH ERRORS: "+errors.size());
		
	}

	private static boolean areArrayEquals(String[] array1, String array2[]) {
		boolean areEquals = true;
		if (array1.length == array2.length) {
			for (int i = 0; i < array1.length; i++) {
				if (!array1[i].equals(array2[i])) {
					areEquals = false;
					break;
				}
			}
		} else {
			areEquals = false;
		}
		return areEquals;
	}
}