<%@page import="java.util.List"%>
<%@page import="com.devshaker.model.Level"%>
<%@page import="java.util.LinkedList"%>
<%@page import="com.devshaker.service.LevelLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../init.jsp"%>

<%
long subcatIdSelected = ParamUtil.get(request, "subcatIdSelected", -1);
List <Level> levelList = new LinkedList<Level>();
if(subcatIdSelected!=-1){
	levelList = LevelLocalServiceUtil.getLevelsOfSubcategory(subcatIdSelected);
}

%>

	
	<aui:select  label="level" name="level" cssClass="form-control" disabled="<%=(subcatIdSelected==-1) ?true:false %>">
				<aui:option value="-1">Select a subcategory to filter results</aui:option>
					<c:forEach items="<%=levelList%>" var="levelSaved">
						<aui:option value="${levelSaved.getLevelId()}"> ${levelSaved.getLevelName(themeDisplay.getLocale())} </aui:option>
					</c:forEach>
	</aui:select>